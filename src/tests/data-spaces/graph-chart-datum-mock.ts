import { chartNode2SlabsFlatNode } from '../../app/helpers/chart-node-2-slabs-flat-node.map';
import { NodeType } from '../../app/models/enums/data-space/node-type.enum';
import { GraphChartDatum } from '../../app/models/interfaces/graph-chart-datum.interface';
import { SLabsFlatNode } from '../../app/models/interfaces/tree-node.interface';

export const graphChartDataMock: GraphChartDatum[] = [
  {
    id: 'abc',
    name: NodeType.GOVERNANCE.toString(),
    x: 123,
    y: 456,
    level: -1,
    collapsed: true,
    type: NodeType.GOVERNANCE,
  },
  {
    id: 'def',
    name: NodeType.DATA_PROVIDER.toString(),
    x: 789,
    y: 123,
    level: 0,
    collapsed: false,
    type: NodeType.DATA_PROVIDER,
  },
];

export const slabsFlatNodesMock: SLabsFlatNode[] = [
  chartNode2SlabsFlatNode(graphChartDataMock[0]),
  chartNode2SlabsFlatNode(graphChartDataMock[1]),
];
