import { RowCheckbox } from '../../app/models/interfaces/slabs-custom-components/row-checkbox.interface';
import { TestCaseModel } from '../../app/models/interfaces/slabs-custom-components/test-case.interface';
import {
  testCasesSuite3Mock,
  testCasesSuite5Mock,
} from '../../mocks/test-cases-mock';

export const mockTestCases: TestCaseModel[][] = [
  testCasesSuite3Mock,
  testCasesSuite5Mock,
];

export const mockSuiteCheckboxes: RowCheckbox[] = [
  {
    id: 'TS-1',
    name: 'Suite 1',
    selected: false,
    disabled: false,
    children: [
      {
        id: mockTestCases[0][0].id,
        name: mockTestCases[0][0].name,
        selected: false,
        disabled: false,
      },
      {
        id: mockTestCases[0][1].id,
        name: mockTestCases[0][1].name,
        selected: false,
        disabled: false,
      },
    ],
  },
  {
    id: 'TS-2',
    name: 'Suite 2',
    selected: false,
    disabled: false,
    children: [
      {
        id: mockTestCases[1][0].id,
        name: mockTestCases[1][0].name,
        selected: false,
        disabled: false,
      },
      {
        id: mockTestCases[1][1].id,
        name: mockTestCases[1][1].name,
        selected: false,
        disabled: false,
      },
    ],
  },
];
