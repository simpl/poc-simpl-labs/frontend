import { ITBTestsExecutionData } from '../../app/models/interfaces/slabs-custom-components/itb-tests-execution-data.interface';
import { sessionStartResponseMock } from '../../mocks/session-start-response-mock';
import { slabsCustomComponentsMock } from '../../mocks/slabs-custom-component-mock';
import {
  testCasesSuite1Mock,
  testCasesSuite2Mock,
} from '../../mocks/test-cases-mock';
import { testExecutionStatus1Mock } from '../../mocks/test-execution-status-mock';
import { testSuites2Mock } from '../../mocks/test-suites-mock';

export const nowMock = new Date().toString();

export const itbTestsExecutionDataMock: ITBTestsExecutionData = {
  customComponent: slabsCustomComponentsMock[1],
  sessionStartResponse: sessionStartResponseMock,
  testCases: [testCasesSuite1Mock, testCasesSuite2Mock],
  testExecutionStatus: {
    ...testExecutionStatus1Mock,
    testExecution: {
      ...testExecutionStatus1Mock.testExecution,
      startDate: nowMock,
      endDate: nowMock,
    },
  },
  testSuites: testSuites2Mock,
  isHistoryItem: false,
};
