import { SubmittedTest } from '../../app/models/interfaces/slabs-custom-components/submitted-test.interface';
import { sessionStartResponseMock } from '../../mocks/session-start-response-mock';
import { slabsCustomComponentsMock } from '../../mocks/slabs-custom-component-mock';
import {
  testCasesSuite1Mock,
  testCasesSuite2Mock,
  testCasesSuite3Mock,
  testCasesSuite4Mock,
} from '../../mocks/test-cases-mock';
import {
  testExecutionStatus1Mock,
  testExecutionStatus2Mock,
} from '../../mocks/test-execution-status-mock';
import { testSuites1Mock, testSuites2Mock } from '../../mocks/test-suites-mock';

export const submittedTestsMock: SubmittedTest[] = [
  {
    specificationCheckbox: {
      id: '1',
      name: 'bar',
      selected: false,
      children: [{ id: '2', name: 'foo', selected: false }],
      disabled: false,
    },
    minimized: false,
    customComponent: slabsCustomComponentsMock[0],
    sessionStartResponse: sessionStartResponseMock,
    testCases: [testCasesSuite1Mock, testCasesSuite2Mock],
    testExecutionStatus: testExecutionStatus1Mock,
    testSuites: testSuites1Mock,
    isHistoryItem: false,
  },
  {
    specificationCheckbox: {
      id: '2',
      name: 'foo',
      selected: false,
      children: [
        { id: '21', name: 'bar', selected: false },
        { id: '22', name: 'foo', selected: false },
      ],
      disabled: false,
    },
    minimized: false,
    customComponent: slabsCustomComponentsMock[1],
    sessionStartResponse: sessionStartResponseMock,
    testCases: [testCasesSuite3Mock, testCasesSuite4Mock],
    testExecutionStatus: testExecutionStatus2Mock,
    testSuites: testSuites2Mock,
    isHistoryItem: undefined,
  },
];
