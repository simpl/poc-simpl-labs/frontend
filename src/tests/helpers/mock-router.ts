import {
  NavigationEnd,
  UrlTree,
  NavigationBehaviorOptions,
} from '@angular/router';
import { of } from 'rxjs';

export class MockRouter {
  public events = of(
    new NavigationEnd(
      0,
      'http://localhost:4200/create-data-space',
      'http://localhost:4200',
    ),
  );

  navigateByUrl(url: string | UrlTree, extras?: NavigationBehaviorOptions) {
    return Promise<boolean>;
  }
}
