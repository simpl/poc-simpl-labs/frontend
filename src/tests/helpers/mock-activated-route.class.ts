import { ParamMap, Data } from '@angular/router';
import { Observable, of } from 'rxjs';

import { currentConsumptionsMock } from '../../mocks/current-consumption-mock';
import { dataSpacesMock } from '../../mocks/data-space-mock';

export class MockActivatedRoute {
  snapshot = { paramMap: { get: (param: string) => 'mock-value1' } };

  paramMap: Observable<ParamMap> = of({
    has: (name: string) => true,
    get: (name: string) => 'mock-value2',
    getAll: (name: string) => ['mock-value1', 'mock-value2'],
    keys: ['mock-value1', 'mock-value2'],
  });

  data: Observable<Data> = of({
    currentConsumptions: currentConsumptionsMock,
    dataSpace: dataSpacesMock[0],
  });
}
