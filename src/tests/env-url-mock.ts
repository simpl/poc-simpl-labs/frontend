import { EnvUrl } from '../app/services/app.service';

export const envUrl: EnvUrl = {
  keycloak: {
    config: {
      url: 'https://mock.eu',
      realm: 'realm-mock',
      clientId: 'client-id-mock',
    },
    initOptions: {
      onLoad: 'login-required',
      flow: 'standard',
    },
  },
  apiUrl: 'http://api-mock.org',
  consoleURL: 'https://poc-simpl-labs-kubebox.dev.simpl-europe.eu/',
  simplUrl: '"http://simpl-mock.org/',
};
