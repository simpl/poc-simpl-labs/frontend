// default angular breakpoints:
//   xs: begin: 0, end: 599.9px
//   sm: begin: 600px, end: 959.9px
//   md: begin: 960px, end: 1279.9px
//   lg: begin: 1280px, end: 1919.9px
//   xl: begin: 1920px, end: 4999.99px

export const defaultAngularBreakpoints = {
  xs: {
    begin: 0,
    end: 599.9,
  },
  sm: {
    begin: 600,
    end: 959.9,
  },
  md: {
    begin: 960,
    end: 1279.9,
  },
  lg: {
    begin: 1280,
    end: 1919.9,
  },
  xl: {
    begin: 1920,
    end: 4999.99,
  },
};
