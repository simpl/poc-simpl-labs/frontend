import { NodeType } from '../models/enums/data-space/node-type.enum';
import { NodeModel } from '../models/interfaces/data-space/node.interface';

export const getNodeLastSuffix = (
  nodeList: NodeModel[],
  nodeType: NodeType,
): number => {
  return Math.max(
    ...nodeList
      .filter((n) => n.type.toUpperCase() === nodeType?.toUpperCase())
      .map((n) => {
        let idx =
          n.name.indexOf('_') !== n.name.lastIndexOf('_')
            ? n.name.lastIndexOf('_')
            : -1;
        return idx >= 0 ? +n.name.slice(idx + 1) : 1;
      }),
  );
};
