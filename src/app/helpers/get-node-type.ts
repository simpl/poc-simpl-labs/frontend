import { NodeType } from '../models/enums/data-space/node-type.enum';

export const getNodeType = (name: string): NodeType | undefined => {
  switch (!!name) {
    case name.toUpperCase().includes(NodeType.GOVERNANCE):
      return NodeType.GOVERNANCE;
    case name.toUpperCase().includes(NodeType.CONSUMER):
      return NodeType.CONSUMER;
    case name.toUpperCase().includes(NodeType.DATA_PROVIDER):
      return NodeType.DATA_PROVIDER;
    case name.toUpperCase().includes(NodeType.APPLICATION_PROVIDER):
      return NodeType.APPLICATION_PROVIDER;
    case name.toUpperCase().includes(NodeType.INFRASTRUCTURE_PROVIDER):
      return NodeType.INFRASTRUCTURE_PROVIDER;
    default:
      return;
  }
};
