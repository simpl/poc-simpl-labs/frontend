import { CategoryType } from '../models/enums/data-space/category-type.enum';
import { ComponentType } from '../models/enums/data-space/component-type.enum';
import { NodeType } from '../models/enums/data-space/node-type.enum';

const acronyms = ['IAA', 'IDP', 'SD', 'FE', 'BE'];

const types: string[] = [
  'GOVERNANCE_AUTHORITY',
  ...Object.values(NodeType),
  ...Object.values(CategoryType),
  ...Object.values(ComponentType),
];

export const titlecaseNodeNameFcn = (
  name: string,
  chart: boolean = false,
): string => {
  // Remove '_' from and titlecase the Node default name.
  if (types.some((t) => name?.includes(t)))
    name = name
      .replace(/_/g, ' ')
      .replace(/\S*/g, (str) =>
        acronyms.includes(str)
          ? str
          : str.charAt(0).toUpperCase() + str.substring(1).toLowerCase(),
      );

  // For the Graph chart:
  // Breakdown the Node name in rows. Each row will contain a number of
  // characters less than ROW_LIMIT (= 16).
  if (name?.includes(' ') && chart) {
    const ROW_LIMIT = 16;
    let i = 0;
    name = name
      .split(' ')
      .reduce((newWords, _, __, words) => {
        const LEN = words.length;
        let newWord = words[i];

        for (let j = i + 1; j < LEN + 1; j++)
          if (!!words[j] && (newWord + ' ' + words[j]).length < ROW_LIMIT)
            newWord = newWord + ' ' + words[j];
          else {
            i = j;
            break;
          }

        if (newWord) newWords.push(newWord);
        return newWords;
      }, [] as string[])
      .join('\n');
  }

  return name;
};
