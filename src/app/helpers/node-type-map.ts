import { NodeType } from '../models/enums/data-space/node-type.enum';

const nodeTypeMap = new Map();
nodeTypeMap.set(NodeType.GOVERNANCE, 'Governance Authority');
nodeTypeMap.set(NodeType.DATA_PROVIDER, 'Data Provider');
nodeTypeMap.set(NodeType.CONSUMER, 'Consumer');
nodeTypeMap.set(NodeType.APPLICATION_PROVIDER, 'Application Provider');
nodeTypeMap.set(NodeType.INFRASTRUCTURE_PROVIDER, 'Infrastructure Provider');

export default nodeTypeMap;
