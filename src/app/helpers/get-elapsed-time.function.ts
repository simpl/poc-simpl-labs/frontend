import { ms2HHMMSS } from './ms-2-HHMMSS';

export const getElapsedTime = (
  startTime: string,
  endTime?: string | null,
): string => {
  if (!startTime) return '00:00:00';

  if (!endTime) {
    let now = new Date();
    now.setSeconds(now.getSeconds());
    now.setMinutes(now.getMinutes());
    now.setHours(now.getHours() - 2);
    endTime = now.toString();
  }

  const start = new Date(startTime) as unknown as number;
  const end = new Date(endTime) as unknown as number;

  const diffTime = Math.abs(end - start);

  return diffTime >= 0 ? ms2HHMMSS(diffTime) : '/';
};
