export const roundTo2Decimals = (n: number): number => +n.toFixed(2);
