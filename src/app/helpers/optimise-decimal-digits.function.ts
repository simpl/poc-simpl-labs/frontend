export const optimiseDecimalDigitsFcn = (n: number): string => {
  const str = n?.toString();
  const dotIdx = str?.indexOf('.');
  let position = -1;

  if (dotIdx >= 0) {
    position = str
      .substring(dotIdx + 1)
      .split('')
      .findIndex((x) => +x > 0);
    return `1.1-${position + 1}`;
  }

  return `1.1-2`;
};
