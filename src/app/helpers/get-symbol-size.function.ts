import { titlecaseNodeNameFcn } from './titlecase-node-name.function';

const charWidth = 10;
const charHeight = 15;

export const getSymbolSize = {
  width: (nodeName: string, baseWidth: number): number =>
    baseWidth +
    charWidth *
      (Math.max(
        baseWidth / charWidth,
        ...titlecaseNodeNameFcn(nodeName, true)
          .split('\n')
          .map((row) => row.length),
      ) -
        baseWidth / charWidth),

  height: (nodeName: string, baseHeight: number): number =>
    baseHeight +
    charHeight * titlecaseNodeNameFcn(nodeName, true).split('\n').length,
};
