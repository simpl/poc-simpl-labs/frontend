import { GraphChartDatum } from '../models/interfaces/graph-chart-datum.interface';
import { SLabsFlatNode } from '../models/interfaces/tree-node.interface';

export const chartNode2SlabsFlatNode = (
  chartData: GraphChartDatum,
): SLabsFlatNode => {
  return {
    name: chartData.name,
    expanded: !chartData.collapsed,
    level: chartData.level!,
    type: chartData.type!,
    expandable: chartData.children?.length! > 0,
    id: +chartData.id!,
  };
};
