import { ThemePalette } from '@angular/material/core';

import { dataSpaceStatusChipColors } from '../models/interfaces/data-space/data-space-status-chip-colors';

const mapDataSpaceStatus2Chip = new Map<
  string,
  ThemePalette | 'requested_deletion' | 'paused' | 'installing' | 'undefined'
>();

dataSpaceStatusChipColors.forEach((_, i) =>
  mapDataSpaceStatus2Chip.set(
    dataSpaceStatusChipColors[i].status.toUpperCase(),
    dataSpaceStatusChipColors[i].color,
  ),
);

export default mapDataSpaceStatus2Chip;
