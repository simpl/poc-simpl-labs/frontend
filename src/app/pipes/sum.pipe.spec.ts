import { SumPipe } from './sum.pipe';

describe('SumPipe', () => {
  const pipe = new SumPipe();

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('transforms "[{ prop: x }, { prop: y }]" to "x + y"', () => {
    expect(pipe.transform([{ prop: 2 }, { prop: 3 }], 'prop')).toBe(2 + 3);
  });

  it('transforms " [{ prop1: { prop2: x } }, { prop1: { prop2: y } }]" to "x + y"', () => {
    expect(
      pipe.transform(
        [{ prop1: { prop2: 4 } }, { prop1: { prop2: 5 } }],
        ['prop1', 'prop2'],
      ),
    ).toBe(4 + 5);
  });
});
