import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sum',
  standalone: true,
})
export class SumPipe implements PipeTransform {
  transform(items: any[], attr?: string | string[]): any {
    return items.reduce((a, b) => {
      let value;

      if (Array.isArray(attr)) {
        value = b[attr[0]];
        for (let i = 1; i < attr.length; i++) value = value[attr[i]];
        return a + value;
      }

      return a + b[attr!];
    }, 0);
  }
}
