import { TestBed } from '@angular/core/testing';
import { ResolveFn } from '@angular/router';

import { dataSpaceResolver } from './data-space.resolver';
import { DataSpaceModel } from '../models/interfaces/data-space/data-space.interface';

describe('dataSpaceResolver', () => {
  const executeResolver: ResolveFn<DataSpaceModel> = (...resolverParameters) =>
    TestBed.runInInjectionContext(() =>
      dataSpaceResolver(...resolverParameters),
    );

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeResolver).toBeTruthy();
  });
});
