import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { KeycloakService } from 'keycloak-angular';
import { firstValueFrom, of } from 'rxjs';

import { initializeKeycloak } from './keycloak-init.factory';
import { AppService } from '../services/app.service';

describe('initializeKeycloak', () => {
  let initializeSpy: jasmine.Spy<any>;

  beforeEach(async () => {
    initializeSpy = jasmine.createSpy('initializeKeycloak', initializeKeycloak);

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [KeycloakService, AppService],
    });

    initializeSpy.and.returnValue(() => firstValueFrom(of(true)));
  });

  it('should create an instance', () => {
    expect(initializeKeycloak).toBeTruthy();
  });
});
