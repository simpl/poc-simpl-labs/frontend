import { HttpInterceptorFn, HttpRequest } from '@angular/common/http';
import { inject } from '@angular/core';
import { catchError, throwError } from 'rxjs';

import { DialogService } from '../services/dialog.service';
import { LoadingService } from '../services/loading.service';
import { ResponseStatus } from '../models/enums/response-status.enum';

const errStatus401 = (
  req: HttpRequest<unknown>,
  err: any,
  dialogService: DialogService,
  loadingService: LoadingService,
) => {
  if (req.url.includes('system-monitoring/general/current'))
    req.url.endsWith('cpu') &&
      loadingService.monitoringTimer === 0 &&
      dialogService.openWarningDialog({
        boldContent: `Status ${ResponseStatus._401}`,
        content: `${err.error.message}`,
        title: `Unauthorized request`,
      });
  else if (req.url.endsWith('/dataspaces'))
    loadingService.timer === 0 &&
      dialogService.openWarningDialog({
        boldContent: `Status ${ResponseStatus._401}`,
        content: `${err.error.message}`,
        title: `Unauthorized request`,
      });
  else
    dialogService.openWarningDialog({
      boldContent: `Status ${ResponseStatus._401}`,
      content: `${err.error.message}`,
      title: `Unauthorized request`,
    });
};

const errStatus404 = (
  req: HttpRequest<unknown>,
  err: any,
  dialogService: DialogService,
  loadingService: LoadingService,
) => {
  if (req.url.includes('system-monitoring/general/current'))
    req.url.endsWith('cpu') &&
      loadingService.monitoringTimer === 0 &&
      dialogService.openWarningDialog({
        boldContent: `Status ${ResponseStatus._404}`,
        content: `${err.error.error}`,
        title: `Not found`,
      });
  else if (req.url.endsWith('/dataspaces'))
    loadingService.timer === 0 &&
      dialogService.openWarningDialog({
        boldContent: `Status ${ResponseStatus._404}`,
        content: `${err.error.error}`,
        title: `Not found`,
      });
  else
    dialogService.openWarningDialog({
      boldContent: `Status ${ResponseStatus._404}`,
      content: `${err.error.error}`,
      title: `Not found`,
    });
};

const errStatus500 = (
  req: HttpRequest<unknown>,
  err: any,
  dialogService: DialogService,
  loadingService: LoadingService,
) => {
  if (req.url.includes('system-monitoring/general/current'))
    req.url.endsWith('cpu') &&
      loadingService.monitoringTimer === 0 &&
      dialogService.openWarningDialog({
        boldContent: `Status ${ResponseStatus._500}`,
        content: `${err.error.error}`,
        title: `Internal Server Error`,
      });
  else if (req.url.endsWith('/dataspaces'))
    loadingService.timer === 0 &&
      dialogService.openWarningDialog({
        boldContent: `Status ${ResponseStatus._500}`,
        content: `${err.error.error}`,
        title: `Internal Server Error`,
      });
  else
    dialogService.openWarningDialog({
      boldContent: `Status ${ResponseStatus._500}`,
      content: `${err.error.error}`,
      title: `Internal Server Error`,
    });
};

export const errorHandlerInterceptor: HttpInterceptorFn = (req, next) => {
  const dialogService = inject(DialogService);
  const loadingService = inject(LoadingService);

  return next(req).pipe(
    catchError((err: any) => {
      if (err.status === ResponseStatus._401)
        errStatus401(req, err, dialogService, loadingService);
      else if (err.status === ResponseStatus._404)
        errStatus404(req, err, dialogService, loadingService);
      else if (err.status === ResponseStatus._500)
        errStatus500(req, err, dialogService, loadingService);
      else if (req.url.includes('system-monitoring/general/current'))
        req.url.endsWith('cpu') &&
          loadingService.monitoringTimer === 0 &&
          dialogService.openWarningDialog({
            boldContent: `Status ${err.status}`,
            content: `${err.message}`,
            title: `HTTP error`,
          });
      else if (req.url.endsWith('/dataspaces'))
        loadingService.timer === 0 &&
          dialogService.openWarningDialog({
            boldContent: `Status ${err.status}`,
            content: `${err.message}`,
            title: `HTTP error`,
          });
      else
        dialogService.openWarningDialog({
          boldContent: `Status ${err.status}`,
          content: `${err.message}`,
          title: `HTTP error`,
        });

      return throwError(() => err);
    }),
  );
};
