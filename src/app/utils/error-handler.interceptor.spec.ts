import { TestBed } from '@angular/core/testing';
import {
  HttpEvent,
  HttpInterceptorFn,
  HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { errorHandlerInterceptor } from './error-handler.interceptor';
import { DialogService } from '../services/dialog.service';
import { LoadingService } from '../services/loading.service';

const requestMock = new HttpRequest<unknown>(
  'GET',
  'system-monitoring/general/current',
);

const next = (req: HttpRequest<unknown>) =>
  new Observable<HttpEvent<unknown>>();

describe('errorHandlerInterceptor', () => {
  let interceptor: HttpInterceptorFn;
  let dialogService: DialogService;
  let loadingService: LoadingService;

  beforeEach(() => {
    interceptor = (req, next) =>
      TestBed.runInInjectionContext(() => errorHandlerInterceptor(req, next));

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });

    dialogService = TestBed.inject(DialogService);
    loadingService = TestBed.inject(LoadingService);
  });

  it('should be created', () => {
    expect(interceptor).toBeTruthy();
  });
});
