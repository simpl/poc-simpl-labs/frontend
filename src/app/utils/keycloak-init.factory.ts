import { KeycloakService } from 'keycloak-angular';

import { AppService } from '../services/app.service';

export const initializeKeycloak =
  (appService: AppService, keycloak: KeycloakService) => async () =>
    keycloak.init((await appService.load()).keycloak);
