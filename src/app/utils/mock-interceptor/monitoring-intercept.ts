import { HttpResponse } from '@angular/common/http';
import { Observable, of, delay } from 'rxjs';

import { currentConsumptionsMock } from '../../../mocks/current-consumption-mock';
import dataSpacesCurrentConsumptionsMock from '../../../mocks/data-spaces-current-consumptions-mock';
import { ConsumptionType } from '../../models/enums/monitoring/consumption-type.enum';
import { CurrentConsumption } from '../../models/interfaces/monitoring/current-consumption.interface';
import { HistoricalConsumption } from '../../models/interfaces/monitoring/historical-consumption.interface';
import { Page } from '../../models/interfaces/monitoring/paginated-endpoint.type';
import { TenantConsumption } from '../../models/interfaces/monitoring/tenant-consumption.interface';

const cryptoSecurePRNG = (): number => {
  const crypto = window.crypto;
  let array = new Uint32Array(1);
  crypto.getRandomValues(array);
  const FileId = array[0] / 1e10;
  return FileId;
};

export const getConsumptionPoint = (upperLimit: number): number => {
  let valueBase = cryptoSecurePRNG() * upperLimit;
  valueBase = Math.round(
    (cryptoSecurePRNG() - 0.5) * (upperLimit / 15) + valueBase,
  );
  valueBase <= 0 && (valueBase = cryptoSecurePRNG() * upperLimit);
  return valueBase;
};

export const monitoringIntercept = (element: {
  url: string;
  json: any;
}): Observable<HttpResponse<any>> => {
  if (element.url.includes('system-monitoring/general/current/')) {
    let tenantConsumption: TenantConsumption = {
      name: null!,
      unitOfMeasurement: '',
      allocatable: 1,
      allocated: 1,
      used: 0,
    };

    if (element.url.endsWith(ConsumptionType.CPU)) {
      tenantConsumption = {
        name: 'CPU',
        unitOfMeasurement: 'cores',
        allocatable: 23.43,
        allocated: 0.14,
        used: getConsumptionPoint(0.00001),
        date: '2024-09-18T16:53:37.580646400',
      };
    }

    if (element.url.endsWith(ConsumptionType.MEMORY)) {
      tenantConsumption = {
        name: 'Memory',
        unitOfMeasurement: 'GB',
        allocatable: 83.48,
        allocated: 2.0694732666015625,
        used: getConsumptionPoint(0.13),
        date: '2024-09-18T16:54:25.025791600',
      };
    }

    if (element.url.endsWith(ConsumptionType.STORAGE)) {
      tenantConsumption = {
        name: 'Storage',
        unitOfMeasurement: 'GB',
        allocatable: 1000,
        allocated: 7,
        used: getConsumptionPoint(10),
        date: '2024-09-18T16:54:21.604886500',
      };
    }

    return of(new HttpResponse({ status: 200, body: tenantConsumption })).pipe(
      delay(700),
    );
  } else if (element.url.endsWith('system-monitoring/general/current')) {
    const ccsMock = currentConsumptionsMock.map((cc) => {
      cc.cpuUsed = getConsumptionPoint(0.00001);
      cc.memoryUsed = getConsumptionPoint(0.13);
      cc.storageUsed = getConsumptionPoint(10);
      return cc;
    });

    return of(new HttpResponse({ status: 200, body: ccsMock })).pipe(
      delay(700),
    );
  } else if (
    element.url.includes('system-monitoring/general/historical') ||
    element.url.includes('system-monitoring/component/historical')
  ) {
    let historicalConsumptionsMock: HistoricalConsumption[] = [];

    if (element.url.includes('cpu')) {
      historicalConsumptionsMock = [];

      for (let i = 2; i < 9; i++) {
        historicalConsumptionsMock.push({
          allocated: 0.15,
          used: getConsumptionPoint(0.00001),
          date: `2024-09-${i + 11}T19:24:20.841Z`,
        });
      }
    } else if (element.url.includes('memory')) {
      historicalConsumptionsMock = [];

      for (let i = 2; i < 9; i++) {
        historicalConsumptionsMock.push({
          allocated: 2.07,
          used: getConsumptionPoint(0.13),
          date: `2024-09-${i + 11}T19:24:20.841Z`,
        });
      }
    } else if (element.url.includes('storage')) {
      historicalConsumptionsMock = [];

      for (let i = 2; i < 9; i++) {
        historicalConsumptionsMock.push({
          allocated: 7,
          used: getConsumptionPoint(10),
          date: `2024-09-${i + 11}T19:24:20.841Z`,
        });
      }
    }

    return of(
      new HttpResponse({ status: 200, body: historicalConsumptionsMock }),
    ).pipe(delay(700));
  } else if (element.url.includes('system-monitoring/dataspace/current')) {
    const page = {
      number: 0,
      size: 5,
      totalElements:
        dataSpacesCurrentConsumptionsMock[
          +element.url[element.url.length - 1] - 1
        ].length,
      totalPages:
        Math.round(
          dataSpacesCurrentConsumptionsMock[
            +element.url[element.url.length - 1] - 1
          ].length / 5,
        ) +
        (dataSpacesCurrentConsumptionsMock[
          +element.url[element.url.length - 1] - 1
        ].length %
          5 ===
        0
          ? 0
          : 1),
    };

    const pagedCurrentConsumptionMock: Page<CurrentConsumption> = {
      content: dataSpacesCurrentConsumptionsMock[
        +element.url[element.url.length - 1] - 1
      ]
        .map((cc) => {
          cc.cpuUsed = getConsumptionPoint(0.00001);
          cc.memoryUsed = getConsumptionPoint(0.13);
          cc.storageUsed = getConsumptionPoint(10);
          return cc;
        })
        .slice(page.number * page.size, page.number * page.size + page.size),
      page,
    };

    return of(
      new HttpResponse({
        status: 200,
        body: pagedCurrentConsumptionMock,
      }),
    ).pipe(delay(700));
  }

  return of(new HttpResponse({ status: 200, body: element.json })).pipe(
    delay(50),
  );
};
