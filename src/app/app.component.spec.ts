import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { firstValueFrom, of } from 'rxjs';
import { KeycloakService } from 'keycloak-angular';

import { AppComponent } from './app.component';
import { DataSpaceService } from './services/data-space.service';
import { KeycloakOperationService } from './services/keycloak-operation.service';
import { MockActivatedRoute } from '../tests/helpers/mock-activated-route.class';

const profile = {
  email: 'mrossi@example.it',
  name: 'Maria Rosse',
  username: 'mrossi',
  id: 123,
};

const globalOptions: any = {};

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let mockKeycloakService;
  let mockKeycloakOperationService;
  let mockNotificationService = new NotificationsService(globalOptions);

  beforeEach(async () => {
    mockKeycloakOperationService = jasmine.createSpyObj(
      'KeycloakOperationService',
      ['getUserProfile'],
    );
    mockKeycloakService = jasmine.createSpyObj('KeycloakService', [
      'getUserProfile',
    ]);

    mockKeycloakOperationService.getUserProfile.and.returnValue(
      firstValueFrom(of(profile)),
    );

    await TestBed.configureTestingModule({
      imports: [AppComponent, HttpClientTestingModule, BrowserAnimationsModule],
      providers: [
        { provide: ActivatedRoute, useClass: MockActivatedRoute },
        {
          provide: KeycloakService,
          useValue: mockKeycloakOperationService,
        },
        {
          provide: KeycloakOperationService,
          useValue: mockKeycloakOperationService,
        },
        { provide: NotificationsService, useValue: mockNotificationService },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    let mockDataSpaceService =
      fixture.debugElement.injector.get(DataSpaceService);
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have the 'Simpl-Labs' title`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('Simpl-Labs');
  });
});
