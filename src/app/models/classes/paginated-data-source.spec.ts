import { of } from 'rxjs';

import { CurrentConsumption } from '../interfaces/monitoring/current-consumption.interface';
import {
  PaginatedEndpoint,
  Sort,
} from '../interfaces/monitoring/paginated-endpoint.type';
import { PaginatedDataSource } from './paginated-data-source';
import { currentConsumptionsMock } from '../../../mocks/current-consumption-mock';

describe('PaginatedCurrentConsumptionDataSource', () => {
  let paginatedDataSource: PaginatedDataSource<CurrentConsumption>;

  beforeEach(() => {
    paginatedDataSource = new PaginatedDataSource(
      {} as PaginatedEndpoint<CurrentConsumption>,
      {} as Sort<CurrentConsumption>,
      5,
    );
  });

  it('should create an instance', () => {
    expect(paginatedDataSource).toBeTruthy();
  });

  it('should call #sort$.next()', () => {
    const sortMock: Sort<CurrentConsumption> = {
      order: 'desc',
      property: 'id',
    };
    spyOn(paginatedDataSource.sort$, 'next');
    paginatedDataSource.sortBy(sortMock);
    expect(paginatedDataSource.sort$.next).toHaveBeenCalledWith(sortMock);
  });

  it('should call #pageNumber$.next()', () => {
    spyOn(paginatedDataSource.pageNumber$, 'next');
    paginatedDataSource.fetch(1);
    expect(paginatedDataSource.pageNumber$.next).toHaveBeenCalledWith(1);
  });

  it('should call #page$.pipe()', () => {
    spyOn(paginatedDataSource.page$, 'pipe').and.returnValue(
      of(currentConsumptionsMock),
    );
    paginatedDataSource.connect();
    expect(paginatedDataSource.page$.pipe).toHaveBeenCalled();
  });

  it('should call console.debug("Slabs")', () => {
    spyOn(console, 'debug');
    paginatedDataSource.disconnect();
    expect(console.debug).toHaveBeenCalledWith('Slabs');
  });
});
