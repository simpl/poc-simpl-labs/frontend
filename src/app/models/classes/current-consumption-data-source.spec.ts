import { currentConsumptionsMock } from '../../../mocks/current-consumption-mock';
import { CurrentConsumptionDataSource } from './current-consumption-data-source';

describe('CurrentConsumptionDataSource', () => {
  let currentConsumptionDataSource: CurrentConsumptionDataSource;

  beforeEach(() => {
    currentConsumptionDataSource = new CurrentConsumptionDataSource(
      currentConsumptionsMock,
    );
  });

  it('should create an instance', () => {
    expect(currentConsumptionDataSource).toBeTruthy();
  });

  it('should call console.debug("Slabs")', () => {
    spyOn(console, 'debug');
    currentConsumptionDataSource.disconnect();
    expect(console.debug).toHaveBeenCalledWith('Slabs');
  });
});
