import { DataSource } from '@angular/cdk/collections';
import { ReplaySubject, Observable } from 'rxjs';

import { CurrentConsumption } from '../interfaces/monitoring/current-consumption.interface';

export class CurrentConsumptionDataSource extends DataSource<CurrentConsumption> {
  private readonly _dataStream$ = new ReplaySubject<CurrentConsumption[]>();

  constructor(initialData: CurrentConsumption[]) {
    super();
    this.setData(initialData);
  }

  connect(): Observable<CurrentConsumption[]> {
    return this._dataStream$;
  }

  setData(data: CurrentConsumption[]) {
    this._dataStream$.next(data);
  }

  disconnect(): void {
    console.debug('Slabs');
  }
}
