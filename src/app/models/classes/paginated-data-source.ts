import { map, Observable, share, startWith, Subject, switchMap } from 'rxjs';

import {
  Page,
  PaginatedEndpoint,
  Sort,
} from '../interfaces/monitoring/paginated-endpoint.type';
import { BasicDataSource } from '../interfaces/monitoring/basic-data-source';

export class PaginatedDataSource<T> implements BasicDataSource<T> {
  public readonly pageNumber$ = new Subject<number>();
  public readonly sort$ = new Subject<Sort<T>>();

  public page$: Observable<Page<T>>;

  constructor(endpoint: PaginatedEndpoint<T>, initialSort: Sort<T>, size = 5) {
    this.page$ = this.sort$.pipe(
      startWith(initialSort),
      switchMap((sort) =>
        this.pageNumber$.pipe(
          startWith(0),
          switchMap((page) => endpoint({ page, sort, size })),
        ),
      ),
      share(),
    );
  }

  sortBy(sort: Sort<T>): void {
    this.sort$.next(sort);
  }

  fetch(page: number): void {
    this.pageNumber$.next(page);
  }

  connect(): Observable<T[]> {
    return this.page$.pipe(map((page) => page?.content));
  }

  disconnect() {
    console.debug('Slabs');
  }
}
