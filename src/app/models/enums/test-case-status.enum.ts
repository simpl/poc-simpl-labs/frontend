export enum TestCaseStatus {
  SUCCESS = 'Success',
  FAILURE = 'Failure',
  ONGOING = 'Ongoing',
  UNDEFINED = 'Undefined',
}
