export enum ResponseStatus {
  _401 = 401,
  _404 = 404,
  _500 = 500,
}
