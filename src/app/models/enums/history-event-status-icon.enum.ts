export enum HistoryEventStatusIcon {
  ERROR = 'error',
  WARNING = 'warning',
  CONFIRMED = 'check',
}
