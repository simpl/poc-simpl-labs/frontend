export enum DataSpaceStatus {
  REQUESTED = 'REQUESTED', // installation requested, not started yet
  REQUESTED_DELETION = 'REQUESTED_DELETION',
  INSTALLING = 'INSTALLING', // installation in progress
  RUNNING = 'RUNNING', // installation finished - started
  ERROR = 'ERROR', // installation stopped
  PAUSED = 'PAUSED',
}
