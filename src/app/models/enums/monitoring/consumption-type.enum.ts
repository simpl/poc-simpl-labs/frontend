export enum ConsumptionType {
  CPU = 'cpu',
  MEMORY = 'memory',
  STORAGE = 'storage',
}
