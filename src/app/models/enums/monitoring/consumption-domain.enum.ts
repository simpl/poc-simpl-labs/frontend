export enum ConsumptionDomain {
  GENERAL = `general`,
  DATASPACE = `dataspace`,
  NODE = `node`,
  CATEGORY = `category`,
  COMPONENT = `component`,
}
