export enum NavIcon {
  DASHBOARD = 'home',
  MONITORING = 'bar_chart',
  DATA_SPACES = 'view_in_ar',
  COMPONENTS = 'extension',
  CONFORMANCE = 'policy',
  TEMPLATES = 'view_comfy',
  TEMPLATE_PREVIEW = 'preview',
  LEARNING = 'import_contacts',
  SETTINGS = 'settings',
}
