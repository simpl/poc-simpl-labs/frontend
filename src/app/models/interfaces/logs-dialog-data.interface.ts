import { LogsModel } from './slabs-custom-components/logs.interface';
import { TestCaseModel } from './slabs-custom-components/test-case.interface';

export interface LogsDialogData extends LogsModel {
  testCase: TestCaseModel;
  duration: string;
}
