import { ComponentType } from '../../enums/data-space/component-type.enum';

export interface Condition {
  name: string;
  accepted: boolean;
  componentType?: ComponentType;
  children?: Condition[];
}
