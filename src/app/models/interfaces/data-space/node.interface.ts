import { CategoryModel } from './category.interface';
import { NodeType } from '../../enums/data-space/node-type.enum';

export interface NodeModel {
  id: number | null;
  name: string;
  type: NodeType;
  categoryList: CategoryModel[];
}
