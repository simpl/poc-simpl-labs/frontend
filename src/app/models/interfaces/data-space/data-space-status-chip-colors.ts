import { ThemePalette } from '@angular/material/core';

import { DataSpaceStatus } from '../../enums/data-space/data-space-status.enum';

interface DataSpaceStatusChipColor {
  status: DataSpaceStatus;
  color: ThemePalette | 'requested_deletion' | 'paused' | 'installing';
}

export const dataSpaceStatusChipColors: DataSpaceStatusChipColor[] = [
  { status: DataSpaceStatus.RUNNING, color: 'primary' },
  { status: DataSpaceStatus.REQUESTED, color: 'accent' },
  { status: DataSpaceStatus.REQUESTED_DELETION, color: 'requested_deletion' },
  { status: DataSpaceStatus.ERROR, color: 'warn' },
  { status: DataSpaceStatus.INSTALLING, color: 'installing' },
  { status: DataSpaceStatus.PAUSED, color: 'paused' },
];
