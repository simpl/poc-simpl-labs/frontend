import { PageContentType } from '../../enums/data-space/page-content-type.enum';
import { CategoryModel } from './category.interface';
import { DataSpaceModel } from './data-space.interface';
import { NodeModel } from './node.interface';

export interface PageContentUpdate {
  dataSpace: DataSpaceModel;
  nodeList: NodeModel[];
  pageContentType: PageContentType | string;
  tableCategory: CategoryModel;
}
