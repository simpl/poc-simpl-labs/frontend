import { DataSpaceStatus } from '../../enums/data-space/data-space-status.enum';
import { DataSpaceType } from '../../enums/data-space/data-space-type.enum';
import { NodeModel } from './node.interface';
import { User } from '../user.interface';

export interface DataSpaceModel {
  id?: number;
  name: string;
  description: string;
  nodeList: NodeModel[];
  status: DataSpaceStatus;
  createDate?: string;
  user: User;
  tagList: string[];
  type?: DataSpaceType;
  debug?: boolean;
}
