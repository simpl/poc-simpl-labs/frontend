import { CategoryType } from '../../enums/data-space/category-type.enum';
import { ComponentModel } from './component.interface';

export interface ComponentInfo extends ComponentModel {
  categoryType: CategoryType;
}
