export interface CreatedConfirmDialogData {
  success: boolean;
  title: string;
  text: string;
}
