export interface GraphSeriesLink {
  source: string;
  target: string;
  lineStyle?: {
    curveness: number;
    color: string;
  };
}
