export interface LearningSection {
  id: number;
  title: string;
  description: string;
  date: string;
}
