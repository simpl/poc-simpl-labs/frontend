export interface ErrorDialogData {
  title: string;
  content: string;
  boldContent?: string;
}
