export interface WarningDialogData<T> {
  title: string;
  content: string;
  boldContent?: string;
  confirm?: boolean;
  callerData?: T;
}
