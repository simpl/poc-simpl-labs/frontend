import { CategoryType } from '../enums/data-space/category-type.enum';
import { NodeType } from '../enums/data-space/node-type.enum';

export interface GraphChartDatum {
  name: string;
  x: number;
  y: number;
  id?: string;
  type?: NodeType | CategoryType;
  draggable?: boolean;
  children?: GraphChartDatum[];
  collapsed?: boolean;
  level?: number;
  ancestorName?: string;
  fixed?: boolean;
  symbolSize?: number | number[];
  category?: number | string;
  cursor?: string;
  label?: {
    align?: 'left' | 'center' | 'right';
    verticalAlign?: 'top' | 'middle' | 'bottom';
  };
}
