import { DataSpaceStatus } from '../../enums/data-space/data-space-status.enum';
import { NodeType } from '../../enums/data-space/node-type.enum';

export interface CurrentConsumption {
  id: number;
  name: string;
  status: DataSpaceStatus;
  cpuAllocated: number;
  cpuUsed: number;
  memoryAllocated: number;
  memoryUsed: number;
  storageAllocated: number;
  storageUsed: number;
  componentId: number;
  componentName: string;
  nodeId: number;
  nodeName: string;
  nodeType?: NodeType;
  categoryId: number;
  categoryName: string;
  unitOfMeasurement?: string;
}
