import { ConsumptionType } from '../../enums/monitoring/consumption-type.enum';

export interface HistoricalConsumption {
  allocated: number;
  used: number;
  date: string;
}

export interface QuantityHistoricalConsumption {
  name: ConsumptionType | string;
  historicalConsumptions: HistoricalConsumption[];
  average?: number;
}

export interface SelectedPointPopupData extends HistoricalConsumption {
  name: ConsumptionType;
}
