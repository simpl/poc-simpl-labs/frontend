import { ConsumptionType } from '../../enums/monitoring/consumption-type.enum';

export interface TenantConsumption {
  name: ConsumptionType | string;
  unitOfMeasurement: string;
  allocatable: number;
  allocated: number;
  used: number;
  date?: string;
}
