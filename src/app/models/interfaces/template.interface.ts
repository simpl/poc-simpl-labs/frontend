import { DataSpaceModel } from './data-space/data-space.interface';

export interface TemplateModel {
  id: number;
  title: string;
  description: string;
  author: string;
  lastUpdate: string;
  license: string;
  tags: string[];
  imageUrl: string;
  isMVDS: boolean;
  dataSpace: DataSpaceModel;
  dataSpaceId: number;
}
