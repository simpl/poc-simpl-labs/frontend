/**
 * Data with nested structure.
 * Each node has a name and an optional list.
 */
export interface SLabsNode {
  name: string;
  type: string;
  list?: SLabsNode[];
  expanded?: boolean;
  id: number;
}

/** Flat node with expandable and level information */
export interface SLabsFlatNode {
  expandable: boolean;
  name: string;
  level: number;
  type: string;
  id: number;
  expanded?: boolean;
}
