import { TestCaseModel } from './test-case.interface';

export interface TestExecutionModel {
  id: number;
  startDate: string;
  endDate: string | null;
  status: string;
  totalTestsNumber: number;
  passedTestsNumber: number;
  failedTestsNumber: number;
  undefinedTestsNumber: number;
  componentId: string;
  componentName: string;
  specificationId: number;
  specificationName: string;
  specificationLastUpdate: string;
}

export interface TestExecutionStatus {
  testExecution: TestExecutionModel;
  testCasesList: TestCaseModel[];
  elapsedTime?: string;
}
