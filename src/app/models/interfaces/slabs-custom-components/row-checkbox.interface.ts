export interface RowCheckbox {
  id: string;
  name: string;
  selected: boolean;
  disabled?: boolean;
  children?: RowCheckbox[];
}
