export interface TestSuiteModel {
  id: string;
  name: string;
  description: string;
  lastUpdate: string;
  testCasesNumber: number;
  resultError: number;
  resultNotExecuted: number;
  resultSuccess: number;
}
