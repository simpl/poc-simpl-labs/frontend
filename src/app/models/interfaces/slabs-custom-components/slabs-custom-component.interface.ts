import { CategoryType } from '../../enums/data-space/category-type.enum';
import { NodeType } from '../../enums/data-space/node-type.enum';
import { SimplComponentStatus } from '../../enums/simpl-components/simpl-component-status.enum';
import { TestCaseStatus } from '../../enums/test-case-status.enum';

export interface SlabsCustomComponentModel {
  id: number;
  version: string;
  name: string;
  description: string;
  specificationDescription: string;
  specificationId: number;
  specificationLastUpdate?: string;
  nodeType?: NodeType;
  category?: CategoryType;
  status: TestCaseStatus;
  resultSuccess: number | undefined;
  resultError: number | undefined;
  resultNotExecuted: number | undefined;
  lastSessionId?: number;
  customImage: string;
}

export type SimplComponent = Partial<SlabsCustomComponentModel> & {
  componentStatus: SimplComponentStatus;
  lastUpdate: string;
};
