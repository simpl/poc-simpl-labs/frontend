import { TestCaseStatus } from '../../enums/test-case-status.enum';

export interface TestCaseStep {
  id: string;
  description: string;
  date: string;
  progressiveId: string;
  result: TestCaseStatus;
}

export interface TestCaseModel {
  id: string;
  testSuiteId: string;
  name: string;
  description: string;
  status: TestCaseStatus;
  startDate: string;
  endDate: string;
  testSuiteDescription: string;
  testSuiteLastUpdate: string;
  stepsList: TestCaseStep[];
  session: string;
  version: string;
}
