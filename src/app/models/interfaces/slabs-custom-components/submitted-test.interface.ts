import { ITBTestsExecutionData } from './itb-tests-execution-data.interface';
import { RowCheckbox } from './row-checkbox.interface';

export interface SubmittedTest extends ITBTestsExecutionData {
  specificationCheckbox: RowCheckbox;
  minimized: boolean | undefined;
}
