export interface SessionStartResponse {
  code: number;
  message: string;
  createdSessions: [
    {
      testSuite: string;
      testCase: string;
      session: string;
    },
  ];
  executionProgressive: string;
}
