export interface SessionStartRequest {
  testSuite: string[];
  testCase: string[];
  forceSequentialExecution: boolean;
  inputMapping?: [
    {
      testSuite: string[];
      testCase: string[];
      input: {
        name: string;
        embeddingMethod: string;
        value: string;
        type: string;
        encoding: string;
        item: string[];
      };
    },
  ];
  customComponentId: number;
}
