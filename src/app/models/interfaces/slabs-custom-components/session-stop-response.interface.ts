export interface SessionStopResponse {
  executionProgressive: string;
}
