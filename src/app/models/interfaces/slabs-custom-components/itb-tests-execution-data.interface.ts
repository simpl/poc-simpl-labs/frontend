import { SessionStartResponse } from './session-start-response.interface';
import { SlabsCustomComponentModel } from './slabs-custom-component.interface';
import { TestCaseModel } from './test-case.interface';
import { TestExecutionStatus } from './test-execution-status.interface';
import { TestSuiteModel } from './test-suite.interface';

export interface ITBTestsExecutionData {
  testSuites: TestSuiteModel[];
  testCases: TestCaseModel[][];
  testExecutionStatus: TestExecutionStatus;
  customComponent: SlabsCustomComponentModel;
  sessionStartResponse: SessionStartResponse;
  isHistoryItem?: boolean;
}
