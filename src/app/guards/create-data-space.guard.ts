import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';

import { DataSpaceService } from '../services/data-space.service';

export const createDataSpaceGuard: CanActivateFn = () => {
  const dataSpaceService = inject(DataSpaceService);
  const router = inject(Router);

  return (
    dataSpaceService.getDataSpacesNumber() < dataSpaceService.dataSpacesLimit ||
    router.navigateByUrl('data-spaces')
  );
};
