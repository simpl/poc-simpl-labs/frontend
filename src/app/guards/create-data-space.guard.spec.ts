import { TestBed } from '@angular/core/testing';
import { CanActivateFn, Router, RouterModule } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { createDataSpaceGuard } from './create-data-space.guard';
import { routes } from '../router/app.routes';

describe('createDataSpaceGuard', () => {
  const executeGuard: CanActivateFn = (...guardParameters) =>
    TestBed.runInInjectionContext(() =>
      createDataSpaceGuard(...guardParameters),
    );

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterModule.forRoot(routes)],
    });
  });

  it('should be created', () => {
    expect(executeGuard).toBeTruthy();
  });

  it('should redirect to "data-spaces" if nDataSpaces = 2', () => {
    const router = TestBed.inject(Router);
    spyOn(router, 'navigateByUrl');
    executeGuard(null!, null!);
    expect(router.navigateByUrl).toHaveBeenCalledWith('data-spaces');
  });
});
