import {
  APP_INITIALIZER,
  ApplicationConfig,
  importProvidersFrom,
} from '@angular/core';
import {
  HTTP_INTERCEPTORS,
  provideHttpClient,
  withInterceptors,
  withInterceptorsFromDi,
} from '@angular/common/http';
import { provideRouter } from '@angular/router';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { KeycloakBearerInterceptor, KeycloakService } from 'keycloak-angular';

import { routes } from './router/app.routes';
import { initializeKeycloak } from './utils/keycloak-init.factory';
import { MockInterceptor } from './utils/mock-interceptor/mock.interceptor';
import { LoadingSpinnerInterceptor } from './utils/loading-spinner.interceptor';
import { errorHandlerInterceptor } from './utils/error-handler.interceptor';
import { AppService } from './services/app.service';

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes),
    provideAnimationsAsync(),
    KeycloakService,
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [AppService, KeycloakService],
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoadingSpinnerInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: KeycloakBearerInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MockInterceptor,
      multi: true,
    },
    importProvidersFrom(SimpleNotificationsModule.forRoot()),
    provideHttpClient(
      withInterceptorsFromDi(),
      withInterceptors([errorHandlerInterceptor]),
    ),
  ],
};
