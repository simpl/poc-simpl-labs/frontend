import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NavigationEnd, Router } from '@angular/router';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { CommonModule } from '@angular/common';
import { MatChipsModule } from '@angular/material/chips';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { Subscription, map } from 'rxjs';

import { TemplateModel } from '../../models/interfaces/template.interface';
import { DataSpaceService } from '../../services/data-space.service';
import {
  authors,
  lastUpdates,
  policies,
  sectors,
} from '../wizard/constants/data-space-creation.data';
import { TemplateService } from '../../services/template.service';
import { NodeType } from '../../models/enums/data-space/node-type.enum';
import { DataSpaceModel } from '../../models/interfaces/data-space/data-space.interface';
import { DialogService } from '../../services/dialog.service';

@Component({
  selector: 'app-templates',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatSelectModule,
    MatGridListModule,
    MatCardModule,
    MatChipsModule,
    MatButtonModule,
  ],
  templateUrl: './templates.component.html',
  styleUrl: './templates.component.scss',
})
export class TemplatesComponent implements OnInit, OnDestroy {
  @Input() stepTitle = '';
  isWizard = false;

  _basicTemplates: TemplateModel[] = [];
  _allTemplates: TemplateModel[] = [];
  _mvdsTemplate!: TemplateModel;

  selectedTemplate!: TemplateModel;

  // FILTERS
  _authors = authors;
  _policies = policies;
  _sectors = sectors;
  _lastUpdates = lastUpdates;

  searchForm = this._fb.group({
    inputText: [''],
    author: [null],
    policy: [null],
    sector: [null],
    lastUpdate: [null],
  });

  // SUBSCRIPTIONS
  routerEventsSub = new Subscription();
  newTemplateSub = new Subscription();
  basicTemplatesSub = new Subscription();
  allTemplatesSub = new Subscription();

  constructor(
    private readonly _fb: FormBuilder,
    private readonly dataSpaceService: DataSpaceService,
    private readonly router: Router,
    private readonly templateService: TemplateService,
    private readonly dialogService: DialogService,
    public dialog: MatDialog,
  ) {
    this.routerEventsSub = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.isWizard = event.url.includes('data-space');
        this.stepTitle = this.isWizard ? 'Select a template' : '';
      }
    });

    // TEMPORARLY DISABLED
    this.searchForm.disable();
  }

  ngOnInit(): void {
    this.basicTemplatesSub = this.templateService
      .getBasicTemplates()
      .pipe(
        map((templates) => {
          return templates.map((t) => {
            t.dataSpace.nodeList = t.dataSpace.nodeList.map((n) => {
              n.name =
                n.name === `${NodeType.GOVERNANCE}`
                  ? `${NodeType.GOVERNANCE}_AUTHORITY`
                  : n.name;
              return n;
            });
            return t;
          });
        }),
      )
      .subscribe((templates) => {
        this._basicTemplates = templates;
        this._mvdsTemplate = this._basicTemplates.find((t) => t.isMVDS)!;

        if (this.isWizard) {
          this.selectedTemplate = this.selectedTemplate || this._mvdsTemplate;

          const newTemplate: TemplateModel = { ...this.selectedTemplate };
          const newDataSpace: DataSpaceModel = { ...newTemplate.dataSpace };

          this.dataSpaceService.newDataSpaceSubject$.next(newDataSpace);
        }
      });

    this.allTemplatesSub = this.templateService
      .getAllTemplates()
      .pipe(
        map((templates) => {
          return templates.map((t) => {
            t.dataSpace.nodeList = t.dataSpace.nodeList.map((n) => {
              n.name =
                n.name === `${NodeType.GOVERNANCE}`
                  ? `${NodeType.GOVERNANCE}_AUTHORITY`
                  : n.name;
              return n;
            });
            return t;
          });
        }),
      )
      .subscribe((templates) => (this._allTemplates = templates));
  }

  ngOnDestroy(): void {
    this.routerEventsSub.unsubscribe();
    this.newTemplateSub.unsubscribe();
    this.basicTemplatesSub.unsubscribe();
    this.allTemplatesSub.unsubscribe();
  }

  selectTemplate(template: TemplateModel) {
    if (!this.isWizard) return;

    /**
     * For the current POC only the Minimum Viable Data Space Template can be selected
     */
    if (template.isMVDS) {
      this.selectedTemplate = { ...template };
      const newDataSpace = { ...this.selectedTemplate.dataSpace };
      this.dataSpaceService.newDataSpaceSubject$.next(newDataSpace);
      return;
    }

    this.dialogService.openWarningDialog({
      title: 'Template functionality disabled',
      content:
        'For the current POC only the Minimum Viable Data Space Template can be selected',
    });
  }

  previewTemplate(template: TemplateModel) {
    this.router.navigateByUrl('/template-preview', { state: { template } });
  }
}
