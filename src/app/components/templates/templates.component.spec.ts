import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { of, Subject } from 'rxjs';
import { Router } from '@angular/router';

import { TemplatesComponent } from './templates.component';
import { DataSpaceService } from '../../services/data-space.service';
import { TemplateService } from '../../services/template.service';
import {
  allTemplatesMock,
  basicTemplatesMock,
} from '../wizard/constants/data-space-creation.data';
import { DataSpaceModel } from '../../models/interfaces/data-space/data-space.interface';
import { DialogService } from '../../services/dialog.service';
import { MockRouter } from '../../../tests/helpers/mock-router';

describe('TemplatesComponent', () => {
  let component: TemplatesComponent;
  let fixture: ComponentFixture<TemplatesComponent>;
  let dataSpaceServiceSpy: jasmine.SpyObj<DataSpaceService>;
  let templateServiceSpy: jasmine.SpyObj<TemplateService>;
  let dialogServiceSpy: jasmine.SpyObj<DialogService>;
  let mockRouter = new MockRouter();

  beforeEach(async () => {
    dataSpaceServiceSpy = jasmine.createSpyObj('DataSpaceService', [
      'createDataSpace',
    ]);
    templateServiceSpy = jasmine.createSpyObj('TemplateService', [
      'getAllTemplates',
      'getBasicTemplates',
    ]);
    dialogServiceSpy = jasmine.createSpyObj('DialogService', [
      'openWarningDialog',
    ]);

    dataSpaceServiceSpy.newDataSpaceSubject$ = new Subject<DataSpaceModel>();

    await TestBed.configureTestingModule({
      imports: [
        TemplatesComponent,
        HttpClientTestingModule,
        BrowserAnimationsModule,
      ],
      providers: [
        {
          provide: DataSpaceService,
          useValue: dataSpaceServiceSpy,
        },
        {
          provide: TemplateService,
          useValue: templateServiceSpy,
        },
        {
          provide: DialogService,
          useValue: dialogServiceSpy,
        },
        {
          provide: Router,
          useValue: mockRouter,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(TemplatesComponent);
    component = fixture.componentInstance;
    templateServiceSpy.getBasicTemplates.and.returnValue(
      of(basicTemplatesMock),
    );
    templateServiceSpy.getAllTemplates.and.returnValue(of(allTemplatesMock));
    dataSpaceServiceSpy.newDataSpaceSubject$.next = jasmine.createSpy(
      'dataSpaceServiceSpy.newDataSpaceSubject$.next',
      () => new Subject<DataSpaceModel>(),
    );

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set #_basicTemplates', () => {
    expect(component._basicTemplates).toEqual(basicTemplatesMock);
  });

  it('should set #_allTemplates', () => {
    expect(component._allTemplates).toEqual(allTemplatesMock);
  });

  it('should set #selectedTemplate value', () => {
    component.selectedTemplate = undefined!;
    component.isWizard = true;
    component.selectTemplate(basicTemplatesMock[0]);
    expect(component.selectedTemplate).toEqual(basicTemplatesMock[0]);
  });

  it('should not set #selectedTemplate value', () => {
    component.selectedTemplate = undefined!;
    component.isWizard = false;
    component.selectTemplate(basicTemplatesMock[0]);
    expect(component.selectedTemplate).toBeUndefined();
  });

  it('should open WarningDialog', () => {
    component.selectedTemplate = undefined!;
    component.isWizard = true;
    component.selectTemplate(basicTemplatesMock[1]);
    expect(component.selectedTemplate).toBeUndefined();
    expect(dialogServiceSpy.openWarningDialog).toHaveBeenCalledWith({
      title: 'Template functionality disabled',
      content:
        'For the current POC only the Minimum Viable Data Space Template can be selected',
    });
  });

  it('should navigate by Url to "/template-preview"', () => {
    spyOn(mockRouter, 'navigateByUrl');
    component.previewTemplate(basicTemplatesMock[2]);
    expect(mockRouter.navigateByUrl).toHaveBeenCalledWith('/template-preview', {
      state: { template: basicTemplatesMock[2] },
    });
  });
});
