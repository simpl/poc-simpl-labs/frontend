import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SimpleChange, SimpleChanges } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Subject } from 'rxjs';

import * as echarts from 'echarts/core';
import { GraphChart } from 'echarts/charts';
import { DatasetComponent, TransformComponent } from 'echarts/components';
import { LabelLayout, UniversalTransition } from 'echarts/features';
import { CanvasRenderer } from 'echarts/renderers';

import { DataSpaceGraphChartComponent } from './data-space-graph-chart.component';
import { dataSpacesMock } from '../../../mocks/data-space-mock';
import { graphChartDataMock } from '../../../tests/data-spaces/graph-chart-datum-mock';
import { GraphChartDatum } from '../../models/interfaces/graph-chart-datum.interface';
import { TreeMenuService } from '../../services/tree-menu.service';

const dummyElement = document.createElement('div');

document.getElementById = jasmine
  .createSpy('HTML Element')
  .and.returnValue(dummyElement);

const defineProperty = Object.defineProperty;
Object.defineProperty = (o, p, c) =>
  defineProperty(o, p, { ...{}, ...(c ?? {}), ...{ configurable: true } });

const eventMock: echarts.ECElementEvent = {
  $vars: ['var-mock-1', 'var-mock-2'],
  componentIndex: 123,
  componentSubType: 'comp-sub-type-mock',
  componentType: 'comp-type',
  data: {
    name: dataSpacesMock[1].nodeList[0].name,
    x: 75.5,
    y: 0,
    id: dataSpacesMock[1].nodeList[0].id,
    type: dataSpacesMock[1].nodeList[0].type,
  } as unknown as GraphChartDatum,
  dataIndex: 1,
  name: dataSpacesMock[1].nodeList[0].name,
  type: 'click',
  value: 1,
  event: {
    target: {
      //@ts-ignore
      style: {
        text: 'text-mock',
        image: 'data:image/svg+xml;charset=utf8,',
      },
    },
  },
};

describe('DataSpaceGraphChartComponent', () => {
  let component: DataSpaceGraphChartComponent;
  let fixture: ComponentFixture<DataSpaceGraphChartComponent>;
  let changes: SimpleChanges;
  let treeMenuServiceSpy: jasmine.SpyObj<TreeMenuService>;

  beforeEach(async () => {
    treeMenuServiceSpy = jasmine.createSpyObj('TreeMenuService', [
      'updateBreadcrumbs',
    ]);
    treeMenuServiceSpy.graphChartNodeSubject$ = new Subject<GraphChartDatum>();

    await TestBed.configureTestingModule({
      imports: [
        DataSpaceGraphChartComponent,
        HttpClientTestingModule,
        BrowserAnimationsModule,
      ],
      providers: [{ provide: TreeMenuService, useValue: treeMenuServiceSpy }],
    }).compileComponents();

    fixture = TestBed.createComponent(DataSpaceGraphChartComponent);
    component = fixture.componentInstance;
    component.dataSpace = { ...dataSpacesMock[1] };

    echarts.use([
      DatasetComponent,
      TransformComponent,
      GraphChart,
      LabelLayout,
      UniversalTransition,
      CanvasRenderer,
    ]);

    const change = new SimpleChange(
      dataSpacesMock[0],
      dataSpacesMock[1],
      false,
    );
    changes = { dataSpace: change };
    component.ngOnChanges(changes);

    spyOn(component, 'generateGraph').and.callThrough();
    spyOn(component, 'createSeriesData').and.returnValue(graphChartDataMock);
    component.graphChartDatum = {
      name: '',
      x: 0,
      y: 0,
      id: null!,
    };
    component.dataSpace = { ...dataSpacesMock[1] };
    component.dsGraph = echarts.init(
      document.getElementById('ds-graph-chart-1'),
    );

    fixture.detectChanges();

    spyOn(component.dataSpace.nodeList, 'filter');
    spyOn(component.dataSpace.nodeList, 'sort');
    spyOn(component.dataSpace.nodeList, 'map');
    spyOn(component.dataSpace.nodeList, 'findIndex');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call #generateGraph()', fakeAsync(() => {
    component.ngOnChanges(changes);
    tick(500);
    expect(component.generateGraph).toHaveBeenCalled();
  }));

  it('should create the #seriesData', () => {
    component.generateGraph(dataSpacesMock[1], true);
    expect(component.createSeriesData).toHaveBeenCalled();
  });

  it('should call #multiNodeSeriesLinks()', () => {
    spyOn(component, 'multiNodeSeriesLinks');
    component.createSeriesLinks([...graphChartDataMock]);
    expect(component.multiNodeSeriesLinks).toHaveBeenCalled();
  });

  it('should call #singleNodeSeriesLinks()', () => {
    spyOn(component, 'singleNodeSeriesLinks');
    component.dataSpace.nodeList = component.dataSpace.nodeList.slice(0, 1);
    component.createSeriesLinks([...graphChartDataMock]);
    expect(component.singleNodeSeriesLinks).toHaveBeenCalled();
  });

  it('should handle click on graph node when !image', () => {
    spyOn(treeMenuServiceSpy.graphChartNodeSubject$, 'next');
    component.seriesData = [...graphChartDataMock];
    component.clickHandler({
      ...eventMock,
      //@ts-ignore
      event: { target: { style: { text: 'text-mock', image: '' } } },
    });
    expect(treeMenuServiceSpy.graphChartNodeSubject$.next).toHaveBeenCalled();
  });
});
