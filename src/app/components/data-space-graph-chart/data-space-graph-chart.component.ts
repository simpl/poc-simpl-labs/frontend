import {
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import * as echarts from 'echarts/core';
import { GraphChart } from 'echarts/charts';
import { DatasetComponent, TransformComponent } from 'echarts/components';
import { LabelLayout, UniversalTransition } from 'echarts/features';
import { CanvasRenderer } from 'echarts/renderers';
import { fromEvent, debounceTime, Subscription } from 'rxjs';

import { NodeType } from '../../models/enums/data-space/node-type.enum';
import { NodeModel } from '../../models/interfaces/data-space/node.interface';
import { DataSpaceModel } from '../../models/interfaces/data-space/data-space.interface';
import { OptionsMenuComponent } from '../_shared/options-menu/options-menu.component';
import { GraphChartDatum } from '../../models/interfaces/graph-chart-datum.interface';
import { TreeMenuService } from '../../services/tree-menu.service';
import { chartNode2SlabsFlatNode } from '../../helpers/chart-node-2-slabs-flat-node.map';
import { getSymbolSize } from '../../helpers/get-symbol-size.function';
import { GraphSeriesLink } from '../../models/interfaces/data-space/graph-series-link.interface';
import {
  DataSpaceGraphOption,
  getDataSpaceGraphOption,
} from './data-space-graph-option';

@Component({
  selector: 'app-data-space-graph-chart',
  standalone: true,
  templateUrl: './data-space-graph-chart.component.html',
  styleUrl: './data-space-graph-chart.component.scss',
  imports: [OptionsMenuComponent],
})
export class DataSpaceGraphChartComponent implements OnChanges, OnDestroy {
  @Input() isTemplatePreview = false;
  @Input() dataSpace!: DataSpaceModel;
  @Input() dsNode!: NodeModel;

  @ViewChild('dsGraphChart', { static: true, read: ElementRef<HTMLElement> })
  dsGraphChart!: ElementRef<HTMLElement>;

  @ViewChild(OptionsMenuComponent, {
    read: OptionsMenuComponent,
    static: true,
  })
  optionsMenuComponent!: OptionsMenuComponent;

  graphChartDatum: GraphChartDatum = {
    name: '',
    x: 0,
    y: 0,
    id: null!,
  };

  dsGraph!: echarts.ECharts;
  seriesData!: GraphChartDatum[];

  symbolWidth = 100;
  symbolHeight = 80;
  primary = '#3860ed';
  accent = '#bbb3ff';
  grey300 = '#e0e0e0';
  grey500 = '#9e9e9e';

  windowResizeEventSub = new Subscription();

  constructor(private readonly treeMenuService: TreeMenuService) {
    echarts.use([
      DatasetComponent,
      TransformComponent,
      GraphChart,
      LabelLayout,
      UniversalTransition,
      CanvasRenderer,
    ]);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('dataSpace')) {
      const newDataSpace = changes['dataSpace'].currentValue as DataSpaceModel;
      !!newDataSpace &&
        setTimeout(
          () => this.generateGraph(newDataSpace, this.isTemplatePreview),
          500,
        );
    }
  }

  ngOnDestroy(): void {
    this.windowResizeEventSub.unsubscribe();
    if (this.dsGraph) this.dsGraph.dispose();
  }

  generateGraph(dataSpace: DataSpaceModel, isView: boolean) {
    this.dsGraph = echarts.init(this.dsGraphChart.nativeElement);

    const dsOption: DataSpaceGraphOption = getDataSpaceGraphOption(
      dataSpace,
      isView,
    );
    this.dsGraph.setOption(dsOption);

    this.dsGraph.setOption({
      series: {
        data: this.createSeriesData().map((d) => {
          delete d.type;
          return d;
        }),
        links: this.createSeriesLinks(this.createSeriesData()),
      },
    });

    this.windowResizeEventSub = fromEvent(window, 'resize')
      .pipe(debounceTime(10))
      .subscribe(() => this.dsGraph.resize());

    this.dsGraph.on('click', (elmEvent: echarts.ECElementEvent) =>
      this.clickHandler(elmEvent),
    );
  }

  createSeriesData(): GraphChartDatum[] {
    if (this.dataSpace.nodeList.length === 1) {
      let angle = 0;
      let incr = (2 * Math.PI) / this.dataSpace.nodeList[0].categoryList.length;

      return [
        this.dataSpace.nodeList[0],
        ...this.dataSpace.nodeList[0].categoryList,
      ].map((node, i) => {
        const datum: GraphChartDatum = {
          fixed: false,
          type: node.type,
          name: node.name ?? node.type,
          draggable: true,
          x: 0,
          y: 0,
          symbolSize: [
            getSymbolSize.width(node.name ?? node.type.toString(), 100),
            getSymbolSize.height(node.name ?? node.type.toString(), 20),
          ],
        };

        if (i > 0) {
          let x = 3 * Math.cos(angle);
          let y = 3 * Math.pow(-1, i) * Math.sin(angle);
          datum.x = Math.abs(x) < 0.00001 ? Math.round(x) : x;
          datum.y = Math.abs(y) < 0.00001 ? Math.round(y) : y;
          datum.symbolSize = [100, 80];
        }

        if (i % 2 !== 0) angle = angle + incr;

        return datum;
      });
    }

    const nodeList: NodeModel[] = this.dataSpace.nodeList
      .filter((node) => node.type !== NodeType.GOVERNANCE)
      .sort((node) => (node.type === NodeType.CONSUMER ? -1 : 1));

    nodeList.unshift(
      this.dataSpace.nodeList.find(
        (node) => node.type === NodeType.GOVERNANCE,
      )!,
    );

    let oddCounter = -1;
    let evenCounter = 0;

    return nodeList.map((node, i) => {
      const datum: GraphChartDatum = {
        type: node.type,
        name: node.name,
        draggable: true,
        x: null!,
        y: null!,
        symbolSize: [
          getSymbolSize.width(node.name, this.symbolWidth),
          getSymbolSize.height(node.name, this.symbolHeight),
        ],
      };

      let factor: number;

      if (node.type === NodeType.GOVERNANCE) factor = 0;
      else factor = i % 2 === 0 ? i - evenCounter : i - oddCounter;

      datum.x = Math.pow(-1, i) * factor;
      datum.y = factor;

      i % 2 === 0 ? oddCounter++ : evenCounter++;

      return datum;
    });
  }

  createSeriesLinks(seriesData: GraphChartDatum[]): GraphSeriesLink[] {
    const seriesLinks: GraphSeriesLink[] = [];
    const LEN = seriesData.length;

    if (this.dataSpace.nodeList.length === 1) {
      this.singleNodeSeriesLinks(seriesData, seriesLinks);
      return seriesLinks;
    }

    this.multiNodeSeriesLinks(LEN, seriesData, seriesLinks);
    return seriesLinks;
  }

  multiNodeSeriesLinks(
    LEN: number,
    seriesData: GraphChartDatum[],
    seriesLinks: GraphSeriesLink[],
  ): void {
    for (let i = 0; i < LEN; i++) {
      let link: GraphSeriesLink = {
        source: '',
        target: '',
        lineStyle: { curveness: 0.3, color: this.grey500 },
      };

      for (let j = i + 1; j < LEN; j++) {
        if (
          (seriesData[i].type?.toUpperCase().includes('PROVIDER') &&
            seriesData[j].type?.toUpperCase().includes('PROVIDER')) ||
          (seriesData[i].type?.toUpperCase().includes('USER') &&
            seriesData[j].type?.toUpperCase().includes('USER'))
        )
          continue;

        link = this.createLink(seriesData, i, j);
        seriesLinks.push(link);
      }
    }
  }

  createLink(
    seriesData: GraphChartDatum[],
    i: number,
    j: number,
  ): GraphSeriesLink {
    let color = '';

    if (seriesData[i].type?.toUpperCase().includes('GOVERNANCE'))
      color = this.primary;
    else {
      color = seriesData[i].type?.toUpperCase().includes('USER')
        ? this.accent
        : this.grey500;
    }

    let link = {
      source: seriesData[i].name,
      target: seriesData[j].name,
      lineStyle: {
        curveness: seriesData[i].type?.toUpperCase().includes('USER')
          ? -0.3
          : Math.pow(-1, j) * 0.3,
        color,
      },
    };

    return link;
  }

  singleNodeSeriesLinks(
    seriesData: GraphChartDatum[],
    seriesLinks: GraphSeriesLink[],
  ): void {
    const categories = this.dataSpace.nodeList[0].categoryList;

    let link: GraphSeriesLink = {
      source: '',
      target: '',
      lineStyle: { curveness: 0.3, color: this.grey500 },
    };

    for (let i = 0; i < categories.length + 1; i++) {
      link = {
        source: this.dataSpace.nodeList[0].name,
        target: seriesData[i].name,
        lineStyle: {
          color: this.grey500,
          curveness: 0,
        },
      };

      seriesLinks.push(link);
    }
  }

  clickHandler(elmEvent: echarts.ECElementEvent) {
    const { text, image } = elmEvent.event!.target['style' as keyof unknown];
    const { nodeList } = this.dataSpace;

    if ((image + '').includes('data:image/svg+xml;charset=utf8,')) {
      this.graphChartDatum = { ...(elmEvent.data as GraphChartDatum) };

      this.graphChartDatum.x = elmEvent.event!.event['pageX' as keyof unknown];
      this.graphChartDatum.y =
        (2 * elmEvent.event!.event['pageY' as keyof unknown]) / 3;

      this.seriesData = this.createSeriesData();

      const idx = this.seriesData.findIndex((n) => n.name === elmEvent.name);

      this.graphChartDatum.type = this.seriesData[idx].type;

      return this.optionsMenuComponent.openMenu();
    }

    if (text) {
      const graphChartData: GraphChartDatum = elmEvent.data as GraphChartDatum;

      graphChartData.collapsed = false;
      graphChartData.level = 0;

      if (nodeList.length === 1) {
        graphChartData.ancestorName = nodeList[0].name;
        graphChartData.collapsed = graphChartData.name === nodeList[0].name;
        graphChartData.level = graphChartData.name !== nodeList[0].name ? 1 : 0;
      }

      this.treeMenuService.graphChartNodeSubject$.next(graphChartData);

      this.treeMenuService.updateBreadcrumbs(
        chartNode2SlabsFlatNode(graphChartData),
        false,
      );
    }
  }
}
