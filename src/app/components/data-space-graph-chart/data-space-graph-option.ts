import { LabelFormatterCallback } from 'echarts';
import type { GraphSeriesOption } from 'echarts/charts';
import type { DatasetComponentOption } from 'echarts/components';
import type { ComposeOption } from 'echarts/core';
import { CallbackDataParams } from 'echarts/types/dist/shared';

import { titlecaseNodeNameFcn } from '../../helpers/titlecase-node-name.function';
import { svgMoreHoriz } from '../../helpers/svg-more-horiz.icon';
import { DataSpaceModel } from '../../models/interfaces/data-space/data-space.interface';
import { ChartNodeMode } from '../../models/enums/data-space/chart-node-mode.enum';

const primary = '#3860ed';
const grey300 = '#e0e0e0';

export type DataSpaceGraphOption = ComposeOption<
  GraphSeriesOption | DatasetComponentOption
>;

export const encodeSvgToDataURI = (svg: string) =>
  'data:image/svg+xml;charset=utf8,' +
  encodeURIComponent(
    svg.replace(
      '<svg',
      svg.indexOf('xmlns') > -1
        ? '<svg'
        : '<svg xmlns="http://www.w3.org/2000/svg"',
    ),
  );

export const nodeLabelFormatWrapper = (
  dataSpace: DataSpaceModel,
  isView: boolean,
): LabelFormatterCallback<CallbackDataParams> => {
  const chartNodeMode = isView ? ChartNodeMode.VIEW : ChartNodeMode.EDIT;

  return (params: CallbackDataParams) => {
    return [
      isView || dataSpace.nodeList.length === 1 ? '' : '{moreHorizIcon|}\n',
      `{nodeName|${titlecaseNodeNameFcn(params.name, true)}}`,
      dataSpace.nodeList.length === 1 &&
      params.data!['x' as keyof unknown] === 0 &&
      params.data!['y' as keyof unknown] === 0
        ? ''
        : `\n\n{actionButton|${chartNodeMode}}`,
    ].join('');
  };
};

export const getDataSpaceGraphOption = (
  dataSpace: DataSpaceModel,
  isView: boolean,
): DataSpaceGraphOption => {
  return {
    animationDurationUpdate: 700,
    animationEasingUpdate: 'quinticInOut',
    series: [
      {
        id: 0,
        type: 'graph',
        name: 'graph1',
        top: '20%',
        bottom: '20%',
        autoCurveness: true,
        symbol: 'rect',
        symbolKeepAspect: false,
        roam: true,
        label: {
          show: true,
          fontFamily: 'Arial, sans-serif',
          formatter: nodeLabelFormatWrapper(dataSpace, isView),
          rich: {
            moreHorizIcon: {
              height: 28,
              borderColor: primary,
              align: 'right',
              backgroundColor: {
                image: encodeSvgToDataURI(svgMoreHoriz(primary)),
              },
            },
            nodeName: {
              lineHeight: 17,
              fontSize: 15,
              borderColor: primary,
              align: 'center',
            },
            actionButton: {
              lineHeight: 12,
              fontSize: 12,
              fontWeight: 500,
              borderColor: primary,
              borderWidth: 1,
              borderRadius: 2,
              color: primary,
              align: 'center',
              padding: [4, 6, 2, 6],
            },
          },
        },
        color: 'white',
        itemStyle: {
          borderWidth: 2,
          borderType: 'solid',
          borderColor: grey300,
          borderCap: 'round',
        },
        data: [],
        links: [],
        lineStyle: {
          opacity: 1,
          width: 2,
        },
      },
    ],
  };
};
