import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { Route, RouterModule } from '@angular/router';

import { routes } from '../../../router/app.routes';

@Component({
  selector: 'app-navigation-list',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    MatIconModule,
    MatButtonModule,
    MatToolbarModule,
    MatListModule,
  ],
  templateUrl: './navigation-list.component.html',
})
export class NavigationListComponent {
  navItems: Route[] = routes
    .filter((route) => (route.data ? !!route.data['icon'] : !!route.children))
    .map((route) => {
      let navItem = route;

      return !route.children || route.children.length === 0
        ? navItem
        : route.children
            .filter((child) => !child.path)
            .map((c) => {
              navItem = { ...c };
              navItem.path = route.path;
              navItem.title = route.path;
              return navItem;
            })[0];
    });
}
