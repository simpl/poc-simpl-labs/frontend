import { Component } from '@angular/core';

@Component({
  selector: 'app-simpl-labs-logo',
  standalone: true,
  imports: [],
  templateUrl: './simpl-labs-logo.component.svg',
})
export class SimplLabsLogoComponent {}
