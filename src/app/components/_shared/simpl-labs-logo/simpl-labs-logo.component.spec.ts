import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SimplLabsLogoComponent } from './simpl-labs-logo.component';

describe('SimplLabsLogoComponent', () => {
  let component: SimplLabsLogoComponent;
  let fixture: ComponentFixture<SimplLabsLogoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SimplLabsLogoComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(SimplLabsLogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
