import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

import { AppService } from '../../../services/app.service';

@Component({
  selector: 'app-kubebox-console',
  standalone: true,
  imports: [],
  templateUrl: './kubebox-console.component.html',
  styleUrl: './kubebox-console.component.scss',
})
export class KubeboxConsoleComponent implements OnInit {
  @ViewChild('consoleContainer', { static: true })
  consoleContainer!: ElementRef<HTMLDivElement>;

  consoleURL = '';

  constructor(private readonly appService: AppService) {
    this.consoleURL = this.appService.appUrls?.consoleURL;
  }

  ngOnInit(): void {
    const iFrameEl: HTMLIFrameElement = document.createElement('iframe');
    iFrameEl.setAttribute('title', 'Kubebox console');
    iFrameEl.setAttribute('src', this.consoleURL);
    iFrameEl.classList.add('console-iframe');
    this.consoleContainer.nativeElement.appendChild(iFrameEl);
  }
}
