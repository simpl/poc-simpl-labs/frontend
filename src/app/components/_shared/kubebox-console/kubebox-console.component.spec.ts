import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { KubeboxConsoleComponent } from './kubebox-console.component';

describe('KubeboxConsoleComponent', () => {
  let component: KubeboxConsoleComponent;
  let fixture: ComponentFixture<KubeboxConsoleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, KubeboxConsoleComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(KubeboxConsoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
