import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { WarningDialogComponent } from './warning-dialog.component';
import { WarningDialogData } from '../../../models/interfaces/warning-dialog-data.interface';

const warningDialogDataMock: WarningDialogData<undefined> = {
  content: '',
  title: '',
  boldContent: undefined,
  callerData: undefined,
  confirm: undefined,
};

describe('WarningDialogComponent', () => {
  let component: WarningDialogComponent<unknown>;
  let fixture: ComponentFixture<WarningDialogComponent<unknown>>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [WarningDialogComponent],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: warningDialogDataMock },
        { provide: MatDialogRef, useValue: {} },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(WarningDialogComponent<unknown>);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
