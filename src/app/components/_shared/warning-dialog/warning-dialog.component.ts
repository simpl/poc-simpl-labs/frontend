import { Component, Inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';
import { WarningDialogData } from '../../../models/interfaces/warning-dialog-data.interface';

@Component({
  selector: 'app-warning-dialog',
  standalone: true,
  imports: [MatDialogModule, MatButtonModule],
  templateUrl: './warning-dialog.component.html',
})
export class WarningDialogComponent<T> {
  constructor(@Inject(MAT_DIALOG_DATA) public data: WarningDialogData<T>) {}
}
