import { Component } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';

import { KubeboxConsoleComponent } from '../kubebox-console/kubebox-console.component';

@Component({
  selector: 'app-console-dialog',
  standalone: true,
  imports: [
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    KubeboxConsoleComponent,
  ],
  templateUrl: './console-dialog.component.html',
  styleUrl: './console-dialog.component.scss',
})
export class ConsoleDialogComponent {
  constructor(public dialogRef: MatDialogRef<ConsoleDialogComponent>) {}

  goBack() {
    this.dialogRef.close();
  }
}
