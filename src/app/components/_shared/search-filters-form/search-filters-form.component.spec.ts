import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SearchFiltersFormComponent } from './search-filters-form.component';

describe('SearchFiltersFormComponent', () => {
  let component: SearchFiltersFormComponent;
  let fixture: ComponentFixture<SearchFiltersFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SearchFiltersFormComponent, BrowserAnimationsModule],
    }).compileComponents();

    fixture = TestBed.createComponent(SearchFiltersFormComponent);
    component = fixture.componentInstance;
    component.statuses = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
