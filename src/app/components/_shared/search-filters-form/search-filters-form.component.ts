import { CommonModule } from '@angular/common';
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import {
  ReactiveFormsModule,
  FormsModule,
  FormGroup,
  FormBuilder,
} from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-search-filters-form',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatIconModule,
  ],
  templateUrl: './search-filters-form.component.html',
  styleUrl: './search-filters-form.component.scss',
})
export class SearchFiltersFormComponent
  implements OnInit, OnChanges, OnDestroy
{
  @Input() isConformanceTest: boolean = false;
  @Input() storedItems!: any[];
  @Input() statuses: any;
  @Input() itemsName = '';

  @Output() filterItemsEvent = new EventEmitter<any[]>();

  noneStatus = '-- none --';
  filtersForm!: FormGroup;
  filteredItems: any[] = [];

  filtersChangeSubscription = new Subscription();

  get filterNameCtrl() {
    return this.filtersForm.get('name');
  }

  constructor(private readonly _fb: FormBuilder) {
    this.filtersForm = this._fb.group({
      name: [''],
      status: [''],
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('storedItems')) {
      const { previousValue, currentValue } = changes['storedItems'];
      if (previousValue !== currentValue) {
        this._filterItems(this.filtersForm.value);
        this.filterItemsEvent.emit(this.filteredItems);
      }
    }
  }

  ngOnInit(): void {
    this.filtersChangeSubscription = this.filtersForm.valueChanges.subscribe(
      (filters) => {
        this._filterItems(filters);
        this.filterItemsEvent.emit(this.filteredItems);
      },
    );
  }

  ngOnDestroy(): void {
    this.filtersChangeSubscription.unsubscribe();
  }

  private _filterItems(filters: { name: string; status: any }) {
    this.filteredItems = this.storedItems
      .filter((item) => this._filterByName(item, filters.name))
      .filter((item) => this._filterByStatus(item, filters.status));
  }

  private _filterByName(item: any, name: string): boolean {
    return name ? item.name.toLowerCase().includes(name.toLowerCase()) : true;
  }

  private _filterByStatus(item: any, status: string): boolean {
    return status ? item.status.toLowerCase() === status.toLowerCase() : true;
  }
}
