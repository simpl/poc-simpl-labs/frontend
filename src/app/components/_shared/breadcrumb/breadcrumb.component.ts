import { CommonModule } from '@angular/common';
import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

import { TreeMenuService } from '../../../services/tree-menu.service';
import { SLabsFlatNode } from '../../../models/interfaces/tree-node.interface';
import { GraphChartDatum } from '../../../models/interfaces/graph-chart-datum.interface';
import { TitlecaseNodeName } from '../../../pipes/titlecase-node-name.pipe';
import { chartNode2SlabsFlatNode } from '../../../helpers/chart-node-2-slabs-flat-node.map';

@Component({
  selector: 'app-breadcrumb',
  standalone: true,
  imports: [CommonModule, MatIconModule, MatButtonModule, TitlecaseNodeName],
  templateUrl: './breadcrumb.component.html',
})
export class BreadcrumbComponent implements OnChanges, OnInit {
  @Input() dataSpaceName = '';
  @Input() dataSpaceId!: number;
  @Input() isMonitoring = false;

  dataSpaceBreadcrumb!: SLabsFlatNode;
  breadcrumbs: SLabsFlatNode[] = [];

  constructor(private readonly treeMenuService: TreeMenuService) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (
      changes.hasOwnProperty('dataSpaceName') &&
      changes.hasOwnProperty('dataSpaceId')
    ) {
      const dataSpaceName = changes['dataSpaceName'].currentValue;
      const dataSpaceId = changes['dataSpaceId'].currentValue;

      if (dataSpaceName) {
        this.treeMenuService.updateBreadcrumbs(
          {
            name: dataSpaceName,
            expandable: true,
            level: -1,
            type: '',
            expanded: true,
            id: dataSpaceId,
          },
          this.isMonitoring,
        );
      }
    }
  }

  ngOnInit(): void {
    this.breadcrumbs = this.treeMenuService.breadcrumbs;
  }

  updateBreadcrumbs(node: SLabsFlatNode) {
    const chartNode: GraphChartDatum = {
      name: node.name,
      collapsed: !node.expanded,
      level: node.level,
      x: null!,
      y: null!,
      id: node.id?.toString(),
    };

    this.treeMenuService.graphChartNodeSubject$.next(chartNode);

    this.treeMenuService.updateBreadcrumbs(
      chartNode2SlabsFlatNode(chartNode),
      this.isMonitoring,
    );
  }
}
