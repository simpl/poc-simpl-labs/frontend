import {
  ComponentFixture,
  ComponentFixtureAutoDetect,
  TestBed,
} from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SimpleChange, SimpleChanges } from '@angular/core';
import { Subject } from 'rxjs';

import { BreadcrumbComponent } from './breadcrumb.component';
import { TreeMenuService } from '../../../services/tree-menu.service';
import { GraphChartDatum } from '../../../models/interfaces/graph-chart-datum.interface';
import { slabsFlatNodesMock } from '../../../../tests/data-spaces/graph-chart-datum-mock';

describe('BreadcrumbComponent', () => {
  let component: BreadcrumbComponent;
  let fixture: ComponentFixture<BreadcrumbComponent>;
  let treeMenuServiceSpy: jasmine.SpyObj<TreeMenuService>;
  let breadcrumbButton: HTMLElement;

  beforeEach(async () => {
    treeMenuServiceSpy = jasmine.createSpyObj('TreeMenuService', [
      'updateBreadcrumbs',
    ]);

    treeMenuServiceSpy.graphChartNodeSubject$ = new Subject<GraphChartDatum>();

    await TestBed.configureTestingModule({
      imports: [BreadcrumbComponent, HttpClientTestingModule],
      providers: [
        { provide: ComponentFixtureAutoDetect, useValue: true },
        {
          provide: TreeMenuService,
          useValue: treeMenuServiceSpy,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(BreadcrumbComponent);
    component = fixture.componentInstance;
    component.dataSpaceName = '';

    component.breadcrumbs = [
      {
        expandable: true,
        level: 1,
        name: '1',
        type: '1',
        expanded: false,
        id: 2,
      },
      {
        expandable: false,
        level: 2,
        name: '2',
        type: '2',
        expanded: true,
        id: 3,
      },
    ];
    fixture.detectChanges();

    spyOn(component, 'updateBreadcrumbs').and.callThrough();
    spyOn(treeMenuServiceSpy.graphChartNodeSubject$, 'next');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have #dataSpaceName undefined', () => {
    expect(component.dataSpaceName).toBe('');
  });

  it('should have #dataSpaceName defined', () => {
    component.dataSpaceName = 'foo';
    expect(component.dataSpaceName).toBe('foo');
  });

  it('should call #treeMenuService.updateBreadcrumbs()', () => {
    const changeA = new SimpleChange('', 'bar', false);
    const changeB = new SimpleChange(null, 1, false);
    const changes: SimpleChanges = {
      dataSpaceName: changeA,
      dataSpaceId: changeB,
    };
    component.ngOnChanges(changes);
    expect(treeMenuServiceSpy.updateBreadcrumbs).toHaveBeenCalled();
  });

  it('should call #updateBreadcrumbs()', () => {
    component.breadcrumbs = [
      {
        expandable: true,
        level: 1,
        name: '1',
        type: '1',
        expanded: false,
        id: 2,
      },
      {
        expandable: false,
        level: 2,
        name: '2',
        type: '2',
        expanded: true,
        id: 3,
      },
    ];
    fixture.detectChanges();
    breadcrumbButton = fixture.nativeElement.querySelector(
      '.simplut-breadcrumb-btn',
    );
    breadcrumbButton.click();

    expect(component.updateBreadcrumbs).toHaveBeenCalled();
  });

  it('should call #treeMenuService.graphChartNodeSubject$.next() and #treeMenuService.updateBreadcrumbs()', () => {
    const graphChartDatumMock = {
      name: slabsFlatNodesMock[0].name,
      collapsed: !slabsFlatNodesMock[0].expanded,
      level: slabsFlatNodesMock[0].level,
      x: null!,
      y: null!,
      id: slabsFlatNodesMock[0].id.toString(),
    };

    component.updateBreadcrumbs(slabsFlatNodesMock[0]);

    expect(treeMenuServiceSpy.updateBreadcrumbs).toHaveBeenCalled();
    expect(treeMenuServiceSpy.graphChartNodeSubject$.next).toHaveBeenCalledWith(
      graphChartDatumMock,
    );
  });
});
