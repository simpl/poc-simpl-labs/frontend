import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { of, Subject } from 'rxjs';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HeaderComponent } from './header.component';
import { SectionName } from '../../../models/enums/navigation/section-name.enum';
import { basicTemplatesMock } from '../../wizard/constants/data-space-creation.data';
import { MockActivatedRoute } from '../../../../tests/helpers/mock-activated-route.class';
import { TemplateService } from '../../../services/template.service';
import { TemplateModel } from '../../../models/interfaces/template.interface';
import { SlabsCustomComponentService } from '../../../services/slabs-custom-component.service';
import { SlabsCustomComponentModel } from '../../../models/interfaces/slabs-custom-components/slabs-custom-component.interface';
import { TestSuiteModel } from '../../../models/interfaces/slabs-custom-components/test-suite.interface';
import { TestCaseModel } from '../../../models/interfaces/slabs-custom-components/test-case.interface';
import { slabsCustomComponentsMock } from '../../../../mocks/slabs-custom-component-mock';
import { submittedTestsMock } from '../../../../tests/slabs-custom-components/submitted-test-mock';
import { testSuites1Mock } from '../../../../mocks/test-suites-mock';
import { testCasesSuite1Mock } from '../../../../mocks/test-cases-mock';
import { EventsHistoryDialogComponent } from '../events-history-dialog/events-history-dialog.component';
import { routes } from '../../../router/app.routes';
import { MockRouter } from '../../../../tests/helpers/mock-router';
import { DataSpaceService } from '../../../services/data-space.service';
import { DataSpaceModel } from '../../../models/interfaces/data-space/data-space.interface';
import { dataSpacesMock } from '../../../../mocks/data-space-mock';
import { MonitoringService } from '../../../services/monitoring.service';
import { eventsHistoryMock } from '../events-history-dialog/events-history-mock';
import { sessionStartResponseMock } from '../../../../mocks/session-start-response-mock';
import { testExecutionStatus1Mock } from '../../../../mocks/test-execution-status-mock';
import { SubmittedTest } from '../../../models/interfaces/slabs-custom-components/submitted-test.interface';

const dialogRefMock: Partial<MatDialogRef<EventsHistoryDialogComponent>> = {
  close: () => {},
  afterClosed: () => of({}),
};

const dialogMock: Partial<MatDialog> = {
  open: () => dialogRefMock as MatDialogRef<EventsHistoryDialogComponent>,
};

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let templateServiceSpy: jasmine.SpyObj<TemplateService>;
  let customComponentServiceSpy: jasmine.SpyObj<SlabsCustomComponentService>;
  let dataSpaceServiceSpy: jasmine.SpyObj<DataSpaceService>;
  let monitoringServiceSpy: jasmine.SpyObj<MonitoringService>;
  let mockRouter = new MockRouter();

  beforeEach(async () => {
    templateServiceSpy = jasmine.createSpyObj('TemplateService', [
      'getBasicTemplates',
    ]);
    customComponentServiceSpy = jasmine.createSpyObj(
      'SlabsCustomComponentService',
      [
        'loadCustomComponents',
        'startSessionsExecution',
        'getTestExecutionStatus',
        'updateSubmittedTests',
      ],
    );
    dataSpaceServiceSpy = jasmine.createSpyObj('DataSpaceService', [
      'getDataSpaces',
    ]);
    monitoringServiceSpy = jasmine.createSpyObj('MonitoringService', [
      'getEventsHistory',
    ]);

    templateServiceSpy.viewedTemplateSubject$ = new Subject<TemplateModel>();

    customComponentServiceSpy.testSuitesSubject$ = new Subject<
      TestSuiteModel[]
    >();
    customComponentServiceSpy.testCasesSubject$ = new Subject<
      TestCaseModel[][]
    >();
    customComponentServiceSpy.viewedComponentSubject$ = new Subject<
      SlabsCustomComponentModel | undefined
    >();
    customComponentServiceSpy.submittedTestsSubject$ = new Subject<
      SubmittedTest[]
    >();

    customComponentServiceSpy.submittedTests = submittedTestsMock;

    dataSpaceServiceSpy.dataSpaceSubject$ = new Subject<DataSpaceModel>();

    await TestBed.configureTestingModule({
      imports: [
        HeaderComponent,
        HttpClientTestingModule,
        RouterModule.forRoot(routes),
        BrowserAnimationsModule,
      ],
      providers: [
        {
          provide: Router,
          useValue: mockRouter,
        },
        { provide: ActivatedRoute, useClass: MockActivatedRoute },
        { provide: TemplateService, useValue: templateServiceSpy },
        { provide: DataSpaceService, useValue: dataSpaceServiceSpy },
        { provide: MonitoringService, useValue: monitoringServiceSpy },
        {
          provide: SlabsCustomComponentService,
          useValue: customComponentServiceSpy,
        },
        { provide: MatDialog, useValue: dialogMock },
        { provide: MatDialogRef, useValue: dialogRefMock },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    component.eventsHistory = [];

    monitoringServiceSpy.getEventsHistory.and.returnValue(
      of(eventsHistoryMock),
    );
    customComponentServiceSpy.startSessionsExecution.and.returnValue(
      of(sessionStartResponseMock),
    );
    customComponentServiceSpy.getTestExecutionStatus.and.returnValue(
      of(testExecutionStatus1Mock),
    );
    spyOn(dialogMock, 'open' as any);

    fixture.detectChanges();

    spyOn(mockRouter, 'navigateByUrl').and.returnValue(
      new Promise((value) => value) as never,
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call #goBack() on button click', () => {
    spyOn(component, 'goBack');

    component.sectionName = SectionName.TEMPLATE_PREVIEW;
    fixture.detectChanges();

    const goBackBtn = fixture.nativeElement.querySelector(
      '.simplut-goback-btn',
    );
    goBackBtn.click();

    expect(component.goBack).toHaveBeenCalled();
  });

  it('should call #selectViewedTemplate() on button click', () => {
    spyOn(component, 'selectViewedTemplate');

    component.sectionName = SectionName.TEMPLATE_PREVIEW;
    component.previousUrl = 'http://localhost:4200/create-data-space';
    component.viewedTemplate = basicTemplatesMock[0];

    fixture.detectChanges();

    const selectTemplateBtn = fixture.nativeElement.querySelector(
      '.simplut-select-template-btn',
    );
    selectTemplateBtn.click();

    expect(component.selectViewedTemplate).toHaveBeenCalled();
  });

  it('should call #startConformanceTests() on button click', () => {
    spyOn(component, 'startConformanceTests');

    component.sectionName = SectionName.CONFORMANCE_TESTS;
    component.previousUrl =
      'http://localhost:4200/conformance/custom-components';

    fixture.detectChanges();

    const startBtn = fixture.nativeElement.querySelector('.start-button');
    startBtn.dispatchEvent(new Event('click'));

    expect(component.startConformanceTests).toHaveBeenCalled();
  });

  it('should call #toggleSideNav() on button click', () => {
    spyOn(component, 'toggleSideNav');

    component.sideNavOpened = false;

    fixture.detectChanges();

    const toggleNavBtn = fixture.nativeElement.querySelector(
      '.simplut-toggle-nav-btn',
    );
    toggleNavBtn.click();

    expect(component.toggleSideNav).toHaveBeenCalled();
  });

  it('should call #openEventsHistory() on button click', () => {
    spyOn(component, 'openEventsHistory');

    component.sectionName = SectionName.MONITORING;
    component.currentUrl = 'http://localhost:4200/monitoring';

    fixture.detectChanges();

    const eventsBtn = fixture.nativeElement.querySelector('.events-button');
    eventsBtn.click();

    expect(component.openEventsHistory).toHaveBeenCalled();
  });

  it('should set #viewedTemplate value', () => {
    templateServiceSpy.viewedTemplateSubject$.next(basicTemplatesMock[0]);
    expect(component.viewedTemplate).toEqual(basicTemplatesMock[0]);
  });

  it('should set #viewedDataSpace value', () => {
    dataSpaceServiceSpy.dataSpaceSubject$.next(dataSpacesMock[0]);
    expect(component.viewedDataSpace).toEqual(dataSpacesMock[0]);
  });

  it('should set #viewedComponent value', () => {
    customComponentServiceSpy.viewedComponentSubject$.next(
      slabsCustomComponentsMock[0],
    );
    expect(component.viewedComponent).toEqual(slabsCustomComponentsMock[0]);
  });

  it('should set #testSuites value', () => {
    customComponentServiceSpy.testSuitesSubject$.next(testSuites1Mock);
    expect(component.testSuites).toEqual(testSuites1Mock);
  });

  it('should set #testCases value', () => {
    customComponentServiceSpy.testCasesSubject$.next([testCasesSuite1Mock]);
    expect(component.testCases).toEqual([testCasesSuite1Mock]);
  });

  it('should set #submittedTests value', () => {
    customComponentServiceSpy.submittedTestsSubject$.next(submittedTestsMock);

    expect(component.submittedTest).toEqual(
      submittedTestsMock.find(
        (st) => st.customComponent.id === component.viewedComponent?.id,
      ),
    );
  });

  it('should #goBack()', () => {
    component.goBack();
    expect(mockRouter.navigateByUrl).toHaveBeenCalled();
  });

  it('should #toggleSideNav()', () => {
    spyOn(component.sideNavToggleEvent, 'emit');
    component.toggleSideNav(true);
    expect(component.sideNavToggleEvent.emit).toHaveBeenCalledWith(true);
  });

  it('should #selectViewedTemplate()', () => {
    component.selectViewedTemplate();
    expect(mockRouter.navigateByUrl).toHaveBeenCalled();
  });

  it('should set #eventsHistory value', () => {
    expect(component.eventsHistory).toEqual(eventsHistoryMock);
  });

  it('should start Conformance Tests', () => {
    component.submittedTest = submittedTestsMock[0];
    component.startConformanceTests();
    expect(customComponentServiceSpy.startSessionsExecution).toHaveBeenCalled();
  });

  it('should open EventsHistory Dialog', () => {
    component.openEventsHistory();
    expect(dialogMock.open).toHaveBeenCalledWith(EventsHistoryDialogComponent, {
      disableClose: true,
      autoFocus: false,
      data: component.eventsHistory,
    });
  });
});
