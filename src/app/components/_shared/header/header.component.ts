import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog } from '@angular/material/dialog';
import {
  ActivatedRoute,
  NavigationEnd,
  Router,
  RouterLink,
} from '@angular/router';
import { Subscription, filter, map, switchMap } from 'rxjs';

import { SectionName } from '../../../models/enums/navigation/section-name.enum';
import { TemplateModel } from '../../../models/interfaces/template.interface';
import { TemplateService } from '../../../services/template.service';
import { DataSpaceService } from '../../../services/data-space.service';
import { SlabsCustomComponentService } from '../../../services/slabs-custom-component.service';
import { SlabsCustomComponentModel } from '../../../models/interfaces/slabs-custom-components/slabs-custom-component.interface';
import { TestSuiteModel } from '../../../models/interfaces/slabs-custom-components/test-suite.interface';
import { SessionStartRequest } from '../../../models/interfaces/slabs-custom-components/session-start-request.interface';
import { SessionStartResponse } from '../../../models/interfaces/slabs-custom-components/session-start-response.interface';
import { ITBTestsExecutionComponent } from '../../conformance-tests/itb-tests-execution/itb-tests-execution.component';
import { TestCaseModel } from '../../../models/interfaces/slabs-custom-components/test-case.interface';
import { EventsHistoryDialogComponent } from '../events-history-dialog/events-history-dialog.component';
import { MonitoringService } from '../../../services/monitoring.service';
import { DataSpaceModel } from '../../../models/interfaces/data-space/data-space.interface';
import { SubmittedTest } from '../../../models/interfaces/slabs-custom-components/submitted-test.interface';
import { EventsHistoryData } from '../../../models/interfaces/events-history-data.interface';
import { ITBTestsExecutionData } from '../../../models/interfaces/slabs-custom-components/itb-tests-execution-data.interface';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [
    CommonModule,
    RouterLink,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
  ],
  templateUrl: './header.component.html',
  styleUrl: './header.component.scss',
})
export class HeaderComponent implements OnInit, OnDestroy {
  @Input() sectionName = '';
  @Input() sideNavOpened = true;
  @Output() sideNavToggleEvent = new EventEmitter<boolean>();

  sections = SectionName;
  noSideNavSections: string[] = [
    SectionName.CREATE_DATA_SPACE,
    SectionName.TEMPLATE_PREVIEW,
    SectionName.DATA_SPACE_VIEW,
    SectionName.CONFORMANCE_TESTS,
    SectionName.DATA_SPACE_CONSUMPTION,
  ];

  previousUrl = '';
  currentUrl = '';
  viewedTemplate!: TemplateModel;
  viewedDataSpace!: DataSpaceModel;

  viewedComponent: SlabsCustomComponentModel | undefined;
  testSuites: TestSuiteModel[] | undefined;
  testCases: TestCaseModel[][] | undefined;
  submittedTest: SubmittedTest | undefined;
  sessionStartResponse!: SessionStartResponse;

  eventsHistory: EventsHistoryData[] = [];

  routerEventsSub1 = new Subscription();
  routerEventsSub2 = new Subscription();
  viewedTemplateSub = new Subscription();
  viewedDataSpaceSub = new Subscription();
  testSuitesSub = new Subscription();
  testCasesSub = new Subscription();
  viewedComponentSub = new Subscription();
  startSessionsExecutionSub = new Subscription();
  getTestExecutionStatusSub = new Subscription();
  submittedTestsSub = new Subscription();
  eventsHistorySub = new Subscription();

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly templateService: TemplateService,
    private readonly dataSpaceService: DataSpaceService,
    private readonly slabsCustomComponentService: SlabsCustomComponentService,
    private readonly monitoringService: MonitoringService,
    private readonly _itbTestsDialog: MatDialog,
    private readonly _eventsHistoryDialog: MatDialog,
  ) {
    this.slabsCustomComponentService.loadCustomComponents();
  }

  ngOnInit(): void {
    this.routerEventsSub1 = this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        map(() => this.activatedRoute),
        map((route) => {
          if (route.firstChild?.component) return route.firstChild;
          if (route.firstChild?.firstChild?.component)
            return route.firstChild?.firstChild;
          return route.firstChild?.firstChild?.firstChild;
        }),
        switchMap((route) => (route ? route['data'] : '')),
        map((data: any) => data['sectionName']),
      )
      .subscribe((sectionName) => {
        this.sectionName = sectionName;
        if (this.noSideNavSections.includes(sectionName))
          this.toggleSideNav(false);
        else this.toggleSideNav(true);
      });

    this.routerEventsSub2 = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.previousUrl = this.currentUrl;
        this.currentUrl = event.url;
      }
    });

    this.viewedTemplateSub = this.templateService.viewedTemplateSubject$
      .asObservable()
      .subscribe((template) => (this.viewedTemplate = template));

    this.viewedComponentSub =
      this.slabsCustomComponentService.viewedComponentSubject$
        .asObservable()
        .subscribe(
          (viewedComponent) => (this.viewedComponent = viewedComponent),
        );

    this.testSuitesSub = this.slabsCustomComponentService.testSuitesSubject$
      .asObservable()
      .subscribe((testSuites) => (this.testSuites = testSuites));

    this.testCasesSub = this.slabsCustomComponentService.testCasesSubject$
      .asObservable()
      .subscribe((testCases) => (this.testCases = testCases));

    this.submittedTestsSub =
      this.slabsCustomComponentService.submittedTestsSubject$
        .asObservable()
        .subscribe((submittedTests) => {
          this.submittedTest = submittedTests.find(
            (st) => st.customComponent.id === this.viewedComponent?.id,
          );
        });

    this.eventsHistorySub = this.monitoringService
      .getEventsHistory()
      .subscribe((eventsHistory) => (this.eventsHistory = eventsHistory));

    this.viewedDataSpaceSub = this.dataSpaceService.dataSpaceSubject$
      .asObservable()
      .subscribe((dataSpace) => (this.viewedDataSpace = dataSpace));
  }

  ngOnDestroy(): void {
    this.routerEventsSub1.unsubscribe();
    this.routerEventsSub2.unsubscribe();
    this.viewedTemplateSub.unsubscribe();
    this.viewedDataSpaceSub.unsubscribe();
    this.testSuitesSub.unsubscribe();
    this.testCasesSub.unsubscribe();
    this.viewedComponentSub.unsubscribe();
    this.startSessionsExecutionSub.unsubscribe();
    this.getTestExecutionStatusSub.unsubscribe();
    this.submittedTestsSub.unsubscribe();
    this.eventsHistorySub.unsubscribe();
  }

  canRunTests(): boolean {
    const submittedTest = this.slabsCustomComponentService.submittedTests.find(
      (st) => st.customComponent.id === this.viewedComponent?.id,
    );

    const anySelected: boolean =
      submittedTest?.specificationCheckbox.children?.some((ts) =>
        ts.children?.some((tc) => tc.selected),
      )!;

    return (
      anySelected &&
      !this.slabsCustomComponentService.isTestRunning(
        submittedTest?.testExecutionStatus!,
      )
    );
  }

  goBack(): Promise<boolean> {
    this.submittedTest = this.slabsCustomComponentService.submittedTests.find(
      (st) => st.customComponent.id === this.viewedComponent?.id,
    );

    if (this.currentUrl.includes('/monitoring/infrastructure/data-space/'))
      return this.router.navigateByUrl('/monitoring/infrastructure');

    if (this.previousUrl.includes('conformance/conformance-tests'))
      return this.router.navigateByUrl(this.previousUrl, {
        state: { submittedTest: this.submittedTest },
      });

    return this.currentUrl.includes('conformance/conformance-tests')
      ? this.router.navigateByUrl('/conformance')
      : this.router.navigateByUrl(this.previousUrl);
  }

  toggleSideNav(isOpened: boolean) {
    this.sideNavToggleEvent.emit(isOpened);
  }

  selectViewedTemplate() {
    this.router
      .navigateByUrl('/data-spaces/create-data-space', {
        state: { template: this.viewedTemplate },
      })
      .then(() =>
        this.dataSpaceService.newDataSpaceSubject$.next(
          this.viewedTemplate.dataSpace,
        ),
      );
  }

  startConformanceTests() {
    const { specificationCheckbox } = this.submittedTest!;

    const sessionStartRequest: SessionStartRequest = {
      customComponentId: this.viewedComponent?.id!,
      forceSequentialExecution: true,

      testSuite: specificationCheckbox?.children
        ?.filter((ts) => ts.selected || ts.children?.some((tc) => tc.selected))
        .map((ts) => ts.id.toString())!,

      testCase: specificationCheckbox?.children?.reduce(
        (testCases, testSuite) => {
          testSuite.children?.forEach((tc) =>
            tc.selected ? testCases.push(tc.id.toString()) : null,
          );
          return testCases;
        },
        [] as string[],
      )!,
    };

    this.startSessionsExecutionSub = this.slabsCustomComponentService
      .startSessionsExecution(sessionStartRequest)
      .subscribe({
        next: (sessionStartResponse) => {
          this.sessionStartResponse = sessionStartResponse;
          this.submittedTest!.sessionStartResponse = sessionStartResponse;

          if (sessionStartResponse.createdSessions?.length > 0)
            this.openITBTestsExecution();
        },
        error: () => {},
      });
  }

  openITBTestsExecution() {
    this.getTestExecutionStatusSub = this.slabsCustomComponentService
      .getTestExecutionStatus(
        this.viewedComponent?.specificationId!,
        this.viewedComponent?.id!,
      )
      .subscribe((testExecutionStatus) => {
        this.submittedTest!.testExecutionStatus = testExecutionStatus;

        this.slabsCustomComponentService.updateSubmittedTests(
          this.submittedTest!,
        );

        const data: ITBTestsExecutionData = {
          testExecutionStatus,
          testSuites: this.testSuites!,
          testCases: this.testCases!,
          sessionStartResponse: this.sessionStartResponse,
          customComponent: this.viewedComponent!,
        };

        this._itbTestsDialog.open(ITBTestsExecutionComponent, {
          disableClose: true,
          hasBackdrop: false,
          autoFocus: false,
          panelClass: ['fullscreen-dialog'],
          data,
        });
      });
  }

  selectedTestsNumber(): number {
    const submittedTest = this.slabsCustomComponentService.submittedTests.find(
      (st) => st.customComponent.id === this.viewedComponent?.id,
    );

    return this.canRunTests()
      ? submittedTest?.specificationCheckbox?.children?.reduce(
          (sum, tsc) =>
            sum + tsc.children?.filter((tcc) => tcc.selected).length!,
          0,
        )!
      : 0;
  }

  openEventsHistory() {
    this._eventsHistoryDialog.open(EventsHistoryDialogComponent, {
      disableClose: true,
      autoFocus: false,
      data: this.eventsHistory,
    });
  }
}
