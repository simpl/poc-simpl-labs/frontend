import { dataSpacesMock } from '../../../../mocks/data-space-mock';
import { slabsCustomComponentsMock } from '../../../../mocks/slabs-custom-component-mock';
import nodeTypeMap from '../../../helpers/node-type-map';
import { titlecaseNodeNameFcn } from '../../../helpers/titlecase-node-name.function';
import { HistoryEventStatusIcon } from '../../../models/enums/history-event-status-icon.enum';
import { EventsHistoryData } from '../../../models/interfaces/events-history-data.interface';

export const eventsHistoryMock: EventsHistoryData[] = [
  {
    date: '2024-09-17T12:17:11.135',
    event: { text: 'Component 1', status: HistoryEventStatusIcon.WARNING },
    path: `${dataSpacesMock[0].name} / ${nodeTypeMap.get(dataSpacesMock[0].nodeList[0].name)} / ${dataSpacesMock[0].nodeList[0].categoryList[1].type} / ${titlecaseNodeNameFcn(slabsCustomComponentsMock[0].name)}`,
    details: 'CPU usage is 100%',
  },
  {
    date: '2024-07-16T09:08:12.125',
    event: { text: 'Component 2', status: HistoryEventStatusIcon.CONFIRMED },
    path: `${dataSpacesMock[1].name} / ${nodeTypeMap.get(dataSpacesMock[1].nodeList[1].name)} / ${dataSpacesMock[1].nodeList[0].categoryList[2].type} / ${titlecaseNodeNameFcn(slabsCustomComponentsMock[1].name)}`,
    details: 'CPU usage is 100%',
  },
];
