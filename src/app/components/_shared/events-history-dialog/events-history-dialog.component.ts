import { CommonModule } from '@angular/common';
import { Component, Inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';

import { EventsHistoryData } from '../../../models/interfaces/events-history-data.interface';

@Component({
  selector: 'app-events-history-dialog',
  standalone: true,
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    MatTableModule,
  ],
  templateUrl: './events-history-dialog.component.html',
  styleUrl: './events-history-dialog.component.scss',
})
export class EventsHistoryDialogComponent {
  tableColumns = ['date', 'event', 'path', 'details'];
  tableDataSource!: MatTableDataSource<EventsHistoryData>;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: EventsHistoryData[],
    public dialogRef: MatDialogRef<EventsHistoryDialogComponent>,
  ) {
    this.tableDataSource = new MatTableDataSource<EventsHistoryData>(this.data);
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
