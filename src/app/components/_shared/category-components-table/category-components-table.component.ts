import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { CategoryModel } from '../../../models/interfaces/data-space/category.interface';
import { ComponentModel } from '../../../models/interfaces/data-space/component.interface';
import { ConfigureComponentDialogComponent } from '../../wizard/configure-component-dialog/configure-component-dialog.component';
import { CategoryType } from '../../../models/enums/data-space/category-type.enum';
import { ConsoleDialogComponent } from '../console-dialog/console-dialog.component';
import { TitlecaseNodeName } from '../../../pipes/titlecase-node-name.pipe';
import { ChangeComponentDialogComponent } from '../../wizard/change-component-dialog/change-component-dialog.component';
import { SlabsCustomComponentService } from '../../../services/slabs-custom-component.service';
import { AppService } from '../../../services/app.service';

@Component({
  selector: 'app-category-components-table',
  standalone: true,
  imports: [
    CommonModule,
    MatTableModule,
    MatSortModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    TitlecaseNodeName,
  ],
  templateUrl: './category-components-table.component.html',
  styleUrl: './category-components-table.component.scss',
})
export class CategoryComponentsTableComponent
  implements OnChanges, AfterViewInit
{
  @Input() isTemplatePreview = false;
  @Input() isDataSpaceView = false;
  @Input() category!: CategoryModel;

  @Output() componentConfigurationEvent = new EventEmitter<CategoryModel>();
  @Output() componentInfoShowEvent = new EventEmitter<{
    categoryType: CategoryType;
    comp: ComponentModel;
  }>();

  @ViewChild(MatSort) sort!: MatSort;

  simplUrl = '';

  displayedColumns: string[] = ['type', 'author', 'moreInfo', 'optionsMenu'];

  dataSource: MatTableDataSource<ComponentModel> =
    new MatTableDataSource<ComponentModel>();

  constructor(
    public dialog: MatDialog,
    private readonly slabsCustomComponentService: SlabsCustomComponentService,
    private readonly appService: AppService,
  ) {
    this.simplUrl = this.appService.appUrls?.simplUrl;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('category')) {
      const oldCategory = changes['category'].previousValue as CategoryModel;
      const newCategory = changes['category'].currentValue as CategoryModel;

      if (newCategory !== oldCategory)
        this.dataSource = new MatTableDataSource(newCategory.componentList);
    }
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  openConfigureComponentDialog(comp: ComponentModel) {
    const data = { ...comp };

    const configureComponentDialogRef: MatDialogRef<ConfigureComponentDialogComponent> =
      this.dialog.open(ConfigureComponentDialogComponent, {
        data,
        panelClass: ['configure-component-dialog'],
        disableClose: true,
      });

    configureComponentDialogRef
      .afterClosed()
      .subscribe((comp: ComponentModel) => {
        if (comp) {
          comp.param1 = comp.param1.toString();
          comp.param2 = comp.param2.toString();
          comp.param3 = comp.param3.toString();
          comp.param4 = comp.param4.toString();
          comp.param5 = comp.param5.toString();
          comp.param6 = comp.param6.toString();

          const idx = this.category.componentList.findIndex(
            (c) => c.type === comp.type,
          );

          if (idx >= 0) {
            this.category.componentList[idx] = comp;
            this.dataSource = new MatTableDataSource(
              this.category.componentList,
            );
            this.componentConfigurationEvent.emit(this.category);
          }
        }
      });
  }

  openChangeComponentDialog(comp: ComponentModel) {
    const data = { ...comp };

    const changeComponentDialogRef: MatDialogRef<ChangeComponentDialogComponent> =
      this.dialog.open(ChangeComponentDialogComponent, {
        data,
        panelClass: ['change-component-dialog'],
        disableClose: true,
      });

    changeComponentDialogRef.afterClosed().subscribe((comp: ComponentModel) => {
      if (comp) {
        const idx = this.category.componentList.findIndex(
          (c) => c.type === comp.type,
        );
        if (idx >= 0) {
          this.category.componentList[idx] = comp;
          this.dataSource = new MatTableDataSource(this.category.componentList);
          this.componentConfigurationEvent.emit(this.category);
        }
      }
    });
  }

  openComponentConsole() {
    this.dialog.open(ConsoleDialogComponent, {
      disableClose: true,
      panelClass: ['fullscreen-dialog'],
    });
  }

  showComponentInfo(comp: ComponentModel) {
    this.componentInfoShowEvent.emit({
      categoryType: this.category.type,
      comp,
    });
  }

  getCustomComponentName(comp: ComponentModel): string {
    return this.slabsCustomComponentService.customComponents.find(
      (cc) => cc.id === comp.customComponentId,
    )?.name!;
  }
}
