import {
  ComponentFixture,
  ComponentFixtureAutoDetect,
  TestBed,
} from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SimpleChange } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
} from '@angular/material/dialog';
import { of, Subject } from 'rxjs';

import { CategoryComponentsTableComponent } from './category-components-table.component';
import { CategoryModel } from '../../../models/interfaces/data-space/category.interface';
import { CategoryType } from '../../../models/enums/data-space/category-type.enum';
import { ComponentType } from '../../../models/enums/data-space/component-type.enum';
import { ComponentModel } from '../../../models/interfaces/data-space/component.interface';
import { SlabsCustomComponentService } from '../../../services/slabs-custom-component.service';
import { slabsCustomComponentsMock } from '../../../../mocks/slabs-custom-component-mock';
import { ConfigureComponentDialogComponent } from '../../wizard/configure-component-dialog/configure-component-dialog.component';
import { SlabsCustomComponentModel } from '../../../models/interfaces/slabs-custom-components/slabs-custom-component.interface';
import { ChangeComponentDialogComponent } from '../../wizard/change-component-dialog/change-component-dialog.component';

const categoryMock: CategoryModel = {
  id: 1,
  type: CategoryType.LOGGING,
  componentList: [
    {
      id: null,
      type: ComponentType.BE_COMPONENT,
      param1: 'INFO',
      param2: '30000',
      param3: '/app/component/default.log',
      param4: '/app/component/configuration.properties',
      param5: '80',
      param6: 'default-uri',
      customImage: null,
      customComponentId: null,
    },
    {
      id: null,
      type: ComponentType.FE_COMPONENT,
      param1: 'DEBUG',
      param2: '40000',
      param3: '/app/component/default.log',
      param4: '/app/component/configuration.properties',
      param5: '90',
      param6: 'default-uri',
      customImage: 'custom-image',
      customComponentId: 123,
    },
  ],
};

let componentMock: ComponentModel = categoryMock.componentList[0];

const configureDialogRefMock: Partial<
  MatDialogRef<ConfigureComponentDialogComponent>
> = {
  close: () => {},
  afterClosed: () => of(categoryMock.componentList[1]),
};
const configureDialogMock: Partial<MatDialog> = {
  open: () =>
    configureDialogRefMock as MatDialogRef<ConfigureComponentDialogComponent>,
};

const changeDialogRefMock: Partial<
  MatDialogRef<ChangeComponentDialogComponent>
> = {
  close: () => {},
  afterClosed: () => of(categoryMock.componentList[1]),
};
const changeDialogMock: Partial<MatDialog> = {
  open: () =>
    changeDialogRefMock as MatDialogRef<ChangeComponentDialogComponent>,
};

describe('CategoryComponentsTableComponent', () => {
  let component: CategoryComponentsTableComponent;
  let fixture: ComponentFixture<CategoryComponentsTableComponent>;
  let compTableMenuBtn: HTMLElement;
  let configCompBtn: HTMLElement;
  let changeCompBtn: HTMLElement;
  let consoleCompBtn: HTMLElement;
  let uiCompBtn: HTMLElement;
  let compMoreInfoBtn: HTMLElement;
  let host: any;
  let customComponentServiceSpy: jasmine.SpyObj<SlabsCustomComponentService>;

  beforeEach(async () => {
    customComponentServiceSpy = jasmine.createSpyObj(
      'SlabsCustomComponentService',
      ['loadCustomComponents'],
    );

    customComponentServiceSpy.customComponentsSubject$ = new Subject<
      SlabsCustomComponentModel[]
    >();
    customComponentServiceSpy.customComponents = slabsCustomComponentsMock;

    await TestBed.configureTestingModule({
      imports: [
        CategoryComponentsTableComponent,
        HttpClientTestingModule,
        BrowserAnimationsModule,
      ],
      providers: [
        {
          provide: ComponentFixtureAutoDetect,
          useValue: true,
        },
        {
          provide: SlabsCustomComponentService,
          useValue: customComponentServiceSpy,
        },
        {
          provide: MatDialogRef,
          useValue: [configureDialogRefMock, changeDialogRefMock],
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(CategoryComponentsTableComponent);
    component = fixture.componentInstance;
    component.category = categoryMock;
    host = fixture.nativeElement;
    fixture.detectChanges();

    spyOn(component, 'showComponentInfo').and.callThrough();
    spyOn(component.componentInfoShowEvent, 'emit');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fill table #dataSource.data', () => {
    const change = new SimpleChange(undefined, categoryMock, false);
    component.ngOnChanges({ category: change });

    expect(component.dataSource.data).toEqual(
      new MatTableDataSource(categoryMock.componentList).data,
    );
  });

  it('should NOT fill table #dataSource.data', () => {
    const change = new SimpleChange(categoryMock, categoryMock, true);
    component.ngOnChanges({ category: change });

    expect(component.dataSource.data).toEqual(new MatTableDataSource([]).data);
  });

  it('should not have the menu open', () => {
    const menu = host.parent?.nativeElement.querySelector(
      '.mat-mdc-menu-panel',
    );
    expect(menu).toBeUndefined();
  });

  it('open the menu when clicking on the compTableMenuBtn', () => {
    const change = new SimpleChange(undefined, categoryMock, false);
    component.ngOnChanges({ category: change });

    fixture.detectChanges();

    compTableMenuBtn = fixture.debugElement.nativeElement.querySelector(
      '.simplut-comp-table-menu-btn',
    );

    compTableMenuBtn.click();

    const menu = host.parentNode?.querySelector('.mat-mdc-menu-panel');
    expect(menu).toBeTruthy();
  });

  it('should have the wizard-mode menu buttons options', () => {
    const categoryChange = new SimpleChange(undefined, categoryMock, false);
    component.ngOnChanges({ category: categoryChange });

    fixture.detectChanges();

    compTableMenuBtn = fixture.debugElement.nativeElement.querySelector(
      '.simplut-comp-table-menu-btn',
    );

    compTableMenuBtn.click();

    configCompBtn = host.parentNode?.querySelector('.simplut-config-comp-btn');
    changeCompBtn = host.parentNode?.querySelector('.simplut-change-comp-btn');
    consoleCompBtn = host.parentNode?.querySelector(
      '.simplut-console-comp-btn',
    );
    uiCompBtn = host.parentNode?.querySelector('.simplut-ui-comp-btn');

    expect(consoleCompBtn).toBeFalsy();
    expect(uiCompBtn).toBeFalsy();
    expect(configCompBtn).toBeTruthy();
    expect(changeCompBtn).toBeTruthy();
  });

  it('should have the view-mode menu buttons options', () => {
    const categoryChange = new SimpleChange(undefined, categoryMock, false);
    component.ngOnChanges({ category: categoryChange });

    component.isDataSpaceView = true;

    fixture.detectChanges();

    compTableMenuBtn = fixture.debugElement.nativeElement.querySelector(
      '.simplut-comp-table-menu-btn',
    );

    compTableMenuBtn.click();

    configCompBtn = host.parentNode?.querySelector('.simplut-config-comp-btn');
    changeCompBtn = host.parentNode?.querySelector('.simplut-change-comp-btn');
    consoleCompBtn = host.parentNode?.querySelector(
      '.simplut-console-comp-btn',
    );
    uiCompBtn = host.parentNode?.querySelector('.simplut-ui-comp-btn');

    expect(configCompBtn).toBeFalsy();
    expect(changeCompBtn).toBeFalsy();
    expect(consoleCompBtn).toBeTruthy();
    expect(uiCompBtn).toBeTruthy();
    expect(uiCompBtn.attributes.getNamedItem('disabled')?.value).toBe('true');
  });

  it('should open the ConfigureComponent Dialog', () => {
    const change = new SimpleChange(undefined, categoryMock, false);
    component.ngOnChanges({ category: change });
    fixture.detectChanges();
    compTableMenuBtn = fixture.debugElement.nativeElement.querySelector(
      '.simplut-comp-table-menu-btn',
    );
    compTableMenuBtn.click();
    configCompBtn = host.parentNode?.querySelector('.simplut-config-comp-btn');
    configCompBtn.click();
    let configureComponentDialog = host.parentNode?.querySelector(
      '.configure-component-dialog',
    );
    expect(configureComponentDialog).toBeTruthy();
  });

  it('should open the ChangeComponent Dialog', () => {
    const change = new SimpleChange(undefined, categoryMock, false);
    component.ngOnChanges({ category: change });
    fixture.detectChanges();
    compTableMenuBtn = fixture.debugElement.nativeElement.querySelector(
      '.simplut-comp-table-menu-btn',
    );
    compTableMenuBtn.click();
    changeCompBtn = host.parentNode?.querySelector('.simplut-change-comp-btn');
    changeCompBtn.click();
    let changeComponentDialog = host.parentNode?.querySelector(
      '.change-component-dialog',
    );
    expect(changeComponentDialog).toBeTruthy();
  });

  it('should open the Console Dialog', () => {
    const change = new SimpleChange(undefined, categoryMock, false);
    component.ngOnChanges({ category: change });
    component.isDataSpaceView = true;
    fixture.detectChanges();
    compTableMenuBtn = fixture.debugElement.nativeElement.querySelector(
      '.simplut-comp-table-menu-btn',
    );
    compTableMenuBtn.click();
    consoleCompBtn = host.parentNode?.querySelector(
      '.simplut-console-comp-btn',
    );
    consoleCompBtn.click();
    compMoreInfoBtn = fixture.debugElement.nativeElement.querySelector(
      '.simplut-comp-more-info-btn',
    );
    let consoleDialog = host.parentNode?.querySelector('.console-dialog');
    expect(consoleDialog).toBeTruthy();
  });

  it('should call the method #showComponentInfo()', () => {
    const change = new SimpleChange(undefined, categoryMock, false);
    component.ngOnChanges({ category: change });
    fixture.detectChanges();
    compMoreInfoBtn = fixture.debugElement.nativeElement.querySelector(
      '.simplut-comp-more-info-btn',
    );
    compMoreInfoBtn.click();
    fixture.detectChanges();
    expect(component.showComponentInfo).toHaveBeenCalled();
  });

  it('should call the method #componentInfoShowEvent.emit()', () => {
    component.category = categoryMock;
    component.showComponentInfo(componentMock);
    expect(component.componentInfoShowEvent.emit).toHaveBeenCalled();
  });

  it('should call the method slabsCustomComponentService.customComponents.find()', () => {
    spyOn(customComponentServiceSpy.customComponents, 'find');
    component.getCustomComponentName(componentMock);
    expect(customComponentServiceSpy.customComponents.find).toHaveBeenCalled();
  });

  it('should call the method #dialog.open', () => {
    component.dialog = TestBed.inject(MatDialog);
    spyOn(component.dialog, 'open').and.returnValue(
      configureDialogRefMock as MatDialogRef<ConfigureComponentDialogComponent>,
    );
    component.openConfigureComponentDialog(componentMock);
    expect(component.dialog.open).toHaveBeenCalled();
  });

  it('should update a component configuration', () => {
    component.dialog = TestBed.inject(MatDialog);
    const dialogRef = component.dialog.open(ConfigureComponentDialogComponent, {
      data: componentMock,
      panelClass: ['configure-component-dialog'],
      disableClose: true,
    });
    const comp =
      dialogRef.componentRef?.injector.get<ComponentModel>(MAT_DIALOG_DATA)!;
    const idx = component.category.componentList.findIndex(
      (c) => c.type === comp.type,
    );
    comp.param1 = 'WARNING';
    dialogRef.close();
    expect(component.category.componentList[idx].param1).toBe('WARNING');
  });

  it('should change a component', () => {
    component.dialog = TestBed.inject(MatDialog);
    const dialogRef = component.dialog.open(ChangeComponentDialogComponent, {
      data: componentMock,
      panelClass: ['change-component-dialog'],
      disableClose: true,
    });
    const comp =
      dialogRef.componentRef?.injector.get<ComponentModel>(MAT_DIALOG_DATA)!;
    const idx = component.category.componentList.findIndex(
      (c) => c.type === comp.type,
    );
    comp.customImage = 'MY_CUSTOM_IMAGE';
    dialogRef.close();
    expect(component.category.componentList[idx].customImage).toEqual(
      'MY_CUSTOM_IMAGE',
    );
  });
});
