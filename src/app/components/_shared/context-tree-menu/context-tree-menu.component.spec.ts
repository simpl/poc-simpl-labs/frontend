import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SimpleChange } from '@angular/core';
import { FlatTreeControl } from '@angular/cdk/tree';

import { ContextTreeMenuComponent } from './context-tree-menu.component';
import { NodeModel } from '../../../models/interfaces/data-space/node.interface';
import { dataSpacesMock } from '../../../../mocks/data-space-mock';
import {
  SLabsFlatNode,
  SLabsNode,
} from '../../../models/interfaces/tree-node.interface';
import {
  MatTreeFlattener,
  MatTreeFlatDataSource,
} from '@angular/material/tree';
import { TreeMenuService } from '../../../services/tree-menu.service';
import { Subject } from 'rxjs';
import { GraphChartDatum } from '../../../models/interfaces/graph-chart-datum.interface';

const mockNodeList: NodeModel[] = [...dataSpacesMock[1].nodeList];

describe('ContextTreeMenuComponent', () => {
  let component: ContextTreeMenuComponent;
  let fixture: ComponentFixture<ContextTreeMenuComponent>;
  let treeMenuServiceSpy: jasmine.SpyObj<TreeMenuService>;
  let slabsTreeNodeBtn: HTMLElement;
  let host: any;

  beforeEach(async () => {
    treeMenuServiceSpy = jasmine.createSpyObj('TreeMenuService', [
      'renameNode',
      'duplicateNode',
      'deleteNode',
      'updateBreadcrumbs',
    ]);

    treeMenuServiceSpy.graphChartNodeSubject$ = new Subject<GraphChartDatum>();
    treeMenuServiceSpy.treeMenuNodeSubject$ = new Subject<SLabsFlatNode>();

    await TestBed.configureTestingModule({
      imports: [
        ContextTreeMenuComponent,
        HttpClientTestingModule,
        BrowserAnimationsModule,
      ],
      providers: [
        {
          provide: TreeMenuService,
          useValue: treeMenuServiceSpy,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ContextTreeMenuComponent);
    component = fixture.componentInstance;
    component.nodeList = [...mockNodeList];
    host = fixture.nativeElement;
    component.nodeList = [...mockNodeList];
    fixture.detectChanges();

    component._transformer = (node: SLabsNode, level: number) => {
      return {
        expandable: !!node.list && node.list.length > 0,
        name: node.name,
        level: level,
        type: node.type,
        id: node.id,
      };
    };

    component.treeControl = new FlatTreeControl<SLabsFlatNode>(
      (node) => node.level,
      (node) => node.expandable,
    );

    component.treeFlattener = new MatTreeFlattener(
      component._transformer,
      (node) => node.level,
      (node) => node.expandable,
      (node) => node.list,
    );

    component.treeFlatDataSource = new MatTreeFlatDataSource(
      component.treeControl,
      component.treeFlattener,
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should NOT fill tree menu #treeFlatDataSource.data', () => {
    const change = new SimpleChange(undefined, undefined, false);
    component.ngOnChanges({ nodeList: change });

    expect(component.treeFlatDataSource.data.length).toBe(0);
  });

  it('should fill tree menu #treeFlatDataSource.data', () => {
    const change = new SimpleChange([], mockNodeList, false);
    component.ngOnChanges({ nodeList: change });

    expect(component.treeFlatDataSource.data.length).toBeGreaterThan(0);
  });

  it('should call #updateFromChart()', () => {
    spyOn(component, 'updateFromChart');

    component.mapDataSpace2TreeData(mockNodeList);

    const graphChartNode: GraphChartDatum = {
      name: 'xyz',
      x: 1,
      y: 2,
      id: '1',
    };

    treeMenuServiceSpy.graphChartNodeSubject$.next(graphChartNode);

    expect(component.updateFromChart).toHaveBeenCalledWith(graphChartNode);
  });

  it('should call #onTreeMenuNodeClick() on button click', () => {
    spyOn(component, 'onTreeMenuNodeClick');

    component.nodeList = [...mockNodeList];
    fixture.detectChanges();

    component.mapDataSpace2TreeData(component.nodeList);

    slabsTreeNodeBtn = fixture.nativeElement.querySelector(
      '.simplut-tree-node-btn',
    );
    slabsTreeNodeBtn.click();

    expect(component.onTreeMenuNodeClick).toHaveBeenCalled();
  });

  it('should not have the menu open', () => {
    component.nodeList = [...mockNodeList];
    component.mapDataSpace2TreeData(component.nodeList);
    fixture.detectChanges();

    const menu = host.parent?.nativeElement.querySelector(
      '.mat-mdc-menu-panel',
    );
    expect(menu).toBeUndefined();
  });

  it('open the menu when clicking on the #nodeMenuBtn', () => {
    component.nodeList = [...mockNodeList];
    component.mapDataSpace2TreeData(component.nodeList);

    fixture.detectChanges();

    const nodeMenuBtn = fixture.nativeElement.querySelector(
      '.simplut-node-menu-btn',
    );

    nodeMenuBtn.click();

    const menu = host.parentNode?.querySelector('.mat-mdc-menu-panel');
    expect(menu).toBeTruthy();
  });

  it('should not display the three-dots menu on the tree menu item', () => {
    component.isView = true;
    component.nodeList = [...mockNodeList];
    component.mapDataSpace2TreeData(component.nodeList);
    fixture.detectChanges();

    const nodeMenuBtn = fixture.nativeElement.querySelector(
      '.simplut-node-menu-btn',
    );

    expect(nodeMenuBtn).toBeNull();
  });

  it('should call #treeMenuService.updateBreadcrumbs()', () => {
    component.mapDataSpace2TreeData(mockNodeList);
    const node: SLabsFlatNode = {
      name: 'abc',
      type: 'ttt',
      level: 1,
      expandable: true,
      expanded: false,
      id: 2,
    };
    component.onTreeMenuNodeClick(node);

    expect(treeMenuServiceSpy.updateBreadcrumbs).toHaveBeenCalledWith(
      node,
      component.isMonitoring,
    );
  });

  it('should call #treeControl.collapseAll()', () => {
    spyOn(component.treeControl, 'collapseAll');
    const graphChartNode: GraphChartDatum = {
      name: mockNodeList[1].name,
      x: 123,
      y: 456,
      level: 0,
      collapsed: true,
      id: '1',
    };
    component.updateFromChart(graphChartNode);

    expect(component.treeControl.collapseAll).toHaveBeenCalled();
  });

  it('should NOT call #treeControl.collapseAll()', () => {
    component.mapDataSpace2TreeData(mockNodeList);
    spyOn(component.treeControl, 'collapseAll');
    const graphChartNode: GraphChartDatum = {
      name: mockNodeList[1].name,
      x: 123,
      y: 456,
      level: 1,
      id: '2',
    };
    component.updateFromChart(graphChartNode);

    expect(component.treeControl.collapseAll).not.toHaveBeenCalled();
  });
});
