import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, map } from 'rxjs';
import { MatChipsModule } from '@angular/material/chips';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatDrawer, MatSidenavModule } from '@angular/material/sidenav';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';

import { DataSpaceModel } from '../../models/interfaces/data-space/data-space.interface';
import { ContextTreeMenuComponent } from '../_shared/context-tree-menu/context-tree-menu.component';
import { DataSpaceGraphChartComponent } from '../data-space-graph-chart/data-space-graph-chart.component';
import { DataSpaceStatus } from '../../models/enums/data-space/data-space-status.enum';
import { SLabsFlatNode } from '../../models/interfaces/tree-node.interface';
import { NodeModel } from '../../models/interfaces/data-space/node.interface';
import { TreeMenuService } from '../../services/tree-menu.service';
import { CategoryComponentsTableComponent } from '../_shared/category-components-table/category-components-table.component';
import { CategoryModel } from '../../models/interfaces/data-space/category.interface';
import { GraphChartDatum } from '../../models/interfaces/graph-chart-datum.interface';
import { chartNode2SlabsFlatNode } from '../../helpers/chart-node-2-slabs-flat-node.map';
import { BreadcrumbComponent } from '../_shared/breadcrumb/breadcrumb.component';
import { CategoryType } from '../../models/enums/data-space/category-type.enum';
import { ComponentModel } from '../../models/interfaces/data-space/component.interface';
import { ComponentMoreInfoComponent } from '../wizard/component-more-info/component-more-info.component';
import { DataSpaceService } from '../../services/data-space.service';
import { DialogService } from '../../services/dialog.service';
import { KubeboxConsoleComponent } from '../_shared/kubebox-console/kubebox-console.component';
import { DataSpaceType } from '../../models/enums/data-space/data-space-type.enum';
import mapDataSpaceStatus2Chip from '../../helpers/data-space-chip-colors';
import { DrawerService } from '../../services/drawer.service';
import { PageContentType } from '../../models/enums/data-space/page-content-type.enum';
import { ComponentInfo } from '../../models/interfaces/data-space/component-info.interface';

@Component({
  selector: 'app-data-space-view',
  standalone: true,
  imports: [
    CommonModule,
    MatChipsModule,
    MatTabsModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatMenuModule,
    MatButtonModule,
    MatTableModule,
    ContextTreeMenuComponent,
    DataSpaceGraphChartComponent,
    CategoryComponentsTableComponent,
    BreadcrumbComponent,
    ComponentMoreInfoComponent,
    KubeboxConsoleComponent,
  ],
  templateUrl: './data-space-view.component.html',
  styleUrl: './data-space-view.component.scss',
})
export class DataSpaceViewComponent implements OnInit, OnDestroy {
  @Input() dataSpaceName: string = '';

  @ViewChild('componentInfoDrawer', { static: true })
  componentInfoDrawer!: MatDrawer;

  dataSpace!: DataSpaceModel;
  _nodeList!: NodeModel[];
  chartNode!: NodeModel;

  pageContentType = PageContentType.GRAPH_CHART.toString();
  pageContentTypes = PageContentType;
  tableCategory!: CategoryModel;
  componentInfo!: ComponentInfo;

  statuses = DataSpaceStatus;
  dataSpaceTypes = Object.entries(DataSpaceType);

  infoTableDataSource = new MatTableDataSource<{
    property: string;
    value: string | string[];
  }>();
  infoTableColumns: string[] = ['property', 'value'];

  status2ChipColor = mapDataSpaceStatus2Chip;

  activatedRouteDataSub = new Subscription();
  treeMenuNodeSubscription = new Subscription();
  graphChartNodeSubscription = new Subscription();
  deleteDataSpaceSub = new Subscription();

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly treeMenuService: TreeMenuService,
    private readonly dialogService: DialogService,
    private readonly dataSpaceService: DataSpaceService,
    private readonly router: Router,
    private readonly drawerService: DrawerService,
  ) {}

  ngOnInit(): void {
    this.activatedRouteDataSub = this.activatedRoute.data
      .pipe(map((data) => data['dataSpace']))
      .subscribe((dataSpace: DataSpaceModel) => {
        if (dataSpace) {
          this.dataSpace = dataSpace;
          this._nodeList = [...dataSpace.nodeList];
          this.getInfoTableData(dataSpace);
        }
      });

    this.treeMenuNodeSubscription = this.treeMenuService.treeMenuNodeSubject$
      .asObservable()
      .subscribe((treeFlatNode) => this.updatePageContent(treeFlatNode));

    this.graphChartNodeSubscription =
      this.treeMenuService.graphChartNodeSubject$
        .asObservable()
        .subscribe((graphChartDatum: GraphChartDatum) =>
          this.updatePageContent(chartNode2SlabsFlatNode(graphChartDatum)),
        );
  }

  ngOnDestroy(): void {
    this.activatedRouteDataSub.unsubscribe();
    this.treeMenuNodeSubscription.unsubscribe();
    this.graphChartNodeSubscription.unsubscribe();
    this.deleteDataSpaceSub.unsubscribe();
  }

  getInfoTableData(dataSpace: DataSpaceModel = this.dataSpace) {
    const props = ['type', 'tagList'];

    this.infoTableDataSource = new MatTableDataSource(
      this.dataSpaceService.createInfoTableData(dataSpace, props),
    );
  }

  updatePageContent(treeFlatNode: SLabsFlatNode) {
    const pageContentUpdate = this.dataSpaceService.updatePageContent(
      treeFlatNode,
      this.dataSpace,
      this._nodeList,
      this.pageContentType,
      this.tableCategory,
    );

    this.dataSpace = pageContentUpdate.dataSpace;
    this._nodeList = pageContentUpdate.nodeList;
    this.pageContentType = pageContentUpdate.pageContentType;
    this.tableCategory = pageContentUpdate.tableCategory;
  }

  openComponentInfoDrawer(event: {
    categoryType: CategoryType;
    comp: ComponentModel;
  }) {
    this.componentInfo = event.comp as ComponentInfo;
    this.componentInfo.categoryType = event.categoryType;

    this.drawerService.openComponentDrawer(this.componentInfoDrawer);
  }

  closeComponentInfoDrawer() {
    this.drawerService.closeComponentDrawer(this.componentInfoDrawer);
  }

  deleteDataSpace(dataSpace: DataSpaceModel) {
    let dialogRef = this.dialogService.openWarningDialog<number>({
      title: `Delete Data Space`,
      content: `Do you really want to delete the Data Space`,
      boldContent: dataSpace.name,
      confirm: true,
      callerData: dataSpace.id,
    });

    dialogRef.afterClosed().subscribe((data) => {
      if (data?.confirm && data?.callerData >= 0)
        this.deleteDataSpaceSub = this.dataSpaceService
          .deleteDataSpace(data.callerData)
          .subscribe((ds) => {
            if (ds) this.router.navigateByUrl('/data-spaces');
          });
    });
  }

  getDataSpaceType(dsType: string): string {
    dsType = dsType.toString().replace(' ', '_').toUpperCase();
    return this.dataSpaceTypes.find(
      (entry) => entry[0].toUpperCase() === dsType,
    )![1];
  }
}
