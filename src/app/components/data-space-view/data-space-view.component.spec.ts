import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Observable, of, Subject } from 'rxjs';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { DataSpaceViewComponent } from './data-space-view.component';
import { dataSpacesMock } from '../../../mocks/data-space-mock';
import { DataSpaceModel } from '../../models/interfaces/data-space/data-space.interface';
import { MockActivatedRoute } from '../../../tests/helpers/mock-activated-route.class';
import { TreeMenuService } from '../../services/tree-menu.service';
import { SLabsFlatNode } from '../../models/interfaces/tree-node.interface';
import { GraphChartDatum } from '../../models/interfaces/graph-chart-datum.interface';
import {
  graphChartDataMock,
  slabsFlatNodesMock,
} from '../../../tests/data-spaces/graph-chart-datum-mock';
import { CategoryType } from '../../models/enums/data-space/category-type.enum';
import { DrawerService } from '../../services/drawer.service';
import { DialogService } from '../../services/dialog.service';
import { WarningDialogComponent } from '../_shared/warning-dialog/warning-dialog.component';
import { DataSpaceType } from '../../models/enums/data-space/data-space-type.enum';
import { ComponentModel } from '../../models/interfaces/data-space/component.interface';
import { PageContentType } from '../../models/enums/data-space/page-content-type.enum';
import { WarningDialogData } from '../../models/interfaces/warning-dialog-data.interface';

const mockDataSpace: DataSpaceModel = { ...dataSpacesMock[0] };
const mockComponent: ComponentModel = {
  ...dataSpacesMock[0].nodeList[0].categoryList[0].componentList[0],
};

class MockWarningDialogRef
  implements Partial<MatDialogRef<WarningDialogComponent<any>>>
{
  close(dialogResult?: any): void {
    return;
  }

  afterClosed(): Observable<WarningDialogData<any>> {
    return of({
      title: 'string',
      content: 'string',
      boldContent: 'string',
      confirm: false,
    });
  }
}

const mockWarningDialog: Partial<MatDialog> = {
  open: () => {
    return new MockWarningDialogRef() as MatDialogRef<
      WarningDialogComponent<any>
    >;
  },
};

describe('DataSpaceViewComponent', () => {
  let component: DataSpaceViewComponent;
  let fixture: ComponentFixture<DataSpaceViewComponent>;
  let treeMenuServiceSpy: jasmine.SpyObj<TreeMenuService>;
  let drawerServiceSpy: jasmine.SpyObj<DrawerService>;
  let dialogServiceSpy: jasmine.SpyObj<DialogService>;

  beforeEach(async () => {
    treeMenuServiceSpy = jasmine.createSpyObj('TreeMenuService', [
      'updateBreadcrumbs',
    ]);
    drawerServiceSpy = jasmine.createSpyObj('DrawerService', [
      'openComponentDrawer',
      'closeComponentDrawer',
    ]);
    dialogServiceSpy = jasmine.createSpyObj('DialogService', [
      'openWarningDialog',
    ]);

    treeMenuServiceSpy.treeMenuNodeSubject$ = new Subject<SLabsFlatNode>();
    treeMenuServiceSpy.graphChartNodeSubject$ = new Subject<GraphChartDatum>();

    await TestBed.configureTestingModule({
      imports: [
        DataSpaceViewComponent,
        HttpClientTestingModule,
        BrowserAnimationsModule,
      ],
      providers: [
        { provide: ActivatedRoute, useClass: MockActivatedRoute },
        { provide: TreeMenuService, useValue: treeMenuServiceSpy },
        { provide: DrawerService, useValue: drawerServiceSpy },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: MatDialog, useValue: mockWarningDialog },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(DataSpaceViewComponent);
    component = fixture.componentInstance;

    dialogServiceSpy.openWarningDialog.and.returnValue(
      new MockWarningDialogRef() as MatDialogRef<
        WarningDialogComponent<any>,
        WarningDialogData<any>
      >,
    );

    component.dataSpace = mockDataSpace;
    component.dataSpaceTypes = Object.entries(DataSpaceType);
    component._nodeList = mockDataSpace.nodeList;

    const history = JSON.parse(JSON.stringify(window.history));
    history.state = {} as any;
    history.state.dataSpace = mockDataSpace;
    const route = new MockActivatedRoute();
    spyOn(route.paramMap, 'pipe').and.returnValue(history.state.dataSpace);
    spyOn(component, 'getDataSpaceType').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set #pageContentType equal to GRAPH_CHART', () => {
    component._nodeList = mockDataSpace.nodeList;
    component.pageContentType = '';
    component.updatePageContent({
      ...slabsFlatNodesMock[0],
      expandable: true,
      level: -1,
      id: null!,
    });
    expect(component.pageContentType).toBe(
      PageContentType.GRAPH_CHART.toString(),
    );
  });

  it('should set #pageContentType equal to GRAPH_CHART when level = 1', () => {
    component.pageContentType = '';
    component.updatePageContent({
      ...slabsFlatNodesMock[0],
      expandable: true,
      level: 1,
      expanded: false,
      id: 2,
    });
    expect(component.pageContentType).toBe(
      PageContentType.GRAPH_CHART.toString(),
    );
  });

  it('should call #updatePageContent() w/ treeNode', () => {
    spyOn(component, 'updatePageContent');
    treeMenuServiceSpy.treeMenuNodeSubject$.next({
      ...slabsFlatNodesMock[0],
      expandable: true,
      level: 1,
      expanded: false,
    });
    expect(component.updatePageContent).toHaveBeenCalled();
  });

  it('should call #updatePageContent() w/ graphNode', () => {
    spyOn(component, 'updatePageContent');
    treeMenuServiceSpy.graphChartNodeSubject$.next(graphChartDataMock[0]);
    expect(component.updatePageContent).toHaveBeenCalled();
  });

  it('should open ComponentInfoDrawer', () => {
    component.openComponentInfoDrawer({
      categoryType: CategoryType.LOGGING,
      comp: mockComponent,
    });
    expect(drawerServiceSpy.openComponentDrawer).toHaveBeenCalled();
  });

  it('should close ComponentInfoDrawer', () => {
    component.closeComponentInfoDrawer();
    expect(drawerServiceSpy.closeComponentDrawer).toHaveBeenCalled();
  });

  it('should open WarningDialog', () => {
    component.deleteDataSpace(mockDataSpace);
    expect(dialogServiceSpy.openWarningDialog).toHaveBeenCalled();
  });

  it('should set #infoTableDataSource value', () => {
    component.getInfoTableData(mockDataSpace);
    expect(component.infoTableDataSource).toBeTruthy();
    expect(component.infoTableDataSource.data).toBeTruthy();
    expect(component.infoTableDataSource.data.length).toBeGreaterThan(0);
  });

  it('should get DataSpace Type', () => {
    spyOn(component.dataSpaceTypes, 'find').and.returnValue([
      'Collaboration',
      DataSpaceType.COLLABORATION,
    ]);
    component.getDataSpaceType(DataSpaceType.COLLABORATION.toString());
    expect(component.dataSpaceTypes.find).toHaveBeenCalled();
  });

  it('should get the DataSpace Type', () => {
    component.dataSpaceTypes = Object.entries(DataSpaceType);
    const dst = component.getDataSpaceType(DataSpaceType.COMPLIANCE_TESTING);
    let dsType = DataSpaceType.COMPLIANCE_TESTING.toString()
      .replace(' ', '_')
      .toUpperCase();

    expect(dst).toBe(
      component.dataSpaceTypes.find(
        (entry) => entry[0].toUpperCase() === dsType,
      )![1],
    );
  });
});
