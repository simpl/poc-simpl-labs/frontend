import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-test-result-bar',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './test-result-bar.component.html',
})
export class TestResultBarComponent {
  @Input() resultNotExecuted = 0;
  @Input() resultError = 0;
  @Input() resultSuccess = 0;
}
