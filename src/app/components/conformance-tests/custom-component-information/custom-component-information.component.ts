import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatChipsModule } from '@angular/material/chips';

import { SlabsCustomComponentModel } from '../../../models/interfaces/slabs-custom-components/slabs-custom-component.interface';
import { TitlecaseNodeName } from '../../../pipes/titlecase-node-name.pipe';
import mapStatus2ChipColor from '../../../helpers/custom-component-chip-colors';

@Component({
  selector: 'app-custom-component-information',
  standalone: true,
  imports: [CommonModule, MatTableModule, MatChipsModule, TitlecaseNodeName],
  templateUrl: './custom-component-information.component.html',
  styleUrl: './custom-component-information.component.scss',
})
export class CustomComponentInformationComponent implements OnChanges {
  @Input() customComponent!: SlabsCustomComponentModel;

  status2ChipColor = mapStatus2ChipColor;

  tableDataSource = new MatTableDataSource<{
    property: string;
    value: string | string[];
  }>();
  tableColumns: string[] = ['property', 'value'];

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('customComponent')) {
      const customComponent = changes['customComponent']
        .currentValue as SlabsCustomComponentModel;
      if (customComponent) this.mapComponent2TableData(customComponent);
    }
  }

  mapComponent2TableData(
    customComponent: SlabsCustomComponentModel = this.customComponent,
  ) {
    const props = ['category', 'version', 'status'];

    this.tableDataSource.data = Object.entries(customComponent)
      .filter((entry) => props.includes(entry[0]))
      .map((entry) => ({
        property: entry[0],
        value: entry[1],
      }));
  }
}
