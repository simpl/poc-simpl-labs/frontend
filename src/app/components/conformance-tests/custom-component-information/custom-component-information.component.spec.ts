import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomComponentInformationComponent } from './custom-component-information.component';
import { slabsCustomComponentsMock } from '../../../../mocks/slabs-custom-component-mock';

describe('CustomComponentInformationComponent', () => {
  let component: CustomComponentInformationComponent;
  let fixture: ComponentFixture<CustomComponentInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CustomComponentInformationComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(CustomComponentInformationComponent);
    component = fixture.componentInstance;
    component.customComponent = slabsCustomComponentsMock[0];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();

    // injectAsync([TestComponentBuilder], (tcb: TestComponentBuilder) => {
    //   return tcb.createAsync(TestCmpWrapper).then(
    //     (rootCmp: {
    //       debugElement: {
    //         children: { componentInstance: SlabsCustomComponentModel }[];
    //       };
    //     }) => {
    //       let cmpInstance: SlabsCustomComponentModel = <
    //         SlabsCustomComponentModel
    //       >rootCmp.debugElement.children[0].componentInstance;
    //       // expect(cmpInstance.openProductPage()).toBe(/* whatever */)
    //     },
    //   );
    // });
  });
});
