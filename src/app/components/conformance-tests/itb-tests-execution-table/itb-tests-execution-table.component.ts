import {
  AfterViewInit,
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import {
  trigger,
  state,
  style,
  transition,
  animate,
} from '@angular/animations';
import { Observable, Subscription } from 'rxjs';
import { CommonModule } from '@angular/common';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialog } from '@angular/material/dialog';
import { saveAs } from 'file-saver-es';

import { TestCaseModel } from '../../../models/interfaces/slabs-custom-components/test-case.interface';
import mapStatus2ChipColor from '../../../helpers/custom-component-chip-colors';
import { SlabsCustomComponentService } from '../../../services/slabs-custom-component.service';
import { TestCaseLogsDialogComponent } from '../test-case-logs-dialog/test-case-logs-dialog.component';
import { LogsDialogData } from '../../../models/interfaces/logs-dialog-data.interface';

@Component({
  selector: 'app-itb-tests-execution-table',
  standalone: true,
  imports: [
    CommonModule,
    MatTableModule,
    MatSort,
    MatSortModule,
    MatChipsModule,
    MatIconModule,
    MatButtonModule,
  ],
  templateUrl: './itb-tests-execution-table.component.html',
  styleUrl: './itb-tests-execution-table.component.scss',
  animations: [
    trigger('detailExpand', [
      state('collapsed,void', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'),
      ),
    ]),
  ],
})
export class ItbTestsExecutionTableComponent
  implements OnChanges, OnInit, AfterViewInit, OnDestroy
{
  @Input() testCasesList$!: Observable<TestCaseModel[]>;
  @Input() testRunning = true;
  @Input() isHistoryItem = false;
  @Input() duration = '00:00:00';

  @ViewChild(MatSort) sort!: MatSort;

  testCaseColumns: string[] = [];
  testCaseColumnsWithExpand: string[] = [];
  expandedElement!: TestCaseModel | null;
  previousStatus = '';

  testCaseDataSource!: MatTableDataSource<TestCaseModel>;

  status2ChipColor = mapStatus2ChipColor;

  testCasesListSub = new Subscription();
  testSessionLogsSub = new Subscription();
  testSessionReportSub = new Subscription();

  constructor(
    private readonly slabsCustomComponentService: SlabsCustomComponentService,
    public dialog: MatDialog,
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('testCasesList$'))
      this.testCasesListSub = (
        changes['testCasesList$'].currentValue as Observable<TestCaseModel[]>
      ).subscribe((testCases) => {
        testCases.sort((a, b) => a.name.localeCompare(b.name));

        const currentStatus = testCases
          .map((tc) => tc.status.toString())
          .join();

        if (currentStatus !== this.previousStatus) {
          this.previousStatus = currentStatus;
          this.testCaseDataSource = new MatTableDataSource(testCases);
        }
      });
  }

  ngOnInit(): void {
    this.testCaseColumns = [
      'name',
      'testSuiteDescription',
      'testSuiteLastUpdate',
      'description',
      'startDate',
      'endDate',
      'status',
      'logs',
      'report',
    ];
    this.testCaseColumnsWithExpand = ['expand', ...this.testCaseColumns];
  }

  ngAfterViewInit() {
    this.testCaseDataSource.sort = this.sort;
  }

  ngOnDestroy(): void {
    this.testCasesListSub.unsubscribe();
    this.testSessionLogsSub.unsubscribe();
    this.testSessionReportSub.unsubscribe();
  }

  openStepsLogs(testCase: TestCaseModel, event?: MouseEvent | KeyboardEvent) {
    event?.stopPropagation();

    this.testSessionLogsSub = this.slabsCustomComponentService
      .getTestSessionLogs(testCase.session)
      .subscribe((res) => {
        const data: LogsDialogData = {
          logs: res.logs,
          testCase,
          duration: this.duration,
        };

        if (data.logs)
          this.dialog.open(TestCaseLogsDialogComponent, {
            disableClose: true,
            panelClass: ['fullscreen-dialog'],
            data,
          });
      });
  }

  downloadReport(testCase: TestCaseModel, event: MouseEvent) {
    event?.stopPropagation();

    this.testSessionReportSub = this.slabsCustomComponentService
      .getTestSessionReport(testCase.session)
      .subscribe((res) => {
        const blob = new Blob([res.report], { type: 'text' });
        saveAs(blob, `report_${testCase.name}.xml`);
      });
  }
}
