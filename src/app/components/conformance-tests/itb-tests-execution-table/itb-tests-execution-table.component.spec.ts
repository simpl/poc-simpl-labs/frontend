import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableDataSource } from '@angular/material/table';
import { of } from 'rxjs';
import { SimpleChange, SimpleChanges } from '@angular/core';

import { ItbTestsExecutionTableComponent } from './itb-tests-execution-table.component';
import { TestCaseModel } from '../../../models/interfaces/slabs-custom-components/test-case.interface';
import { SlabsCustomComponentService } from '../../../services/slabs-custom-component.service';
import { testCasesSuite1Mock } from '../../../../mocks/test-cases-mock';

const mockTestCaseDataSource = new MatTableDataSource<TestCaseModel>();
mockTestCaseDataSource.data = testCasesSuite1Mock;

describe('ItbTestsExecutionTableComponent', () => {
  let component: ItbTestsExecutionTableComponent;
  let fixture: ComponentFixture<ItbTestsExecutionTableComponent>;
  let customComponentServiceSpy: jasmine.SpyObj<SlabsCustomComponentService>;

  beforeEach(async () => {
    customComponentServiceSpy = jasmine.createSpyObj(
      'SlabsCustomComponentService',
      ['getTestSessionLogs', 'getTestSessionReport'],
    );

    await TestBed.configureTestingModule({
      imports: [
        ItbTestsExecutionTableComponent,
        HttpClientTestingModule,
        BrowserAnimationsModule,
      ],
      providers: [
        {
          provide: SlabsCustomComponentService,
          useValue: customComponentServiceSpy,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ItbTestsExecutionTableComponent);
    component = fixture.componentInstance;
    component.testCaseDataSource = mockTestCaseDataSource;
    component.testCasesList$ = of(testCasesSuite1Mock);

    spyOn(component, 'testCasesList$' as any).and.returnValue(
      of(testCasesSuite1Mock),
    );

    fixture.detectChanges();

    customComponentServiceSpy.getTestSessionLogs.and.returnValue(
      of({ logs: 'logs-mock' }),
    );
    customComponentServiceSpy.getTestSessionReport.and.returnValue(
      of({ report: 'report-mock' }),
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should #openStepsLogs()', () => {
    component.openStepsLogs(component.testCaseDataSource.data[0]);
    expect(customComponentServiceSpy.getTestSessionLogs).toHaveBeenCalled();
  });

  it('should #downloadReport()', () => {
    component.downloadReport(
      component.testCaseDataSource.data[1],
      new MouseEvent('click'),
    );
    expect(customComponentServiceSpy.getTestSessionReport).toHaveBeenCalled();
  });

  it('should set #testCaseDataSource value', () => {
    const testCases = [...testCasesSuite1Mock];

    const change = new SimpleChange(undefined, of(testCases), true);
    const changes: SimpleChanges = {
      testCasesList$: change,
    };
    component.ngOnChanges(changes);

    testCases.sort((a, b) => a.name.localeCompare(b.name));

    expect(component.testCaseDataSource.data).toEqual(
      new MatTableDataSource(testCases).data,
    );
  });
});
