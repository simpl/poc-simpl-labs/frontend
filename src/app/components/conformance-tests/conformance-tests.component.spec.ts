import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { of } from 'rxjs';

import { ConformanceTestsComponent } from './conformance-tests.component';
import { testCasesSuite5Mock } from '../../../mocks/test-cases-mock';
import { submittedTestsMock } from '../../../tests/slabs-custom-components/submitted-test-mock';
import { DrawerService } from '../../services/drawer.service';
import { MockActivatedRoute } from '../../../tests/helpers/mock-activated-route.class';
import { SlabsCustomComponentService } from '../../services/slabs-custom-component.service';
import { slabsCustomComponentsMock } from '../../../mocks/slabs-custom-component-mock';

describe('ConformanceTestsComponent', () => {
  let component: ConformanceTestsComponent;
  let fixture: ComponentFixture<ConformanceTestsComponent>;
  let drawerServiceSpy: jasmine.SpyObj<DrawerService>;
  let customComponentService: SlabsCustomComponentService;

  beforeEach(async () => {
    drawerServiceSpy = jasmine.createSpyObj('DrawerService', [
      'closeTestCaseDrawer',
      'openTestCaseDrawer',
    ]);

    await TestBed.configureTestingModule({
      imports: [
        ConformanceTestsComponent,
        HttpClientTestingModule,
        BrowserAnimationsModule,
      ],
      providers: [
        { provide: ActivatedRoute, useClass: MockActivatedRoute },
        { provide: DrawerService, useValue: drawerServiceSpy },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ConformanceTestsComponent);
    component = fixture.componentInstance;
    customComponentService = TestBed.inject(SlabsCustomComponentService);

    component.submittedTest = submittedTestsMock[1];

    spyOn(
      customComponentService.submittedTestsSubject$,
      'asObservable',
    ).and.returnValue(of(submittedTestsMock));

    spyOn(
      customComponentService.customComponentsSubject$,
      'asObservable',
    ).and.returnValue(of(slabsCustomComponentsMock));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set #viewedTestCase value', () => {
    component.openTestCaseInfoDrawer(testCasesSuite5Mock[0]);
    expect(component.viewedTestCase).toEqual(testCasesSuite5Mock[0]);
  });

  it('should call closeTestCaseDrawer()', () => {
    component.closeTestCaseInfoDrawer();
    expect(drawerServiceSpy.closeTestCaseDrawer).toHaveBeenCalled();
  });

  it('should set #submittedTest value', () => {
    const submittedTest = submittedTestsMock.find(
      (st) =>
        st.customComponent.id === component.submittedTest?.customComponent.id,
    );
    expect(component.submittedTest).toEqual(submittedTest);
  });

  it('should set #customComponent value', () => {
    const customComponent = slabsCustomComponentsMock.find(
      (cc) => cc.id === component.submittedTest?.customComponent.id,
    )!;
    expect(component.submittedTest?.customComponent).toEqual(customComponent);
  });
});
