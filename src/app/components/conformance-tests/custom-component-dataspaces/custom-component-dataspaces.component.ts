import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatChipsModule } from '@angular/material/chips';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';

import { SlabsCustomComponentService } from '../../../services/slabs-custom-component.service';
import { SlabsCustomComponentModel } from '../../../models/interfaces/slabs-custom-components/slabs-custom-component.interface';
import { DataSpaceModel } from '../../../models/interfaces/data-space/data-space.interface';
import { DataSpaceComponent } from '../../data-space/data-space.component';

@Component({
  selector: 'app-custom-component-dataspaces',
  standalone: true,
  imports: [
    CommonModule,
    MatGridListModule,
    MatCardModule,
    MatIconModule,
    MatMenuModule,
    MatChipsModule,
    MatButtonModule,
    DataSpaceComponent,
  ],
  templateUrl: './custom-component-dataspaces.component.html',
  styleUrl: './custom-component-dataspaces.component.scss',
})
export class CustomComponentDataspacesComponent implements OnInit {
  @Input() customComponent!: SlabsCustomComponentModel;
  dataSpaces$!: Observable<DataSpaceModel[]>;

  constructor(
    private readonly slabsCustomComponentService: SlabsCustomComponentService,
  ) {}

  ngOnInit(): void {
    this.dataSpaces$ =
      this.slabsCustomComponentService.getCustomComponentDataSpaces(
        this.customComponent.id,
      );
  }
}
