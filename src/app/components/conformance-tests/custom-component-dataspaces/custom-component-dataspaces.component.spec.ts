import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { CustomComponentDataspacesComponent } from './custom-component-dataspaces.component';
import { slabsCustomComponentsMock } from '../../../../mocks/slabs-custom-component-mock';

describe('CustomComponentDataspacesComponent', () => {
  let component: CustomComponentDataspacesComponent;
  let fixture: ComponentFixture<CustomComponentDataspacesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        CustomComponentDataspacesComponent,
        HttpClientTestingModule,
        BrowserAnimationsModule,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(CustomComponentDataspacesComponent);
    component = fixture.componentInstance;
    component.customComponent = slabsCustomComponentsMock[1];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
