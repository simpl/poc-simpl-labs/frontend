import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { TestCaseInfoComponent } from './test-case-info.component';

describe('TestCaseInfoComponent', () => {
  let component: TestCaseInfoComponent;
  let fixture: ComponentFixture<TestCaseInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TestCaseInfoComponent, BrowserAnimationsModule],
    }).compileComponents();

    fixture = TestBed.createComponent(TestCaseInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should #closeDrawer.emit() event', () => {
    spyOn(component.closeDrawer, 'emit');
    component.onCloseDrawerClick();
    expect(component.closeDrawer.emit).toHaveBeenCalled();
  });
});
