import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

import { TestCaseModel } from '../../../models/interfaces/slabs-custom-components/test-case.interface';
import { MatTabsModule } from '@angular/material/tabs';

@Component({
  selector: 'app-test-case-info',
  standalone: true,
  imports: [MatButtonModule, MatIconModule, MatTabsModule],
  templateUrl: './test-case-info.component.html',
  styleUrl: './test-case-info.component.scss',
})
export class TestCaseInfoComponent {
  @Input() testCase!: TestCaseModel;
  @Output() closeDrawer = new EventEmitter<void>();

  constructor() {}

  onCloseDrawerClick() {
    this.closeDrawer.emit();
  }
}
