import {
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { forkJoin, map, Observable, of, Subscription, tap, timer } from 'rxjs';
import { Router } from '@angular/router';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatExpansionModule } from '@angular/material/expansion';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { saveAs } from 'file-saver-es';
import JSZip from 'jszip';

import mapStatus2ChipColor from '../../../helpers/custom-component-chip-colors';
import { SlabsCustomComponentService } from '../../../services/slabs-custom-component.service';
import { SessionStopRequest } from '../../../models/interfaces/slabs-custom-components/session-stop-request.interface';
import { SessionStartResponse } from '../../../models/interfaces/slabs-custom-components/session-start-response.interface';
import { ItbTestsExecutionTableComponent } from '../itb-tests-execution-table/itb-tests-execution-table.component';
import { TestExecutionStatus } from '../../../models/interfaces/slabs-custom-components/test-execution-status.interface';
import { LoadingService } from '../../../services/loading.service';
import { TestCaseModel } from '../../../models/interfaces/slabs-custom-components/test-case.interface';
import { SlabsCustomComponentModel } from '../../../models/interfaces/slabs-custom-components/slabs-custom-component.interface';
import { DialogService } from '../../../services/dialog.service';
import { getElapsedTime } from '../../../helpers/get-elapsed-time.function';
import { SubmittedTest } from '../../../models/interfaces/slabs-custom-components/submitted-test.interface';
import { WarningDialogData } from '../../../models/interfaces/warning-dialog-data.interface';
import { ITBTestsExecutionData } from '../../../models/interfaces/slabs-custom-components/itb-tests-execution-data.interface';
import { TestCaseStatus } from '../../../models/enums/test-case-status.enum';

@Component({
  selector: 'app-itb-tests-execution',
  standalone: true,
  imports: [
    CommonModule,
    MatChipsModule,
    MatIconModule,
    MatExpansionModule,
    MatToolbarModule,
    MatButtonModule,
    ItbTestsExecutionTableComponent,
  ],
  templateUrl: './itb-tests-execution.component.html',
  styleUrl: './itb-tests-execution.component.scss',
})
export class ITBTestsExecutionComponent implements OnInit, OnDestroy {
  @ViewChildren('testDialogs') testDialogs!: QueryList<ElementRef<HTMLElement>>;

  minimized = false;
  testRunning = true;
  status2ChipColor = mapStatus2ChipColor;
  testCasesList$!: Observable<TestCaseModel[]>;

  startTime!: string;
  endTime!: string;
  ongoingNumber!: number;
  undefinedNumber!: number;
  duration = '00:00:00';
  elapsedTimeIntervalRef!: NodeJS.Timeout;
  elapsedTime = getElapsedTime;

  submittedTest!: SubmittedTest;
  sessionStartResponse!: SessionStartResponse;
  specificationId!: number;
  componentId!: number;

  timerSubscription = new Subscription();
  testExecutionStatusSub = new Subscription();
  submittedTestsSub = new Subscription();
  getCustomComponentsSub = new Subscription();
  getTestSuitesSub = new Subscription();
  testCasesListSub = new Subscription();

  constructor(
    private readonly router: Router,
    private readonly slabsCustomComponentService: SlabsCustomComponentService,
    private readonly _loadingService: LoadingService,
    private readonly dialogService: DialogService,
    public readonly dialogRef: MatDialogRef<ITBTestsExecutionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ITBTestsExecutionData,
  ) {
    if (!this.data.isHistoryItem)
      this.elapsedTimeIntervalRef = setInterval(
        () => (this.duration = this.elapsedTime(this.startTime)),
        1000,
      );
    else {
      this.startTime = data.testExecutionStatus.testExecution.startDate;
      this.endTime = data.testExecutionStatus.testExecution.endDate!;
    }
  }

  ngOnInit(): void {
    if (!this.data.isHistoryItem)
      this.timerSubscription = timer(
        0,
        this.slabsCustomComponentService.timerInterval,
      )
        .pipe(
          tap((_timer) => this._loadingService.updateITBTimer(_timer)),
          map(
            (_timer) =>
              (this.testExecutionStatusSub = this.getExecutionStatus()),
          ),
        )
        .subscribe();

    this.testCasesList$ = of(this.data.testExecutionStatus.testCasesList);

    this.submittedTestsSub =
      this.slabsCustomComponentService.submittedTestsSubject$
        .asObservable()
        .subscribe((submittedTests) => {
          const submittedTest = submittedTests.find(
            (st) =>
              !!st.testExecutionStatus &&
              st.testExecutionStatus.testExecution.id ===
                this.data.testExecutionStatus.testExecution.id,
          )!;

          const testDialog = this.testDialogs.find(
            (dialog) =>
              +dialog.nativeElement.id.replace('itb-tests-execution-', '') ===
              submittedTest?.testExecutionStatus.testExecution.id,
          );

          submittedTest.minimized
            ? testDialog?.nativeElement.classList.add('d-none')
            : testDialog?.nativeElement.classList.remove('d-none');

          this.minimized = submittedTest.minimized!;
        });
  }

  ngOnDestroy(): void {
    clearInterval(this.elapsedTimeIntervalRef);
    this.timerSubscription.unsubscribe();
    this.testExecutionStatusSub.unsubscribe();
    this.submittedTestsSub.unsubscribe();
    this.getCustomComponentsSub.unsubscribe();
    this.getTestSuitesSub.unsubscribe();
    this.testCasesListSub.unsubscribe();
  }

  getExecutionStatus(): Subscription {
    this.specificationId =
      this.specificationId ||
      this.data.testExecutionStatus.testExecution.specificationId;

    this.componentId =
      this.componentId ||
      +this.data.testExecutionStatus.testExecution.componentId;

    return this.slabsCustomComponentService
      .getTestExecutionStatus(this.specificationId, this.componentId)
      .subscribe((testExecutionStatus) =>
        this.updateTestExecutionStatus(testExecutionStatus),
      );
  }

  updateTestExecutionStatus(testExecutionStatus: TestExecutionStatus) {
    const {
      testExecution: { startDate, endDate, componentId },
      testCasesList,
    } = testExecutionStatus;

    this.data.testExecutionStatus = { ...testExecutionStatus };

    this.startTime = startDate;

    if (endDate) {
      this.timerSubscription.unsubscribe();
      clearInterval(this.elapsedTimeIntervalRef);

      this.endTime = endDate
        ? new Date(new Date(endDate).getTime()).toString()
        : new Date(
            Math.max(
              ...testCasesList.map((tc) => new Date(tc.endDate).getTime()),
            ),
          ).toString();

      this.minimized = false;
      this.testRunning = false;

      if (
        startDate &&
        testCasesList.every(
          (tc) =>
            tc.status.toUpperCase() !== TestCaseStatus.UNDEFINED.toUpperCase(),
        )
      )
        this.openTerminationDialog({
          title: 'Conformance test completed',
          content:
            'The conformance test is completed. Continue to see the results.',
        });
    }

    this.updateCustomComponents(
      testCasesList,
      componentId,
      testExecutionStatus,
    );
  }

  updateCustomComponents(
    testCasesList: TestCaseModel[],
    componentId: string,
    testExecutionStatus: TestExecutionStatus,
  ) {
    this.getCustomComponentsSub = this.slabsCustomComponentService
      .getCustomComponents()
      .subscribe((customComponents) => {
        this.ongoingNumber = testCasesList.filter(
          (tc) =>
            tc.status.toUpperCase() === TestCaseStatus.ONGOING.toUpperCase(),
        ).length;

        this.undefinedNumber = testCasesList.filter(
          (tc) =>
            tc.status.toUpperCase() === TestCaseStatus.UNDEFINED.toUpperCase(),
        ).length;

        this.testCasesList$ = of(testCasesList);

        const customComponent = customComponents.find(
          (cc) => cc.id === +componentId,
        )!;

        this.updateSubmittedTests(customComponent, testExecutionStatus);

        this.slabsCustomComponentService.customComponentsSubject$.next(
          customComponents,
        );
      });
  }

  updateSubmittedTests(
    customComponent: SlabsCustomComponentModel,
    testExecutionStatus: TestExecutionStatus,
  ) {
    this.getTestSuitesSub = this.slabsCustomComponentService
      .getTestSuitesBySpecificationId(
        customComponent.specificationId,
        customComponent.id,
      )
      .subscribe((testSuites) => {
        this.submittedTest =
          this.slabsCustomComponentService.submittedTests.find(
            (st) => st.customComponent.id === customComponent?.id,
          )!;

        if (this.submittedTest) {
          this.submittedTest.minimized = this.minimized;
          this.submittedTest.testExecutionStatus = testExecutionStatus;
          this.submittedTest.testSuites = testSuites;
          this.submittedTest.customComponent = customComponent!;

          this.slabsCustomComponentService.updateSubmittedTests(
            this.submittedTest,
          );
        }
      });
  }

  stopTestsExecution() {
    const sessionStopRequest: SessionStopRequest = {
      session: this.data.sessionStartResponse.createdSessions.map(
        (cs) => cs.session,
      ),
    };

    this.slabsCustomComponentService.stopTestsExecution(sessionStopRequest);
  }

  minimizeConformanceTests() {
    this.slabsCustomComponentService.minimizePage(this.submittedTest);
  }

  closeDialog() {
    const { specificationId, componentId } =
      this.data.testExecutionStatus.testExecution;

    const submittedTest = this.slabsCustomComponentService.submittedTests.find(
      (st) => st.customComponent.id === +componentId,
    );

    this.dialogRef.afterClosed().subscribe(() => {
      this.router.navigateByUrl('/conformance/conformance-tests', {
        state: { submittedTest },
      });
    });

    this.getTestSuitesSub = this.slabsCustomComponentService
      .getTestSuitesBySpecificationId(specificationId, +componentId)
      .subscribe((testSuites) => {
        forkJoin(
          testSuites.map((ts) =>
            this.slabsCustomComponentService.getTestCasesByTestSuiteId(
              ts.id,
              +componentId,
            ),
          ),
        ).subscribe((testCases) => {
          submittedTest!.testSuites = testSuites;
          submittedTest!.testCases = testCases;
          this.slabsCustomComponentService.updateSubmittedTests(submittedTest!);
        });
      });

    this.dialogRef.close();
  }

  downloadAllReports() {
    this.testCasesListSub = this.testCasesList$.subscribe((testCases) =>
      forkJoin(
        testCases.map((testCase) =>
          this.slabsCustomComponentService.getTestSessionReport(
            testCase.session,
          ),
        ),
      )
        .pipe(map((list) => list.map((tc) => tc.report)))
        .subscribe((reports) => {
          let zip = new JSZip();

          reports.forEach((report, i) => zip.file(testCases[i].name, report));

          zip
            .generateAsync({ type: 'blob' })
            .then((content) =>
              saveAs(content, `reports_${this.data.customComponent?.name}.zip`),
            );
        }),
    );
  }

  openTerminationDialog<T>(warningDialogData: WarningDialogData<T>) {
    let warningDialogRef =
      this.dialogService.openWarningDialog<T>(warningDialogData);

    warningDialogRef
      .afterClosed()
      .subscribe(() =>
        this.slabsCustomComponentService.maximizePage(this.submittedTest),
      );
  }
}
