import { CommonModule } from '@angular/common';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { map, Observable, Subscription } from 'rxjs';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { BreakpointObserver } from '@angular/cdk/layout';
import { MatDialog } from '@angular/material/dialog';

import { TestExecutionStatus } from '../../../models/interfaces/slabs-custom-components/test-execution-status.interface';
import { SlabsCustomComponentService } from '../../../services/slabs-custom-component.service';
import { SlabsCustomComponentModel } from '../../../models/interfaces/slabs-custom-components/slabs-custom-component.interface';
import mapStatus2ChipColor from '../../../helpers/custom-component-chip-colors';
import { getElapsedTime } from '../../../helpers/get-elapsed-time.function';
import { ITBTestsExecutionComponent } from '../itb-tests-execution/itb-tests-execution.component';
import { SubmittedTest } from '../../../models/interfaces/slabs-custom-components/submitted-test.interface';
import { ITBTestsExecutionData } from '../../../models/interfaces/slabs-custom-components/itb-tests-execution-data.interface';
import { TestCaseStatus } from '../../../models/enums/test-case-status.enum';

@Component({
  selector: 'app-tests-history',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatIconModule,
    MatButtonModule,
    MatSelectModule,
    MatGridListModule,
    MatCardModule,
    MatChipsModule,
  ],
  templateUrl: './tests-history.component.html',
  styleUrl: './tests-history.component.scss',
})
export class TestsHistoryComponent implements OnInit, OnDestroy {
  @Input() customComponent!: SlabsCustomComponentModel;

  searchForm = this._fb.group({
    date: [''],
    status: [''],
  });

  status2ChipColor = mapStatus2ChipColor;
  testsHistory: TestExecutionStatus[] = [];
  cols$ = new Observable<number>();
  submittedTest!: SubmittedTest;

  submittedTestsSub = new Subscription();
  testsHistorySub = new Subscription();

  constructor(
    private readonly _fb: FormBuilder,
    private readonly _breakpointObserver: BreakpointObserver,
    private readonly slabsCustomComponentService: SlabsCustomComponentService,
    private readonly _dialog: MatDialog,
  ) {
    this.cols$ = this._breakpointObserver
      .observe(`(max-width: ${1430}px)`)
      .pipe(map((state) => (state.matches ? 1 : 2)));
  }

  ngOnInit(): void {
    this.testsHistorySub = this.slabsCustomComponentService
      .getComponentTestsHistory(this.customComponent.id)
      .subscribe(
        (testsHistory) =>
          (this.testsHistory = testsHistory.map((th) => {
            th.elapsedTime = getElapsedTime(
              th.testExecution.startDate,
              th.testExecution.endDate,
            );
            return th;
          })),
      );

    this.searchForm.disable();

    this.submittedTestsSub =
      this.slabsCustomComponentService.submittedTestsSubject$
        .asObservable()
        .subscribe((submittedTests) => {
          this.submittedTest = submittedTests.find(
            (st) => st.customComponent.id === this.customComponent?.id,
          )!;

          if (this.submittedTest.testExecutionStatus) {
            const {
              testExecutionStatus: {
                testExecution: { endDate, componentId, id },
              },
            } = this.submittedTest;

            if (
              !!endDate &&
              id !==
                this.testsHistory[this.testsHistory?.length - 1]?.testExecution
                  .id
            )
              this.testsHistorySub = this.slabsCustomComponentService
                .getComponentTestsHistory(+componentId)
                .subscribe(
                  (testsHistory) => (this.testsHistory = testsHistory),
                );
          }
        });
  }

  ngOnDestroy(): void {
    this.submittedTestsSub.unsubscribe();
    this.testsHistorySub.unsubscribe();
  }

  openTestResultSummary(testExecutionStatus: TestExecutionStatus) {
    if (
      testExecutionStatus.testExecution.status.toUpperCase() ===
      TestCaseStatus.ONGOING.toUpperCase()
    )
      return this.slabsCustomComponentService.maximizePage(this.submittedTest);

    const data: ITBTestsExecutionData = {
      isHistoryItem: true,
      testExecutionStatus,
      testSuites: this.submittedTest?.testSuites,
      testCases: this.submittedTest?.testCases,
      sessionStartResponse: this.submittedTest?.sessionStartResponse,
      customComponent: this.customComponent,
    };

    this._dialog.open(ITBTestsExecutionComponent, {
      disableClose: true,
      hasBackdrop: false,
      panelClass: ['fullscreen-dialog'],
      data,
    });
  }
}
