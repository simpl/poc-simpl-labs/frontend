import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { TestSuitesTableComponent } from './test-suites-table.component';

describe('TestSuitesTableComponent', () => {
  let component: TestSuitesTableComponent;
  let fixture: ComponentFixture<TestSuitesTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TestSuitesTableComponent, BrowserAnimationsModule],
    }).compileComponents();

    fixture = TestBed.createComponent(TestSuitesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
