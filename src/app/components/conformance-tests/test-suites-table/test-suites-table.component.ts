import {
  AfterViewInit,
  Component,
  computed,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  Signal,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { MatChipsModule } from '@angular/material/chips';
import { MatProgressBarModule } from '@angular/material/progress-bar';

import { TestSuiteModel } from '../../../models/interfaces/slabs-custom-components/test-suite.interface';
import { TestCaseModel } from '../../../models/interfaces/slabs-custom-components/test-case.interface';
import mapStatus2ChipColor from '../../../helpers/custom-component-chip-colors';
import { SlabsCustomComponentModel } from '../../../models/interfaces/slabs-custom-components/slabs-custom-component.interface';
import { TestResultBarComponent } from '../test-result-bar/test-result-bar.component';
import { RowCheckbox } from '../../../models/interfaces/slabs-custom-components/row-checkbox.interface';

@Component({
  selector: 'app-test-suites-table',
  standalone: true,
  imports: [
    CommonModule,
    MatTableModule,
    MatSortModule,
    MatIconModule,
    MatButtonModule,
    MatCheckboxModule,
    MatChipsModule,
    MatProgressBarModule,
    TestResultBarComponent,
  ],
  templateUrl: './test-suites-table.component.html',
  styleUrl: './test-suites-table.component.scss',
  animations: [
    trigger('detailExpand', [
      state('collapsed,void', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'),
      ),
    ]),
  ],
})
export class TestSuitesTableComponent
  implements OnChanges, OnInit, AfterViewInit
{
  @Input() customComponent!: SlabsCustomComponentModel;
  @Input() testSuites: TestSuiteModel[] = [];
  @Input() testCases: TestCaseModel[][] = [];
  @Input() suiteCheckboxes: RowCheckbox[] = [];

  @Output() checkedEvent = new EventEmitter<{
    selected: boolean;
    iSuite: number;
    iCase?: number;
  }>();
  @Output() testCaseInfoShowEvent = new EventEmitter<TestCaseModel>();

  @ViewChild(MatSort) sort!: MatSort;

  // Test Suites
  testSuiteColumns: string[] = [];
  testSuiteColumnsWithExpand: string[] = [];
  expandedElement!: TestSuiteModel | null;
  testSuiteDataSource = new MatTableDataSource<TestSuiteModel>();
  mapProp2Column = new Map<string, string>();
  previousResult = '';

  // Test Cases
  testCaseColumns: string[] = [];
  testCaseDataSources: MatTableDataSource<TestCaseModel>[] = [];

  status2ChipColor = mapStatus2ChipColor;

  readonly partiallyComplete: Signal<boolean>[] = [];

  constructor() {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('suiteCheckboxes')) {
      const suiteCheckboxes = changes['suiteCheckboxes']
        .currentValue as RowCheckbox[];

      suiteCheckboxes.forEach((_, i) => {
        this.partiallyComplete[i] = computed(() => {
          const rowCheckbox = suiteCheckboxes[i];
          if (!rowCheckbox.children) return false;
          return (
            rowCheckbox.children.some((x) => x.selected) &&
            !rowCheckbox.children.every((x) => x.selected)
          );
        });
      });
    }

    if (changes.hasOwnProperty('testCases'))
      this.setTestCases(changes['testCases'].currentValue);

    if (changes.hasOwnProperty('testSuites')) {
      const testSuites = changes['testSuites'].currentValue as TestSuiteModel[];

      const currentResult = testSuites
        .map(
          (ts) =>
            `${ts.resultError}_${ts.resultNotExecuted}_${ts.resultSuccess}`,
        )
        .join();

      if (currentResult !== this.previousResult) {
        this.previousResult = currentResult;
        this.setTablesData(testSuites);
      }
    }
  }

  ngOnInit(): void {
    this.testSuiteColumns = [
      'tsCheckbox',
      ...[
        'name',
        'description',
        'lastUpdate',
        'testCasesNumber',
        'testCasesResult',
      ],
      'info',
    ];

    this.testSuiteColumnsWithExpand = ['expand', ...this.testSuiteColumns];

    this.testSuiteColumns.forEach((prop) => {
      let col: string;

      switch (prop) {
        case 'name':
          col = 'Test suite';
          break;
        case 'lastUpdate':
          col = 'Suite last update';
          break;
        case 'testCasesNumber':
          col = 'N° Test case';
          break;
        case 'testCasesResult':
          col = 'Test cases result';
          break;
        default:
          col = prop;
          break;
      }

      this.mapProp2Column.set(prop, col);
    });
  }

  ngAfterViewInit() {
    this.testSuiteDataSource.sort = this.sort;
  }

  setTablesData(testSuites: TestSuiteModel[]) {
    this.testSuiteDataSource = new MatTableDataSource(testSuites);
  }

  setTestCases(testCases: TestCaseModel[][]) {
    this.testCaseColumns = [
      'tcCheckbox',
      ...Object.keys(testCases[0][0]).filter(
        (key) => key === 'name' || key === 'description' || key === 'status',
      ),
      'info',
    ];

    this.testCaseDataSources = testCases.map(
      (tc) => new MatTableDataSource(tc),
    );
  }

  onCheckboxChange(selected: boolean, iSuite: number, iCase?: number) {
    this.checkedEvent.emit({
      selected,
      iSuite,
      iCase,
    });
  }

  showTestCaseInfo(testCase: TestCaseModel) {
    this.testCaseInfoShowEvent.emit(testCase);
  }
}
