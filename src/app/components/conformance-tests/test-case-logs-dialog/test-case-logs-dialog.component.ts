import { Component, Inject } from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatToolbarModule } from '@angular/material/toolbar';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { saveAs } from 'file-saver-es';

import { getElapsedTime } from '../../../helpers/get-elapsed-time.function';
import { LogsDialogData } from '../../../models/interfaces/logs-dialog-data.interface';

@Component({
  selector: 'app-test-case-logs-dialog',
  standalone: true,
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
  ],
  templateUrl: './test-case-logs-dialog.component.html',
  styleUrl: './test-case-logs-dialog.component.scss',
})
export class TestCaseLogsDialogComponent {
  logs: string[] = [];
  elapsedTime = '';

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: LogsDialogData,
    public dialogRef: MatDialogRef<TestCaseLogsDialogComponent>,
  ) {
    this.logs = this.data.logs
      .substring(1, this.data.logs.length - 1)
      .split(',');

    this.elapsedTime = getElapsedTime(
      this.data.testCase.startDate,
      this.data.testCase.endDate,
    );
  }

  exportLogs() {
    const logs: string = this.data.logs
      .substring(1, this.data.logs.length - 1)
      .replaceAll(', ', '\n');

    const blob = new Blob([logs], { type: 'text' });
    saveAs(blob, `logs_${this.data.testCase.name}.txt`);
  }

  goBack() {
    this.dialogRef.close();
  }
}
