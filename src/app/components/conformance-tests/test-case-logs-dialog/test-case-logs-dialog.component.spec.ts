import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { TestCaseLogsDialogComponent } from './test-case-logs-dialog.component';
import { testCasesSuite1Mock } from '../../../../mocks/test-cases-mock';
import { LogsDialogData } from '../../../models/interfaces/logs-dialog-data.interface';

const mockData: LogsDialogData = {
  duration: '',
  logs: '',
  testCase: testCasesSuite1Mock[0],
};

const dialogRefMock: Partial<MatDialogRef<TestCaseLogsDialogComponent>> = {
  close: () => {},
};

describe('TestCaseLogsDialogComponent', () => {
  let component: TestCaseLogsDialogComponent;
  let fixture: ComponentFixture<TestCaseLogsDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TestCaseLogsDialogComponent],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: mockData },
        { provide: MatDialogRef, useValue: dialogRefMock },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(TestCaseLogsDialogComponent);
    component = fixture.componentInstance;

    component.data.logs = 'logs-mock';
    component.data.testCase = testCasesSuite1Mock[0];

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close #dialogRef', () => {
    spyOn(component.dialogRef, 'close');
    component.goBack();
    expect(component.dialogRef.close).toHaveBeenCalled();
  });
});
