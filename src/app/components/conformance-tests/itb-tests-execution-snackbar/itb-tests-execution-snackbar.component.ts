import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  QueryList,
  TemplateRef,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Subscription } from 'rxjs';
import { MatIconModule } from '@angular/material/icon';
import {
  NotificationsService,
  NotificationType,
  Options,
  SimpleNotificationsModule,
} from 'angular2-notifications';

import { SessionStopRequest } from '../../../models/interfaces/slabs-custom-components/session-stop-request.interface';
import { TestExecutionStatus } from '../../../models/interfaces/slabs-custom-components/test-execution-status.interface';
import { SlabsCustomComponentService } from '../../../services/slabs-custom-component.service';
import { SubmittedTest } from '../../../models/interfaces/slabs-custom-components/submitted-test.interface';

@Component({
  selector: 'app-itb-tests-execution-snackbar',
  standalone: true,
  imports: [CommonModule, MatIconModule, SimpleNotificationsModule],
  templateUrl: './itb-tests-execution-snackbar.component.html',
  styleUrl: './itb-tests-execution-snackbar.component.scss',
})
export class ITBTestsExecutionSnackbarComponent implements OnInit, OnDestroy {
  @ViewChild('snackBarRef') snackBarRef!: TemplateRef<any>;
  @ViewChildren('testSnackBars') testSnackBars!: QueryList<
    ElementRef<HTMLElement>
  >;

  snackBarOptions!: Options;
  submittedTests!: SubmittedTest[];
  testRunning = true;

  submittedTestsSub = new Subscription();

  constructor(
    private readonly slabsCustomComponentService: SlabsCustomComponentService,
    private readonly _snackbarService: NotificationsService,
  ) {
    this.snackBarOptions = {
      clickToClose: false,
      clickIconToClose: false,
      timeOut: 0,
    };
  }

  ngOnInit(): void {
    this.submittedTestsSub =
      this.slabsCustomComponentService.submittedTestsSubject$
        .asObservable()
        .subscribe((submittedTests) => {
          this.submittedTests = submittedTests;
          submittedTests.forEach((submittedTest) => {
            if (submittedTest.testExecutionStatus) {
              const {
                testExecutionStatus: {
                  testExecution: { id, endDate },
                },
                testExecutionStatus,
                minimized,
              } = submittedTest;

              this.testRunning =
                this.slabsCustomComponentService.isTestRunning(
                  testExecutionStatus,
                );

              const testSnackBar = this.testSnackBars.find(
                (x) =>
                  +x.nativeElement.id.replace('tests-snack-bar-', '') ===
                  submittedTest.testExecutionStatus.testExecution.id,
              );

              if (endDate) this.removeSnackbar(id.toString());
              else if (!testSnackBar) this.createSnackbar(submittedTest);

              minimized
                ? testSnackBar?.nativeElement.classList.remove('hide-snack-bar')
                : testSnackBar?.nativeElement.classList.add('hide-snack-bar');
            }
          });
        });
  }

  ngOnDestroy(): void {
    this.submittedTestsSub.unsubscribe();
  }

  createSnackbar(context: SubmittedTest) {
    this._snackbarService.html(
      this.snackBarRef,
      NotificationType.Bare,
      { id: context.testExecutionStatus.testExecution.id.toString() },
      undefined,
      context,
    );
  }

  stopTestsExecution(testExecutionStatus: TestExecutionStatus) {
    const submittedTest = this.submittedTests.find(
      (st) =>
        st.testExecutionStatus.testExecution.id ===
        testExecutionStatus.testExecution.id,
    )!;

    const sessionStopRequest: SessionStopRequest = {
      session: submittedTest.sessionStartResponse.createdSessions.map(
        (cs) => cs.session,
      ),
    };

    this.slabsCustomComponentService.stopTestsExecution(sessionStopRequest);
  }

  maximizeSnackbar(_testExecutionStatus: TestExecutionStatus) {
    const submittedTest = this.submittedTests.find(
      (st) =>
        st.testExecutionStatus?.testExecution.id ===
        _testExecutionStatus.testExecution.id,
    );

    submittedTest!.testExecutionStatus = _testExecutionStatus;
    this.slabsCustomComponentService.maximizePage(submittedTest!);
  }

  removeSnackbar(id: string) {
    this._snackbarService.remove(id);
  }
}
