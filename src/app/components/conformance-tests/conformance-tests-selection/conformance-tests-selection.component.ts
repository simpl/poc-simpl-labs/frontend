import {
  Component,
  computed,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  signal,
  SimpleChanges,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SlabsCustomComponentService } from '../../../services/slabs-custom-component.service';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { TestSuitesTableComponent } from '../test-suites-table/test-suites-table.component';
import { TestSuiteModel } from '../../../models/interfaces/slabs-custom-components/test-suite.interface';
import { TestCaseModel } from '../../../models/interfaces/slabs-custom-components/test-case.interface';
import { Subscription } from 'rxjs';
import mapStatus2ChipColor from '../../../helpers/custom-component-chip-colors';
import { RowCheckbox } from '../../../models/interfaces/slabs-custom-components/row-checkbox.interface';
import { SubmittedTest } from '../../../models/interfaces/slabs-custom-components/submitted-test.interface';

@Component({
  selector: 'app-conformance-tests-selection',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatExpansionModule,
    MatCheckboxModule,
    MatChipsModule,
    TestSuitesTableComponent,
  ],
  templateUrl: './conformance-tests-selection.component.html',
  styleUrl: './conformance-tests-selection.component.scss',
})
export class ConformanceTestsSelectionComponent
  implements OnInit, OnChanges, OnDestroy
{
  @Input() submittedTest: SubmittedTest | undefined;
  @Output() onTestCaseInfoShow = new EventEmitter<TestCaseModel>();

  searchForm = this._fb.group({
    specification: [null],
    lastUpdate: [null],
    status: [null],
  });

  status2ChipColor = mapStatus2ChipColor;
  testSuites!: TestSuiteModel[];
  testCases!: TestCaseModel[][];
  rowCheckbox!: RowCheckbox;

  readonly specificationCheckbox = signal(this.rowCheckbox);

  readonly partiallyComplete = computed<boolean>(() => {
    const specificationCheckbox = { ...this.specificationCheckbox() };

    if (!specificationCheckbox.children) return false;

    return (
      (specificationCheckbox.children.some((scb) => scb.selected) &&
        !specificationCheckbox.children.every((scb) => scb.selected)) ||
      (specificationCheckbox.children.some((scb) =>
        scb.children?.some((tcb) => tcb.selected),
      ) &&
        specificationCheckbox.children.every(
          (scb) => !scb.children?.every((tcb) => tcb.selected),
        ))
    );
  });

  suiteCheckboxes: RowCheckbox[] = [];

  submittedTestsSub = new Subscription();

  constructor(
    private readonly _fb: FormBuilder,
    private readonly slabsCustomComponentService: SlabsCustomComponentService,
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('submittedTest'))
      this.setSpecificationCheckbox();
  }

  ngOnInit(): void {
    this.searchForm.disable();
  }

  ngOnDestroy(): void {
    this.submittedTestsSub.unsubscribe();
  }

  updateCheckboxes(selected: boolean, iSuite?: number, iCase?: number) {
    this.specificationCheckbox.update((specificationCheckbox) => {
      if (iSuite === undefined) {
        specificationCheckbox.selected = selected;
        specificationCheckbox.children?.forEach((ts) => {
          ts.selected = selected;
          ts.children?.forEach((tc) => (tc.selected = selected));
        });
      } else if (iCase === undefined) {
        specificationCheckbox.children![iSuite].selected = selected;
        specificationCheckbox.children![iSuite].children?.forEach(
          (tc) => (tc.selected = selected),
        );
        specificationCheckbox.selected =
          specificationCheckbox.children?.every((ts) => ts.selected) ?? true;
      } else {
        specificationCheckbox.children![iSuite].children![iCase].selected =
          selected;
        specificationCheckbox.children![iSuite].selected =
          specificationCheckbox.children![iSuite].children?.every(
            (tc) => tc.selected,
          ) ?? true;
        specificationCheckbox.selected =
          specificationCheckbox.children?.every((ts) => ts.selected) ?? true;
      }

      this.suiteCheckboxes = [...specificationCheckbox.children!];

      return { ...specificationCheckbox };
    });

    this.slabsCustomComponentService.updateSubmittedTests({
      ...this.submittedTest!,
      specificationCheckbox: this.specificationCheckbox(),
    });
  }

  onTableCheckboxChange(event: {
    selected: boolean;
    iSuite: number;
    iCase?: number;
  }) {
    this.updateCheckboxes(event.selected, event.iSuite, event.iCase);
  }

  onCheckboxChange(selected: boolean) {
    this.updateCheckboxes(selected);
  }

  setSpecificationCheckbox() {
    this.testSuites = this.submittedTest?.testSuites!;
    this.testCases = this.submittedTest?.testCases!;

    const _specCheckbox = this.submittedTest?.specificationCheckbox!;

    const disabled = this.slabsCustomComponentService.isTestRunning(
      this.submittedTest?.testExecutionStatus!,
    );

    _specCheckbox.disabled = disabled;
    _specCheckbox.children?.forEach((tsc) => {
      tsc.disabled = disabled;
      tsc.children?.forEach((tcc) => {
        tcc.disabled = disabled;
      });
    });

    this.specificationCheckbox.set({ ..._specCheckbox });
    this.suiteCheckboxes = [...this.specificationCheckbox().children!];

    this.submittedTest!.specificationCheckbox = this.specificationCheckbox();
  }

  openTestCaseInfoDrawer(testCase: TestCaseModel) {
    this.onTestCaseInfoShow.emit(testCase);
  }
}
