import { CommonModule } from '@angular/common';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, Subscription } from 'rxjs';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatChipsModule } from '@angular/material/chips';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDrawer, MatSidenavModule } from '@angular/material/sidenav';

import mapStatus2ChipColor from '../../helpers/custom-component-chip-colors';
import { TestCaseModel } from '../../models/interfaces/slabs-custom-components/test-case.interface';
import { SlabsCustomComponentService } from '../../services/slabs-custom-component.service';
import { TestCaseInfoComponent } from './test-case-info/test-case-info.component';
import { TestsHistoryComponent } from './tests-history/tests-history.component';
import { CustomComponentInformationComponent } from './custom-component-information/custom-component-information.component';
import { KubeboxConsoleComponent } from '../_shared/kubebox-console/kubebox-console.component';
import { TestResultBarComponent } from './test-result-bar/test-result-bar.component';
import { DrawerService } from '../../services/drawer.service';
import { CustomComponentDataspacesComponent } from './custom-component-dataspaces/custom-component-dataspaces.component';
import { ConformanceTestsSelectionComponent } from './conformance-tests-selection/conformance-tests-selection.component';
import { SubmittedTest } from '../../models/interfaces/slabs-custom-components/submitted-test.interface';

@Component({
  selector: 'app-conformance-tests',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    MatSelectModule,
    MatExpansionModule,
    MatChipsModule,
    MatIconModule,
    MatTabsModule,
    MatProgressBarModule,
    MatSidenavModule,
    TestCaseInfoComponent,
    TestsHistoryComponent,
    CustomComponentInformationComponent,
    KubeboxConsoleComponent,
    CustomComponentDataspacesComponent,
    TestResultBarComponent,
    ConformanceTestsSelectionComponent,
  ],
  templateUrl: './conformance-tests.component.html',
  styleUrl: './conformance-tests.component.scss',
})
export class ConformanceTestsComponent implements OnInit, OnDestroy {
  @ViewChild('testCaseInfoDrawer', { static: true })
  testCaseInfoDrawer!: MatDrawer;

  viewedTestCase!: TestCaseModel;

  status2ChipColor = mapStatus2ChipColor;

  submittedTest: SubmittedTest | undefined;
  routeParamMapSub = new Subscription();
  submittedTestsSub = new Subscription();
  customComponentsSub = new Subscription();

  constructor(
    private readonly slabsCustomComponentService: SlabsCustomComponentService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly drawerService: DrawerService,
  ) {
    this.routeParamMapSub = this.activatedRoute.paramMap
      .pipe(map(() => window.history.state))
      .subscribe((state) => {
        this.submittedTest = state.submittedTest;

        this.slabsCustomComponentService.viewedComponentSubject$.next(
          state.submittedTest.customComponent,
        );
      });
  }

  ngOnInit(): void {
    this.submittedTestsSub =
      this.slabsCustomComponentService.submittedTestsSubject$
        .asObservable()
        .subscribe((submittedTests) => {
          this.submittedTest = {
            ...submittedTests.find(
              (st) =>
                +st.customComponent.id ===
                this.submittedTest?.customComponent.id,
            )!,
          };
        });
  }

  ngOnDestroy(): void {
    this.routeParamMapSub.unsubscribe();
    this.submittedTestsSub.unsubscribe();
    this.customComponentsSub.unsubscribe();
  }

  openTestCaseInfoDrawer(testCase: TestCaseModel) {
    this.viewedTestCase = { ...testCase };
    this.drawerService.openTestCaseDrawer(this.testCaseInfoDrawer);
  }

  closeTestCaseInfoDrawer() {
    this.drawerService.closeTestCaseDrawer(this.testCaseInfoDrawer);
  }
}
