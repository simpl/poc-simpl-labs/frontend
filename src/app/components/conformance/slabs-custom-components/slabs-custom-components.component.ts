import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatChipsModule } from '@angular/material/chips';
import { Subscription } from 'rxjs';
import { MatButtonModule } from '@angular/material/button';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatRadioModule } from '@angular/material/radio';
import { FormsModule } from '@angular/forms';

import { SlabsCustomComponentService } from '../../../services/slabs-custom-component.service';
import { SlabsCustomComponentModel } from '../../../models/interfaces/slabs-custom-components/slabs-custom-component.interface';
import mapStatus2ChipColor from '../../../helpers/custom-component-chip-colors';
import { TestResultBarComponent } from '../../conformance-tests/test-result-bar/test-result-bar.component';

@Component({
  selector: 'app-slabs-custom-components',
  standalone: true,
  imports: [
    CommonModule,
    MatTableModule,
    MatSortModule,
    MatButtonModule,
    MatIconModule,
    MatChipsModule,
    MatRadioModule,
    FormsModule,
    TestResultBarComponent,
  ],
  templateUrl: './slabs-custom-components.component.html',
  styleUrl: './slabs-custom-components.component.scss',
})
export class SLabsCustomComponentsComponent
  implements OnInit, AfterViewInit, OnDestroy
{
  @Input() isWizard = false;
  @Output() onComponentSelect = new EventEmitter<SlabsCustomComponentModel>();
  @ViewChild(MatSort) sort!: MatSort;

  customComponents: SlabsCustomComponentModel[] = [];
  selectedComponent!: SlabsCustomComponentModel;

  displayedColumns: string[] = [];
  tableDataSource: MatTableDataSource<SlabsCustomComponentModel> =
    new MatTableDataSource<SlabsCustomComponentModel>();

  status2ChipColor = mapStatus2ChipColor;

  getCustomComponentsSub = new Subscription();
  getSpecificationSub = new Subscription();
  customComponentsSub = new Subscription();

  constructor(
    private readonly slabsCustomComponentService: SlabsCustomComponentService,
    private readonly router: Router,
  ) {}

  ngOnInit(): void {
    const { customComponents } = this.slabsCustomComponentService;

    if (customComponents && customComponents.length > 0) {
      this.customComponents = customComponents;
      this.setTableData(customComponents);
    }

    this.customComponentsSub =
      this.slabsCustomComponentService.customComponentsSubject$
        .asObservable()
        .subscribe((customComponents) => {
          this.customComponents = customComponents;
          this.setTableData(customComponents);
        });
  }

  ngAfterViewInit() {
    this.tableDataSource.sort = this.sort;
  }

  ngOnDestroy(): void {
    this.getCustomComponentsSub.unsubscribe();
    this.getSpecificationSub.unsubscribe();
    this.customComponentsSub.unsubscribe();
  }

  setTableData(customComponents: SlabsCustomComponentModel[]) {
    this.displayedColumns = [
      'name',
      'version',
      'specificationDescription',
      'status',
      'result',
    ];

    this.isWizard
      ? this.displayedColumns.unshift('selectComponent')
      : this.displayedColumns.push('detailView');

    this.tableDataSource = new MatTableDataSource(customComponents);
    this.tableDataSource.sort = this.sort;
  }

  goToConformanceTests(compId: number) {
    const { submittedTests } = this.slabsCustomComponentService;

    const submittedTest = submittedTests.find(
      (st) => +st.customComponent.id === compId,
    );

    this.router.navigateByUrl(`conformance/conformance-tests`, {
      state: { submittedTest },
    });
  }

  selectComponent(customComponent: SlabsCustomComponentModel) {
    this.selectedComponent = customComponent;
    this.onComponentSelect.emit(customComponent);
  }
}
