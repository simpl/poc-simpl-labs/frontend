import { SimplComponentStatus } from '../../models/enums/simpl-components/simpl-component-status.enum';
import { SimplComponent } from '../../models/interfaces/slabs-custom-components/slabs-custom-component.interface';

export const simplComponents: SimplComponent[] = [
  {
    id: 1,
    name: 'Custom Component',
    version: 'v0.1.0',
    componentStatus: SimplComponentStatus.BUILT,
    lastUpdate: '2024-03-28T19:24:20.841Z',
  },
  {
    id: 2,
    name: 'Custom Component',
    version: 'v0.1.1',
    componentStatus: SimplComponentStatus.BUILT,
    lastUpdate: '2024-04-11T19:24:20.841Z',
  },
  {
    id: 3,
    name: 'Custom Component',
    version: 'v0.2.0',
    componentStatus: SimplComponentStatus.UPLOADED,
    lastUpdate: '2024-10-03T19:24:20.841Z',
  },
];
