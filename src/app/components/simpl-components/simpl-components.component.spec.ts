import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SimplComponentsComponent } from './simpl-components.component';

describe('SimplComponentsComponent', () => {
  let component: SimplComponentsComponent;
  let fixture: ComponentFixture<SimplComponentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SimplComponentsComponent, BrowserAnimationsModule],
    }).compileComponents();

    fixture = TestBed.createComponent(SimplComponentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
