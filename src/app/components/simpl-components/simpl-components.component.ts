import { BreakpointObserver } from '@angular/cdk/layout';
import { CommonModule } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { delay, map, Observable, of, Subscription } from 'rxjs';

import { simplComponents } from './simpl-components-mock';
import { SearchFiltersFormComponent } from '../_shared/search-filters-form/search-filters-form.component';
import { LoadingService } from '../../services/loading.service';
import { SimplComponentStatus } from '../../models/enums/simpl-components/simpl-component-status.enum';
import { SimplComponent } from '../../models/interfaces/slabs-custom-components/slabs-custom-component.interface';

@Component({
  selector: 'app-simpl-components',
  standalone: true,
  imports: [
    CommonModule,
    MatGridListModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatChipsModule,
    SearchFiltersFormComponent,
  ],
  templateUrl: './simpl-components.component.html',
  styleUrl: './simpl-components.component.scss',
})
export class SimplComponentsComponent implements OnInit, OnDestroy {
  cols$ = new Observable<number>();

  simplComponents: SimplComponent[] = [];
  filteredComponents: SimplComponent[] = [];
  _simplComponents: any[] = [];

  statuses = SimplComponentStatus;

  simplComponentsSub = new Subscription();

  constructor(
    private readonly _breakpointObserver: BreakpointObserver,
    private readonly _loadingService: LoadingService,
  ) {
    this.cols$ = this._breakpointObserver
      .observe([`(max-width: ${1430}px)`, `(max-width: ${2080}px)`])
      .pipe(
        map((state) => {
          if (state.breakpoints[`(max-width: ${1430}px)`]) return 1;
          if (state.breakpoints[`(max-width: ${2080}px)`]) return 2;
          return 3;
        }),
      );
  }

  ngOnInit(): void {
    this._loadingService.setLoading(true, 'simpl-components');

    this.simplComponentsSub = of(simplComponents)
      .pipe(delay(700))
      .subscribe((sc) => {
        this.simplComponents = sc;
        this.filteredComponents = this.simplComponents;
        this._simplComponents = this.simplComponents.map((c) => ({
          name: c.name,
          status: c.componentStatus,
          version: c.version,
          lastUpdate: c.lastUpdate,
        }));

        this._loadingService.setLoading(false, 'simpl-components');
      });
  }

  ngOnDestroy(): void {
    this.simplComponentsSub.unsubscribe();
  }

  onFilterSimplComponents(event: any[]) {
    this.filteredComponents = event.map((c) => ({
      name: c.name,
      componentStatus: c.status,
      version: c.version,
      lastUpdate: c.lastUpdate,
    }));
  }
}
