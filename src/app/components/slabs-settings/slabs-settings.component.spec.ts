import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SlabsSettingsComponent } from './slabs-settings.component';

describe('SlabsSettingsComponent', () => {
  let component: SlabsSettingsComponent;
  let fixture: ComponentFixture<SlabsSettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SlabsSettingsComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(SlabsSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
