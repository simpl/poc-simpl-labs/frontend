import { Component } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';

@Component({
  selector: 'app-slabs-settings',
  standalone: true,
  imports: [MatGridListModule, MatCardModule],
  templateUrl: './slabs-settings.component.html',
  styleUrl: './slabs-settings.component.scss',
})
export class SlabsSettingsComponent {}
