import {
  Component,
  Input,
  OnChanges,
  OnDestroy,
  SimpleChanges,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { MatTableModule } from '@angular/material/table';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

import mapDataSpaceStatus2Chip from '../../../../helpers/data-space-chip-colors';
import { CurrentConsumption } from '../../../../models/interfaces/monitoring/current-consumption.interface';
import { MonitoringService } from '../../../../services/monitoring.service';
import { SumPipe } from '../../../../pipes/sum.pipe';
import { CurrentConsumptionDataSource } from '../../../../models/classes/current-consumption-data-source';
import { optimiseDecimalDigitsFcn } from '../../../../helpers/optimise-decimal-digits.function';

@Component({
  selector: 'app-current-consumption',
  standalone: true,
  imports: [
    CommonModule,
    MatTableModule,
    MatChipsModule,
    MatIconModule,
    MatButtonModule,
    SumPipe,
  ],
  templateUrl: './current-consumption.component.html',
  styleUrl: './current-consumption.component.scss',
})
export class CurrentConsumptionComponent implements OnChanges, OnDestroy {
  @Input() elapsedTime = 0;

  currentConsumptions: CurrentConsumption[] = [];

  status2ChipColor = mapDataSpaceStatus2Chip;

  currentConsumptionDataSource!: CurrentConsumptionDataSource;

  currentConsumptionCols = [
    'name',
    'status',
    'cpu',
    'memory',
    'storage',
    'detailView',
  ];

  secondHeaderCols: string[] = [];
  firstFooterCols: string[] = [];

  mapProp2Column = new Map<string, string>();

  optimiseDecimalDigits = optimiseDecimalDigitsFcn;

  currentConsumptionsSub = new Subscription();

  constructor(
    private readonly monitoringService: MonitoringService,
    private readonly router: Router,
  ) {
    this.currentConsumptionCols.forEach((col) => {
      switch (col) {
        case 'name':
          this.mapProp2Column.set(col, 'Data Space');
          break;
        case 'status':
          this.mapProp2Column.set(col, 'Status');
          break;
        case 'cpu':
          this.mapProp2Column.set(col, 'CPU');
          break;
        default:
          this.mapProp2Column.set(col, col);
          return;
      }
    });

    this.secondHeaderCols = this.currentConsumptionCols.map((col) => `_${col}`);

    this.firstFooterCols = this.currentConsumptionCols.map((col) =>
      col === 'name' ? 'total' : `__${col}`,
    );

    this.currentConsumptionDataSource = new CurrentConsumptionDataSource(
      this.currentConsumptions,
    );
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('elapsedTime')) {
      const oldElapsedTime = changes['elapsedTime'].previousValue as number;
      const newElapsedTime = changes['elapsedTime'].currentValue as number;

      if (newElapsedTime !== oldElapsedTime)
        this.currentConsumptionsSub = this.monitoringService
          .getCurrentConsumptions('general')
          .subscribe((currentConsumptions) => {
            this.currentConsumptions =
              currentConsumptions as CurrentConsumption[];
            this.currentConsumptionDataSource.setData([
              ...(currentConsumptions as CurrentConsumption[]),
            ]);
          });
    }
  }

  ngOnDestroy(): void {
    this.currentConsumptionsSub.unsubscribe();
  }

  viewDataSpaceDetail(currentConsumption: CurrentConsumption) {
    this.router.navigateByUrl(
      `monitoring/infrastructure/data-space/${currentConsumption.id}`,
    );
  }
}
