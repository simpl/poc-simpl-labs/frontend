import * as echarts from 'echarts/core';
import { GridComponentOption, MarkLineComponentOption } from 'echarts';
import { LineSeriesOption } from 'echarts/charts';

export type HistoricalConsumptionOption = echarts.ComposeOption<
  LineSeriesOption | GridComponentOption | MarkLineComponentOption
>;

export const historicalConsumptionOption: HistoricalConsumptionOption = {
  xAxis: {
    type: 'category',
    axisTick: {
      show: false,
    },
    boundaryGap: false,
    data: [],
    min: undefined,
    max: undefined,
  },
  yAxis: {
    type: 'value',
    axisTick: {
      show: false,
    },
    boundaryGap: ['50%', 0],
    min: 0,
  },
  grid: {
    left: 40,
    right: 20,
    bottom: 20,
    top: 20,
    show: true,
  },
  series: [
    {
      triggerLineEvent: true,
      animation: true,
      animationDuration: 5,
      animationDurationUpdate: 5,
      animationEasing: 'backInOut',
      animationEasingUpdate: 'circularIn',
      name: 'consumption',
      type: 'line',
      symbolSize: 1,
      lineStyle: {
        width: 3,
      },
      data: [],
    },
  ],
};
