import {
  ComponentFixture,
  ComponentFixtureAutoDetect,
  TestBed,
} from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { InfrastructureMonitoringComponent } from './infrastructure-monitoring.component';
import { MonitoringChartService } from '../../../services/monitoring-chart.service';
import { ConsumptionType } from '../../../models/enums/monitoring/consumption-type.enum';
import { LoadingService } from '../../../services/loading.service';
import { firstValueFrom, timer } from 'rxjs';

let MockLoadingService: Partial<LoadingService> = {
  monitoringTimer: 0,
  updateMonitoringTimer() {},
};

let MockMonitoringChartService: Partial<MonitoringChartService> = {
  timerInterval: 3000,
  counter: null!,
  elapsedTime: null!,
  tenantCPUData: undefined,
  tenantMemoryData: undefined,
  tenantStorageData: undefined,
  minTime: null!,
  maxTime: null!,
  tenantCPUChart: null!,
  tenantMemoryChart: null!,
  tenantStorageChart: null!,
  consumptionColors: {
    [ConsumptionType.CPU]: '#4387AF',
    [ConsumptionType.MEMORY]: '#001D6C',
    [ConsumptionType.STORAGE]: '#540CAC',
    tenantBg: '#f2f4f8',
    historicalAvg: '#da1e28',
    historicalBg: '#fcfcfc',
  },

  initializeTenantCharts() {},
  initializeHistoricalCharts() {},

  resetTenantCharts() {
    this.counter = 1;
    this.elapsedTime = 2;
    this.tenantCPUData = [
      [1, 2],
      [3, 4],
    ];
    this.tenantMemoryData = [];
    this.tenantStorageData = [[3, 4]];
    this.minTime = 99;
    this.maxTime = -123;
    !!this.tenantCPUChart && this.tenantCPUChart.dispose();
    !!this.tenantMemoryChart && this.tenantMemoryChart.dispose();
    !!this.tenantStorageChart && this.tenantStorageChart.dispose();
  },
};

describe('InfrastructureMonitoringComponent', () => {
  let component: InfrastructureMonitoringComponent;
  let fixture: ComponentFixture<InfrastructureMonitoringComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        InfrastructureMonitoringComponent,
        BrowserAnimationsModule,
        HttpClientTestingModule,
      ],
      providers: [
        {
          provide: MonitoringChartService,
          useValue: MockMonitoringChartService,
        },
        {
          provide: LoadingService,
          useValue: MockLoadingService,
        },
        { provide: ComponentFixtureAutoDetect, useValue: true },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(InfrastructureMonitoringComponent);
    fixture.detectChanges();
    TestBed.inject(MonitoringChartService);
    TestBed.inject(LoadingService);
    component = fixture.debugElement.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('#_elapsedTime should be undefined after createComponent()', () => {
    expect(component._elapsedTime).toBeUndefined();
  });

  it('#_elapsedTime should be defined', async () => {
    await firstValueFrom(timer(0, MockMonitoringChartService.timerInterval!));
    expect(component._elapsedTime).toBe(0);
  });

  it('should reset all the Tenant chart data and dispose the charts', () => {
    expect(MockMonitoringChartService.timerInterval).toBe(3000);
    expect(MockMonitoringChartService.counter).toBe(1);
    expect(MockMonitoringChartService.elapsedTime).toBe(2);
    expect(MockMonitoringChartService.tenantCPUData?.length).toBe(2);
    expect(MockMonitoringChartService.tenantMemoryData?.length).toBe(0);
    expect(MockMonitoringChartService.tenantStorageData?.length).toBe(1);
    expect(MockMonitoringChartService.minTime).toBe(99);
    expect(MockMonitoringChartService.maxTime).toBe(-123);
    expect(MockMonitoringChartService.tenantCPUChart).toBeNull();
    expect(MockMonitoringChartService.tenantMemoryChart).toBeNull();
    expect(MockMonitoringChartService.tenantStorageChart).toBeNull();
  });
});
