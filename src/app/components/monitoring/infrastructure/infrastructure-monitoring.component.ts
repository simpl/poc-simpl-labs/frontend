import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';
import { Subscription, timer, tap, map } from 'rxjs';

import { CurrentConsumptionComponent } from './current-consumption/current-consumption.component';
import { TenantConsumptionComponent } from './tenant-consumption/tenant-consumption.component';
import { LoadingService } from '../../../services/loading.service';
import { HistoricalConsumptionComponent } from './historical-consumption/historical-consumption.component';
import { MonitoringChartService } from '../../../services/monitoring-chart.service';
import { ConsumptionDomain } from '../../../models/enums/monitoring/consumption-domain.enum';

@Component({
  selector: 'app-monitoring',
  standalone: true,
  imports: [
    MatTabsModule,
    MatIconModule,
    TenantConsumptionComponent,
    CurrentConsumptionComponent,
    HistoricalConsumptionComponent,
  ],
  templateUrl: './infrastructure-monitoring.component.html',
  styleUrl: './infrastructure-monitoring.component.scss',
})
export class InfrastructureMonitoringComponent implements OnInit, OnDestroy {
  _elapsedTime!: number;
  _historicalDomain: string | ConsumptionDomain = ConsumptionDomain.GENERAL;

  timerSubscription = new Subscription();

  constructor(
    private readonly monitoringChartService: MonitoringChartService,
    private readonly _loadingService: LoadingService,
  ) {}

  ngOnInit(): void {
    this.monitoringChartService.resetTenantCharts();

    this.timerSubscription = timer(0, this.monitoringChartService.timerInterval)
      .pipe(
        tap((_timer) => this._loadingService.updateMonitoringTimer(_timer)),
        map((_timer) => (this._elapsedTime = _timer)),
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.timerSubscription.unsubscribe();
  }
}
