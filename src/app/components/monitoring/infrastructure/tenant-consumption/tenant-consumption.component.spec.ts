import {
  ComponentFixture,
  ComponentFixtureAutoDetect,
  TestBed,
} from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { TenantConsumptionComponent } from './tenant-consumption.component';
import { MonitoringService } from '../../../../services/monitoring.service';
import { of, Subscription } from 'rxjs';
import { ConsumptionType } from '../../../../models/enums/monitoring/consumption-type.enum';

describe('TenantConsumptionComponent', () => {
  let component: TenantConsumptionComponent;
  let fixture: ComponentFixture<TenantConsumptionComponent>;
  let mockMonitoringService: jasmine.SpyObj<MonitoringService>;

  beforeEach(async () => {
    const monitoringServiceSpy = jasmine.createSpyObj('MonitoringService', [
      'consumptionColors',
      'getTenantConsumption',
      'getCurrentConsumptions',
      'initializeTenantCharts',
      'resetTenantCharts',
    ]);

    await TestBed.configureTestingModule({
      imports: [TenantConsumptionComponent, HttpClientTestingModule],
      providers: [
        { provide: MonitoringService, useValue: monitoringServiceSpy },
        { provide: ComponentFixtureAutoDetect, useValue: true },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(TenantConsumptionComponent);
    component = fixture.componentInstance;
    TestBed.inject(MonitoringService);
    mockMonitoringService = TestBed.inject(
      MonitoringService,
    ) as jasmine.SpyObj<MonitoringService>;
    component.elapsedTime = 0;
    component.tenantConsumptions = [
      {
        name: 'CPU',
        unitOfMeasurement: 'cores',
        allocatable: 23.43,
        allocated: 0.14,
        used: 0.00001,
        date: '2024-09-18T16:53:37.580646400',
      },
    ];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call #getConsumption()', async () => {
    const stubTenantConsumption = of({
      name: 'CPU',
      unitOfMeasurement: 'cores',
      allocatable: 23.43,
      allocated: 0.14,
      used: 0.00001,
      date: '2024-09-18T16:53:37.580646400',
    });
    mockMonitoringService.getTenantConsumption.and.returnValue(
      stubTenantConsumption,
    );

    expect(component.getConsumption(ConsumptionType.CPU))
      .withContext('service returned stub #tenantConsumption')
      .toBeInstanceOf(Subscription);
    expect(mockMonitoringService.getTenantConsumption.calls.count())
      .withContext('spy method was called once')
      .toBe(1);
    expect(
      mockMonitoringService.getTenantConsumption.calls.mostRecent().returnValue,
    ).toBe(stubTenantConsumption);
  });
});
