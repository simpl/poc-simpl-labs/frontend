import { CommonModule } from '@angular/common';
import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  QueryList,
  SimpleChanges,
  ViewChildren,
} from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatGridListModule } from '@angular/material/grid-list';
import { map, Observable, Subscription } from 'rxjs';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';

import { ConsumptionType } from '../../../../models/enums/monitoring/consumption-type.enum';
import { TenantConsumption } from '../../../../models/interfaces/monitoring/tenant-consumption.interface';
import { MonitoringService } from '../../../../services/monitoring.service';
import { optimiseDecimalDigitsFcn } from '../../../../helpers/optimise-decimal-digits.function';
import { MonitoringChartService } from '../../../../services/monitoring-chart.service';

@Component({
  selector: 'app-tenant-consumption',
  standalone: true,
  imports: [CommonModule, MatGridListModule, MatCardModule, MatDividerModule],
  templateUrl: './tenant-consumption.component.html',
  styleUrl: './tenant-consumption.component.scss',
})
export class TenantConsumptionComponent
  implements OnChanges, AfterViewInit, OnDestroy
{
  @Input() elapsedTime = 0;
  @ViewChildren('tenantCharts') tenantCharts!: QueryList<
    ElementRef<HTMLElement>
  >;

  idPrefix = 'tenant-';
  consumptionTypes = ConsumptionType;
  tenantConsumptions: TenantConsumption[] = [];
  map2color = new Map<ConsumptionType | string, string>();
  cols$ = new Observable<number>();

  optimiseDecimalDigits = optimiseDecimalDigitsFcn;

  tenantConsumptionSubs: Subscription[] = [];

  constructor(
    private readonly _breakpointObserver: BreakpointObserver,
    private readonly monitoringService: MonitoringService,
    private readonly monitoringChartService: MonitoringChartService,
  ) {
    this.cols$ = this._breakpointObserver
      .observe([Breakpoints.XSmall, Breakpoints.Small, Breakpoints.Medium])
      .pipe(
        map((state) => {
          if (
            state.breakpoints[Breakpoints.XSmall] ||
            state.breakpoints[Breakpoints.Small]
          )
            return 1;

          if (state.breakpoints[Breakpoints.Medium]) return 2;

          return 3;
        }),
      );

    Object.values(this.consumptionTypes).forEach((consumptionName) => {
      this.map2color.set(
        consumptionName,
        this.monitoringChartService.consumptionColors[consumptionName],
      );

      const consumption: TenantConsumption = {
        allocated: 0,
        allocatable: 0,
        used: 0,
        name: consumptionName,
        unitOfMeasurement:
          consumptionName === ConsumptionType.CPU ? 'cores' : 'GB',
      };

      this.tenantConsumptions.push(consumption);
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('elapsedTime')) {
      const oldElapsedTime = changes['elapsedTime'].previousValue as number;
      const newElapsedTime = changes['elapsedTime'].currentValue as number;

      if (newElapsedTime !== oldElapsedTime)
        this.tenantConsumptions.forEach(
          (consumption, i) =>
            (this.tenantConsumptionSubs[i] = this.getConsumption(
              consumption.name.toLowerCase(),
            )),
        );
    }
  }

  ngAfterViewInit(): void {
    this.tenantConsumptions.forEach((tenantConsumption, i) =>
      this.monitoringChartService.initializeTenantCharts(
        tenantConsumption.name,
        this.tenantCharts.get(i) as ElementRef<HTMLElement>,
      ),
    );
  }

  ngOnDestroy(): void {
    this.tenantConsumptionSubs.forEach((sub) => sub.unsubscribe());
    this.monitoringChartService.resetTenantCharts();
  }

  getConsumption(endPoint: ConsumptionType | string): Subscription {
    return this.monitoringService
      .getTenantConsumption(endPoint.toLowerCase())
      .subscribe((tenantConsumption) => {
        const idx = this.tenantConsumptions.findIndex(
          (tc) =>
            tc.name.toUpperCase() === tenantConsumption.name.toUpperCase(),
        );

        if (idx >= 0) this.tenantConsumptions[idx] = tenantConsumption;

        this.monitoringChartService.updateTenantCharts(
          tenantConsumption.used,
          endPoint,
        );
      });
  }
}
