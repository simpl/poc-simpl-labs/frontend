import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SimpleChange } from '@angular/core';
import { map, of, Subject } from 'rxjs';

import { DataSpaceCurrentConsumptionComponent } from './data-space-current-consumption.component';
import { CurrentConsumption } from '../../../../models/interfaces/monitoring/current-consumption.interface';
import { Page } from '../../../../models/interfaces/monitoring/paginated-endpoint.type';
import { currentConsumptionsMock } from '../../../../../mocks/current-consumption-mock';
import { PaginatedDataSource } from '../../../../models/classes/paginated-data-source';
import { MonitoringService } from '../../../../services/monitoring.service';
import { dataSpacesMock } from '../../../../../mocks/data-space-mock';
import { DataSpaceStatus } from '../../../../models/enums/data-space/data-space-status.enum';
import { NodeType } from '../../../../models/enums/data-space/node-type.enum';
import { graphChartDataMock } from '../../../../../tests/data-spaces/graph-chart-datum-mock';
import { TreeMenuService } from '../../../../services/tree-menu.service';
import { GraphChartDatum } from '../../../../models/interfaces/graph-chart-datum.interface';

const currentConsumptionMock: CurrentConsumption = {
  id: 1,
  name: 'string',
  status: DataSpaceStatus.RUNNING,
  cpuAllocated: 1,
  cpuUsed: 1,
  memoryAllocated: 1,
  memoryUsed: 1,
  storageAllocated: 1,
  storageUsed: 1,
  componentId: 1,
  componentName: 'string',
  nodeId: 1,
  nodeName: NodeType.GOVERNANCE.toString(),
  nodeType: NodeType.GOVERNANCE,
  categoryId: 1,
  categoryName: 'string',
  unitOfMeasurement: 'string',
};

describe('DataSpaceCurrentConsumptionComponent', () => {
  let component: DataSpaceCurrentConsumptionComponent;
  let fixture: ComponentFixture<DataSpaceCurrentConsumptionComponent>;
  let monitoringServiceSpy: jasmine.SpyObj<MonitoringService>;
  let treeMenuServiceSpy: jasmine.SpyObj<TreeMenuService>;

  beforeEach(async () => {
    monitoringServiceSpy = jasmine.createSpyObj('MonitoringService', [
      'getCurrentConsumptions',
    ]);
    treeMenuServiceSpy = jasmine.createSpyObj('TreeMenuService', [
      'updateBreadcrumbs',
    ]);

    treeMenuServiceSpy.graphChartNodeSubject$ = new Subject<GraphChartDatum>();

    await TestBed.configureTestingModule({
      imports: [
        DataSpaceCurrentConsumptionComponent,
        HttpClientTestingModule,
        BrowserAnimationsModule,
      ],
      providers: [
        {
          provide: MonitoringService,
          useValue: monitoringServiceSpy,
        },
        {
          provide: TreeMenuService,
          useValue: treeMenuServiceSpy,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(DataSpaceCurrentConsumptionComponent);
    component = fixture.componentInstance;
    component.paginator = {
      number: 0,
      size: 5,
      totalElements: 14,
      totalPages: 3,
    };
    component.dataSource = new PaginatedDataSource<CurrentConsumption>(
      (pageReq) =>
        monitoringServiceSpy
          .getCurrentConsumptions(
            component.currentDomain,
            component.currentId,
            pageReq,
          )
          .pipe(
            map((p) => component.setPagedData(p as Page<CurrentConsumption>)),
          ),
      { property: 'nodeId', order: 'desc' },
      component.paginator?.size,
    );
    fixture.detectChanges();
    const domainChange = new SimpleChange(undefined, 'dataspace', true);
    component.ngOnChanges({ currentDomain: domainChange });
    const idChange = new SimpleChange(undefined, dataSpacesMock[1].id, true);
    component.ngOnChanges({ currentDomain: idChange });
    const sortByChange = new SimpleChange(undefined, 'nodeId', true);
    component.ngOnChanges({ currentDomain: sortByChange });

    spyOn(component, 'updateDataSource').and.callThrough();

    component.dataSource.page$ = of<Page<CurrentConsumption>>({
      content: currentConsumptionsMock,
      page: {
        size: 0,
        number: 0,
        totalElements: 0,
        totalPages: 0,
      },
    });
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set #paginator.totalElements value w/ #setPagedData execution', () => {
    component.setPagedData({
      content: [currentConsumptionMock],
      page: {
        size: 5,
        number: 1,
        totalElements: 14,
        totalPages: 3,
      },
    });
    expect(component.paginator.totalElements).toBe(14);
  });

  it('should call service.getCurrentConsumptions() w/ #updateDataSource execution', () => {
    component.updateDataSource('current-domain', 2, 5, 'nodeId');
    expect(monitoringServiceSpy.getCurrentConsumptions).toHaveBeenCalled();
  });

  it('should call service.getCurrentConsumptions() w/ #updateDataSource execution', () => {
    component.onPaginatorChange({
      length: 14,
      pageIndex: 1,
      pageSize: 5,
      previousPageIndex: 0,
    });
    expect(component.updateDataSource).toHaveBeenCalled();
  });

  it('should view details', () => {
    spyOn(component, 'updateTreeMenuBreadcrumbs');
    component.viewDetails(currentConsumptionMock);
    expect(component.updateTreeMenuBreadcrumbs).toHaveBeenCalled();
  });

  it('should update TreeMenuBreadcrumbs', () => {
    component.updateTreeMenuBreadcrumbs(graphChartDataMock[0]);
    expect(treeMenuServiceSpy.updateBreadcrumbs).toHaveBeenCalled();
  });
});
