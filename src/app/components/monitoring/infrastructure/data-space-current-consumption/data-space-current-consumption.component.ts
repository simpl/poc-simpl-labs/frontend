import {
  AfterViewInit,
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { map, Subscription, timer } from 'rxjs';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatPaginatorModule, PageEvent } from '@angular/material/paginator';

import { CurrentConsumption } from '../../../../models/interfaces/monitoring/current-consumption.interface';
import { MonitoringService } from '../../../../services/monitoring.service';
import { optimiseDecimalDigitsFcn } from '../../../../helpers/optimise-decimal-digits.function';
import { SLabsFlatNode } from '../../../../models/interfaces/tree-node.interface';
import { NodeType } from '../../../../models/enums/data-space/node-type.enum';
import { TitlecaseNodeName } from '../../../../pipes/titlecase-node-name.pipe';
import { PaginatedDataSource } from '../../../../models/classes/paginated-data-source';
import {
  Page,
  Paginator,
} from '../../../../models/interfaces/monitoring/paginated-endpoint.type';
import { MonitoringChartService } from '../../../../services/monitoring-chart.service';
import { TreeMenuService } from '../../../../services/tree-menu.service';
import { GraphChartDatum } from '../../../../models/interfaces/graph-chart-datum.interface';
import { chartNode2SlabsFlatNode } from '../../../../helpers/chart-node-2-slabs-flat-node.map';
import { ConsumptionDomain } from '../../../../models/enums/monitoring/consumption-domain.enum';

@Component({
  selector: 'app-data-space-current-consumption',
  standalone: true,
  imports: [
    CommonModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatPaginatorModule,
    TitlecaseNodeName,
  ],
  templateUrl: './data-space-current-consumption.component.html',
  styleUrl: './data-space-current-consumption.component.scss',
})
export class DataSpaceCurrentConsumptionComponent
  implements OnChanges, OnInit, AfterViewInit, OnDestroy
{
  @Input() currentDomain: string | ConsumptionDomain =
    ConsumptionDomain.DATASPACE;
  @Input() currentId!: number;
  @Input() sortBy: keyof CurrentConsumption = 'nodeId';

  lastElapsedTime!: number;
  nextElapsedTime!: number;
  paginator!: Paginator;

  tableColumns = [
    'nodeName',
    'categoryName',
    'componentName',
    'cpu',
    'memory',
    'storage',
    'details',
  ];

  secondHeaderCols = this.tableColumns.map((col) => `_${col}`);

  optimiseDecimalDigits = optimiseDecimalDigitsFcn;

  currentConsumptions!: CurrentConsumption[];

  dataSource!: PaginatedDataSource<CurrentConsumption>;

  treeFlatNode!: SLabsFlatNode;

  treeMenuNodeSub = new Subscription();
  graphChartNodeSub = new Subscription();
  timerSubscription = new Subscription();
  dataSourcePageSub = new Subscription();

  constructor(
    private readonly monitoringService: MonitoringService,
    private readonly monitoringChartService: MonitoringChartService,
    private readonly treeMenuService: TreeMenuService,
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('currentDomain'))
      this.currentDomain =
        changes['currentDomain'].currentValue ?? this.currentDomain;

    if (changes.hasOwnProperty('currentId'))
      this.currentId = changes['currentId'].currentValue ?? this.currentId;

    if (changes.hasOwnProperty('sortBy'))
      this.sortBy = changes['sortBy'].currentValue ?? this.sortBy;

    if (this.currentDomain || this.currentId || this.sortBy)
      this.updateDataSource(
        this.currentDomain,
        this.currentId,
        this.paginator?.size || 5,
        this.sortBy,
      );
  }

  ngOnInit(): void {
    this.dataSourcePageSub = this.dataSource?.page$.subscribe((p) => {
      this.paginator = p.page;
      this.currentConsumptions = p.content;
    });
  }

  ngAfterViewInit(): void {
    this.timerSubscription = timer(0, this.monitoringChartService.timerInterval)
      .pipe(
        map((_timer) => {
          _timer === 0 &&
            this.updateDataSource(
              this.currentDomain,
              this.currentId,
              this.paginator?.size,
            );
          this.dataSource.fetch(this.paginator ? this.paginator.number : 0);
        }),
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.timerSubscription.unsubscribe();
    this.treeMenuNodeSub.unsubscribe();
    this.graphChartNodeSub.unsubscribe();
  }

  updateDataSource(
    currentDomain: string | ConsumptionDomain,
    currentId: number,
    size: number = 5,
    sortBy: keyof CurrentConsumption = 'nodeId',
  ) {
    this.dataSource = new PaginatedDataSource<CurrentConsumption>(
      (pageReq) =>
        this.monitoringService
          .getCurrentConsumptions(currentDomain, currentId, pageReq)
          .pipe(map((p) => this.setPagedData(p as Page<CurrentConsumption>))),
      { property: sortBy, order: 'desc' },
      size,
    );
  }

  setPagedData(page: Page<CurrentConsumption>): Page<CurrentConsumption> {
    this.paginator && (this.paginator.totalElements = page.page.totalElements);
    page.content = page.content.map((c) => {
      c.nodeName =
        c.nodeName.toUpperCase() === `${NodeType.GOVERNANCE.toUpperCase()}`
          ? `${NodeType.GOVERNANCE}_AUTHORITY`
          : c.nodeName;
      return c;
    });
    return page;
  }

  onPaginatorChange(event: PageEvent) {
    this.paginator.number = event.pageIndex;
    this.paginator.size = event.pageSize;
    this.paginator.totalElements = event.length;

    this.updateDataSource(this.currentDomain, this.currentId, event.pageSize);
  }

  viewDetails(currentConsumption: CurrentConsumption) {
    let chartNode: GraphChartDatum = {
      id: currentConsumption.nodeId.toString(),
      name: currentConsumption.nodeName,
      level: 0,
      x: null!,
      y: null!,
      type: currentConsumption.nodeType,
      collapsed: false,
    };
    this.updateTreeMenuBreadcrumbs(chartNode);

    chartNode = {
      ...chartNode,
      level: 1,
      id: currentConsumption.categoryId.toString(),
      name: currentConsumption.categoryName,
    };
    this.updateTreeMenuBreadcrumbs(chartNode);

    chartNode = {
      ...chartNode,
      level: 2,
      id: currentConsumption.componentId.toString(),
      name: currentConsumption.componentName,
    };
    this.updateTreeMenuBreadcrumbs(chartNode);
  }

  updateTreeMenuBreadcrumbs(chartNode: GraphChartDatum) {
    this.treeMenuService.graphChartNodeSubject$.next(chartNode);
    this.treeMenuService.updateBreadcrumbs(
      chartNode2SlabsFlatNode(chartNode),
      true,
    );
  }
}
