import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { DataSpaceConsumptionComponent } from './data-space-consumption.component';
import { MockActivatedRoute } from '../../../../../tests/helpers/mock-activated-route.class';
import { SLabsFlatNode } from '../../../../models/interfaces/tree-node.interface';
import { MonitoringService } from '../../../../services/monitoring.service';
import { TreeMenuService } from '../../../../services/tree-menu.service';
import { GraphChartDatum } from '../../../../models/interfaces/graph-chart-datum.interface';
import {
  graphChartDataMock,
  slabsFlatNodesMock,
} from '../../../../../tests/data-spaces/graph-chart-datum-mock';
import { chartNode2SlabsFlatNode } from '../../../../helpers/chart-node-2-slabs-flat-node.map';
import { ConsumptionDomain } from '../../../../models/enums/monitoring/consumption-domain.enum';

describe('DataSpaceConsumptionComponent', () => {
  let component: DataSpaceConsumptionComponent;
  let fixture: ComponentFixture<DataSpaceConsumptionComponent>;
  let monitoringServiceSpy: jasmine.SpyObj<MonitoringService>;
  let treeMenuServiceSpy: jasmine.SpyObj<TreeMenuService>;

  beforeEach(async () => {
    monitoringServiceSpy = jasmine.createSpyObj('MonitoringService', [
      'getAllCurrentConsumptions',
      'getHistoricalConsumptions',
    ]);
    treeMenuServiceSpy = jasmine.createSpyObj('TreeMenuService', [
      'updateBreadcrumbs',
    ]);

    treeMenuServiceSpy.treeMenuNodeSubject$ = new Subject<SLabsFlatNode>();
    treeMenuServiceSpy.graphChartNodeSubject$ = new Subject<GraphChartDatum>();

    await TestBed.configureTestingModule({
      imports: [
        DataSpaceConsumptionComponent,
        HttpClientTestingModule,
        BrowserAnimationsModule,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useClass: MockActivatedRoute,
        },
        {
          provide: MonitoringService,
          useValue: monitoringServiceSpy,
        },
        {
          provide: TreeMenuService,
          useValue: treeMenuServiceSpy,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(DataSpaceConsumptionComponent);
    component = fixture.componentInstance;

    spyOn(component, 'filterNodeListByCurrentConsumption').and.callThrough();
    monitoringServiceSpy.getHistoricalConsumptions.and.returnValue(of([]));
    monitoringServiceSpy.getAllCurrentConsumptions.and.returnValue(of([]));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should treat the case with #treeFlatNode.level = -1', () => {
    const treeNode: SLabsFlatNode = {
      expandable: true,
      id: 1,
      level: -1,
      name: 'node-name',
      type: undefined!,
    };
    component.setEndPoint(treeNode);

    expect(component.ancestorNode).toBeUndefined();
    expect(component._domain).toBe(ConsumptionDomain.DATASPACE);
    expect(component._domainId).toBe(component.dataSpace.id!);
  });

  it('should treat the case with #treeFlatNode.level = 0', () => {
    const treeNode: SLabsFlatNode = {
      expandable: true,
      id: 2,
      level: 0,
      name: 'node-name',
      type: undefined!,
      expanded: true,
    };
    component.setEndPoint(treeNode);

    expect(component.ancestorNode).toEqual(treeNode);
    expect(component._domain).toBe(ConsumptionDomain.NODE);
    expect(component._domainId).toBe(treeNode.id);

    component.setEndPoint({ ...treeNode, expanded: false });

    expect(component.ancestorNode).toEqual({ ...treeNode, expanded: false });
    expect(component._domain).toBe(ConsumptionDomain.DATASPACE);
    expect(component._domainId).toBe(component.dataSpace.id!);
  });

  it('should treat the case with #treeFlatNode.level = 1', () => {
    component.ancestorNode = {
      expandable: true,
      id: 1,
      level: 0,
      name: 'ancestor-name',
      type: undefined!,
    };
    const treeNode: SLabsFlatNode = {
      expandable: true,
      id: 2,
      level: 1,
      name: 'node-name',
      type: undefined!,
      expanded: true,
    };
    component.setEndPoint(treeNode);

    expect(component._domain).toBe(ConsumptionDomain.CATEGORY);
    expect(component._domainId).toBe(treeNode.id);

    component.setEndPoint({ ...treeNode, expanded: false });

    expect(component._domain).toBe(ConsumptionDomain.NODE);
    expect(component._domainId).toBe(component.ancestorNode.id);
  });

  it('should treat the case with #treeFlatNode.level = 2', () => {
    const treeNode: SLabsFlatNode = {
      expandable: false,
      id: 123,
      level: 2,
      name: 'node-name',
      type: undefined!,
    };
    component.setEndPoint(treeNode);

    expect(component._domain).toBe(ConsumptionDomain.COMPONENT);
    expect(component._domainId).toBe(treeNode.id);
  });

  it('should treat the "default" case (e.g. #treeFlatNode.level > 2)', () => {
    const lastDomain = component._domain;
    const lastDomainId = component._domainId;
    const treeNode: SLabsFlatNode = {
      expandable: false,
      id: null!,
      level: 222,
      name: 'node-name',
      type: undefined!,
    };
    component.setEndPoint(treeNode);

    expect(component._domain).toBe(lastDomain);
    expect(component._domainId).toBe(lastDomainId);
  });

  it('should call #filterNodeListByCurrentConsumption()', () => {
    expect(component.filterNodeListByCurrentConsumption).toHaveBeenCalled();
  });

  it('should set #treeFlatNode value w/ treeMenuNodeSubject$.next()', () => {
    spyOn(component, 'setEndPoint');
    treeMenuServiceSpy.treeMenuNodeSubject$.next({ ...slabsFlatNodesMock[0] });
    expect(component.treeFlatNode).toEqual(slabsFlatNodesMock[0]);
    expect(component.setEndPoint).toHaveBeenCalledWith(slabsFlatNodesMock[0]);
  });

  it('should set #treeFlatNode value w/ graphChartNodeSubject$.next()', () => {
    spyOn(component, 'setEndPoint');
    treeMenuServiceSpy.graphChartNodeSubject$.next({
      ...graphChartDataMock[0],
    });
    expect(component.treeFlatNode).toEqual(
      chartNode2SlabsFlatNode(graphChartDataMock[0]),
    );
    expect(component.setEndPoint).toHaveBeenCalledWith(
      chartNode2SlabsFlatNode(graphChartDataMock[0]),
    );
  });
});
