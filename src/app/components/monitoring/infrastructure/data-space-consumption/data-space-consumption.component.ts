import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommonModule } from '@angular/common';
import { map, Observable, Subscription } from 'rxjs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatTabsModule } from '@angular/material/tabs';

import { ContextTreeMenuComponent } from '../../../_shared/context-tree-menu/context-tree-menu.component';
import { BreadcrumbComponent } from '../../../_shared/breadcrumb/breadcrumb.component';
import { DataSpaceModel } from '../../../../models/interfaces/data-space/data-space.interface';
import { CurrentConsumption } from '../../../../models/interfaces/monitoring/current-consumption.interface';
import { DataSpaceService } from '../../../../services/data-space.service';
import { DataSpaceCurrentConsumptionComponent } from '../data-space-current-consumption/data-space-current-consumption.component';
import { HistoricalConsumptionComponent } from '../historical-consumption/historical-consumption.component';
import { MonitoringService } from '../../../../services/monitoring.service';
import { TreeMenuService } from '../../../../services/tree-menu.service';
import { SLabsFlatNode } from '../../../../models/interfaces/tree-node.interface';
import { chartNode2SlabsFlatNode } from '../../../../helpers/chart-node-2-slabs-flat-node.map';
import { ComponentModel } from '../../../../models/interfaces/data-space/component.interface';
import { ConsumptionDomain } from '../../../../models/enums/monitoring/consumption-domain.enum';

@Component({
  selector: 'app-data-space-consumption',
  standalone: true,
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatTabsModule,
    ContextTreeMenuComponent,
    BreadcrumbComponent,
    DataSpaceCurrentConsumptionComponent,
    HistoricalConsumptionComponent,
  ],
  templateUrl: './data-space-consumption.component.html',
  styleUrl: './data-space-consumption.component.scss',
})
export class DataSpaceConsumptionComponent implements OnInit, OnDestroy {
  dataSpace$!: Observable<DataSpaceModel>;
  dataSpace!: DataSpaceModel;

  _domain: string | ConsumptionDomain = ConsumptionDomain.DATASPACE;
  _domainId!: number;
  _sortBy: keyof CurrentConsumption = 'nodeId';

  treeFlatNode!: SLabsFlatNode;
  ancestorNode!: SLabsFlatNode;

  dataSpaceSub = new Subscription();
  treeMenuNodeSub = new Subscription();
  graphChartNodeSub = new Subscription();
  currentConsumptionsSub = new Subscription();

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly dataSpaceService: DataSpaceService,
    private readonly treeMenuService: TreeMenuService,
    private readonly monitoringService: MonitoringService,
  ) {}

  ngOnInit(): void {
    this.dataSpace$ = this.activatedRoute.data.pipe(
      map((data) => data['dataSpace']),
    );

    this.dataSpaceSub = this.dataSpace$.subscribe((dataSpace) => {
      this.dataSpace = dataSpace;
      this._domainId = dataSpace.id!;

      this.currentConsumptionsSub = this.monitoringService
        .getAllCurrentConsumptions(dataSpace.id as number)
        .subscribe((currentConsumptions) =>
          this.filterNodeListByCurrentConsumption(currentConsumptions),
        );

      this.dataSpaceService.dataSpaceSubject$.next(dataSpace);
    });

    this.treeMenuNodeSub = this.treeMenuService.treeMenuNodeSubject$
      .asObservable()
      .subscribe((treeFlatNode) => {
        this.treeFlatNode = treeFlatNode;
        this.setEndPoint(treeFlatNode);
      });

    this.graphChartNodeSub = this.treeMenuService.graphChartNodeSubject$
      .asObservable()
      .subscribe((graphChartNode) => {
        this.treeFlatNode = chartNode2SlabsFlatNode(graphChartNode);
        this.setEndPoint(chartNode2SlabsFlatNode(graphChartNode));
      });
  }

  ngOnDestroy(): void {
    this.dataSpaceSub.unsubscribe();
    this.treeMenuNodeSub.unsubscribe();
    this.graphChartNodeSub.unsubscribe();
    this.currentConsumptionsSub.unsubscribe();
  }

  setEndPoint(treeNode: SLabsFlatNode) {
    const { level, expanded, id } = treeNode;

    switch (level) {
      case -1: {
        this.ancestorNode = undefined!;
        this._domain = ConsumptionDomain.DATASPACE;
        this._domainId = this.dataSpace.id!;
        this._sortBy = 'nodeId';
        break;
      }
      case 0: {
        this.ancestorNode = { ...treeNode };
        if (expanded) {
          this._domain = ConsumptionDomain.NODE;
          this._domainId = id!;
          this._sortBy = 'categoryId';
        } else {
          this._domain = ConsumptionDomain.DATASPACE;
          this._domainId = this.dataSpace.id!;
          this._sortBy = 'nodeId';
        }
        break;
      }
      case 1: {
        if (expanded) {
          this._domain = ConsumptionDomain.CATEGORY;
          this._domainId = id!;
          this._sortBy = 'componentId';
        } else {
          this._domain = ConsumptionDomain.NODE;
          this._domainId = this.ancestorNode.id!;
          this._sortBy = 'categoryId';
        }
        break;
      }
      case 2: {
        this._domain = ConsumptionDomain.COMPONENT;
        this._domainId = id!;
        break;
      }
      default:
        return;
    }
  }

  filterNodeListByCurrentConsumption(
    currentConsumptions: CurrentConsumption[],
  ) {
    const filterComponentList = (
      componentList: ComponentModel[],
    ): ComponentModel[] =>
      componentList.filter((co) =>
        currentConsumptions.some((cc) => cc.componentId === co.id),
      );

    this.dataSpace.nodeList = this.dataSpace.nodeList.filter((node) => {
      node.categoryList = node.categoryList.filter((ca) => {
        ca.componentList = filterComponentList(ca.componentList);
        return currentConsumptions.some((cc) => cc.categoryId === ca.id);
      });
      return currentConsumptions.some((cc) => cc.nodeId === node.id);
    });
  }
}
