import { CommonModule } from '@angular/common';
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

import { ConsumptionType } from '../../../../../models/enums/monitoring/consumption-type.enum';
import { SelectedPointPopupData } from '../../../../../models/interfaces/monitoring/historical-consumption.interface';
import { optimiseDecimalDigitsFcn } from '../../../../../helpers/optimise-decimal-digits.function';

@Component({
  selector: 'app-selected-point-popup',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './selected-point-popup.component.html',
  styleUrl: './selected-point-popup.component.scss',
})
export class SelectedPointPopupComponent {
  optimiseDecimalDigits = optimiseDecimalDigitsFcn;

  constructor(@Inject(MAT_DIALOG_DATA) public data: SelectedPointPopupData) {
    !this.data &&
      (this.data = {
        allocated: 1,
        name: '' as ConsumptionType,
        date: '',
        used: 0,
      });
  }
}
