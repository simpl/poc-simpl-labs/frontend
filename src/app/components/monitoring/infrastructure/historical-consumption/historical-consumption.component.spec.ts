import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormGroup, FormControl } from '@angular/forms';
import { of } from 'rxjs';

import { HistoricalConsumptionComponent } from './historical-consumption.component';
import { MonitoringChartService } from '../../../../services/monitoring-chart.service';
import { ConsumptionType } from '../../../../models/enums/monitoring/consumption-type.enum';
import { HistoricalConsumption } from '../../../../models/interfaces/monitoring/historical-consumption.interface';
import { MonitoringService } from '../../../../services/monitoring.service';

const historicalConsumptionsMock: HistoricalConsumption[] = [
  { allocated: 2, used: 1, date: 'date-mock1' },
  { allocated: 0.2, used: 0.1, date: 'date-mock2' },
];

describe('HistoricalConsumptionComponent', () => {
  let component: HistoricalConsumptionComponent;
  let fixture: ComponentFixture<HistoricalConsumptionComponent>;
  let monitoringChartServiceSpy: jasmine.SpyObj<MonitoringChartService>;
  let monitoringServiceSpy: jasmine.SpyObj<MonitoringService>;

  beforeEach(async () => {
    monitoringChartServiceSpy = jasmine.createSpyObj('MonitoringChartService', [
      'initializeHistoricalCharts',
      'updateHistoricalCharts',
    ]);

    monitoringServiceSpy = jasmine.createSpyObj('MonitoringService', [
      'getHistoricalConsumptions',
    ]);

    await TestBed.configureTestingModule({
      imports: [
        HistoricalConsumptionComponent,
        HttpClientTestingModule,
        BrowserAnimationsModule,
      ],
      providers: [
        {
          provide: MonitoringChartService,
          useValue: monitoringChartServiceSpy,
        },
        {
          provide: MonitoringService,
          useValue: monitoringServiceSpy,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(HistoricalConsumptionComponent);
    component = fixture.componentInstance;

    component.today = new Date();
    component.aWeekAgo = null;
    component.cpuRange = new FormGroup({
      start: new FormControl<Date | null>(null),
      end: new FormControl<Date | null>(null),
    });
    component.memoryRange = new FormGroup({
      start: new FormControl<Date | null>(null),
      end: new FormControl<Date | null>(null),
    });
    component.storageRange = new FormGroup({
      start: new FormControl<Date | null>(null),
      end: new FormControl<Date | null>(null),
    });
    fixture.detectChanges();

    monitoringServiceSpy.getHistoricalConsumptions.and.returnValue(
      of(historicalConsumptionsMock),
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update chart', () => {
    component.updateChart(ConsumptionType.CPU, historicalConsumptionsMock);
    expect(monitoringChartServiceSpy.updateHistoricalCharts).toHaveBeenCalled();
  });

  it('should set LastPastWeek', () => {
    component.setLastPastWeek();
    expect(component.aWeekAgo).toEqual(
      new Date(+component.today! - 6 * 24 * 3600 * 1000),
    );
    expect(component.cpuRange.value).toEqual({
      start: component.aWeekAgo,
      end: component.today,
    });
    expect(component.memoryRange.value).toEqual({
      start: component.aWeekAgo,
      end: component.today,
    });
    expect(component.storageRange.value).toEqual({
      start: component.aWeekAgo,
      end: component.today,
    });
    expect(monitoringServiceSpy.getHistoricalConsumptions).toHaveBeenCalled();
  });
});
