import {
  AfterViewInit,
  Component,
  ElementRef,
  Inject,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  QueryList,
  SimpleChanges,
  ViewChildren,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  DateAdapter,
  MAT_DATE_LOCALE,
  MatDateFormats,
  provideNativeDateAdapter,
} from '@angular/material/core';
import {
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';
import { Subscription } from 'rxjs';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatIconModule } from '@angular/material/icon';

import {
  HistoricalConsumption,
  QuantityHistoricalConsumption,
} from '../../../../models/interfaces/monitoring/historical-consumption.interface';
import { ConsumptionType } from '../../../../models/enums/monitoring/consumption-type.enum';
import { MonitoringService } from '../../../../services/monitoring.service';
import { MonitoringChartService } from '../../../../services/monitoring-chart.service';

export const DATE_FORMATS: MatDateFormats = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

export const usformat = (date: Date | null): string =>
  `${date!.getFullYear()}-${(date!.getMonth() + 1).toString().length < 2 ? '0' + (date!.getMonth() + 1).toString() : date!.getMonth() + 1}-${date!.getDate().toString().length < 2 ? '0' + date!.getDate().toString() : date!.getDate()}`;

@Component({
  selector: 'app-historical-consumption',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatButtonModule,
    MatIconModule,
  ],
  providers: [provideNativeDateAdapter(DATE_FORMATS)],
  templateUrl: './historical-consumption.component.html',
  styleUrl: './historical-consumption.component.scss',
})
export class HistoricalConsumptionComponent
  implements OnChanges, OnInit, AfterViewInit, OnDestroy
{
  @Input() historicalDomain = '';
  @Input() domainId!: number | string;

  @ViewChildren('historicalCharts') historicalCharts!: QueryList<
    ElementRef<HTMLElement>
  >;

  idPrefix = 'historical-';
  consumptionTypes = ConsumptionType;
  quantityHistoricalConsumptions: QuantityHistoricalConsumption[] = [];

  minDate: Date | null = new Date('01-01-2024');
  today: Date | null = new Date();
  aWeekAgo!: Date | null;

  cpuRange = new FormGroup({
    start: new FormControl<Date | null>(null),
    end: new FormControl<Date | null>(null),
  });

  memoryRange = new FormGroup({
    start: new FormControl<Date | null>(null),
    end: new FormControl<Date | null>(null),
  });

  storageRange = new FormGroup({
    start: new FormControl<Date | null>(null),
    end: new FormControl<Date | null>(null),
  });

  cpuHistoricalSub = new Subscription();
  memoryHistoricalSub = new Subscription();
  storageHistoricalSub = new Subscription();

  constructor(
    @Inject(MAT_DATE_LOCALE) private _locale: string,
    private readonly _adapter: DateAdapter<any>,
    private readonly monitoringService: MonitoringService,
    private readonly monitoringChartService: MonitoringChartService,
  ) {
    Object.values(this.consumptionTypes).forEach((consumptionName) => {
      const quantity: QuantityHistoricalConsumption = {
        historicalConsumptions: [],
        name: consumptionName,
      };
      this.quantityHistoricalConsumptions.push(quantity);
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    let historicalDomain;
    let domainId;

    if (changes.hasOwnProperty('historicalDomain'))
      historicalDomain = changes['historicalDomain'].currentValue;

    if (changes.hasOwnProperty('domainId'))
      domainId = changes['domainId'].currentValue;

    if (historicalDomain || domainId) this.setLastPastWeek();
  }

  ngOnInit(): void {
    this.updateLocale();

    this.cpuRange.valueChanges.subscribe((range) => {
      const { start, end } = range;

      if (start && end) {
        const startDate: Date | string = new Date(start);
        const endDate: Date | string = new Date(end);

        this.cpuHistoricalSub = this.monitoringService
          .getHistoricalConsumptions(
            this.historicalDomain,
            ConsumptionType.CPU,
            usformat(startDate),
            usformat(endDate),
            this.domainId,
          )
          .subscribe((data) => this.updateChart(ConsumptionType.CPU, data));
      }
    });

    this.memoryRange.valueChanges.subscribe((range) => {
      const { start, end } = range;

      if (start && end) {
        const startDate: Date | string = new Date(start);
        const endDate: Date | string = new Date(end);

        this.memoryHistoricalSub = this.monitoringService
          .getHistoricalConsumptions(
            this.historicalDomain,
            ConsumptionType.MEMORY,
            usformat(startDate),
            usformat(endDate),
            this.domainId,
          )
          .subscribe((data) => this.updateChart(ConsumptionType.MEMORY, data));
      }
    });

    this.storageRange.valueChanges.subscribe((range) => {
      const { start, end } = range;

      if (start && end) {
        const startDate: Date | string = new Date(start);
        const endDate: Date | string = new Date(end);

        this.storageHistoricalSub = this.monitoringService
          .getHistoricalConsumptions(
            this.historicalDomain,
            ConsumptionType.STORAGE,
            usformat(startDate),
            usformat(endDate),
            this.domainId,
          )
          .subscribe((data) => this.updateChart(ConsumptionType.STORAGE, data));
      }
    });
  }

  ngAfterViewInit(): void {
    this.quantityHistoricalConsumptions.forEach((quantity, i) =>
      this.monitoringChartService.initializeHistoricalCharts(
        quantity.name,
        this.historicalCharts.get(i) as ElementRef<HTMLElement>,
      ),
    );
  }

  ngOnDestroy(): void {
    this.cpuHistoricalSub.unsubscribe();
    this.memoryHistoricalSub.unsubscribe();
    this.storageHistoricalSub.unsubscribe();
  }

  setLastPastWeek() {
    this.aWeekAgo = new Date(+this.today! - 6 * 24 * 3600 * 1000);
    this.cpuRange.setValue({ start: this.aWeekAgo, end: this.today });
    this.memoryRange.setValue({ start: this.aWeekAgo, end: this.today });
    this.storageRange.setValue({ start: this.aWeekAgo, end: this.today });

    let startDate: Date | string = new Date(this.aWeekAgo);
    let endDate: Date | string = new Date(this.today!);
    startDate = usformat(startDate);
    endDate = usformat(endDate);

    this.cpuHistoricalSub = this.monitoringService
      .getHistoricalConsumptions(
        this.historicalDomain,
        ConsumptionType.CPU,
        startDate,
        endDate,
        this.domainId,
      )
      .subscribe((data) => this.updateChart(ConsumptionType.CPU, data));
    this.memoryHistoricalSub = this.monitoringService
      .getHistoricalConsumptions(
        this.historicalDomain,
        ConsumptionType.MEMORY,
        startDate,
        endDate,
        this.domainId,
      )
      .subscribe((data) => this.updateChart(ConsumptionType.MEMORY, data));
    this.storageHistoricalSub = this.monitoringService
      .getHistoricalConsumptions(
        this.historicalDomain,
        ConsumptionType.STORAGE,
        startDate,
        endDate,
        this.domainId,
      )
      .subscribe((data) => this.updateChart(ConsumptionType.STORAGE, data));
  }

  updateChart(
    name: ConsumptionType,
    historicalConsumptions: HistoricalConsumption[],
  ) {
    const consumptionData: (string | number)[][] = historicalConsumptions.map(
      (h) => {
        const month = new Date(new Date(h.date).getTime()).getMonth() + 1;
        const day = new Date(new Date(h.date).getTime()).getDate();
        return [month + '/' + day, (h.used / h.allocated) * 100];
      },
    );

    const quantity = this.quantityHistoricalConsumptions.find(
      (qhc) => qhc.name === name,
    )!;

    quantity.historicalConsumptions = historicalConsumptions;

    quantity.average =
      consumptionData
        .map((d) => +d[1])
        .reduce((sum, elm) => {
          return sum + elm;
        }, 0) / consumptionData.length;

    this.monitoringChartService.updateHistoricalCharts(quantity);
  }

  updateLocale() {
    this._locale = 'it';
    this._adapter.setLocale(this._locale);
  }
}
