import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Subject } from 'rxjs';

import { TemplatePreviewComponent } from './template-preview.component';
import { TemplateModel } from '../../models/interfaces/template.interface';
import { basicTemplatesMock } from '../wizard/constants/data-space-creation.data';
import { MockActivatedRoute } from '../../../tests/helpers/mock-activated-route.class';
import { TreeMenuService } from '../../services/tree-menu.service';
import { SLabsFlatNode } from '../../models/interfaces/tree-node.interface';
import { GraphChartDatum } from '../../models/interfaces/graph-chart-datum.interface';
import { CategoryType } from '../../models/enums/data-space/category-type.enum';
import {
  graphChartDataMock,
  slabsFlatNodesMock,
} from '../../../tests/data-spaces/graph-chart-datum-mock';
import { NodeModel } from '../../models/interfaces/data-space/node.interface';
import { DataSpaceService } from '../../services/data-space.service';
import { DrawerService } from '../../services/drawer.service';
import { dataSpacesMock } from '../../../mocks/data-space-mock';
import { PageContentType } from '../../models/enums/data-space/page-content-type.enum';
import { ComponentInfo } from '../../models/interfaces/data-space/component-info.interface';
import { PageContentUpdate } from '../../models/interfaces/data-space/page-content-update.interface';

const mockPreviewTemplate: TemplateModel = basicTemplatesMock[0];

class DataSpaceServiceMock {
  chartNode!: NodeModel;
  updatePageContent(treeFlatNode: SLabsFlatNode): PageContentUpdate {
    const pageContentType = treeFlatNode.expanded
      ? PageContentType.COMPONENTS_TABLE
      : PageContentType.GRAPH_CHART;

    return {
      dataSpace: mockPreviewTemplate.dataSpace,
      nodeList: mockPreviewTemplate.dataSpace.nodeList,
      pageContentType,
      tableCategory: mockPreviewTemplate.dataSpace.nodeList[1].categoryList[0],
    };
  }
  createInfoTableData() {
    return [
      {
        property: 'property-mock',
        value: 'value-mock',
      },
    ];
  }
}

describe('TemplatePreviewComponent', () => {
  let component: TemplatePreviewComponent;
  let fixture: ComponentFixture<TemplatePreviewComponent>;
  let treeMenuServiceSpy: jasmine.SpyObj<TreeMenuService>;
  let drawerServiceSpy: jasmine.SpyObj<DrawerService>;
  let drawerService: DrawerService;

  beforeEach(async () => {
    drawerServiceSpy = jasmine.createSpyObj('DrawerService', [
      'openComponentDrawer',
      'closeComponentDrawer',
    ]);
    treeMenuServiceSpy = jasmine.createSpyObj('TreeMenuService', [
      'updateBreadcrumbs',
    ]);

    treeMenuServiceSpy.treeMenuNodeSubject$ = new Subject<SLabsFlatNode>();
    treeMenuServiceSpy.graphChartNodeSubject$ = new Subject<GraphChartDatum>();

    await TestBed.configureTestingModule({
      imports: [
        TemplatePreviewComponent,
        HttpClientTestingModule,
        BrowserAnimationsModule,
      ],
      providers: [
        DrawerService,
        { provide: ActivatedRoute, useClass: MockActivatedRoute },
        { provide: TreeMenuService, useValue: treeMenuServiceSpy },
        { provide: DrawerService, useValue: drawerServiceSpy },
        { provide: DataSpaceService, useClass: DataSpaceServiceMock },
      ],
    }).compileComponents();

    drawerService = TestBed.inject(DrawerService);

    fixture = TestBed.createComponent(TemplatePreviewComponent);
    component = fixture.componentInstance;
    component.previewTemplate = mockPreviewTemplate;
    component._nodeList = mockPreviewTemplate.dataSpace.nodeList;
    component.chartNode = mockPreviewTemplate.dataSpace.nodeList[0];

    fixture.detectChanges();
    spyOn(component, 'updatePageContent').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call #updatePageContent() on graphChartNodeSubject$.next', () => {
    treeMenuServiceSpy.graphChartNodeSubject$.next(graphChartDataMock[0]);
    expect(component.updatePageContent).toHaveBeenCalled();
  });

  it('should call #updatePageContent() on treeMenuNodeSubject$.next', () => {
    treeMenuServiceSpy.treeMenuNodeSubject$.next(slabsFlatNodesMock[0]);
    expect(component.updatePageContent).toHaveBeenCalled();
  });

  it('should fill #tableDataSource', () => {
    component.getInfoTableData();
    expect(component.infoTableDataSource.data.length).toBeGreaterThan(0);
  });

  it('should set #pageContentType equal to "GRAPH_CHART" when level = -1', () => {
    component.updatePageContent(slabsFlatNodesMock[0]);
    expect(component.pageContentType).toBe(PageContentType.GRAPH_CHART);
  });

  it('should update #pageContentType w/ level = 0 and expanded = true', () => {
    component.updatePageContent({
      ...slabsFlatNodesMock[0],
      level: 0,
      expanded: true,
      expandable: true,
    });

    expect(component.pageContentType).toBe(PageContentType.COMPONENTS_TABLE);
  });

  it('should update #pageContentType w/ level = 0 and expanded = false', () => {
    component.updatePageContent({
      ...slabsFlatNodesMock[0],
      level: 0,
      expanded: false,
      expandable: true,
    });

    expect(component.pageContentType).toBe(PageContentType.GRAPH_CHART);
  });

  it('should set #pageContentType equal to "COMPONENTS_TABLE" when level = 1', () => {
    component.updatePageContent({
      ...slabsFlatNodesMock[0],
      level: 1,
      expanded: true,
      expandable: true,
    });

    expect(component.pageContentType).toBe(PageContentType.COMPONENTS_TABLE);
  });

  it('should set #pageContentType equal to "GRAPH_CHART" when level = 1', () => {
    component.updatePageContent({
      ...slabsFlatNodesMock[0],
      level: 1,
      expanded: false,
    });

    expect(component.pageContentType).toBe(PageContentType.GRAPH_CHART);
  });

  it('should set #pageContentType equal to "GRAPH_CHART" when level = -1', () => {
    component.openComponentInfoDrawer({
      categoryType: CategoryType.IAA_LOCAL,
      comp: mockPreviewTemplate.dataSpace.nodeList[0].categoryList[0]
        .componentList[0],
    });
    expect(component.componentInfo).toEqual(
      mockPreviewTemplate.dataSpace.nodeList[0].categoryList[0]
        .componentList[0] as ComponentInfo,
    );
  });

  it('should call closeComponentDrawer()', () => {
    component._nodeList = [...dataSpacesMock[1].nodeList];
    component.chartNode = { ...dataSpacesMock[1].nodeList[0] };
    component.closeComponentInfoDrawer();
    expect(drawerService.closeComponentDrawer).toHaveBeenCalled();
  });
});
