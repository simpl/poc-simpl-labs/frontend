import { dataSpacesMock } from '../../../../mocks/data-space-mock';
import { DataSpaceModel } from '../../../models/interfaces/data-space/data-space.interface';
import { TemplateModel } from '../../../models/interfaces/template.interface';

export const mvDataSpace: DataSpaceModel = dataSpacesMock[1];

// Minimum Viable Data Space (MVDS) Template
const mvdsTemplate: TemplateModel = {
  id: 10,
  title: 'Minimum viable Data Space',
  description:
    'Use this set up to start working on a data space with its base configuration',
  author: 'Simpl',
  lastUpdate: '12/04/2024',
  license: 'Read',
  tags: ['Minimum viable Data Space', 'tag2'],
  imageUrl: '/mvds_template.png',
  isMVDS: true,
  dataSpace: mvDataSpace,
  dataSpaceId: mvDataSpace.id!,
};

// Basic Templates
export const basicTemplatesMock: TemplateModel[] = [
  mvdsTemplate,
  {
    id: 11,
    title: '5 nodes Data Space example',
    description:
      'This template is an example of data space exchanging health data',
    author: 'Mario Rossi',
    lastUpdate: 'string',
    license: 'string',
    tags: ['tag1', 'tag2'],
    imageUrl: '/template_ex1.png',
    isMVDS: false,
    dataSpace: dataSpacesMock[0],
    dataSpaceId: dataSpacesMock[0].id!,
  },
  {
    id: 12,
    title: '4 nodes Data Space example',
    description:
      'A data space to track and exchange data about the european travel industry',
    author: 'Maria Rose',
    lastUpdate: 'string',
    license: 'string',
    tags: ['tag1', 'tag2', 'tag3'],
    imageUrl: '/template_ex2.png',
    isMVDS: false,
    dataSpace: dataSpacesMock[2],
    dataSpaceId: dataSpacesMock[2].id!,
  },
];

// All Templates
export const allTemplatesMock: TemplateModel[] = [
  {
    id: 13,
    title: 'Green Deal Dataspace Example',
    description:
      'The open ecosystem GDDS is the first cross-domain data space for projects and services...',
    // '...that allows users to offer their own solutions or search for and find resources for their own projects.',
    author: 'Green Deal Dataspace e.V.',
    lastUpdate: 'string',
    license: 'string',
    tags: ['tag1', 'tag2', 'tag3', 'tag4'],
    imageUrl: '/template_ex2.png',
    isMVDS: false,
    dataSpace: dataSpacesMock[4],
    dataSpaceId: dataSpacesMock[4].id!,
  },
  {
    id: 14,
    title: 'Data space example 4',
    description:
      'This template is number 4. Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    author: 'Mariuccio Rossini',
    lastUpdate: 'string',
    license: 'string',
    tags: ['tag1', 'tag2', 'tag3'],
    imageUrl: '/template_ex1.png',
    isMVDS: false,
    dataSpace: dataSpacesMock[3],
    dataSpaceId: dataSpacesMock[3].id!,
  },
  {
    id: 15,
    title: 'Data space example 6',
    description:
      'This template is number 6. Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    author: 'Marina Rosette',
    lastUpdate: 'string',
    license: 'string',
    tags: [],
    imageUrl: '/template_ex1.png',
    isMVDS: false,
    dataSpace: dataSpacesMock[0],
    dataSpaceId: dataSpacesMock[0].id!,
  },
];

// Filters values
export const authors = [
  { id: 1, name: 'Author 1' },
  { id: 2, name: 'Author 2' },
  { id: 3, name: 'Author 3' },
];
export const policies = [
  { id: 1, name: 'Policy 1' },
  { id: 2, name: 'Policy 2' },
  { id: 3, name: 'Policy 3' },
];
export const sectors = [
  { id: 1, name: 'Sector 1' },
  { id: 2, name: 'Sector 2' },
  { id: 3, name: 'Sector 3' },
];
export const lastUpdates = [
  { id: 1, name: 'Last update 1' },
  { id: 2, name: 'Last update 2' },
  { id: 3, name: 'Last update 3' },
];
