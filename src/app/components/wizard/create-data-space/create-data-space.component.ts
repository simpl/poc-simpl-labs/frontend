import {
  AfterViewInit,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription, map } from 'rxjs';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import {
  MatStep,
  MatStepper,
  MatStepperModule,
} from '@angular/material/stepper';
import {
  FormBuilder,
  FormControl,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { MatMenuModule } from '@angular/material/menu';
import { MatDialog } from '@angular/material/dialog';

import { TemplateModel } from '../../../models/interfaces/template.interface';
import { basicTemplatesMock } from '../constants/data-space-creation.data';
import { TemplatesComponent } from '../../templates/templates.component';
import { DataSpaceService } from '../../../services/data-space.service';
import { DataSpaceSetupComponent } from '../data-space-setup/data-space-setup.component';
import { DataSpaceModel } from '../../../models/interfaces/data-space/data-space.interface';
import {
  DataSpaceDetailsComponent,
  DataSpaceDetailsModel,
} from '../data-space-details/data-space-details.component';
import { DataSpaceConditionsComponent } from '../data-space-conditions/data-space-conditions.component';
import { CreatedConfirmDialogComponent } from '../created-confirm-dialog/created-confirm-dialog.component';
import { CreatedConfirmDialogData } from '../../../models/interfaces/data-space/created-confirm-dialog-data.interface';

@Component({
  selector: 'app-create-data-space',
  standalone: true,
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { displayDefaultIndicatorType: false, showError: true },
    },
  ],
  templateUrl: './create-data-space.component.html',
  styleUrl: './create-data-space.component.scss',
  imports: [
    TemplatesComponent,
    DataSpaceSetupComponent,
    DataSpaceDetailsComponent,
    DataSpaceConditionsComponent,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatToolbarModule,
    MatButtonModule,
    MatStepperModule,
    MatInputModule,
    MatMenuModule,
  ],
})
export class CreateDataSpaceComponent
  implements OnInit, AfterViewInit, OnDestroy
{
  @ViewChild('createDataSpaceRef', { static: true })
  _createDataSpaceRef!: ElementRef;
  @ViewChild('stepper', { static: true }) _stepperRef!: MatStepper;

  _steps = new QueryList<MatStep>();
  stepperSelectedIndex = 0;

  _basicTemplates: TemplateModel[] = [...basicTemplatesMock];
  _mvdsTemplate: TemplateModel = {
    ...this._basicTemplates.find((t) => t.isMVDS)!,
  };

  templatesForm = this._fb.group({
    templatesCtrl: [this._mvdsTemplate, Validators.required],
  });
  setUpDSForm = this._fb.group({
    setUpDSCtrl: [this._mvdsTemplate.dataSpace, Validators.required],
  });
  dsDetailsForm = this._fb.group({
    dsDetailsCtrl: [null, Validators.required],
  });
  conditionsForm = this._fb.group({
    conditionsCtrl: [null, Validators.required],
  });

  get templatesCtrl(): FormControl {
    return this.templatesForm.get('templatesCtrl') as FormControl;
  }

  get setUpDSCtrl(): FormControl {
    return this.setUpDSForm.get('setUpDSCtrl') as FormControl;
  }

  get dsDetailsCtrl(): FormControl {
    return this.dsDetailsForm.get('dsDetailsCtrl') as FormControl;
  }

  get conditionsCtrl(): FormControl {
    return this.conditionsForm.get('conditionsCtrl') as FormControl;
  }

  newDataSpaceSub = new Subscription();
  newDataSpaceSetupSub = new Subscription();
  newDataSpaceDetailsSub = new Subscription();
  newDataSpaceConditionsSub = new Subscription();
  createDataSpaceSub = new Subscription();
  routeParamMapSub = new Subscription();

  constructor(
    private readonly _fb: FormBuilder,
    private readonly dataSpaceService: DataSpaceService,
    private readonly activatedRoute: ActivatedRoute,
    public dialog: MatDialog,
  ) {}

  ngOnInit(): void {
    this.newDataSpaceSub = this.dataSpaceService.newDataSpaceSubject$
      .asObservable()
      .subscribe((dataSpace: DataSpaceModel) => {
        if (dataSpace) {
          this.setUpDSCtrl?.setValue(dataSpace);
        }
      });

    this.newDataSpaceSetupSub = this.dataSpaceService.newDataSpaceSetupSubject$
      .asObservable()
      .subscribe((dataSpace) => this.setUpDSCtrl.setValue(dataSpace));

    this.newDataSpaceDetailsSub =
      this.dataSpaceService.newDataSpaceDetailsSubject$
        .asObservable()
        .subscribe((dataSpaceDetails: DataSpaceDetailsModel) => {
          if (dataSpaceDetails) {
            const newDataSpace: DataSpaceModel = this.setUpDSCtrl.value;
            newDataSpace.name = dataSpaceDetails.name;
            newDataSpace.description = dataSpaceDetails.description;
            newDataSpace.type = dataSpaceDetails.type;
            newDataSpace.tagList = dataSpaceDetails.tagList;

            if (dataSpaceDetails.name) {
              this.setUpDSCtrl.setValue(newDataSpace);
              this.dsDetailsCtrl.setValue(dataSpaceDetails);
            } else this.dsDetailsCtrl.reset();
          }
        });

    this.newDataSpaceConditionsSub =
      this.dataSpaceService.newDataSpaceConditionsSubject$
        .asObservable()
        .subscribe((conditions) => {
          conditions.every((condition) => condition)
            ? this.conditionsCtrl.setValue(conditions)
            : this.conditionsCtrl.reset();
        });
  }

  ngAfterViewInit(): void {
    this._createDataSpaceRef.nativeElement.scrollIntoView({ top: 0 });

    this._stepperRef._stepHeader.first
      ._getHostElement()
      .parentElement?.classList.add('slabs-stepper-header-container');

    setTimeout(() => {
      this._steps = this._stepperRef._steps;
      this.stepperSelectedIndex = this._stepperRef.selectedIndex;
    });

    this._stepperRef.selectedIndexChange.subscribe(() => {
      this._steps = this._stepperRef._steps;
      this.stepperSelectedIndex = this._stepperRef.selectedIndex;
    });

    this.routeParamMapSub = this.activatedRoute.paramMap
      .pipe(map(() => window.history.state.template))
      .subscribe((viewedTemplate: TemplateModel) => {
        if (viewedTemplate)
          setTimeout(() => {
            this.setUpDSCtrl.setValue(viewedTemplate.dataSpace);
            this._stepperRef.next();
          });
      });
  }

  ngOnDestroy(): void {
    this.newDataSpaceSub.unsubscribe();
    this.newDataSpaceSetupSub.unsubscribe();
    this.newDataSpaceDetailsSub.unsubscribe();
    this.newDataSpaceConditionsSub.unsubscribe();
    this.createDataSpaceSub.unsubscribe();
    this.routeParamMapSub.unsubscribe();
  }

  isButtonEnabled(): boolean {
    return (
      this._steps.get(this.stepperSelectedIndex)?.stepControl.valid ||
      (this.templatesCtrl.value &&
        this.setUpDSCtrl.value &&
        this.dsDetailsCtrl.value &&
        this.conditionsCtrl.value)
    );
  }

  createDataSpace() {
    const newDataSpace = this.dataSpaceService.newDataSpace;

    newDataSpace.user = {
      email: '',
      name: '',
      username: '',
      id: null!,
    };

    delete newDataSpace.createDate;
    delete newDataSpace.id;

    const successData: CreatedConfirmDialogData = {
      success: true,
      title: 'Your data space is being created.',
      text: 'You will receive a notification when it is ready to use.',
    };

    const errorData: CreatedConfirmDialogData = {
      success: false,
      title: 'Unfortunately your data space is not being created.',
      text: 'Something went wrong in data space creation...',
    };

    this.createDataSpaceSub = this.dataSpaceService
      .createDataSpace(newDataSpace)
      .subscribe({
        next: (newDataSpace: DataSpaceModel) => {
          if (!newDataSpace) return this.openCreatedConfirmDialog(errorData);

          this.dataSpaceService.postDataSpaceSubject$.next(newDataSpace);
          this.openCreatedConfirmDialog(successData);
        },
        error: (err: Error) => {
          this.openCreatedConfirmDialog(errorData);
          console.log(err);
        },
      });
  }

  openCreatedConfirmDialog(data: CreatedConfirmDialogData) {
    this.dialog.open(CreatedConfirmDialogComponent, {
      disableClose: true,
      data,
      panelClass: ['fullscreen-dialog'],
    });
  }
}
