import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { of, Subject } from 'rxjs';

import { CreateDataSpaceComponent } from './create-data-space.component';
import { DataSpaceService } from '../../../services/data-space.service';
import { DataSpaceModel } from '../../../models/interfaces/data-space/data-space.interface';
import { DataSpaceDetailsModel } from '../data-space-details/data-space-details.component';
import { dataSpacesMock } from '../../../../mocks/data-space-mock';

const stubParamMap: ParamMap = {
  has: (name: string) => !!name,
  get: (name: string) => name,
  getAll: (name: string) => [name],
  keys: ['a', 'b'],
};

class MockActivatedRoute {
  paramMap = of(stubParamMap);
}

const dataSpaceDetailsMock: DataSpaceDetailsModel = {
  description: dataSpacesMock[0].description,
  name: dataSpacesMock[0].name,
  tagList: dataSpacesMock[0].tagList,
  type: dataSpacesMock[0].type,
};

describe('CreateDataSpaceComponent', () => {
  let component: CreateDataSpaceComponent;
  let fixture: ComponentFixture<CreateDataSpaceComponent>;
  let dataSpaceServiceSpy: jasmine.SpyObj<DataSpaceService>;

  beforeEach(async () => {
    dataSpaceServiceSpy = jasmine.createSpyObj('DataSpaceService', [
      'createDataSpace',
    ]);

    dataSpaceServiceSpy.newDataSpaceSubject$ = new Subject<DataSpaceModel>();
    dataSpaceServiceSpy.newDataSpaceSetupSubject$ =
      new Subject<DataSpaceModel>();
    dataSpaceServiceSpy.newDataSpaceDetailsSubject$ =
      new Subject<DataSpaceDetailsModel>();
    dataSpaceServiceSpy.newDataSpaceConditionsSubject$ = new Subject<
      boolean[]
    >();
    dataSpaceServiceSpy.postDataSpaceSubject$ = new Subject<DataSpaceModel>();

    await TestBed.configureTestingModule({
      imports: [
        CreateDataSpaceComponent,
        HttpClientTestingModule,
        BrowserAnimationsModule,
      ],
      providers: [
        { provide: ActivatedRoute, useClass: MockActivatedRoute },
        { provide: DataSpaceService, useValue: dataSpaceServiceSpy },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(CreateDataSpaceComponent);
    component = fixture.componentInstance;

    dataSpaceServiceSpy.newDataSpaceSubject$.next(dataSpacesMock[1]);

    fixture.detectChanges();

    dataSpaceServiceSpy.createDataSpace.and.returnValue(of(dataSpacesMock[1]));

    spyOn(component.setUpDSCtrl, 'setValue');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call #setUpDSCtrl.setValue() w/ newDataSpaceSubject$.next()', () => {
    dataSpaceServiceSpy.newDataSpaceSubject$.next(dataSpacesMock[1]);
    expect(component.setUpDSCtrl.setValue).toHaveBeenCalled();
  });

  it('should call #setUpDSCtrl.setValue() w/ newDataSpaceSetupSubject$.next()', () => {
    dataSpaceServiceSpy.newDataSpaceSetupSubject$.next(dataSpacesMock[1]);
    expect(component.setUpDSCtrl.setValue).toHaveBeenCalled();
  });

  it('should call #dsDetailsCtrl.setValue() w/ newDataSpaceDetailsSubject$.next()', () => {
    spyOn(component.dsDetailsCtrl, 'setValue');
    dataSpaceServiceSpy.newDataSpaceDetailsSubject$.next(dataSpaceDetailsMock);
    expect(component.setUpDSCtrl.setValue).toHaveBeenCalled();
    expect(component.dsDetailsCtrl.setValue).toHaveBeenCalled();
  });

  it('should call #conditionsCtrl.setValue()', () => {
    spyOn(component.conditionsCtrl, 'setValue');
    dataSpaceServiceSpy.newDataSpaceConditionsSubject$.next([true, true]);
    expect(component.conditionsCtrl.setValue).toHaveBeenCalled();
  });

  it('should create a new DS', () => {
    dataSpaceServiceSpy.newDataSpace = dataSpacesMock[1];
    component.createDataSpace();
    expect(dataSpaceServiceSpy.createDataSpace).toHaveBeenCalled();
  });
});
