import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { AddNodesDialogComponent } from './add-nodes-dialog.component';
import { NodeType } from '../../../models/enums/data-space/node-type.enum';
import { AddNodesDialogData } from '../../../models/interfaces/data-space/add-nodes-dialog-data.interface';

const addNodesDialogDataMock: AddNodesDialogData = {
  templateNodes: [
    { name: NodeType.GOVERNANCE.toString(), type: NodeType.GOVERNANCE },
    { name: NodeType.DATA_PROVIDER.toString(), type: NodeType.DATA_PROVIDER },
    { name: NodeType.CONSUMER.toString(), type: NodeType.CONSUMER },
  ],
};

const dialogRefMock = {
  close: () => {},
};

describe('AddNodesDialogComponent', () => {
  let component: AddNodesDialogComponent;
  let fixture: ComponentFixture<AddNodesDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AddNodesDialogComponent],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: addNodesDialogDataMock },
        { provide: MatDialogRef, useValue: dialogRefMock },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(AddNodesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add a node', () => {
    const LEN = component.selectedNodes.length;
    component.addNode(addNodesDialogDataMock.templateNodes[0].name);
    expect(component.selectedNodes.length).toBe(LEN + 1);
  });

  it('should remove a node', () => {
    component.addNode(addNodesDialogDataMock.templateNodes[0].name);
    fixture.detectChanges();
    const LEN = component.selectedNodes.length;
    component.removeNode(addNodesDialogDataMock.templateNodes[0].name);
    expect(component.selectedNodes.length).toBe(LEN);
  });

  it('should close dialog', () => {
    spyOn(dialogRefMock, 'close');
    component.closeDialog();
    expect(dialogRefMock.close).toHaveBeenCalled();
  });

  it('should remove all selected nodes', () => {
    component.removeAll();
    expect(component.selectedNodes.length).toBe(0);
  });
});
