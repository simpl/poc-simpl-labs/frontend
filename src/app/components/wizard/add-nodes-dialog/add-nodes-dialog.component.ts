import { Component, Inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import {
  MatDialogTitle,
  MatDialogContent,
  MatDialogActions,
  MatDialogClose,
  MAT_DIALOG_DATA,
  MatDialogRef,
} from '@angular/material/dialog';
import { NodeType } from '../../../models/enums/data-space/node-type.enum';
import { MatIconModule } from '@angular/material/icon';
import { DS_NODE_LIST } from '../constants/available-nodes-list.data';
import { AddNodesDialogData } from '../../../models/interfaces/data-space/add-nodes-dialog-data.interface';

@Component({
  selector: 'app-add-nodes-dialog',
  standalone: true,
  imports: [
    MatDialogTitle,
    MatDialogContent,
    MatDialogActions,
    MatDialogClose,
    MatButtonModule,
    MatIconModule,
  ],
  templateUrl: './add-nodes-dialog.component.html',
  styleUrl: './add-nodes-dialog.component.scss',
})
export class AddNodesDialogComponent {
  availableNodes: string[] = DS_NODE_LIST.filter(
    (n) => n.type !== NodeType.GOVERNANCE,
  ).map((n) => n.type);
  selectedNodes: { name: string; type: NodeType }[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: AddNodesDialogData,
    public dialogRef: MatDialogRef<AddNodesDialogComponent>,
  ) {}

  addNode(nodeName: string) {
    const filteredSelected = this.data.templateNodes
      .filter((node) => node.name.includes(nodeName))
      .concat(this.selectedNodes.filter((n) => n.name.includes(nodeName)));
    const LEN = filteredSelected.length;

    this.selectedNodes.push({
      name: `${nodeName}${LEN === 0 ? '' : '_' + (LEN + 1)}`,
      type: nodeName as NodeType,
    });
  }

  removeNode(nodeName: string) {
    this.selectedNodes = this.selectedNodes.filter((n) => n.name !== nodeName);
  }

  closeDialog() {
    this.dialogRef.close();
  }

  removeAll() {
    this.selectedNodes = [];
  }
}
