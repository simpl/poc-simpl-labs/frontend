import { CommonModule } from '@angular/common';
import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';

import { CreatedConfirmDialogData } from '../../../models/interfaces/data-space/created-confirm-dialog-data.interface';

@Component({
  selector: 'app-created-confirm-dialog',
  standalone: true,
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
  ],
  templateUrl: './created-confirm-dialog.component.html',
  styleUrl: './created-confirm-dialog.component.scss',
})
export class CreatedConfirmDialogComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: CreatedConfirmDialogData,
    public dialogRef: MatDialogRef<CreatedConfirmDialogComponent>,
    private readonly router: Router,
  ) {}

  goBack() {
    this.router.navigateByUrl('data-spaces').then(() => this.dialogRef.close());
  }
}
