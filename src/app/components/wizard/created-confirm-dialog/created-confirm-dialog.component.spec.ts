import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';

import { CreatedConfirmDialogComponent } from './created-confirm-dialog.component';
import { MockRouter } from '../../../../tests/helpers/mock-router';

describe('CreatedConfirmDialogComponent', () => {
  let component: CreatedConfirmDialogComponent;
  let fixture: ComponentFixture<CreatedConfirmDialogComponent>;
  let mockRouter = new MockRouter();

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CreatedConfirmDialogComponent],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: {} },
        { provide: MatDialogRef, useValue: {} },
        {
          provide: Router,
          useValue: mockRouter,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(CreatedConfirmDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should go back', () => {
    spyOn(mockRouter, 'navigateByUrl').and.returnValue(
      new Promise((value) => value) as any,
    );
    component.goBack();
    expect(mockRouter.navigateByUrl).toHaveBeenCalled();
  });
});
