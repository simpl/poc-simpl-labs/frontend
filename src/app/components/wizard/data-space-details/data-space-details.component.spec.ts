import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatChipInput } from '@angular/material/chips';
import { Subject } from 'rxjs';

import {
  DataSpaceDetailsComponent,
  DataSpaceDetailsModel,
} from './data-space-details.component';
import { DataSpaceService } from '../../../services/data-space.service';
import { dataSpacesMock } from '../../../../mocks/data-space-mock';

const tagsMock: string[] = ['tag-1', 'tag-2'];

describe('DataSpaceDetailsComponent', () => {
  let component: DataSpaceDetailsComponent;
  let fixture: ComponentFixture<DataSpaceDetailsComponent>;
  let dataSpaceServiceSpy: jasmine.SpyObj<DataSpaceService>;

  beforeEach(async () => {
    dataSpaceServiceSpy = jasmine.createSpyObj('DataSpaceService', [
      'createDataSpace',
    ]);

    dataSpaceServiceSpy.newDataSpaceDetailsSubject$ =
      new Subject<DataSpaceDetailsModel>();

    await TestBed.configureTestingModule({
      imports: [
        DataSpaceDetailsComponent,
        HttpClientTestingModule,
        BrowserAnimationsModule,
      ],
      providers: [{ provide: DataSpaceService, useValue: dataSpaceServiceSpy }],
    }).compileComponents();

    fixture = TestBed.createComponent(DataSpaceDetailsComponent);
    component = fixture.componentInstance;
    component.dataSpaceDetailsForm = new FormGroup({
      name: new FormControl('', Validators.required),
      description: new FormControl('', Validators.maxLength(1000)),
      type: new FormControl(null),
      tagList: new FormControl([]),
    });
    component.tags.set(tagsMock);
    component.dataSpaceDetails = {
      description: dataSpacesMock[1].description,
      name: dataSpacesMock[1].name,
      tagList: dataSpacesMock[1].tagList,
      type: dataSpacesMock[1].type,
    };
    fixture.detectChanges();

    spyOn(dataSpaceServiceSpy.newDataSpaceDetailsSubject$, 'next');
    spyOn(component.dataSpaceDetailsForm, 'setValue');
    spyOn(component.tagListCtrl, 'setValue');
    spyOnProperty(component, 'tagListCtrl', 'get').and.returnValue(
      component.dataSpaceDetailsForm.get('tagList') as FormControl<any>,
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set tagListCtrl value', fakeAsync(() => {
    component.onBlurEvent();
    tick();
    expect(component.tagListCtrl.setValue).toHaveBeenCalledWith(
      component.tags(),
    );
  }));

  it('should remove a Tag', () => {
    const LEN = component.tags().length;
    component.removeTag(tagsMock[0]);
    expect(component.tags().length).toBe(LEN - 1);
  });

  it('should add a Tag', () => {
    const LEN = component.tags().length;
    component.addTag({
      chipInput: { clear: () => {} } as MatChipInput,
      value: 'tag-3',
      input: {} as HTMLInputElement,
    });
    expect(component.tags().length).toBe(LEN + 1);
  });
});
