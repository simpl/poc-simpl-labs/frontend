import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  WritableSignal,
  signal,
} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatChipInputEvent, MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { debounceTime, distinctUntilChanged } from 'rxjs';

import { DataSpaceModel } from '../../../models/interfaces/data-space/data-space.interface';
import { DataSpaceService } from '../../../services/data-space.service';
import { DataSpaceType } from '../../../models/enums/data-space/data-space-type.enum';

export type DataSpaceDetailsModel = Pick<
  DataSpaceModel,
  'name' | 'description' | 'tagList'
> & { type?: DataSpaceType };

@Component({
  selector: 'app-data-space-details',
  standalone: true,
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatChipsModule,
    MatIconModule,
  ],
  templateUrl: './data-space-details.component.html',
  styleUrl: './data-space-details.component.scss',
})
export class DataSpaceDetailsComponent implements OnChanges, OnInit {
  @Input() dataSpace!: DataSpaceModel;

  stepTitle = 'Data Space details';
  dataSpaceDetailsForm!: FormGroup;
  dataSpaceDetails!: DataSpaceDetailsModel;
  dataSpaceTypes = Object.entries(DataSpaceType);

  readonly tags: WritableSignal<string[]> = signal([]);

  get tagListCtrl(): FormControl {
    return this.dataSpaceDetailsForm.get('tagList') as FormControl;
  }

  constructor(
    private readonly _fb: FormBuilder,
    private readonly dataSpaceService: DataSpaceService,
  ) {
    this.dataSpaceDetailsForm = this._fb.group({
      name: ['', Validators.required],
      description: ['', Validators.maxLength(1000)],
      type: [null],
      tagList: [[]],
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('dataSpace')) {
      const oldDataSpace = changes['dataSpace'].previousValue as DataSpaceModel;
      const newDataSpace = changes['dataSpace'].currentValue as DataSpaceModel;

      if (newDataSpace && newDataSpace !== oldDataSpace) {
        this.dataSpaceDetails = {} as DataSpaceDetailsModel;
        this.dataSpaceDetails.name = newDataSpace.name;
        this.dataSpaceDetails.description = newDataSpace.description;
        this.dataSpaceDetails.type = null!;

        this.tags.update((tags) => {
          const newTags = newDataSpace.tagList?.filter((t) =>
            tags.every((tag) => tag.toLowerCase() !== t.toLowerCase()),
          );
          return [...tags, ...newTags];
        });

        this.dataSpaceDetails.tagList = this.tags();

        this.dataSpaceDetailsForm.setValue(this.dataSpaceDetails);
      }
    }
  }

  ngOnInit(): void {
    this.dataSpaceDetailsForm.valueChanges
      .pipe(debounceTime(250), distinctUntilChanged())
      .subscribe((details: DataSpaceDetailsModel) =>
        this.dataSpaceService.newDataSpaceDetailsSubject$.next(details),
      );
  }

  removeTag(tag: string) {
    this.tags.update((tags) => {
      const index = tags.indexOf(tag);
      if (index < 0) return tags;

      tags.splice(index, 1);

      this.dataSpaceDetails.tagList = [...tags];
      return [...tags];
    });
  }

  addTag(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    if (value) {
      this.tags.update((tags) => {
        if (tags.some((t) => t.toLowerCase() === value.toLowerCase()))
          return [...tags];

        this.dataSpaceDetails.tagList = [...tags, value];
        return [...tags, value];
      });
    }

    event.chipInput.clear();
  }

  onBlurEvent() {
    setTimeout(() => this.tagListCtrl.setValue(this.tags()));
  }
}
