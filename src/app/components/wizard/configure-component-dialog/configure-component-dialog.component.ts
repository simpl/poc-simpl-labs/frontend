import { Component, Inject, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';

import { ComponentModel } from '../../../models/interfaces/data-space/component.interface';

@Component({
  selector: 'app-configure-component-dialog',
  standalone: true,
  imports: [
    MatDialogModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  templateUrl: './configure-component-dialog.component.html',
  styleUrl: './configure-component-dialog.component.scss',
})
export class ConfigureComponentDialogComponent implements OnInit {
  paramsForm!: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: ComponentModel,
    public dialogRef: MatDialogRef<ConfigureComponentDialogComponent>,
    private readonly _fb: FormBuilder,
  ) {}

  ngOnInit(): void {
    this.paramsForm = this._fb.group({
      param1: [this.data.param1, Validators.required],
      param2: [this.data.param2, Validators.required],
      param3: [this.data.param3, Validators.required],
      param4: [this.data.param4, Validators.required],
      param5: [this.data.param5, Validators.required],
      param6: [this.data.param6, Validators.required],
    });
  }

  saveParameters() {
    const newComp: ComponentModel = this.paramsForm.value;
    newComp.id = this.data.id;
    newComp.type = this.data.type;
    newComp.customImage = this.data.customImage;

    this.dialogRef.close(newComp);
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
