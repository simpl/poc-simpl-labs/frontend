import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ConfigureComponentDialogComponent } from './configure-component-dialog.component';
import { ComponentModel } from '../../../models/interfaces/data-space/component.interface';
import { dataSpacesMock } from '../../../../mocks/data-space-mock';

const dialogDataMock: ComponentModel =
  dataSpacesMock[0].nodeList[0].categoryList[0].componentList[0];

const dialogRefMock = {
  close: () => {},
};

describe('ConfigureComponentDialogComponent', () => {
  let component: ConfigureComponentDialogComponent;
  let fixture: ComponentFixture<ConfigureComponentDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ConfigureComponentDialogComponent, BrowserAnimationsModule],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: dialogDataMock },
        { provide: MatDialogRef, useValue: dialogRefMock },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ConfigureComponentDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should save comp parameters', () => {
    spyOn(dialogRefMock, 'close');
    component.saveParameters();
    expect(dialogRefMock.close).toHaveBeenCalled();
  });

  it('should close dialog', () => {
    spyOn(dialogRefMock, 'close');
    component.closeDialog();
    expect(dialogRefMock.close).toHaveBeenCalled();
  });
});
