import {
  Component,
  Input,
  OnDestroy,
  OnInit,
  computed,
  signal,
} from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatExpansionModule } from '@angular/material/expansion';

import { DataSpaceModel } from '../../../models/interfaces/data-space/data-space.interface';
import { DataSpaceService } from '../../../services/data-space.service';
import { NodeModel } from '../../../models/interfaces/data-space/node.interface';
import { TitlecaseNodeName } from '../../../pipes/titlecase-node-name.pipe';
import { Condition } from '../../../models/interfaces/data-space/condition.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-data-space-conditions',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatExpansionModule,
    TitlecaseNodeName,
  ],
  templateUrl: './data-space-conditions.component.html',
  styleUrl: './data-space-conditions.component.scss',
})
export class DataSpaceConditionsComponent implements OnInit, OnDestroy {
  @Input() dataSpace!: DataSpaceModel;

  stepTitle = 'Conditions';
  dataSpaceConditionsForm!: FormGroup;

  readonly condition = signal<Condition>({
    name: 'Accept all',
    accepted: false,
    children: [],
  });

  readonly partiallyComplete = computed(() => {
    const condition = this.condition();

    return !condition.children
      ? false
      : condition.children.some((t) => t.accepted) &&
          !condition.children.every((t) => t.accepted);
  });

  get conditionsArray() {
    return this.dataSpaceConditionsForm.get('conditionsArray') as FormArray;
  }

  newDataSpaceSub = new Subscription();

  constructor(
    private readonly _fb: FormBuilder,
    private readonly dataSpaceService: DataSpaceService,
  ) {
    this.dataSpaceConditionsForm = this._fb.group({
      conditionsArray: this._fb.array([]),
    });
  }

  ngOnInit(): void {
    this.newDataSpaceSub = this.dataSpaceService.newDataSpaceSubject$
      .asObservable()
      .subscribe(
        (newDataSpace) => newDataSpace && this.setConditions(newDataSpace),
      );
  }

  ngOnDestroy(): void {
    this.newDataSpaceSub.unsubscribe();
  }

  setConditions(newDataSpace: DataSpaceModel) {
    this.condition.set({
      name: 'Accept all',
      accepted: false,
      children: [],
    });
    this.conditionsArray.clear();

    const setNodeConditions = (node: NodeModel) => {
      let n1 = `${newDataSpace.name} / ${node.name}`;
      node.categoryList.forEach((ca) => {
        let n2 = `${n1} / ${ca.type}`;
        ca.componentList.forEach((co) => {
          const child = {
            name: n2,
            accepted: false,
            componentType: co.type,
          };
          conditionChildren.push(child);
          this.addCondition(child);
        });
      });
    };

    let conditionChildren: Condition[] = [];
    Object.values(newDataSpace.nodeList).forEach((n) => setNodeConditions(n));

    this.condition.update((condition) => {
      condition.children = conditionChildren;
      return { ...condition };
    });
  }

  addCondition(condition: Condition) {
    this.conditionsArray.push(
      this._fb.control(condition.accepted, Validators.required),
    );
  }

  update(accepted: boolean, index?: number) {
    this.condition.update((condition) => {
      if (index === undefined) {
        condition.accepted = accepted;
        condition.children?.forEach((t) => (t.accepted = accepted));
      } else {
        condition.children![index].accepted = accepted;
        condition.accepted =
          condition.children?.every((t) => t.accepted) ?? true;
      }

      this.dataSpaceConditionsForm.setValue({
        conditionsArray: condition.children?.map((child) => child.accepted),
      });

      this.dataSpaceService.newDataSpaceConditionsSubject$.next(
        this.conditionsArray.value,
      );

      return { ...condition };
    });
  }
}
