import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Subject } from 'rxjs';

import { DataSpaceConditionsComponent } from './data-space-conditions.component';
import { DataSpaceService } from '../../../services/data-space.service';
import { dataSpacesMock } from '../../../../mocks/data-space-mock';
import { DataSpaceModel } from '../../../models/interfaces/data-space/data-space.interface';

describe('DataSpaceConditionsComponent', () => {
  let component: DataSpaceConditionsComponent;
  let fixture: ComponentFixture<DataSpaceConditionsComponent>;
  let dataSpaceServiceSpy: jasmine.SpyObj<DataSpaceService>;

  beforeEach(async () => {
    dataSpaceServiceSpy = jasmine.createSpyObj('DataSpaceService', [
      'updateNewDataSpaceNodeList',
    ]);

    dataSpaceServiceSpy.newDataSpaceSubject$ = new Subject<DataSpaceModel>();
    dataSpaceServiceSpy.newDataSpaceConditionsSubject$ = new Subject<
      boolean[]
    >();

    await TestBed.configureTestingModule({
      imports: [DataSpaceConditionsComponent, HttpClientTestingModule],
      providers: [
        {
          provide: DataSpaceService,
          useValue: dataSpaceServiceSpy,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(DataSpaceConditionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    spyOn(dataSpaceServiceSpy.newDataSpaceConditionsSubject$, 'next');
    spyOn(component.condition, 'update').and.callThrough();
    spyOn(component.conditionsArray, 'clear');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add an element to #conditionsArray', () => {
    const lastLength = component.conditionsArray.length;
    component.addCondition({ accepted: false, name: 'abc' });
    expect(component.conditionsArray.length).toBe(lastLength + 1);
  });

  it('should call #condition().update', () => {
    component.update(true);
    expect(component.condition.update).toHaveBeenCalled();
  });

  it('should call #condition().update on newDataSpaceSubject$.next() execution', () => {
    dataSpaceServiceSpy.newDataSpaceSubject$.next(dataSpacesMock[1]);
    expect(component.condition.update).toHaveBeenCalled();
  });

  it('should call newDataSpaceConditionsSubject$.next', () => {
    component.update(true);
    expect(
      dataSpaceServiceSpy.newDataSpaceConditionsSubject$.next,
    ).toHaveBeenCalled();
  });

  it('should set #condition().accepted to true', () => {
    component.update(true);
    expect(component.condition().accepted).toBe(true);
  });
});
