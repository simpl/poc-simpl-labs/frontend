import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ParamMap, ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';
import {
  MatDialog,
  MatDialogConfig,
  MatDialogContainer,
  MatDialogRef,
} from '@angular/material/dialog';
import { DialogRef } from '@angular/cdk/dialog';
import { ComponentType, OverlayRef } from '@angular/cdk/overlay';

import { DataSpaceSetupComponent } from './data-space-setup.component';
import { dataSpacesMock } from '../../../../mocks/data-space-mock';
import { DataSpaceModel } from '../../../models/interfaces/data-space/data-space.interface';
import { DrawerService } from '../../../services/drawer.service';
import { DataSpaceService } from '../../../services/data-space.service';
import {
  graphChartDataMock,
  slabsFlatNodesMock,
} from '../../../../tests/data-spaces/graph-chart-datum-mock';
import { SLabsFlatNode } from '../../../models/interfaces/tree-node.interface';
import { TreeMenuService } from '../../../services/tree-menu.service';
import { GraphChartDatum } from '../../../models/interfaces/graph-chart-datum.interface';
import { chartNode2SlabsFlatNode } from '../../../helpers/chart-node-2-slabs-flat-node.map';
import { AddNodesDialogComponent } from '../add-nodes-dialog/add-nodes-dialog.component';
import { NodeType } from '../../../models/enums/data-space/node-type.enum';
import { PageContentType } from '../../../models/enums/data-space/page-content-type.enum';

const mockDataSpace: DataSpaceModel = { ...dataSpacesMock[1] };

const stubParamMap: ParamMap = {
  has: (name: string) => !!name,
  get: (name: string) => name,
  getAll: (name: string) => [name],
  keys: ['a', 'b'],
};

class MockActivatedRoute {
  paramMap = of(stubParamMap);
}

let dataSpaceServiceMock: Partial<DataSpaceService> = {
  chartNode: undefined!,
  newDataSpaceSubject$: new Subject<DataSpaceModel>(),
  newDataSpaceSetupSubject$: new Subject<DataSpaceModel>(),
  updatePageContent: (treeFlatNode: SLabsFlatNode) => {
    let pageContentType = !treeFlatNode.expanded
      ? PageContentType.GRAPH_CHART
      : PageContentType.COMPONENTS_TABLE;

    return {
      dataSpace: mockDataSpace,
      nodeList: mockDataSpace.nodeList,
      pageContentType,
      tableCategory: mockDataSpace.nodeList[1].categoryList[0],
    };
  },
};

const dialogMock = {
  open: (
    component: ComponentType<AddNodesDialogComponent>,
    config?: MatDialogConfig<any> | undefined,
  ) => {
    let dialogRef = new DialogRef<AddNodesDialogComponent, any>(
      {} as OverlayRef,
      {},
    );
    return new MatDialogRef<AddNodesDialogComponent, any>(
      dialogRef,
      {},
      {} as MatDialogContainer,
    );
  },
};

describe('DataSpaceSetupComponent', () => {
  let component: DataSpaceSetupComponent;
  let fixture: ComponentFixture<DataSpaceSetupComponent>;
  let drawerServiceSpy: jasmine.SpyObj<DrawerService>;
  let treeMenuServiceSpy: jasmine.SpyObj<TreeMenuService>;

  beforeEach(async () => {
    drawerServiceSpy = jasmine.createSpyObj('DrawerService', [
      'openComponentDrawer',
      'closeComponentDrawer',
    ]);
    treeMenuServiceSpy = jasmine.createSpyObj('TreeMenuService', [
      'updateBreadcrumbs',
    ]);

    treeMenuServiceSpy.treeMenuNodeSubject$ = new Subject<SLabsFlatNode>();
    treeMenuServiceSpy.graphChartNodeSubject$ = new Subject<GraphChartDatum>();

    await TestBed.configureTestingModule({
      imports: [
        DataSpaceSetupComponent,
        HttpClientTestingModule,
        BrowserAnimationsModule,
      ],
      providers: [
        { provide: ActivatedRoute, useClass: MockActivatedRoute },
        { provide: DataSpaceService, useValue: dataSpaceServiceMock },
        { provide: DrawerService, useValue: drawerServiceSpy },
        { provide: TreeMenuService, useValue: treeMenuServiceSpy },
        { provide: MatDialog, useValue: dialogMock },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(DataSpaceSetupComponent);
    component = fixture.componentInstance;
    component._dataSpace = mockDataSpace;
    component.chartNode = mockDataSpace.nodeList[0];
    component.dialog = TestBed.inject(MatDialog);
    dataSpaceServiceMock = TestBed.inject(DataSpaceService);

    spyOn(component.dialog, 'open').and.returnValue({
      close: () => {},
      afterClosed: () =>
        of([
          {
            name: NodeType.GOVERNANCE.toString(),
            type: NodeType.GOVERNANCE,
          },
          {
            name: NodeType.APPLICATION_PROVIDER.toString(),
            type: NodeType.APPLICATION_PROVIDER,
          },
        ]),
    } as MatDialogRef<typeof component>);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update Category Components', () => {
    component._nodeList = mockDataSpace.nodeList;
    component.chartNode = mockDataSpace.nodeList[0];
    component.updateCategoryComponents(
      mockDataSpace.nodeList[0].categoryList[0],
    );
    expect(
      component.chartNode.categoryList.includes(
        mockDataSpace.nodeList[0].categoryList[0],
      ),
    ).toBeTrue();
  });

  it('should open ComponentDrawer', () => {
    component.openComponentInfoDrawer({
      categoryType: mockDataSpace.nodeList[0].categoryList[0].type,
      comp: mockDataSpace.nodeList[0].categoryList[0].componentList[0],
    });
    expect(drawerServiceSpy.openComponentDrawer).toHaveBeenCalled();
  });

  it('should close ComponentDrawer', () => {
    component.closeComponentInfoDrawer();
    expect(drawerServiceSpy.closeComponentDrawer).toHaveBeenCalled();
  });

  it('should set pageContentType to GRAPH_CHART when level < 0', () => {
    component._nodeList = mockDataSpace.nodeList;
    component.updatePageContent(slabsFlatNodesMock[0]);
    expect(component.pageContentType).toBe(PageContentType.GRAPH_CHART);
  });

  it('should set pageContentType to GRAPH_CHART when level = 0', () => {
    component._nodeList = mockDataSpace.nodeList;
    component.updatePageContent({ ...slabsFlatNodesMock[0], level: 0 });
    expect(component.pageContentType).toBe(PageContentType.GRAPH_CHART);
  });

  it('should set pageContentType to COMPONENTS_TABLE when level = 1', () => {
    component._nodeList = mockDataSpace.nodeList;
    component.chartNode = mockDataSpace.nodeList[0];
    component.updatePageContent({
      ...slabsFlatNodesMock[0],
      level: 1,
      expanded: true,
    });
    expect(component.pageContentType).toBe(PageContentType.COMPONENTS_TABLE);
  });

  it('should set pageContentType to GRAPH_CHART when level = 1', () => {
    component._nodeList = mockDataSpace.nodeList;
    component.chartNode = mockDataSpace.nodeList[0];
    component.updatePageContent({
      ...slabsFlatNodesMock[0],
      level: 1,
      expanded: false,
    });
    expect(component.pageContentType).toBe(PageContentType.GRAPH_CHART);
  });

  it('should call newDataSpaceSubject$.next', () => {
    spyOn(dataSpaceServiceMock.newDataSpaceSubject$!, 'next');
    component.updateDataSpace(mockDataSpace);
    expect(
      dataSpaceServiceMock.newDataSpaceSubject$?.next,
    ).toHaveBeenCalledWith(mockDataSpace);
  });

  it('should open AddNodesDialog', () => {
    component.openAddNodesDialog();
    expect(dialogMock.open).toHaveBeenCalled();
  });

  it('should set #dataSpace', () => {
    dataSpaceServiceMock.newDataSpaceSubject$?.next(mockDataSpace);
    expect(component.dataSpace).toEqual(mockDataSpace);
  });

  it('should call #updatePageContent() w/ treeMenuNode', () => {
    spyOn(component, 'updatePageContent');
    treeMenuServiceSpy.treeMenuNodeSubject$.next(slabsFlatNodesMock[0]);
    expect(component.updatePageContent).toHaveBeenCalledWith(
      slabsFlatNodesMock[0],
    );
  });

  it('should call #updatePageContent() w/ graphChartNode', () => {
    spyOn(component, 'updatePageContent');
    treeMenuServiceSpy.graphChartNodeSubject$.next(graphChartDataMock[0]);
    expect(component.updatePageContent).toHaveBeenCalledWith(
      chartNode2SlabsFlatNode(graphChartDataMock[0]),
    );
  });

  it('should call AddNodesDialogComponent-ref.afterClosed()', () => {
    let dialogRef = dialogMock.open(AddNodesDialogComponent);
    spyOn(dialogRef, 'afterClosed').and.returnValue(
      of([
        {
          name: NodeType.GOVERNANCE.toString(),
          type: NodeType.GOVERNANCE,
        },
        {
          name: NodeType.APPLICATION_PROVIDER.toString(),
          type: NodeType.APPLICATION_PROVIDER,
        },
      ]),
    );
    spyOn(dialogRef, 'close').and.callFake(dialogRef.afterClosed);
    dialogRef.close();
    expect(dialogRef.afterClosed).toHaveBeenCalled();
  });
});
