import { CommonModule } from '@angular/common';
import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatDrawer, MatSidenavModule } from '@angular/material/sidenav';
import { Subscription } from 'rxjs';

import { DataSpaceGraphChartComponent } from '../../data-space-graph-chart/data-space-graph-chart.component';
import { SLabsFlatNode } from '../../../models/interfaces/tree-node.interface';
import { NodeType } from '../../../models/enums/data-space/node-type.enum';
import { NodeModel } from '../../../models/interfaces/data-space/node.interface';
import { DataSpaceService } from '../../../services/data-space.service';
import { AddNodesDialogComponent } from '../add-nodes-dialog/add-nodes-dialog.component';
import { DS_NODE_LIST } from '../constants/available-nodes-list.data';
import { DataSpaceModel } from '../../../models/interfaces/data-space/data-space.interface';
import { CategoryComponentsTableComponent } from '../../_shared/category-components-table/category-components-table.component';
import { CategoryModel } from '../../../models/interfaces/data-space/category.interface';
import { ComponentModel } from '../../../models/interfaces/data-space/component.interface';
import { ComponentMoreInfoComponent } from '../component-more-info/component-more-info.component';
import { ContextTreeMenuComponent } from '../../_shared/context-tree-menu/context-tree-menu.component';
import { CategoryType } from '../../../models/enums/data-space/category-type.enum';
import { TreeMenuService } from '../../../services/tree-menu.service';
import { GraphChartDatum } from '../../../models/interfaces/graph-chart-datum.interface';
import { chartNode2SlabsFlatNode } from '../../../helpers/chart-node-2-slabs-flat-node.map';
import { BreadcrumbComponent } from '../../_shared/breadcrumb/breadcrumb.component';
import { DrawerService } from '../../../services/drawer.service';
import { PageContentType } from '../../../models/enums/data-space/page-content-type.enum';
import { ComponentInfo } from '../../../models/interfaces/data-space/component-info.interface';
import { AddNodesDialogData } from '../../../models/interfaces/data-space/add-nodes-dialog-data.interface';

@Component({
  selector: 'app-data-space-setup',
  standalone: true,
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatSidenavModule,
    DataSpaceGraphChartComponent,
    CategoryComponentsTableComponent,
    ComponentMoreInfoComponent,
    ContextTreeMenuComponent,
    BreadcrumbComponent,
  ],
  templateUrl: './data-space-setup.component.html',
  styleUrls: ['./data-space-setup.component.scss'],
})
export class DataSpaceSetupComponent implements OnInit, OnDestroy {
  @Input() _dataSpace!: DataSpaceModel;

  @ViewChild('componentInfoDrawer', { static: true })
  componentInfoDrawer!: MatDrawer;

  stepTitle = '';
  dataSpace!: DataSpaceModel;
  _nodeList!: NodeModel[];

  pageContentType = PageContentType.GRAPH_CHART.toString();
  pageContentTypes = PageContentType;

  chartNode!: NodeModel;
  tableCategory!: CategoryModel;
  componentInfo!: ComponentInfo;

  newDataSpaceSub = new Subscription();
  treeMenuNodeSubscription = new Subscription();
  graphChartNodeSubscription = new Subscription();

  constructor(
    private readonly dataSpaceService: DataSpaceService,
    private readonly treeMenuService: TreeMenuService,
    private readonly drawerService: DrawerService,
    public dialog: MatDialog,
  ) {
    this.stepTitle = 'Set up Data Space';

    this.newDataSpaceSub = this.dataSpaceService.newDataSpaceSubject$
      .asObservable()
      .subscribe((newDataSpace) => {
        if (newDataSpace) {
          this._dataSpace = newDataSpace;
          this._nodeList = [...newDataSpace.nodeList];
          this.dataSpace = { ...newDataSpace };
        }
      });
  }

  ngOnInit(): void {
    this.treeMenuNodeSubscription = this.treeMenuService.treeMenuNodeSubject$
      .asObservable()
      .subscribe((treeFlatNode) => this.updatePageContent(treeFlatNode));

    this.graphChartNodeSubscription =
      this.treeMenuService.graphChartNodeSubject$
        .asObservable()
        .subscribe((graphChartDatum: GraphChartDatum) =>
          this.updatePageContent(chartNode2SlabsFlatNode(graphChartDatum)),
        );
  }

  ngOnDestroy(): void {
    this.newDataSpaceSub.unsubscribe();
    this.treeMenuNodeSubscription.unsubscribe();
    this.graphChartNodeSubscription.unsubscribe();
  }

  openAddNodesDialog() {
    const dialogData: AddNodesDialogData = {
      templateNodes: this._dataSpace.nodeList.map((n) => {
        const { name, type } = n;
        return { name, type };
      }),
    };

    const addNodesDialogRef: MatDialogRef<AddNodesDialogComponent> =
      this.dialog.open(AddNodesDialogComponent, { data: dialogData });

    addNodesDialogRef
      .afterClosed()
      .subscribe((selectedNodes: { name: string; type: NodeType }[]) => {
        if (selectedNodes?.length > 0) {
          const nodes: NodeModel[] = selectedNodes?.map((node) => {
            const listNode = {
              ...DS_NODE_LIST.find((n) => n.type === node.type)!,
            };
            listNode.name = node.name;
            return listNode;
          });

          const newDataSpace = { ...this._dataSpace };
          let nodeList: NodeModel[] = [...newDataSpace.nodeList];
          nodeList = nodeList.concat(nodes);
          newDataSpace.nodeList = nodeList;

          this.updateDataSpace(newDataSpace);
          this.dataSpace = { ...newDataSpace };
        }
      });
  }

  updateDataSpace(_dataSpace: DataSpaceModel) {
    this.dataSpaceService.newDataSpaceSubject$.next(_dataSpace);
  }

  updatePageContent(treeFlatNode: SLabsFlatNode) {
    const pageContentUpdate = this.dataSpaceService.updatePageContent(
      treeFlatNode,
      this.dataSpace,
      this._nodeList,
      this.pageContentType,
      this.tableCategory,
    );

    this.dataSpace = pageContentUpdate.dataSpace;
    this._nodeList = pageContentUpdate.nodeList;
    this.pageContentType = pageContentUpdate.pageContentType;
    this.tableCategory = pageContentUpdate.tableCategory;
  }

  updateCategoryComponents(category: CategoryModel) {
    const categoryIdx = this.chartNode.categoryList.findIndex(
      (c) => c.type === category.type,
    );

    if (categoryIdx >= 0) {
      this.chartNode.categoryList[categoryIdx] = category;

      const nodeIdx = this._dataSpace.nodeList.findIndex(
        (n) => this.chartNode.name === n.name && this.chartNode.type === n.type,
      );

      if (nodeIdx >= 0) this._dataSpace.nodeList[nodeIdx] = this.chartNode;

      this.dataSpaceService.newDataSpaceSetupSubject$.next(this._dataSpace);
    }
  }

  openComponentInfoDrawer(event: {
    categoryType: CategoryType;
    comp: ComponentModel;
  }) {
    this.componentInfo = event.comp as ComponentInfo;
    this.componentInfo.categoryType = event.categoryType;

    this.drawerService.openComponentDrawer(this.componentInfoDrawer);
  }

  closeComponentInfoDrawer() {
    this.drawerService.closeComponentDrawer(this.componentInfoDrawer);
  }
}
