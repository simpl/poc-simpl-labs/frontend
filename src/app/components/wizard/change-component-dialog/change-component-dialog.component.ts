import { Component, Inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';

import { SlabsCustomComponentModel } from '../../../models/interfaces/slabs-custom-components/slabs-custom-component.interface';
import { SLabsCustomComponentsComponent } from '../../conformance/slabs-custom-components/slabs-custom-components.component';
import { ComponentModel } from '../../../models/interfaces/data-space/component.interface';

@Component({
  selector: 'app-change-component-dialog',
  standalone: true,
  imports: [MatDialogModule, MatButtonModule, SLabsCustomComponentsComponent],
  templateUrl: './change-component-dialog.component.html',
  styleUrl: './change-component-dialog.component.scss',
})
export class ChangeComponentDialogComponent {
  selectedComponent!: SlabsCustomComponentModel;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: ComponentModel,
    public dialogRef: MatDialogRef<ChangeComponentDialogComponent>,
  ) {}

  selectComponent(customComponent: SlabsCustomComponentModel) {
    this.selectedComponent = customComponent;
  }

  changeComponent() {
    const newComp: ComponentModel = { ...this.data };
    newComp.customImage = this.selectedComponent.customImage;
    newComp.customComponentId = this.selectedComponent.id;
    this.dialogRef.close(newComp);
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
