import { BreakpointObserver } from '@angular/cdk/layout';
import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { map, Observable } from 'rxjs';

import { learningSectionList } from './learning-section-list-mock';
import { LearningSection } from '../../models/interfaces/learning/learning-section.interface';

@Component({
  selector: 'app-learning',
  standalone: true,
  imports: [
    CommonModule,
    MatGridListModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatChipsModule,
  ],
  templateUrl: './learning.component.html',
  styleUrl: './learning.component.scss',
})
export class LearningComponent {
  cols$ = new Observable<number>();
  learningSectionList: LearningSection[] = [];

  constructor(private readonly _breakpointObserver: BreakpointObserver) {
    this.cols$ = this._breakpointObserver
      .observe([`(max-width: ${1430}px)`, `(max-width: ${2080}px)`])
      .pipe(
        map((state) => {
          if (state.breakpoints[`(max-width: ${1430}px)`]) return 2;
          if (state.breakpoints[`(max-width: ${2080}px)`]) return 3;
          return 4;
        }),
      );

    this.learningSectionList = learningSectionList;
  }
}
