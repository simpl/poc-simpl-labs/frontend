export interface LearningChannel {
  id: number;
  title: string;
  description: string;
  date: string;
}

export const channelList: LearningChannel[] = [
  {
    id: 1,
    title: 'Basics about Simpl',
    description:
      'Learn the fundamentals of Simpl, including its core concepts and how it supports building Data Spaces.',
    date: '2024-03-23T18:23:11.135',
  },
  {
    id: 2,
    title: 'What a Data Space is',
    description:
      'Explore the concept of Data Spaces, how they function, and why they are crucial in modern data ecosystems.',
    date: '2024-03-23T18:23:11.135',
  },
  {
    id: 3,
    title: 'Discovery and learn Simpl-Open',
    description:
      'Get introduced to Simpl-Open, the middleware for building Data Spaces, and understand its key capabilities.',
    date: '2024-03-23T18:23:11.135',
  },
  {
    id: 4,
    title: 'The purpose of Simpl-Labs',
    description:
      'Discover the role of Simpl-Labs, an online platform designed to experiment with Simpl-Open technologies.',
    date: '2024-03-23T18:23:11.135',
  },
  {
    id: 5,
    title: "An overview of Simpl-Labs' features",
    description:
      'Explore the main features of Simpl-Labs and how they enable hands-on experimentation with Data Space technologies',
    date: '2024-03-23T18:23:11.135',
  },
  {
    id: 6,
    title: 'How to guides',
    description:
      'Access step-by-step tutorials to help you get started with Simpl-Labs.',
    date: '2024-03-23T18:23:11.135',
  },
];
