import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { TemplateService } from './template.service';
import { AppService } from './app.service';
import { envUrl } from '../../tests/env-url-mock';

describe('TemplateService', () => {
  let service: TemplateService;
  let appServiceSpy: jasmine.SpyObj<AppService>;

  beforeEach(() => {
    appServiceSpy = jasmine.createSpyObj('AppService', ['load']);
    Object.defineProperty(appServiceSpy, 'appUrls', {
      get: () => ({
        apiUrl: 'http://api-mock.org',
        simplUrl: '"http://simpl-mock.org/',
      }),
    });

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: AppService, useValue: appServiceSpy }],
    });
    service = TestBed.inject(TemplateService);

    spyOnProperty(appServiceSpy, 'appUrls', 'get').and.returnValue(envUrl);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
