import { TestBed } from '@angular/core/testing';
import { KeycloakService } from 'keycloak-angular';

import { KeycloakOperationService } from './keycloak-operation.service';

describe('KeycloakOperationService', () => {
  let service: KeycloakOperationService;
  let mockKeycloakService;

  beforeEach(() => {
    mockKeycloakService = jasmine.createSpyObj('KeycloakService', [
      'isLoggedIn',
      'logout',
      'loadUserProfile',
    ]);

    TestBed.configureTestingModule({
      providers: [{ provide: KeycloakService, useValue: mockKeycloakService }],
    });
    service = TestBed.inject(KeycloakOperationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
