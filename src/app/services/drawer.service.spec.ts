import { TestBed } from '@angular/core/testing';
import { MatDrawer } from '@angular/material/sidenav';
import { FocusOrigin } from '@angular/cdk/a11y';
import { of } from 'rxjs';
import { ElementRef } from '@angular/core';

import { DrawerService } from './drawer.service';

const componentDrawerMock: Partial<MatDrawer> = {
  open: (openedVia?: FocusOrigin | undefined) => new Promise(() => {}),
  close: () => new Promise((value) => value),
  openedStart: of({} as any),
  _content: new ElementRef<HTMLElement>({
    classList: { add: () => {}, remove: () => {} } as DOMTokenList,
  } as HTMLElement),
};

const testCaseDrawerMock: Partial<MatDrawer> = {
  open: (openedVia?: FocusOrigin | undefined) => new Promise(() => {}),
  close: () => new Promise((value) => value),
  openedStart: of({} as any),
  _content: new ElementRef<HTMLElement>({
    classList: { add: () => {}, remove: () => {} } as DOMTokenList,
  } as HTMLElement),
};

const dummyElement = document.createElement('div');

document.getElementById = jasmine
  .createSpy('HTML Element')
  .and.returnValue(dummyElement);

describe('DrawerService', () => {
  let service: DrawerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: MatDrawer,
          useValue: componentDrawerMock,
        },
        {
          provide: MatDrawer,
          useValue: testCaseDrawerMock,
        },
      ],
    });
    service = TestBed.inject(DrawerService);

    spyOn(service, 'toggleClasses').and.callThrough();
    spyOn(componentDrawerMock, 'close' as never).and.returnValue(
      new Promise((value) => value) as never,
    );
    spyOn(testCaseDrawerMock, 'close' as never).and.returnValue(
      new Promise((value) => value) as never,
    );
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should open the componentInfo Drawer', () => {
    spyOn(componentDrawerMock, 'open' as never);
    service.openComponentDrawer(componentDrawerMock as MatDrawer);
    expect(componentDrawerMock.open).toHaveBeenCalled();
  });

  it('should close the componentInfo Drawer', () => {
    service.closeComponentDrawer(componentDrawerMock as MatDrawer);
    expect(componentDrawerMock.close).toHaveBeenCalled();
  });

  it('should open the testCaseInfo Drawer', () => {
    spyOn(testCaseDrawerMock, 'open' as never);
    service.openTestCaseDrawer(testCaseDrawerMock as MatDrawer);
    expect(testCaseDrawerMock.open).toHaveBeenCalled();
  });

  it('should close the testCaseInfo Drawer', () => {
    service.closeTestCaseDrawer(testCaseDrawerMock as MatDrawer);
    expect(testCaseDrawerMock.close).toHaveBeenCalled();
  });

  it('should add Class', () => {
    service.toggleClasses(
      'open',
      'class-toggle',
      componentDrawerMock as MatDrawer,
    );
    expect(document.getElementById).toHaveBeenCalled();
  });

  it('should remove Class', () => {
    service.toggleClasses(
      'close',
      'class-toggle',
      componentDrawerMock as MatDrawer,
    );
    expect(document.getElementById).toHaveBeenCalled();
  });
});
