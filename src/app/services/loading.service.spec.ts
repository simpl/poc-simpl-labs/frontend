import { TestBed } from '@angular/core/testing';

import { LoadingService } from './loading.service';

describe('LoadingService', () => {
  let service: LoadingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LoadingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should update #timer', () => {
    service.updateTimer(4);
    expect(service.timer).toBe(4);
  });

  it('should update #itbTimer', () => {
    service.updateITBTimer(3);
    expect(service.itbTimer).toBe(3);
  });

  it('should update #monitoringTimer', () => {
    service.updateMonitoringTimer(5);
    expect(service.monitoringTimer).toBe(5);
  });

  it('should call #loadingMap.set() w/ #setLoading()', () => {
    spyOn(service.loadingMap, 'set');
    service.setLoading(true, 'url-mock');
    expect(service.loadingMap.set).toHaveBeenCalledWith('url-mock', true);
  });

  it('should add an url to #storedUrls w/ #setLoading()', () => {
    spyOn(service.loadingMap, 'set');
    const LEN = service.storedUrls.length;
    service.setLoading(true, 'url-mock/tests/start');
    expect(service.storedUrls.length).toBe(LEN + 1);
  });

  it('should clear #storedUrls and #loadingMap w/ #setLoading()', () => {
    service.loadingMap.set('/uri-mock/tests/status/url-mock', false);
    service.setLoading(false, '/uri-mock/tests/status/url-mock');
    expect(service.storedUrls.length).toBe(0);
    expect(service.loadingMap.size).toBe(0);
  });

  it('should call #loadingMap.delete w/ #setLoading()', () => {
    spyOn(service.loadingMap, 'delete');
    service.loadingMap.set('/uri-mock/tests/status/url-mock', false);
    service.setLoading(false, '/uri-mock/tests/status/url-mock');
    expect(service.loadingMap.delete).toHaveBeenCalled();
  });
});
