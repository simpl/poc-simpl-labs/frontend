import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { ElementRef } from '@angular/core';
import { fromEvent, of } from 'rxjs';
import * as echarts from 'echarts/core';
import { LineChart } from 'echarts/charts';
import {
  TitleComponent,
  GridComponent,
  DatasetComponent,
  TransformComponent,
} from 'echarts/components';
import { CanvasRenderer } from 'echarts/renderers';
import { LabelLayout, UniversalTransition } from 'echarts/features';

import { ConsumptionType } from '../models/enums/monitoring/consumption-type.enum';
import { MonitoringChartService } from './monitoring-chart.service';
import { SelectedPointPopupComponent } from '../components/monitoring/infrastructure/historical-consumption/selected-point-popup/selected-point-popup.component';

const dummyElement = document.createElement('div');

document.getElementById = jasmine
  .createSpy('HTML Element')
  .and.returnValue(dummyElement);

const eventMock: echarts.ECElementEvent = {
  $vars: ['var-mock-1', 'var-mock-2'],
  componentIndex: 123,
  componentSubType: 'comp-sub-type-mock',
  componentType: 'comp-type',
  data: [],
  dataIndex: 1,
  name: 'name-mock',
  type: 'click',
  value: 1,
  event: {
    target: {
      //@ts-ignore
      style: {
        text: 'text-mock',
        image: 'data:image/svg+xml;charset=utf8,',
      },
    },
  },
};

describe('MonitoringChartService', () => {
  let service: MonitoringChartService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    const spy = jasmine.createSpyObj('HttpClient', ['get']);

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        MonitoringChartService,
        { provide: HttpClient, useValue: spy },
      ],
    });
    service = TestBed.inject(MonitoringChartService);
    httpClientSpy = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;

    echarts.use([
      GridComponent,
      LineChart,
      CanvasRenderer,
      UniversalTransition,
      DatasetComponent,
      TransformComponent,
      LabelLayout,
      UniversalTransition,
      CanvasRenderer,
    ]);

    service.initializeTenantCharts(
      ConsumptionType.CPU,
      new ElementRef(document.getElementById('tenant-cpu')!),
    );
    service.initializeTenantCharts(
      ConsumptionType.MEMORY,
      new ElementRef(document.getElementById('tenant-memory')!),
    );
    service.initializeTenantCharts(
      ConsumptionType.STORAGE,
      new ElementRef(document.getElementById('tenant-storage')!),
    );

    service.initializeHistoricalCharts(
      ConsumptionType.CPU,
      new ElementRef(document.getElementById('tenant-cpu')!),
    );
    service.initializeHistoricalCharts(
      ConsumptionType.MEMORY,
      new ElementRef(document.getElementById('tenant-memory')!),
    );
    service.initializeHistoricalCharts(
      ConsumptionType.STORAGE,
      new ElementRef(document.getElementById('tenant-storage')!),
    );

    spyOn(service.historicalCPUChart, 'resize');

    spyOn(fromEvent(window, 'resize'), 'pipe').and.returnValue(
      of(new Event('resize')),
    );

    spyOn(window, 'resizeBy').and.callThrough();
    spyOn(window, 'resizeTo').and.callThrough();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should have constants defined', () => {
    echarts.use([TitleComponent, GridComponent, LineChart, CanvasRenderer]);

    expect(service.timerInterval).toBe(3000);
    expect(service.counter).toBe(0);
    expect(service.elapsedTime).toBe(0);
    expect(service.increment).toBe(service.timerInterval / 3000);
    expect(service.tenantCPUData).toEqual([]);
    expect(service.tenantMemoryData).toEqual([]);
    expect(service.tenantStorageData).toEqual([]);
    expect(service.consumptionColors).toEqual({
      [ConsumptionType.CPU]: '#4387AF',
      [ConsumptionType.MEMORY]: '#001D6C',
      [ConsumptionType.STORAGE]: '#540CAC',
      tenantBg: '#f2f4f8',
      historicalAvg: '#da1e28',
      historicalBg: '#fcfcfc',
    });
    expect(service.minTime).toBe(-9);
    expect(service.maxTime).toBe(0);
  });

  it('should initialize #tenantCPUChart', () => {
    expect(service.tenantCPUChart).toBeTruthy();
  });

  it('should initialize #tenantMemoryChart', () => {
    expect(service.tenantMemoryChart).toBeTruthy();
  });

  it('should initialize #tenantStorageChart', () => {
    expect(service.tenantStorageChart).toBeTruthy();
  });

  it('should update #tenantCPUChart', () => {
    spyOn(service.tenantCPUChart, 'setOption');
    service.updateTenantCharts(123, ConsumptionType.CPU);
    expect(service.tenantCPUChart.setOption).toHaveBeenCalled();
  });

  it('should update #tenantMemoryChart', () => {
    spyOn(service.tenantMemoryChart, 'setOption');
    service.updateTenantCharts(123, ConsumptionType.MEMORY);
    expect(service.tenantMemoryChart.setOption).toHaveBeenCalled();
  });

  it('should update #tenantStorageChart', () => {
    spyOn(service.tenantStorageChart, 'setOption');
    service.updateTenantCharts(123, ConsumptionType.STORAGE);
    expect(service.tenantStorageChart.setOption).toHaveBeenCalled();
  });

  it('should reset charts', () => {
    spyOn(service.tenantCPUChart, 'dispose');

    service.resetTenantCharts();

    expect(service.counter).toBe(0);
    expect(service.elapsedTime).toBe(0);
    expect(service.tenantCPUData).toEqual([]);
    expect(service.tenantMemoryData).toEqual([]);
    expect(service.tenantStorageData).toEqual([]);
    expect(service.minTime).toBe(-9);
    expect(service.maxTime).toBe(0);
    expect(service.tenantCPUChart.dispose).toHaveBeenCalled();
    expect(service.tenantMemoryChart.dispose).toHaveBeenCalled();
    expect(service.tenantStorageChart.dispose).toHaveBeenCalled();
  });

  it('should initialize #historicalCPUChart', () => {
    expect(service.historicalCPUChart).toBeTruthy();
  });

  it('should initialize #tenantMemoryChart', () => {
    expect(service.historicalMemoryChart).toBeTruthy();
  });

  it('should initialize #tenantStorageChart', () => {
    expect(service.historicalStorageChart).toBeTruthy();
  });

  it('should open HistoricalPopup', () => {
    spyOn(service._historicalPopup, 'open');
    service.openHistoricalPopup(eventMock, ConsumptionType.CPU);
    expect(service._historicalPopup.open).toHaveBeenCalled();
  });

  it('should update #historicalCPUChart', () => {
    spyOn(service.historicalCPUChart, 'setOption');
    service.updateHistoricalCharts({
      average: 123,
      historicalConsumptions: [{ allocated: 1, date: '', used: 0.3 }],
      name: ConsumptionType.CPU,
    });
    expect(service.historicalCPUChart.setOption).toHaveBeenCalled();
  });

  it('should update #historicalMemoryChart', () => {
    spyOn(service.historicalMemoryChart, 'setOption');
    service.updateHistoricalCharts({
      average: 123,
      historicalConsumptions: [{ allocated: 1, date: '', used: 0.3 }],
      name: ConsumptionType.MEMORY,
    });
    expect(service.historicalMemoryChart.setOption).toHaveBeenCalled();
  });

  it('should update #historicalStorageChart', () => {
    spyOn(service.historicalStorageChart, 'setOption');
    service.updateHistoricalCharts({
      average: 123,
      historicalConsumptions: [{ allocated: 1, date: '', used: 0.3 }],
      name: ConsumptionType.STORAGE,
    });
    expect(service.historicalStorageChart.setOption).toHaveBeenCalled();
  });

  it('should set #minTime #maxTime #elapsedTime', () => {
    service.counter += 3 - 1;
    let minTime = service.minTime;
    let maxTime = service.maxTime;
    let increment = service.increment;
    service.updateTenantCharts(123, ConsumptionType.CPU);
    expect(service.minTime).toBe(minTime + increment);
    expect(service.maxTime).toBe(maxTime + increment);
    expect(service.elapsedTime).toBe(service.counter / 3);
  });

  it('should update #updateTenantCharts', () => {
    for (let i = 0; i < 10; i++) {
      service.tenantCPUData.push([i, Math.random() * 5]);
    }
    service.updateTenantCharts(123, ConsumptionType.CPU);
    expect(service.maxValues[ConsumptionType.CPU]).toBe(
      Math.max(...service.tenantCPUData.slice(0, 10).map((x) => x[1])),
    );
  });

  it('should update tenantCharts CPU', () => {
    for (let i = 0; i < 10; i++) {
      service.tenantCPUData.push([i, Math.random() * 5]);
    }
    service.updateTenantCharts(123, ConsumptionType.CPU);
    expect(service.maxValues[ConsumptionType.CPU]).toBe(
      Math.max(...service.tenantCPUData.slice(0, 10).map((x) => x[1])),
    );
  });

  it('should update tenantCharts MEMORY', () => {
    for (let i = 0; i < 10; i++) {
      service.tenantMemoryData.push([i, Math.random() * 5]);
    }
    service.updateTenantCharts(123, ConsumptionType.MEMORY);
    expect(service.maxValues[ConsumptionType.MEMORY]).toBe(
      Math.max(...service.tenantMemoryData.slice(0, 10).map((x) => x[1])),
    );
  });

  it('should update tenantCharts STORAGE', () => {
    for (let i = 0; i < 10; i++) {
      service.tenantStorageData.push([i, Math.random() * 5]);
    }
    service.updateTenantCharts(123, ConsumptionType.STORAGE);
    expect(service.maxValues[ConsumptionType.STORAGE]).toBe(
      Math.max(...service.tenantStorageData.slice(0, 10).map((x) => x[1])),
    );
  });

  it('should open SelectedPoint Popup - CPU', () => {
    spyOn(service._historicalPopup, 'open');
    service.openHistoricalPopup(eventMock, ConsumptionType.CPU);
    const historicalConsumption = service.historicalCPUData.find(
      (_, i) => eventMock.dataIndex === i,
    )!;
    expect(service._historicalPopup.open).toHaveBeenCalledWith(
      SelectedPointPopupComponent,
      {
        data: {
          ...historicalConsumption,
          name: ConsumptionType.CPU,
        },
        width: 'fit-content',
        height: 'fit-content',
      },
    );
  });

  it('should open SelectedPoint Popup - MEMORY', () => {
    spyOn(service._historicalPopup, 'open');
    service.openHistoricalPopup(eventMock, ConsumptionType.MEMORY);
    const historicalConsumption = service.historicalMemoryData.find(
      (_, i) => eventMock.dataIndex === i,
    )!;
    expect(service._historicalPopup.open).toHaveBeenCalledWith(
      SelectedPointPopupComponent,
      {
        data: {
          ...historicalConsumption,
          name: ConsumptionType.MEMORY,
        },
        width: 'fit-content',
        height: 'fit-content',
      },
    );
  });

  it('should open SelectedPoint Popup - STORAGE', () => {
    spyOn(service._historicalPopup, 'open');
    service.openHistoricalPopup(eventMock, ConsumptionType.STORAGE);
    const historicalConsumption = service.historicalStorageData.find(
      (_, i) => eventMock.dataIndex === i,
    )!;
    expect(service._historicalPopup.open).toHaveBeenCalledWith(
      SelectedPointPopupComponent,
      {
        data: {
          ...historicalConsumption,
          name: ConsumptionType.STORAGE,
        },
        width: 'fit-content',
        height: 'fit-content',
      },
    );
  });
});
