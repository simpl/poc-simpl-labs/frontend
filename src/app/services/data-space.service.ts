import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, Subject } from 'rxjs';

import { DataSpaceModel } from '../models/interfaces/data-space/data-space.interface';
import { NodeModel } from '../models/interfaces/data-space/node.interface';
import { DataSpaceDetailsModel } from '../components/wizard/data-space-details/data-space-details.component';
import { NodeType } from '../models/enums/data-space/node-type.enum';
import { SLabsFlatNode } from '../models/interfaces/tree-node.interface';
import { CategoryModel } from '../models/interfaces/data-space/category.interface';
import { TemplateModel } from '../models/interfaces/template.interface';
import { PageContentUpdate } from '../models/interfaces/data-space/page-content-update.interface';
import { PageContentType } from '../models/enums/data-space/page-content-type.enum';
import { AppService } from './app.service';

@Injectable({
  providedIn: 'root',
})
export class DataSpaceService {
  private readonly baseUrl = `${this.appService.appUrls?.apiUrl}/dataspaces`;
  public dataSpacesLimit = 2;
  public timerInterval = 3000;
  public nDataSpaces!: number;

  public newDataSpace!: DataSpaceModel;
  public dataSpaces!: DataSpaceModel[];
  public chartNode!: NodeModel;

  dataSpaceSubject$ = new Subject<DataSpaceModel>();
  dataSpacesSubject$ = new Subject<DataSpaceModel[]>();
  newDataSpaceSubject$ = new Subject<DataSpaceModel>();
  newDataSpaceSetupSubject$ = new Subject<DataSpaceModel>();
  newDataSpaceDetailsSubject$ = new Subject<DataSpaceDetailsModel>();
  newDataSpaceConditionsSubject$ = new Subject<boolean[]>();

  postDataSpaceSubject$: Subject<DataSpaceModel> =
    new Subject<DataSpaceModel>();

  constructor(
    private readonly http: HttpClient,
    private readonly appService: AppService,
  ) {
    this.newDataSpaceSubject$
      .asObservable()
      .subscribe((dataSpace) => (this.newDataSpace = dataSpace));

    this.dataSpacesSubject$
      .asObservable()
      .subscribe((dataSpaces) => (this.dataSpaces = dataSpaces));
  }

  getDataSpacesNumber(): number {
    return this.nDataSpaces;
  }

  setDataSpacesNumber(n: number) {
    this.nDataSpaces = n;
  }

  getDataSpaces(): Observable<DataSpaceModel[]> {
    return this.http.get<DataSpaceModel[]>(this.baseUrl);
  }

  getDataSpace(id: number): Observable<DataSpaceModel> {
    return this.http.get<DataSpaceModel>(`${this.baseUrl}/${id}`).pipe(
      map((ds) => {
        ds.nodeList = ds.nodeList.map((n) => {
          n.name =
            n.name === `${NodeType.GOVERNANCE}`
              ? `${NodeType.GOVERNANCE}_AUTHORITY`
              : n.name;
          return n;
        });
        return ds;
      }),
    );
  }

  createDataSpace(dataSpace: DataSpaceModel): Observable<DataSpaceModel> {
    return this.http.post<DataSpaceModel>(`${this.baseUrl}`, dataSpace);
  }

  deleteDataSpace(id: number): Observable<DataSpaceModel> {
    return this.http.delete<DataSpaceModel>(`${this.baseUrl}/${id}`);
  }

  updateNewDataSpaceNodeList(newNodeList: NodeModel[]) {
    this.newDataSpace.nodeList = newNodeList;
    this.newDataSpaceSubject$.next(this.newDataSpace);
  }

  updatePageContent(
    treeFlatNode: SLabsFlatNode,
    dataSpace: DataSpaceModel,
    nodeList: NodeModel[],
    pageContentType: PageContentType | string,
    tableCategory: CategoryModel,
  ): PageContentUpdate {
    const ds = { ...dataSpace };

    if (treeFlatNode.level < 0) {
      ds.nodeList = [...nodeList];
      dataSpace = ds;
      pageContentType = PageContentType.GRAPH_CHART;
    }

    if (treeFlatNode.level === 0) {
      if (treeFlatNode.expanded) this.updateChart(treeFlatNode, ds, nodeList);
      else ds.nodeList = [...nodeList];

      dataSpace = ds;
      pageContentType = PageContentType.GRAPH_CHART;
    }

    if (treeFlatNode.level === 1) {
      if (treeFlatNode.expanded) {
        tableCategory = this.chartNode.categoryList.find(
          (c) => c.type === treeFlatNode.name,
        )!;

        pageContentType = PageContentType.COMPONENTS_TABLE;
      } else {
        ds.nodeList = [this.chartNode];
        dataSpace = ds;
        pageContentType = PageContentType.GRAPH_CHART;
      }
    }

    return { dataSpace, nodeList, pageContentType, tableCategory };
  }

  updateChart(
    treeFlatNode: SLabsFlatNode,
    ds: DataSpaceModel,
    nodeList: NodeModel[],
  ) {
    this.chartNode =
      nodeList.find((n) => treeFlatNode.name === n.name) || this.chartNode;

    ds.nodeList = [this.chartNode];
  }

  createInfoTableData(
    obj: DataSpaceModel | TemplateModel,
    props: string[],
  ): {
    property: string;
    value: string | string[];
  }[] {
    return Object.entries(obj)
      .filter((entry) => props.includes(entry[0]))
      .map((entry) => {
        entry[0] = entry[0] === 'lastUpdate' ? 'Last update' : entry[0];
        return {
          property: entry[0],
          value: entry[1],
        };
      });
  }
}
