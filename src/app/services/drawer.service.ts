import { Injectable } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';

@Injectable({
  providedIn: 'root',
})
export class DrawerService {
  openComponentDrawer(componentInfoDrawer: MatDrawer) {
    componentInfoDrawer.openedStart.subscribe(() =>
      this.toggleClasses('open', 'r-0', componentInfoDrawer),
    );
    componentInfoDrawer.open();
  }

  closeComponentDrawer(componentInfoDrawer: MatDrawer) {
    componentInfoDrawer
      .close()
      .then(() => this.toggleClasses('close', 'r-0', componentInfoDrawer));
  }

  openTestCaseDrawer(testCaseInfoDrawer: MatDrawer) {
    testCaseInfoDrawer.openedStart.subscribe(() =>
      this.toggleClasses('open', 'p-0', testCaseInfoDrawer),
    );
    testCaseInfoDrawer.open();
  }

  closeTestCaseDrawer(testCaseInfoDrawer: MatDrawer) {
    testCaseInfoDrawer
      .close()
      .then(() => this.toggleClasses('close', 'p-0', testCaseInfoDrawer));
  }

  toggleClasses(
    mode: 'open' | 'close',
    toggleClass: string,
    drawer: MatDrawer,
  ) {
    if (mode === 'open') {
      document.getElementById('slabs-header')?.classList.add('z-index-1');
      drawer?._content.nativeElement.classList.add(toggleClass);
      return;
    }

    document.getElementById('slabs-header')?.classList.remove('z-index-1');
    drawer?._content.nativeElement.classList.remove(toggleClass);
  }
}
