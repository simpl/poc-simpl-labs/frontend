import { Injectable } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { KeycloakProfile } from 'keycloak-js';

@Injectable({ providedIn: 'root' })
export class KeycloakOperationService {
  constructor(private readonly keycloak: KeycloakService) {}

  isLoggedIn(): boolean {
    return this.keycloak.isLoggedIn();
  }

  logout(): Promise<void> {
    return this.keycloak.logout();
  }

  getUserProfile(): Promise<KeycloakProfile> {
    return this.keycloak.loadUserProfile();
  }
}
