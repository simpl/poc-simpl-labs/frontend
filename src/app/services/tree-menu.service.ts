import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { SLabsFlatNode } from '../models/interfaces/tree-node.interface';
import { GraphChartDatum } from '../models/interfaces/graph-chart-datum.interface';
import { TemplateService } from './template.service';
import { NodeModel } from '../models/interfaces/data-space/node.interface';
import { DataSpaceService } from './data-space.service';
import { getNodeLastSuffix } from '../helpers/get-node-last-suffix';
import { NodeType } from '../models/enums/data-space/node-type.enum';

@Injectable({
  providedIn: 'root',
})
export class TreeMenuService {
  breadcrumbs: SLabsFlatNode[] = [];
  dataSpaceBreadCrumb: SLabsFlatNode = {
    name: '',
    expandable: true,
    level: -1,
    type: '',
    expanded: true,
    id: null!,
  };
  inputFieldShown: boolean = false;

  treeMenuNodeSubject$ = new Subject<SLabsFlatNode>();
  graphChartNodeSubject$ = new Subject<GraphChartDatum>();
  breadcrumbsSubject$ = new Subject<SLabsFlatNode[]>();

  constructor(
    private readonly templateService: TemplateService,
    private readonly dataSpaceService: DataSpaceService,
  ) {
    this.templateService.viewedTemplateSubject$
      .asObservable()
      .subscribe((template) => {
        if (template) {
          this.dataSpaceBreadCrumb.name = template.dataSpace.name;
          this.breadcrumbs = [this.dataSpaceBreadCrumb];
        }
      });
  }

  createBreadcrumbs(
    breadcrumbs: SLabsFlatNode[],
    node: SLabsFlatNode,
    isMonitoring: boolean,
  ): SLabsFlatNode[] {
    const idx = node.level + 1;
    const len = breadcrumbs.length;
    breadcrumbs.splice(idx, len - idx);

    if (node.expanded || (isMonitoring && node.level === 2))
      breadcrumbs.push(node);

    return breadcrumbs;
  }

  updateBreadcrumbs(node: SLabsFlatNode, isMonitoring: boolean) {
    this.createBreadcrumbs(this.breadcrumbs, node, isMonitoring);
  }

  renameNode(
    node: GraphChartDatum,
    nodeList: NodeModel[],
    newNodeName: string,
  ): NodeModel[] | void {
    this.inputFieldShown = false;
    if (!newNodeName) return;

    let _nodeList: NodeModel[] = [...nodeList];
    const idx = _nodeList.findIndex((n) => n.name === node.name);
    if (idx >= 0) {
      const renamedNode = { ..._nodeList[idx] };
      renamedNode.name = newNodeName;
      _nodeList.splice(idx, 1, renamedNode);
      this.dataSpaceService.updateNewDataSpaceNodeList(_nodeList);
      return _nodeList;
    }
  }

  duplicateNode(
    node: GraphChartDatum,
    nodeList: NodeModel[],
  ): NodeModel[] | void {
    let _nodeList: NodeModel[] = [...nodeList];
    const found = _nodeList.find((n) => n.name === node.name);

    if (found) {
      const newNode: NodeModel = { ...found };
      const lastSuffix: number = getNodeLastSuffix(
        _nodeList,
        node.type as NodeType,
      );
      newNode.name = newNode.type + ('_' + (lastSuffix + 1));
      _nodeList.push(newNode);
      this.dataSpaceService.updateNewDataSpaceNodeList(_nodeList);
      return _nodeList;
    }
  }

  deleteNode(node: GraphChartDatum, nodeList: NodeModel[]): NodeModel[] | void {
    let _nodeList: NodeModel[] = [...nodeList];
    const idx = _nodeList.findIndex((n) => n.name === node.name);

    if (idx >= 0) {
      _nodeList.splice(idx, 1);
      this.dataSpaceService.updateNewDataSpaceNodeList(_nodeList);
      return _nodeList;
    }
  }
}
