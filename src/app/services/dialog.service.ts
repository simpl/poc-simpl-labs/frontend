import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { WarningDialogComponent } from '../components/_shared/warning-dialog/warning-dialog.component';
import { ErrorDialogComponent } from '../components/_shared/error-dialog/error-dialog.component';
import { ErrorDialogData } from '../models/interfaces/error-dialog-data.interface';
import { WarningDialogData } from '../models/interfaces/warning-dialog-data.interface';

@Injectable({
  providedIn: 'root',
})
export class DialogService {
  constructor(public dialog: MatDialog) {}

  openWarningDialog<T>(
    data: WarningDialogData<T>,
  ): MatDialogRef<WarningDialogComponent<T>> {
    return this.dialog.open(WarningDialogComponent<T>, {
      disableClose: true,
      data,
    });
  }

  openErrorDialog(data: ErrorDialogData): MatDialogRef<ErrorDialogComponent> {
    return this.dialog.open(ErrorDialogComponent, { data });
  }
}
