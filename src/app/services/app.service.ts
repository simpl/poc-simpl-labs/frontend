import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { KeycloakConfig, KeycloakInitOptions } from 'keycloak-js';

import { environment } from '../../environments/environment';

export interface EnvUrl {
  keycloak: {
    config: string | KeycloakConfig | undefined;
    initOptions: KeycloakInitOptions;
  };
  apiUrl: string;
  consoleURL: string;
  simplUrl: string;
}

export enum Env2FName {
  DEV = 'app-urls.dev.json',
  PROD = 'app-urls.json',
}

export enum UrlPath {
  DEV = './assets',
  PROD = './slabs-conf',
}

@Injectable({
  providedIn: 'root',
})
export class AppService {
  private readonly urlsPath = UrlPath.PROD.toString();
  private readonly fileName = Env2FName.PROD.toString();
  private envUrl!: EnvUrl;

  get appUrls() {
    return this.envUrl;
  }

  constructor(private readonly http: HttpClient) {
    if (environment.production) {
      this.urlsPath = UrlPath.PROD.toString();
      this.fileName = Env2FName.PROD.toString();
    } else {
      this.urlsPath = UrlPath.DEV.toString();
      this.fileName = Env2FName.DEV.toString();
    }
  }

  async load(): Promise<EnvUrl> {
    const envUrlPromise: Promise<EnvUrl> = firstValueFrom(
      this.http.get<EnvUrl>(this.urlsPath + '/' + this.fileName),
    );
    this.envUrl = await envUrlPromise;

    return envUrlPromise;
  }
}
