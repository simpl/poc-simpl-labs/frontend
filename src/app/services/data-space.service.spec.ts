import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

import { DataSpaceService } from './data-space.service';
import { dataSpacesMock } from '../../mocks/data-space-mock';
import { slabsFlatNodesMock } from '../../tests/data-spaces/graph-chart-datum-mock';
import { DataSpaceModel } from '../models/interfaces/data-space/data-space.interface';
import { basicTemplatesMock } from '../components/wizard/constants/data-space-creation.data';
import { PageContentType } from '../models/enums/data-space/page-content-type.enum';
import { AppService } from './app.service';
import { envUrl } from '../../tests/env-url-mock';

const mockDataSpace: DataSpaceModel = { ...dataSpacesMock[1] };

describe('DataSpaceService', () => {
  let dataSpaceService: DataSpaceService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;
  let appServiceSpy: jasmine.SpyObj<AppService>;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', [
      'get',
      'post',
      'delete',
    ]);

    appServiceSpy = jasmine.createSpyObj('AppService', ['load']);
    Object.defineProperty(appServiceSpy, 'appUrls', {
      get: () => ({
        apiUrl: 'http://api-mock.org',
        simplUrl: '"http://simpl-mock.org/',
      }),
    });

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        DataSpaceService,
        { provide: HttpClient, useValue: httpClientSpy },
        { provide: AppService, useValue: appServiceSpy },
      ],
    });
    dataSpaceService = TestBed.inject(DataSpaceService);
    dataSpaceService.chartNode = mockDataSpace.nodeList[0];

    httpClientSpy = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;

    httpClientSpy.get.and.returnValue(of(mockDataSpace));

    spyOnProperty(appServiceSpy, 'appUrls', 'get').and.returnValue(envUrl);

    spyOn(dataSpaceService, 'updateChart').and.callThrough();
  });

  it('should be created', () => {
    expect(dataSpaceService).toBeTruthy();
  });

  it('should have constants defined', () => {
    expect(dataSpaceService.dataSpacesLimit).toBe(2);
    expect(dataSpaceService.nDataSpaces).toBeFalsy();
    expect(dataSpaceService.timerInterval).toBe(3000);
  });

  it('#getDataSpacesNumber should return #nDataSpaces', () => {
    dataSpaceService.nDataSpaces = 2;
    expect(dataSpaceService.getDataSpacesNumber()).toBe(2);
  });

  it('#setDataSpacesNumber should set #nDataSpaces', () => {
    dataSpaceService.setDataSpacesNumber(1);
    expect(dataSpaceService.nDataSpaces).toBe(1);
  });

  it('#getDataSpaces should return stubbed #dataSpaces from a spy', () => {
    const stubDataSpaces = of(dataSpacesMock);
    httpClientSpy.get.and.returnValue(stubDataSpaces);

    expect(dataSpaceService.getDataSpaces())
      .withContext('service returned stub dataSpaces')
      .toBe(stubDataSpaces);
    expect(httpClientSpy.get.calls.count())
      .withContext('spy method was called once')
      .toBe(1);
    expect(httpClientSpy.get.calls.mostRecent().returnValue).toBe(
      stubDataSpaces,
    );
  });

  it('#createDataSpace should return stubbed #dataSpace from a spy', () => {
    const stubValue = of(dataSpacesMock[0]);
    httpClientSpy.post.and.returnValue(stubValue);

    expect(dataSpaceService.createDataSpace(dataSpacesMock[0]))
      .withContext('service returned stub dataSpace')
      .toBe(stubValue);
    expect(httpClientSpy.post.calls.count())
      .withContext('spy method was called once')
      .toBe(1);
    expect(httpClientSpy.post.calls.mostRecent().returnValue).toBe(stubValue);
  });

  it('#deleteDataSpace should return stubbed #dataSpace from a spy', () => {
    const stubValue = of(dataSpacesMock[0]);
    httpClientSpy.delete.and.returnValue(stubValue);

    expect(dataSpaceService.deleteDataSpace(dataSpacesMock[0].id!))
      .withContext('service returned stub dataSpace')
      .toBe(stubValue);
    expect(httpClientSpy.delete.calls.count())
      .withContext('spy method was called once')
      .toBe(1);
    expect(httpClientSpy.delete.calls.mostRecent().returnValue).toBe(stubValue);
  });

  it('should set #dataSpaces value', () => {
    dataSpaceService.dataSpacesSubject$.next(dataSpacesMock);
    expect(dataSpaceService.dataSpaces).toEqual(dataSpacesMock);
  });

  it('should set #newDataSpace value', () => {
    dataSpaceService.newDataSpaceSubject$.next(dataSpacesMock[0]);
    expect(dataSpaceService.newDataSpace).toEqual(dataSpacesMock[0]);
  });

  it('should update New DataSpace NodeList', () => {
    dataSpaceService.newDataSpace = mockDataSpace;
    dataSpaceService.updateNewDataSpaceNodeList(mockDataSpace.nodeList);
    expect(dataSpaceService.newDataSpace.nodeList).toEqual(
      mockDataSpace.nodeList,
    );
  });

  it('should get a DataSpace', () => {
    httpClientSpy.get.and.returnValue(of(mockDataSpace));
    dataSpaceService.getDataSpace(2);
    expect(httpClientSpy.get).toHaveBeenCalled();
  });

  it('should not call #updateChart() when level < 0', () => {
    dataSpaceService.updatePageContent(
      {
        ...slabsFlatNodesMock[0],
        expandable: true,
        level: -1,
      },
      mockDataSpace,
      mockDataSpace.nodeList,
      PageContentType.GRAPH_CHART,
      mockDataSpace.nodeList[0].categoryList[0],
    );
    expect(dataSpaceService.updateChart).not.toHaveBeenCalled();
  });

  it('should call #updateChart()', () => {
    dataSpaceService.updatePageContent(
      {
        ...slabsFlatNodesMock[0],
        expandable: true,
        expanded: true,
        level: 0,
      },
      mockDataSpace,
      mockDataSpace.nodeList,
      PageContentType.COMPONENTS_TABLE,
      mockDataSpace.nodeList[0].categoryList[1],
    );
    expect(dataSpaceService.updateChart).toHaveBeenCalled();
  });

  it('should not call #updateChart()', () => {
    dataSpaceService.updatePageContent(
      {
        ...slabsFlatNodesMock[0],
        expandable: true,
        expanded: false,
        level: 0,
      },
      mockDataSpace,
      mockDataSpace.nodeList,
      PageContentType.GRAPH_CHART,
      mockDataSpace.nodeList[0].categoryList[0],
    );
    expect(dataSpaceService.updateChart).not.toHaveBeenCalled();
  });

  it('should not call #updateChart() when !expanded', () => {
    dataSpaceService.updatePageContent(
      {
        ...slabsFlatNodesMock[0],
        expandable: true,
        expanded: false,
        level: 1,
      },
      mockDataSpace,
      mockDataSpace.nodeList,
      PageContentType.GRAPH_CHART,
      mockDataSpace.nodeList[0].categoryList[0],
    );
    expect(dataSpaceService.updateChart).not.toHaveBeenCalled();
  });

  it('should not call #updateChart() when expanded', () => {
    dataSpaceService.chartNode = mockDataSpace.nodeList[0];

    dataSpaceService.updatePageContent(
      {
        ...slabsFlatNodesMock[0],
        expandable: true,
        expanded: true,
        level: 1,
      },
      mockDataSpace,
      mockDataSpace.nodeList,
      PageContentType.GRAPH_CHART,
      mockDataSpace.nodeList[0].categoryList[0],
    );
    expect(dataSpaceService.updateChart).not.toHaveBeenCalled();
  });

  it('should set #chartNode value', () => {
    const { nodeList } = mockDataSpace;
    dataSpaceService.updateChart(
      slabsFlatNodesMock[1],
      mockDataSpace,
      nodeList,
    );
    const node = nodeList.find((n) => slabsFlatNodesMock[1].name === n.name)!;
    expect(dataSpaceService.chartNode).toEqual(node);
  });

  it('should create InfoTable DataSource for DataSpace', () => {
    const props = ['type', 'tagList'];
    const data = dataSpaceService.createInfoTableData(mockDataSpace, props);
    const target = Object.entries(mockDataSpace)
      .filter((entry) => props.includes(entry[0]))
      .map((entry) => {
        entry[0] = entry[0] === 'lastUpdate' ? 'Last update' : entry[0];
        return {
          property: entry[0],
          value: entry[1],
        };
      });
    expect(data).toEqual(target);
  });

  it('should create InfoTable DataSource for Template', () => {
    const props = ['author', 'lastUpdate', 'license', 'tags'];
    const data = dataSpaceService.createInfoTableData(
      basicTemplatesMock[0],
      props,
    );
    const target = Object.entries(basicTemplatesMock[0])
      .filter((entry) => props.includes(entry[0]))
      .map((entry) => {
        entry[0] = entry[0] === 'lastUpdate' ? 'Last update' : entry[0];
        return {
          property: entry[0],
          value: entry[1],
        };
      });
    expect(data).toEqual(target);
  });
});
