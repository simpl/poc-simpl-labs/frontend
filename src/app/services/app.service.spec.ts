import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { AppService } from './app.service';
import { envUrl } from '../../tests/env-url-mock';

describe('AppService', () => {
  let service: AppService;
  let appUrlsSpy: jasmine.Spy;
  let loadSpy: jasmine.Spy;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AppService],
    });
    service = TestBed.inject(AppService);

    appUrlsSpy = spyOnProperty(service, 'appUrls', 'get');
    loadSpy = spyOn(service, 'load');
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return the expected value', () => {
    appUrlsSpy.and.returnValue(envUrl);
    expect(service.appUrls).toEqual(envUrl);
  });

  it('should return null when data is not available', () => {
    appUrlsSpy.and.returnValue(null);
    expect(service.appUrls).toBeNull();
  });
});
