import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { delay, Observable, of } from 'rxjs';

import { TenantConsumption } from '../models/interfaces/monitoring/tenant-consumption.interface';
import { CurrentConsumption } from '../models/interfaces/monitoring/current-consumption.interface';
import { ConsumptionType } from '../models/enums/monitoring/consumption-type.enum';
import { eventsHistoryMock } from '../components/_shared/events-history-dialog/events-history-mock';
import { HistoricalConsumption } from '../models/interfaces/monitoring/historical-consumption.interface';
import {
  Page,
  PageRequest,
} from '../models/interfaces/monitoring/paginated-endpoint.type';
import { EventsHistoryData } from '../models/interfaces/events-history-data.interface';
import { AppService } from './app.service';

@Injectable({
  providedIn: 'root',
})
export class MonitoringService {
  private readonly baseUrl = `${this.appService.appUrls?.apiUrl}/system-monitoring`;
  private readonly generalUrl = 'general';
  private readonly currentUrl = 'current';
  private readonly historicalUrl = 'historical';
  private readonly dataspaceUrl = 'dataspace';

  constructor(
    private readonly http: HttpClient,
    private readonly appService: AppService,
  ) {}

  getTenantConsumption(
    endPoint: ConsumptionType | string,
  ): Observable<TenantConsumption> {
    return this.http.get<TenantConsumption>(
      `${this.baseUrl}/${this.generalUrl}/${this.currentUrl}/${endPoint}`,
    );
  }

  getCurrentConsumptions(
    currentDomain: string,
    domainId: number | string = '',
    pageReq?: PageRequest<CurrentConsumption>,
  ): Observable<Page<CurrentConsumption> | CurrentConsumption[]> {
    const pageQueryPrms = pageReq
      ? `?pageNumber=${pageReq?.page}&pageSize=${pageReq?.size}&sortBy=${pageReq?.sort?.property}`
      : '';
    domainId = domainId ? '/' + domainId : domainId;
    return this.http.get<Page<CurrentConsumption> | CurrentConsumption[]>(
      `${this.baseUrl}/${currentDomain}/${this.currentUrl}${domainId}${pageQueryPrms}`,
    );
  }

  getAllCurrentConsumptions(
    dataSpaceId: number,
  ): Observable<CurrentConsumption[]> {
    return this.http.get<CurrentConsumption[]>(
      `${this.baseUrl}/${this.dataspaceUrl}/${this.currentUrl}/all/${dataSpaceId}`,
    );
  }

  getHistoricalConsumptions(
    historicalDomain: string,
    endPoint: ConsumptionType | string,
    startDate: string,
    endDate: string,
    domainId: number | string = '',
  ): Observable<HistoricalConsumption[]> {
    domainId = domainId ? domainId + '/' : domainId;
    return this.http.get<HistoricalConsumption[]>(
      `${this.baseUrl}/${historicalDomain}/${this.historicalUrl}/${endPoint}/${domainId}${startDate}/${endDate}`,
    );
  }

  getEventsHistory(): Observable<EventsHistoryData[]> {
    return of(eventsHistoryMock).pipe(delay(700));
  }
}
