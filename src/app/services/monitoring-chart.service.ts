import { ElementRef, Injectable } from '@angular/core';
import { debounceTime, fromEvent } from 'rxjs';

import * as echarts from 'echarts/core';
import { LineChart } from 'echarts/charts';
import { GridComponent, MarkLineComponent } from 'echarts/components';
import { CanvasRenderer } from 'echarts/renderers';
import { UniversalTransition } from 'echarts/features';

import { ConsumptionType } from '../models/enums/monitoring/consumption-type.enum';
import { tenantConsumptionOption } from '../components/monitoring/infrastructure/charts-options/tenant-consumption-option';
import {
  HistoricalConsumptionOption,
  historicalConsumptionOption,
} from '../components/monitoring/infrastructure/charts-options/historical-consumption-option';
import { MatDialog } from '@angular/material/dialog';
import { SelectedPointPopupComponent } from '../components/monitoring/infrastructure/historical-consumption/selected-point-popup/selected-point-popup.component';
import {
  HistoricalConsumption,
  QuantityHistoricalConsumption,
  SelectedPointPopupData,
} from '../models/interfaces/monitoring/historical-consumption.interface';

@Injectable({
  providedIn: 'root',
})
export class MonitoringChartService {
  public timerInterval = 3000;
  public counter = 0;
  elapsedTime = 0;
  increment = this.timerInterval / 3000;

  tenantCPUData: number[][] = [];
  tenantMemoryData: number[][] = [];
  tenantStorageData: number[][] = [];

  historicalCPUData: HistoricalConsumption[] = [];
  historicalMemoryData: HistoricalConsumption[] = [];
  historicalStorageData: HistoricalConsumption[] = [];

  consumptionColors = {
    [ConsumptionType.CPU]: '#4387AF',
    [ConsumptionType.MEMORY]: '#001D6C',
    [ConsumptionType.STORAGE]: '#540CAC',
    tenantBg: '#f2f4f8',
    historicalAvg: '#da1e28',
    historicalBg: '#fcfcfc',
  };

  maxValues = {
    [ConsumptionType.CPU]: 0.00002,
    [ConsumptionType.MEMORY]: 0.26,
    [ConsumptionType.STORAGE]: 20,
  };

  tenantCPUChart!: echarts.ECharts;
  tenantMemoryChart!: echarts.ECharts;
  tenantStorageChart!: echarts.ECharts;

  historicalCPUChart!: echarts.ECharts;
  historicalMemoryChart!: echarts.ECharts;
  historicalStorageChart!: echarts.ECharts;

  minTime = -9;
  maxTime = 0;

  constructor(public _historicalPopup: MatDialog) {
    echarts.use([
      GridComponent,
      MarkLineComponent,
      LineChart,
      CanvasRenderer,
      UniversalTransition,
    ]);
  }

  initializeTenantCharts(
    valueType: ConsumptionType | string,
    elm: ElementRef<HTMLElement>,
  ) {
    switch (valueType.toUpperCase()) {
      case ConsumptionType.CPU.toUpperCase():
        this.tenantCPUChart = echarts.init(elm.nativeElement);
        this.tenantCPUChart.setOption(tenantConsumptionOption);
        this.tenantCPUChart.setOption({
          series: {
            areaStyle: { color: this.consumptionColors[ConsumptionType.CPU] },
          },
        });
        break;

      case ConsumptionType.MEMORY.toUpperCase():
        this.tenantMemoryChart = echarts.init(elm.nativeElement);
        this.tenantMemoryChart.setOption(tenantConsumptionOption);
        this.tenantMemoryChart.setOption({
          series: {
            areaStyle: {
              color: this.consumptionColors[ConsumptionType.MEMORY],
            },
          },
        });
        break;

      case ConsumptionType.STORAGE.toUpperCase():
        this.tenantStorageChart = echarts.init(elm.nativeElement);
        this.tenantStorageChart.setOption(tenantConsumptionOption);
        this.tenantStorageChart.setOption({
          series: {
            areaStyle: {
              color: this.consumptionColors[ConsumptionType.STORAGE],
            },
          },
        });
        break;
    }
  }

  updateTenantCharts(value: number, valueType: ConsumptionType | string) {
    this.counter++;

    if (this.counter % 3 === 0) {
      this.minTime += this.increment;
      this.maxTime += this.increment;
      this.elapsedTime = this.counter / 3;
    }

    let closestMinIdx = -1;

    switch (valueType.toUpperCase()) {
      case ConsumptionType.CPU.toUpperCase():
        this.tenantCPUData.length > 0 &&
          this.tenantCPUData.length % 10 === 0 &&
          (this.maxValues[ConsumptionType.CPU] = Math.max(
            ...this.tenantCPUData.slice(0, 10).map((x) => x[1]),
          ));

        this.tenantCPUData.push([this.elapsedTime, value]);

        this.tenantCPUChart.setOption({
          series: {
            data: this.tenantCPUData.map((d) => [
              d[0],
              d[1] / this.maxValues[ConsumptionType.CPU],
            ]),
          },
          xAxis: {
            min: this.minTime,
            max: this.maxTime,
            show: false,
          },
        });

        this.tenantCPUData.some((d, i) => {
          const found = d[0] > this.minTime;
          closestMinIdx = found ? i : closestMinIdx;
          return found;
        });

        this.tenantCPUData =
          closestMinIdx > 0
            ? this.tenantCPUData.slice(
                closestMinIdx - 1,
                this.tenantCPUData.length,
              )
            : this.tenantCPUData;

        setTimeout(() => this.tenantCPUChart.resize(), 10);
        break;

      case ConsumptionType.MEMORY.toUpperCase():
        this.tenantMemoryData.length > 0 &&
          this.tenantMemoryData.length % 10 === 0 &&
          (this.maxValues[ConsumptionType.MEMORY] = Math.max(
            ...this.tenantMemoryData.slice(0, 10).map((x) => x[1]),
          ));

        this.tenantMemoryData.push([this.elapsedTime, value]);

        this.tenantMemoryChart.setOption({
          series: {
            data: this.tenantMemoryData.map((d) => [
              d[0],
              d[1] / this.maxValues[ConsumptionType.MEMORY],
            ]),
          },
          xAxis: {
            min: this.minTime,
            max: this.maxTime,
            show: false,
          },
        });

        this.tenantMemoryData.some((d, i) => {
          const found = d[0] > this.minTime;
          closestMinIdx = found ? i : closestMinIdx;
          return found;
        });

        this.tenantMemoryData =
          closestMinIdx > 0
            ? this.tenantMemoryData.slice(
                closestMinIdx - 1,
                this.tenantMemoryData.length,
              )
            : this.tenantMemoryData;

        setTimeout(() => this.tenantMemoryChart.resize(), 10);
        break;

      case ConsumptionType.STORAGE.toUpperCase():
        this.tenantStorageData.length > 0 &&
          this.tenantStorageData.length % 10 === 0 &&
          (this.maxValues[ConsumptionType.STORAGE] = Math.max(
            ...this.tenantStorageData.slice(0, 10).map((x) => x[1]),
          ));

        this.tenantStorageData.push([this.elapsedTime, value]);

        this.tenantStorageChart.setOption({
          series: {
            data: this.tenantStorageData.map((d) => [
              d[0],
              d[1] / this.maxValues[ConsumptionType.STORAGE],
            ]),
          },
          xAxis: {
            min: this.minTime,
            max: this.maxTime,
            show: false,
          },
        });

        this.tenantStorageData.some((d, i) => {
          const found = d[0] > this.minTime;
          closestMinIdx = found ? i : closestMinIdx;
          return found;
        });

        this.tenantStorageData =
          closestMinIdx > 0
            ? this.tenantStorageData.slice(
                closestMinIdx - 1,
                this.tenantStorageData.length,
              )
            : this.tenantStorageData;

        setTimeout(() => this.tenantStorageChart.resize(), 10);
        break;

      default:
        return;
    }
  }

  resetTenantCharts() {
    this.counter = 0;
    this.elapsedTime = 0;
    this.tenantCPUData = [];
    this.tenantMemoryData = [];
    this.tenantStorageData = [];
    this.minTime = -9;
    this.maxTime = 0;
    !!this.tenantCPUChart && this.tenantCPUChart.dispose();
    !!this.tenantMemoryChart && this.tenantMemoryChart.dispose();
    !!this.tenantStorageChart && this.tenantStorageChart.dispose();
  }

  initializeHistoricalCharts(
    valueType: ConsumptionType | string,
    elm: ElementRef<HTMLElement>,
  ) {
    switch (valueType.toUpperCase()) {
      case ConsumptionType.CPU.toUpperCase():
        this.historicalCPUChart = echarts.init(elm.nativeElement, null, {
          width: 1000,
          height: 400,
        });
        this.historicalCPUChart.setOption(historicalConsumptionOption);
        this.historicalCPUChart.setOption({
          grid: {
            backgroundColor: this.consumptionColors.historicalBg,
          },
          series: [
            {
              name: 'consumption',
              lineStyle: {
                color: this.consumptionColors[ConsumptionType.CPU],
              },
            },
          ],
        });
        this.historicalCPUChart.on(
          'click',
          'series',
          (elmEvent: echarts.ECElementEvent) =>
            elmEvent.dataIndex >= 0 &&
            this.openHistoricalPopup(elmEvent, ConsumptionType.CPU),
        );
        break;

      case ConsumptionType.MEMORY.toUpperCase():
        this.historicalMemoryChart = echarts.init(elm.nativeElement, null, {
          width: 1000,
          height: 400,
        });
        this.historicalMemoryChart.setOption(historicalConsumptionOption);
        this.historicalMemoryChart.setOption({
          grid: {
            backgroundColor: this.consumptionColors.historicalBg,
          },
          series: [
            {
              name: 'consumption',
              lineStyle: {
                color: this.consumptionColors[ConsumptionType.MEMORY],
              },
            },
          ],
        });
        this.historicalMemoryChart.on(
          'click',
          'series',
          (elmEvent: echarts.ECElementEvent) =>
            elmEvent.dataIndex >= 0 &&
            this.openHistoricalPopup(elmEvent, ConsumptionType.MEMORY),
        );
        break;

      case ConsumptionType.STORAGE.toUpperCase():
        this.historicalStorageChart = echarts.init(elm.nativeElement, null, {
          width: 1000,
          height: 400,
        });
        this.historicalStorageChart.setOption(historicalConsumptionOption);
        this.historicalStorageChart.setOption({
          grid: {
            backgroundColor: this.consumptionColors.historicalBg,
          },
          series: [
            {
              name: 'consumption',
              lineStyle: {
                color: this.consumptionColors[ConsumptionType.STORAGE],
              },
            },
          ],
        });
        this.historicalStorageChart.on(
          'click',
          'series',
          (elmEvent: echarts.ECElementEvent) =>
            elmEvent.dataIndex >= 0 &&
            this.openHistoricalPopup(elmEvent, ConsumptionType.STORAGE),
        );
        break;
    }

    fromEvent(window, 'resize')
      .pipe(debounceTime(10))
      .subscribe(() => {
        this.historicalCPUChart.resize();
        this.historicalMemoryChart.resize();
        this.historicalStorageChart.resize();
      });
  }

  openHistoricalPopup(elmEvent: echarts.ECElementEvent, name: ConsumptionType) {
    let historicalConsumption: HistoricalConsumption;

    switch (name.toUpperCase()) {
      case ConsumptionType.CPU.toUpperCase():
        historicalConsumption = this.historicalCPUData.find(
          (_, i) => elmEvent.dataIndex === i,
        )!;
        break;

      case ConsumptionType.MEMORY.toUpperCase():
        historicalConsumption = this.historicalMemoryData.find(
          (_, i) => elmEvent.dataIndex === i,
        )!;
        break;
      case ConsumptionType.STORAGE.toUpperCase():
        historicalConsumption = this.historicalStorageData.find(
          (_, i) => elmEvent.dataIndex === i,
        )!;
        break;
    }

    const data: SelectedPointPopupData = { ...historicalConsumption!, name };
    this._historicalPopup.open(SelectedPointPopupComponent, {
      data,
      width: 'fit-content',
      height: 'fit-content',
    });
  }

  updateHistoricalCharts(quantity: QuantityHistoricalConsumption) {
    let data = quantity.historicalConsumptions.map((h) => {
      const month = new Date(new Date(h.date).getTime()).getMonth() + 1;
      const day = new Date(new Date(h.date).getTime()).getDate();
      return [day + '/' + month, (h.used / h.allocated) * 100];
    });

    const option: HistoricalConsumptionOption = {
      series: [
        {
          name: 'consumption',
          data: data.map((v) => v[1]),
          markLine: {
            data: [{ type: 'average', name: 'Average' }],
            lineStyle: {
              color: this.consumptionColors.historicalAvg,
              width: 3,
              type: 'solid',
            },
            label: { show: false },
            animation: true,
            animationDuration: 5,
            animationDurationUpdate: 5,
            animationEasing: 'backInOut',
            animationEasingUpdate: 'circularIn',
            symbol: 'none',
            silent: true,
            precision: 10,
          },
        },
      ],
      xAxis: {
        data: data.map((v) => v[0]),
      },
    };

    switch (quantity.name.toUpperCase()) {
      case ConsumptionType.CPU.toUpperCase():
        this.historicalCPUData = quantity.historicalConsumptions;

        this.historicalCPUChart.setOption(option);
        setTimeout(() => this.historicalCPUChart.resize(), 10);
        break;

      case ConsumptionType.MEMORY.toUpperCase():
        this.historicalMemoryData = quantity.historicalConsumptions;

        this.historicalMemoryChart.setOption(option);
        setTimeout(() => this.historicalMemoryChart.resize(), 10);
        break;

      case ConsumptionType.STORAGE.toUpperCase():
        this.historicalStorageData = quantity.historicalConsumptions;

        this.historicalStorageChart.setOption(option);
        setTimeout(() => this.historicalStorageChart.resize(), 10);
        break;
    }
  }
}
