import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { forkJoin, map, Observable, Subject, Subscription } from 'rxjs';

import { SlabsCustomComponentModel } from '../models/interfaces/slabs-custom-components/slabs-custom-component.interface';
import { TestSuiteModel } from '../models/interfaces/slabs-custom-components/test-suite.interface';
import { TestCaseModel } from '../models/interfaces/slabs-custom-components/test-case.interface';
import { TestExecutionStatus } from '../models/interfaces/slabs-custom-components/test-execution-status.interface';
import { SessionStartRequest } from '../models/interfaces/slabs-custom-components/session-start-request.interface';
import { SessionStartResponse } from '../models/interfaces/slabs-custom-components/session-start-response.interface';
import { SessionStopRequest } from '../models/interfaces/slabs-custom-components/session-stop-request.interface';
import { SessionStopResponse } from '../models/interfaces/slabs-custom-components/session-stop-response.interface';
import { DialogService } from './dialog.service';
import { DataSpaceModel } from '../models/interfaces/data-space/data-space.interface';
import { RowCheckbox } from '../models/interfaces/slabs-custom-components/row-checkbox.interface';
import { SubmittedTest } from '../models/interfaces/slabs-custom-components/submitted-test.interface';
import { LogsModel } from '../models/interfaces/slabs-custom-components/logs.interface';
import { ReportModel } from '../models/interfaces/slabs-custom-components/report.interface';
import { WarningDialogData } from '../models/interfaces/warning-dialog-data.interface';
import { AppService } from './app.service';

@Injectable({
  providedIn: 'root',
})
export class SlabsCustomComponentService {
  private readonly baseUrl = `${this.appService.appUrls?.apiUrl}/custom-components`;
  private readonly testSuitesUrl = 'test-suites';
  private readonly testCasesUrl = 'test-cases';
  private readonly testsHistoryUrl = 'tests-history';
  private readonly testsUrl = 'tests';
  private readonly statusUrl = 'status';
  private readonly logsUrl = 'logs';
  private readonly reportUrl = 'report';
  private readonly dataspacesUrl = 'dataspaces';

  public timerInterval = 3000;

  customComponents!: SlabsCustomComponentModel[];
  testCases: TestCaseModel[][] | undefined;
  submittedTests: SubmittedTest[] = [];

  customComponentsSubject$ = new Subject<SlabsCustomComponentModel[]>();
  viewedComponentSubject$ = new Subject<
    SlabsCustomComponentModel | undefined
  >();

  testSuitesSubject$ = new Subject<TestSuiteModel[]>();
  testCasesSubject$ = new Subject<TestCaseModel[][]>();
  specustomComponentheckboxSubject$ = new Subject<RowCheckbox | undefined>();
  submittedTestsSubject$ = new Subject<SubmittedTest[]>();

  timerSubscriptions: Subscription[] = [];
  testExecutionStatusSubs: Subscription[] = [];

  constructor(
    private readonly http: HttpClient,
    private readonly dialogService: DialogService,
    private readonly router: Router,
    private readonly appService: AppService,
  ) {
    this.testCasesSubject$
      .asObservable()
      .subscribe((testCases) => (this.testCases = testCases));

    this.customComponentsSubject$
      .asObservable()
      .subscribe(
        (customComponents) => (this.customComponents = customComponents),
      );
  }

  loadCustomComponents() {
    this.getCustomComponents().subscribe((customComponents) => {
      this.customComponents = customComponents;
      this.customComponentsSubject$.next(customComponents);

      this.customComponents.forEach((customComponent) =>
        this.loadTestSuites(customComponent),
      );
    });
  }

  loadTestSuites(customComponent: SlabsCustomComponentModel) {
    this.getTestSuitesBySpecificationId(
      customComponent.specificationId,
      customComponent.id,
    ).subscribe((testSuites) => {
      forkJoin(
        testSuites.map((ts) =>
          this.getTestCasesByTestSuiteId(ts.id, customComponent.id),
        ),
      ).subscribe((testCases) => {
        let rowCheckbox = this.setRowCheckbox(
          customComponent,
          testSuites,
          testCases,
        );

        let submittedTest: SubmittedTest = {
          customComponent,
          specificationCheckbox: rowCheckbox,
          minimized: undefined,
          testCases,
          testSuites,
          testExecutionStatus: null!,
          sessionStartResponse: null!,
        };
        this.updateSubmittedTests(submittedTest);
      });
    });
  }

  setRowCheckbox(
    customComponent: SlabsCustomComponentModel,
    testSuites: TestSuiteModel[],
    testCases: TestCaseModel[][],
  ): RowCheckbox {
    let rowCheckbox: RowCheckbox = {
      id: customComponent.specificationId.toString(),
      name: customComponent.specificationDescription,
      selected: false,
      children: testSuites.map((ts: TestSuiteModel) => ({
        id: ts.id,
        name: ts.name,
        selected: false,
        children: [],
      })),
    };

    rowCheckbox.children?.forEach((scb) => {
      let tcs = testCases.find((ts) =>
        ts.every((tc) => tc.testSuiteId === scb.id),
      );
      scb.children = [];
      tcs?.forEach((tc) => {
        scb.children?.push({
          id: tc.id,
          name: tc.name,
          selected: false,
        });
      });
    });

    return rowCheckbox;
  }

  getCustomComponents(): Observable<SlabsCustomComponentModel[]> {
    return this.http.get<SlabsCustomComponentModel[]>(this.baseUrl);
  }

  getTestSuitesBySpecificationId(
    specId: number,
    compId: number,
  ): Observable<TestSuiteModel[]> {
    return this.http.get<TestSuiteModel[]>(
      `${this.baseUrl}/${this.testSuitesUrl}/${specId}/${compId}`,
    );
  }

  getTestCasesByTestSuiteId(
    suiteId: string,
    compId: number,
  ): Observable<TestCaseModel[]> {
    return this.http.get<TestCaseModel[]>(
      `${this.baseUrl}/${this.testCasesUrl}/${suiteId}/${compId}`,
    );
  }

  getComponentTestsHistory(compId: number): Observable<TestExecutionStatus[]> {
    return this.http.get<TestExecutionStatus[]>(
      `${this.baseUrl}/${this.testsHistoryUrl}/${compId}`,
    );
  }

  getCustomComponentDataSpaces(compId: number): Observable<DataSpaceModel[]> {
    return this.http
      .get<DataSpaceModel[]>(`${this.baseUrl}/${this.dataspacesUrl}/${compId}`)
      .pipe(map((dss) => (Array.isArray(dss) ? dss : [dss])));
  }

  startSessionsExecution(
    sessionStartRequest: SessionStartRequest,
  ): Observable<SessionStartResponse> {
    return this.http.post<SessionStartResponse>(
      `${this.baseUrl}/${this.testsUrl}/start`,
      sessionStartRequest,
    );
  }

  stopSessionsExecution(
    sessionStopRequest: SessionStopRequest,
  ): Observable<SessionStopResponse> {
    return this.http.post<SessionStopResponse>(
      `${this.baseUrl}/${this.testsUrl}/stop`,
      sessionStopRequest,
    );
  }

  getTestExecutionStatus(
    specId: number,
    compId: number,
  ): Observable<TestExecutionStatus> {
    return this.http.get<TestExecutionStatus>(
      `${this.baseUrl}/${this.testsUrl}/${this.statusUrl}/${specId}/${compId}`,
    );
  }

  getTestSessionLogs(testSessionId: string): Observable<LogsModel> {
    return this.http.get<LogsModel>(
      `${this.baseUrl}/${this.testsUrl}/${this.logsUrl}/${testSessionId}`,
    );
  }

  getTestSessionReport(testSessionId: string): Observable<ReportModel> {
    return this.http.get<ReportModel>(
      `${this.baseUrl}/${this.testsUrl}/${this.reportUrl}/${testSessionId}`,
    );
  }

  stopTestsExecution(sessionStopRequest: SessionStopRequest) {
    this.openWarningDialog<SessionStopRequest>({
      title: `Stop conformance test session(s)`,
      content: `Do you really want to stop the conformance test session(s)`,
      confirm: true,
      callerData: sessionStopRequest,
    });
  }

  getSpecificationCheckbox(
    testSuites: TestSuiteModel[],
    testCases: TestCaseModel[][],
    testExecutionStatus: TestExecutionStatus,
  ): RowCheckbox {
    const {
      testCasesList,
      testExecution: { specificationId, specificationName },
    } = testExecutionStatus;

    const suiteCheckboxes: RowCheckbox[] = testSuites?.map((ts) => {
      const caseCheckboxes: RowCheckbox[] = testCases
        .reduce(
          (prev, curr) =>
            prev.concat(curr.filter((tc) => tc.testSuiteId === ts.id)),
          [],
        )
        .map((tc) => ({
          id: tc.id,
          name: tc.name,
          selected: testCasesList.map((_tc) => _tc.id).includes(tc.id),
          disabled: this.isTestRunning(testExecutionStatus),
        }));

      const suiteCheckbox: RowCheckbox = {
        id: ts.id,
        name: ts.name,
        disabled: this.isTestRunning(testExecutionStatus),
        selected: caseCheckboxes?.every((x) => x.selected),
        children: caseCheckboxes,
      };

      return suiteCheckbox;
    });

    return {
      id: specificationId.toString(),
      name: specificationName,
      selected: suiteCheckboxes?.every((scb) => scb.selected),
      children: suiteCheckboxes,
      disabled: this.isTestRunning(testExecutionStatus),
    };
  }

  isTestRunning(testExecutionStatus: TestExecutionStatus): boolean {
    if (!testExecutionStatus) return false;

    const {
      testExecution: { startDate },
      testCasesList,
    } = testExecutionStatus;

    return !!startDate && testCasesList.some((tc) => !tc.endDate);
  }

  openWarningDialog<T>(warningDialogData: WarningDialogData<T>) {
    let warningDialogRef =
      this.dialogService.openWarningDialog<T>(warningDialogData);

    warningDialogRef
      .afterClosed()
      .subscribe((data: WarningDialogData<SessionStopRequest>) => {
        if (data?.confirm && !!data?.callerData) {
          this.stopSessionsExecution(data.callerData).subscribe();
        }
      });
  }

  minimizePage(submittedTest: SubmittedTest) {
    submittedTest.minimized = true;
    this.updateSubmittedTests(submittedTest);
    this.router.navigateByUrl('/conformance');
  }

  maximizePage(submittedTest: SubmittedTest) {
    submittedTest.minimized = false;
    this.updateSubmittedTests(submittedTest);
  }

  updateSubmittedTests(submittedTest: SubmittedTest) {
    const idx = this.submittedTests.findIndex(
      (st) => st.customComponent.id === submittedTest.customComponent.id,
    );

    if (idx >= 0) this.submittedTests[idx] = { ...submittedTest };
    else this.submittedTests.push(submittedTest);

    this.submittedTestsSubject$.next(this.submittedTests);
  }
}
