import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

import { TemplateModel } from '../models/interfaces/template.interface';
import { basicTemplatesMock } from '../components/wizard/constants/data-space-creation.data';
import { AppService } from './app.service';

@Injectable({
  providedIn: 'root',
})
export class TemplateService {
  private readonly basicTemplatesUrl = `${this.appService.appUrls?.apiUrl}/basic-templates`;
  private readonly allTemplatesUrl = `${this.appService.appUrls?.apiUrl}/all-templates`;
  private newTemplate!: TemplateModel;
  private viewedTemplate!: TemplateModel;

  newTemplateSubject$: Subject<TemplateModel> = new Subject<TemplateModel>();
  viewedTemplateSubject$: Subject<TemplateModel> = new Subject<TemplateModel>();

  get selectedTemplate(): TemplateModel {
    return this.newTemplate || basicTemplatesMock.find((t) => t.isMVDS);
  }

  get previewedTemplate(): TemplateModel {
    return this.viewedTemplate;
  }

  constructor(
    private readonly http: HttpClient,
    private readonly appService: AppService,
  ) {
    this.viewedTemplateSubject$
      .asObservable()
      .subscribe((template) => (this.viewedTemplate = template));

    this.newTemplateSubject$
      .asObservable()
      .subscribe((template) => (this.newTemplate = template));
  }

  getBasicTemplates(): Observable<TemplateModel[]> {
    return this.http.get<TemplateModel[]>(this.basicTemplatesUrl);
  }

  getAllTemplates(): Observable<TemplateModel[]> {
    return this.http.get<TemplateModel[]>(this.allTemplatesUrl);
  }
}
