import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { of, Subject } from 'rxjs';
import { MatDialogRef } from '@angular/material/dialog';

import { SlabsCustomComponentService } from './slabs-custom-component.service';
import {
  testCasesSuite1Mock,
  testCasesSuite2Mock,
} from '../../mocks/test-cases-mock';
import { slabsCustomComponentsMock } from '../../mocks/slabs-custom-component-mock';
import { testSuites2Mock } from '../../mocks/test-suites-mock';
import { sessionStartRequestMock } from '../../mocks/session-start-request-mock';
import { testExecutionStatus1Mock } from '../../mocks/test-execution-status-mock';
import { submittedTestsMock } from '../../tests/slabs-custom-components/submitted-test-mock';
import { DialogService } from './dialog.service';
import { RouterModule } from '@angular/router';
import { routes } from '../router/app.routes';
import { RowCheckbox } from '../models/interfaces/slabs-custom-components/row-checkbox.interface';
import { SubmittedTest } from '../models/interfaces/slabs-custom-components/submitted-test.interface';
import { AppService } from './app.service';
import { envUrl } from '../../tests/env-url-mock';

describe('SlabsCustomComponentService', () => {
  let service: SlabsCustomComponentService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;
  let dialogServiceSpy: jasmine.SpyObj<DialogService>;
  let testExecutionStatusSpy: jasmine.Spy<any>;
  let appServiceSpy: jasmine.SpyObj<AppService>;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', [
      'get',
      'post',
      'delete',
    ]);

    dialogServiceSpy = jasmine.createSpyObj('DialogService', [
      'openWarningDialog',
    ]);

    appServiceSpy = jasmine.createSpyObj('AppService', ['load']);
    Object.defineProperty(appServiceSpy, 'appUrls', {
      get: () => ({
        apiUrl: 'http://api-mock.org',
        simplUrl: '"http://simpl-mock.org/',
      }),
    });

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterModule.forRoot(routes)],
      providers: [
        SlabsCustomComponentService,
        { provide: HttpClient, useValue: httpClientSpy },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: AppService, useValue: appServiceSpy },
      ],
    });
    service = TestBed.inject(SlabsCustomComponentService);

    service.testCasesSubject$.next([testCasesSuite2Mock]);
    service.submittedTestsSubject$ = new Subject<SubmittedTest[]>();

    httpClientSpy.get.and.returnValue(of(slabsCustomComponentsMock));

    testExecutionStatusSpy = spyOn(service, 'getTestExecutionStatus');

    spyOn(service, 'getCustomComponents').and.callThrough();
    spyOn(service, 'getTestSuitesBySpecificationId').and.callThrough();
    spyOn(service, 'getTestCasesByTestSuiteId').and.callThrough();
    spyOn(service, 'updateSubmittedTests');

    testExecutionStatusSpy.and.callThrough();

    dialogServiceSpy.openWarningDialog.and.returnValue({
      afterClosed: () => of({}),
    } as MatDialogRef<any, any>);

    spyOn(service.submittedTestsSubject$, 'next');
    spyOn(service.customComponentsSubject$, 'next');

    spyOnProperty(appServiceSpy, 'appUrls', 'get').and.returnValue(envUrl);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should set #testCases value', () => {
    expect(service.testCases?.length).toBeGreaterThan(0);
  });

  it('should set #customComponents value', () => {
    service.loadCustomComponents();
    expect(service.customComponents?.length).toBeGreaterThan(0);
  });

  it('should call #getCustomComponents()', () => {
    service.loadCustomComponents();
    expect(service.getCustomComponents).toHaveBeenCalled();
  });

  it('should call #updateSubmittedTests()', () => {
    service.loadCustomComponents();
    expect(service.updateSubmittedTests).toHaveBeenCalled();
  });

  it('should call httpClient.get() w/ #getCustomComponents()', () => {
    service.getCustomComponents();
    expect(httpClientSpy.get).toHaveBeenCalled();
  });

  it('should call httpClient.get() w/ #getTestCasesByTestSuiteId()', () => {
    service.getTestCasesByTestSuiteId('1', 1);
    expect(httpClientSpy.get).toHaveBeenCalled();
  });

  it('should call httpClient.get() w/ #getComponentTestsHistory()', () => {
    service.getComponentTestsHistory(1);
    expect(httpClientSpy.get).toHaveBeenCalled();
  });

  it('should call httpClient.get() w/ #getCustomComponentDataSpaces()', () => {
    service.getCustomComponentDataSpaces(1);
    expect(httpClientSpy.get).toHaveBeenCalled();
  });

  it('should call httpClient.post() w/ #startSessionsExecution()', () => {
    service.startSessionsExecution(sessionStartRequestMock);
    expect(httpClientSpy.post).toHaveBeenCalled();
  });

  it('should call httpClient.post() w/ #stopSessionsExecution()', () => {
    service.stopSessionsExecution({ session: ['s1', 's2'] });
    expect(httpClientSpy.post).toHaveBeenCalled();
  });

  it('should call httpClient.get() w/ #getTestExecutionStatus()', () => {
    service.getTestExecutionStatus(1, 1);
    expect(httpClientSpy.get).toHaveBeenCalled();
  });

  it('should call httpClient.get() w/ #getTestSessionLogs()', () => {
    service.getTestSessionLogs('session-id-mock');
    expect(httpClientSpy.get).toHaveBeenCalled();
  });

  it('should call httpClient.get() w/ #getTestSessionReport()', () => {
    service.getTestSessionReport('session-id-mock');
    expect(httpClientSpy.get).toHaveBeenCalled();
  });

  it('should call httpClient.get() w/ #stopTestsExecution()', () => {
    spyOn(service, 'openWarningDialog');
    service.stopTestsExecution({ session: ['s1', 's2'] });
    expect(service.openWarningDialog).toHaveBeenCalled();
  });

  it('should create specificationCheckbox', () => {
    const specificationCheckbox = service.getSpecificationCheckbox(
      testSuites2Mock,
      [testCasesSuite1Mock],
      testExecutionStatus1Mock,
    );

    const {
      testCasesList,
      testExecution: { specificationId, specificationName },
    } = testExecutionStatus1Mock;

    const suiteCheckboxes: RowCheckbox[] = testSuites2Mock?.map((ts) => {
      const caseCheckboxes = [testCasesSuite1Mock]
        .reduce(
          (prev, curr) =>
            prev.concat(curr.filter((tc) => tc.testSuiteId === ts.id)),
          [],
        )
        .map((tc) => ({
          id: tc.id,
          name: tc.name,
          selected: testCasesList.map((_tc) => _tc.id).includes(tc.id),
          disabled: service.isTestRunning(testExecutionStatus1Mock),
        }));

      const suiteCheckbox = {
        id: ts.id,
        name: ts.name,
        disabled: service.isTestRunning(testExecutionStatus1Mock),
        selected: caseCheckboxes?.every((x) => x.selected),
        children: caseCheckboxes,
      };

      return suiteCheckbox;
    });

    expect(specificationCheckbox).toEqual({
      id: specificationId.toString(),
      name: specificationName,
      selected: suiteCheckboxes?.every((scb) => scb.selected),
      children: suiteCheckboxes,
      disabled: service.isTestRunning(testExecutionStatus1Mock),
    });
  });

  it('should open the WarningDialog', () => {
    service.openWarningDialog({ content: 'content-mock', title: '' });
    expect(dialogServiceSpy.openWarningDialog).toHaveBeenCalled();
  });

  it('should minimize test execution page', () => {
    service.minimizePage(submittedTestsMock[0]);
    expect(service.updateSubmittedTests).toHaveBeenCalled();
  });

  it('should maximize test execution page', () => {
    service.maximizePage(submittedTestsMock[0]);
    expect(service.updateSubmittedTests).toHaveBeenCalledWith(
      submittedTestsMock[0],
    );
  });
});
