import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoadingService {
  loadingSubject$ = new BehaviorSubject<boolean>(false);
  /**
   * Contains in-progress loading requests
   */
  loadingMap: Map<string, boolean> = new Map<string, boolean>();

  // The timer (in milliseconds) at which the GET dataSpaces method is called.
  timer!: number;
  // The timer (in milliseconds) at which the GET itb tests execution status is called.
  itbTimer!: number;
  // The timer (in milliseconds) at which the GET Tenant Consumption is called.
  monitoringTimer!: number;

  storedUrls: string[] = [];

  constructor() {}

  updateTimer(newTimer: number) {
    this.timer = newTimer;
  }
  updateITBTimer(newTimer: number) {
    this.itbTimer = newTimer;
  }
  updateMonitoringTimer(newTimer: number) {
    this.monitoringTimer = newTimer;
  }

  /**
   * Sets the loadingSubject$ property value based on the following:
   * - If loading is true, add the provided url to the loadingMap with a true value, set loadingSubject$ value to true
   * - If loading is false, remove the loadingMap entry and only when the map is empty will we set loadingSubject$ to false
   * This pattern ensures if there are multiple requests awaiting completion, we don't set loading to false before
   * other requests have completed. At the moment, this function is only called from the @link{HttpRequestInterceptor}
   * @param loading {boolean}
   * @param url {string}
   */
  setLoading(loading: boolean, url: string): void {
    if (!url)
      throw new Error(
        'The request URL must be provided to the LoadingService.setLoading function',
      );

    if (loading === true) {
      this.loadingMap.set(url, loading);
      if (url.endsWith('/tests/start') || url.endsWith('/tests/stop'))
        if (!this.storedUrls.includes(url)) this.storedUrls.push(url);
      this.loadingSubject$.next(true);
    } else if (loading === false && this.loadingMap.has(url)) {
      if (url.includes('/tests/status')) {
        this.storedUrls.forEach((su) => this.loadingMap.delete(su));
        this.storedUrls = [];
      }
      this.loadingMap.delete(url);
    }

    if (this.loadingMap.size === 0) this.loadingSubject$.next(false);
  }
}
