import { Routes } from '@angular/router';

import { NavIcon } from '../models/enums/navigation/nav-icon.enum';
import { SectionName } from '../models/enums/navigation/section-name.enum';
import { DashboardComponent } from '../components/dashboard/dashboard.component';
import { DataSpaceComponent } from '../components/data-space/data-space.component';
import { LearningComponent } from '../components/learning/learning.component';
import { SlabsSettingsComponent } from '../components/slabs-settings/slabs-settings.component';
import { TemplatesComponent } from '../components/templates/templates.component';
import { CreateDataSpaceComponent } from '../components/wizard/create-data-space/create-data-space.component';
import { TemplatePreviewComponent } from '../components/template-preview/template-preview.component';
import { DataSpaceViewComponent } from '../components/data-space-view/data-space-view.component';
import { createDataSpaceGuard } from '../guards/create-data-space.guard';
import { SLabsCustomComponentsComponent } from '../components/conformance/slabs-custom-components/slabs-custom-components.component';
import { ConformanceTestsComponent } from '../components/conformance-tests/conformance-tests.component';
import { InfrastructureMonitoringComponent } from '../components/monitoring/infrastructure/infrastructure-monitoring.component';
import { DataSpaceConsumptionComponent } from '../components/monitoring/infrastructure/data-space-consumption/data-space-consumption.component';
import { dataSpaceResolver } from '../resolvers/data-space.resolver';
import { SimplComponentsComponent } from '../components/simpl-components/simpl-components.component';
import { MonitoringComponent } from '../components/monitoring/monitoring.component';

export const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
    data: { sectionName: SectionName.DASHBOARD, icon: NavIcon.DASHBOARD },
  },
  {
    path: 'monitoring',
    children: [
      {
        path: '',
        component: MonitoringComponent,
        data: {
          sectionName: SectionName.MONITORING,
          icon: NavIcon.MONITORING,
        },
      },
      {
        path: 'infrastructure',
        children: [
          {
            path: '',
            component: InfrastructureMonitoringComponent,
            data: {
              sectionName: SectionName.INFRASTRUCTURE,
            },
          },
          {
            path: 'data-space/:id',
            component: DataSpaceConsumptionComponent,
            resolve: { dataSpace: dataSpaceResolver },
            data: { sectionName: SectionName.DATA_SPACE_CONSUMPTION },
          },
        ],
      },
    ],
  },
  {
    path: 'data-spaces',
    children: [
      {
        path: '',
        component: DataSpaceComponent,
        data: {
          sectionName: SectionName.DATA_SPACES,
          icon: NavIcon.DATA_SPACES,
        },
      },
      {
        path: 'create-data-space',
        component: CreateDataSpaceComponent,
        data: { sectionName: SectionName.CREATE_DATA_SPACE },
        canActivate: [createDataSpaceGuard],
      },
      {
        path: 'data-space-view/:id',
        component: DataSpaceViewComponent,
        resolve: { dataSpace: dataSpaceResolver },
        data: { sectionName: SectionName.DATA_SPACE_VIEW },
      },
    ],
  },
  {
    path: 'components',
    component: SimplComponentsComponent,
    data: {
      sectionName: SectionName.COMPONENTS,
      icon: NavIcon.COMPONENTS,
    },
  },
  {
    path: 'conformance',
    children: [
      {
        path: '',
        component: SLabsCustomComponentsComponent,
        data: {
          sectionName: SectionName.CONFORMANCE,
          icon: NavIcon.CONFORMANCE,
        },
      },
      {
        path: 'conformance-tests',
        component: ConformanceTestsComponent,
        data: { sectionName: SectionName.CONFORMANCE_TESTS },
      },
    ],
  },
  {
    path: 'templates',
    component: TemplatesComponent,
    data: { sectionName: SectionName.TEMPLATES, icon: NavIcon.TEMPLATES },
  },
  {
    path: 'template-preview',
    component: TemplatePreviewComponent,
    data: {
      sectionName: SectionName.TEMPLATE_PREVIEW,
    },
  },
  {
    path: 'learning',
    component: LearningComponent,
    data: { sectionName: SectionName.LEARNING, icon: NavIcon.LEARNING },
  },
  {
    path: 'settings',
    component: SlabsSettingsComponent,
    data: { sectionName: SectionName.SETTINGS, icon: NavIcon.SETTINGS },
  },
  { path: '**', redirectTo: 'dashboard' },
];
