import { CategoryType } from '../app/models/enums/data-space/category-type.enum';
import { TestSuiteModel } from '../app/models/interfaces/slabs-custom-components/test-suite.interface';

// custom-components/test-suites/1/1
export const testSuites1Mock: TestSuiteModel[] = [
  {
    id: '1',
    name: CategoryType.CONSENT + ' v.2',
    description: 'This is the Consent v.2 description',
    lastUpdate: '2024-03-28T19:24:20.841Z',
    testCasesNumber: 2,
    resultSuccess: 0.5,
    resultError: 0.5,
    resultNotExecuted: 0,
  },
  {
    id: '2',
    name: CategoryType.CONSENT + ' v.1.2',
    description: 'This is the Consent v.1.2 description',
    lastUpdate: '2024-02-24T19:24:20.841Z',
    testCasesNumber: 3,
    resultSuccess: 0.33,
    resultError: 0.33,
    resultNotExecuted: 0.33,
  },
];

// custom-components/test-suites/2/2
export const testSuites2Mock: TestSuiteModel[] = [
  {
    id: '3',
    name: CategoryType.IAA_LOCAL + ' v.1.0.1',
    description: 'This is the IAA Local v.1.0.1 description',
    lastUpdate: '2024-02-24T19:24:20.841Z',
    testCasesNumber: 2,
    resultSuccess: 0,
    resultError: 0.5,
    resultNotExecuted: 0.5,
  },
  {
    id: '4',
    name: CategoryType.IAA_LOCAL + ' v.3.2',
    description: 'This is the IAA Local v.3.2 description',
    lastUpdate: '2024-03-28T19:24:20.841Z',
    testCasesNumber: 1,
    resultSuccess: 1,
    resultError: 0,
    resultNotExecuted: 0,
  },
];

// custom-components/test-suites/3/3
export const testSuites3Mock: TestSuiteModel[] = [
  {
    id: '5',
    name: CategoryType.LOGGING + ' v.1.0.1',
    description: 'This is the Logging v.1.0.1 description',
    lastUpdate: '2024-02-24T19:24:20.841Z',
    testCasesNumber: 2,
    resultSuccess: 0,
    resultError: 0.5,
    resultNotExecuted: 0.5,
  },
];
