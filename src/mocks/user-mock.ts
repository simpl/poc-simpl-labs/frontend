import { User } from '../app/models/interfaces/user.interface';

export const usersMock: User[] = [
  {
    id: 1,
    username: 'UserName1',
    email: 'user1@europe.eu',
    name: 'Name 1',
  },
  {
    id: 2,
    username: 'UserName2',
    email: 'user2@europe.eu',
    name: 'Name 2',
  },
  {
    id: 3,
    username: 'UserName3',
    email: 'user3@europe.eu',
    name: 'Name 3',
  },
];
