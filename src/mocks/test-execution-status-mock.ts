import { CategoryType } from '../app/models/enums/data-space/category-type.enum';
import { TestCaseStatus } from '../app/models/enums/test-case-status.enum';
import { TestExecutionStatus } from '../app/models/interfaces/slabs-custom-components/test-execution-status.interface';
import {
  testCasesSuite1Mock,
  testCasesSuite2Mock,
  testCasesSuite3Mock,
  testCasesSuite4Mock,
  testCasesSuite5Mock,
} from './test-cases-mock';

const comp1TestCaseList = testCasesSuite1Mock.concat(
  testCasesSuite2Mock.slice(0, testCasesSuite2Mock.length - 1),
);
const comp2TestCaseList = testCasesSuite3Mock.concat(
  testCasesSuite4Mock.slice(0, testCasesSuite4Mock.length - 1),
);
const comp3TestCaseList = testCasesSuite5Mock.slice(
  0,
  testCasesSuite5Mock.length - 1,
);

export const testExecutionStatus1Mock: TestExecutionStatus = {
  testExecution: {
    id: 1,
    status: TestCaseStatus.UNDEFINED,
    // startDate: '2024-07-22T18:24:20.841Z',
    startDate: null!,
    endDate: null,
    passedTestsNumber: 0,
    failedTestsNumber: 0,
    undefinedTestsNumber: 0,
    totalTestsNumber: comp1TestCaseList.length,
    componentId: '1',
    componentName: 'component 1',
    specificationId: 1,
    specificationName: CategoryType.CONSENT,
    specificationLastUpdate: '2024-07-16T11:59:05.135872',
  },
  testCasesList: comp1TestCaseList,
};

export const testExecutionStatus2Mock: TestExecutionStatus = {
  testExecution: {
    id: 2,
    status: TestCaseStatus.UNDEFINED,
    // startDate: '2024-07-23T08:14:55.841Z',
    startDate: null!,
    endDate: null,
    passedTestsNumber: 0,
    failedTestsNumber: 0,
    undefinedTestsNumber: 0,
    totalTestsNumber: comp2TestCaseList.length,
    componentId: '2',
    componentName: 'component 2',
    specificationId: 2,
    specificationName: CategoryType.IAA_LOCAL,
    specificationLastUpdate: '2024-07-16T11:59:05.135872',
  },
  testCasesList: comp2TestCaseList,
};

export const testExecutionStatus3Mock: TestExecutionStatus = {
  testExecution: {
    id: 3,
    status: TestCaseStatus.UNDEFINED,
    // startDate: '2024-07-23T08:14:55.841Z',
    startDate: null!,
    endDate: null,
    passedTestsNumber: 0,
    failedTestsNumber: 0,
    undefinedTestsNumber: 0,
    totalTestsNumber: comp3TestCaseList.length,
    componentId: '3',
    componentName: 'component 3',
    specificationId: 3,
    specificationName: CategoryType.LOGGING,
    specificationLastUpdate: '2024-07-16T11:59:05.135872',
  },
  testCasesList: comp3TestCaseList,
};
