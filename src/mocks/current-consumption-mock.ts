import { CurrentConsumption } from '../app/models/interfaces/monitoring/current-consumption.interface';
import { dataSpacesMock } from './data-space-mock';

export const currentConsumptionsMock: CurrentConsumption[] = [
  {
    id: dataSpacesMock[0].id!,
    name: dataSpacesMock[0].name,
    status: dataSpacesMock[0].status,
    cpuUsed: 0.000003278,
    cpuAllocated: 0.14,
    memoryUsed: 0.06,
    memoryAllocated: 2.07,
    storageUsed: 4.3,
    storageAllocated: 7,
    componentId: 1,
    componentName: 'Component Name 1',
    nodeId: 1,
    nodeName: 'Node Name 1',
    categoryId: 1,
    categoryName: 'Category Name 1',
  },
  {
    id: dataSpacesMock[1].id!,
    name: dataSpacesMock[1].name,
    status: dataSpacesMock[1].status,
    cpuUsed: 0.000004778,
    cpuAllocated: 0.21,
    memoryUsed: 0.09,
    memoryAllocated: 3.11,
    storageUsed: 6.5,
    storageAllocated: 11,
    componentId: 2,
    componentName: 'Component Name 2',
    nodeId: 2,
    nodeName: 'Node Name 2',
    categoryId: 2,
    categoryName: 'Category Name 2',
  },
];
