import { DataSpaceModel } from '../app/models/interfaces/data-space/data-space.interface';
import { CurrentConsumption } from '../app/models/interfaces/monitoring/current-consumption.interface';
import { dataSpacesMock } from './data-space-mock';

const dataSpacesCurrentConsumptionsMock: CurrentConsumption[][] = [];

const fillMock = (dataSpace: DataSpaceModel): CurrentConsumption[] => {
  const _dataSpace = { ...dataSpace };
  const dataSpaceCurrentConsumptions: CurrentConsumption[] = [];

  _dataSpace.nodeList.forEach((n, i) => {
    n.categoryList.forEach((ca, j) => {
      ca.componentList.forEach((co, k) => {
        dataSpaceCurrentConsumptions.push({
          id: _dataSpace.id!,
          name: _dataSpace.name,
          status: _dataSpace.status,
          nodeId: i + 1,
          nodeName: n.name,
          nodeType: n.type,
          categoryId: j + 1,
          categoryName: ca.name ?? ca.type,
          componentId: k + 1,
          componentName: co.type,
          cpuUsed: 0,
          cpuAllocated: 0.03,
          memoryUsed: 0,
          memoryAllocated: 33,
          storageUsed: 0,
          storageAllocated: 50,
        });
      });
    });
  });

  return dataSpaceCurrentConsumptions;
};

dataSpacesMock
  .slice(0, 2)
  .forEach((ds) => dataSpacesCurrentConsumptionsMock.push(fillMock(ds)));

export default dataSpacesCurrentConsumptionsMock;
