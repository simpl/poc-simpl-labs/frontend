import { SessionStartResponse } from '../app/models/interfaces/slabs-custom-components/session-start-response.interface';

export const sessionStartResponseMock: SessionStartResponse = {
  code: 0,
  createdSessions: [
    {
      testSuite: 'TS1',
      testCase: 'TS1-TC1',
      session: 'fc3d0d41-0b11-468e-b313-ceb7cf2489dc',
    },
  ],
  executionProgressive: '123',
  message: 'Tests session started',
};
