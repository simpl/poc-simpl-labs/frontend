import { SessionStartRequest } from '../app/models/interfaces/slabs-custom-components/session-start-request.interface';

export const sessionStartRequestMock: SessionStartRequest = {
  forceSequentialExecution: true,
  customComponentId: 1,
  testCase: ['11', ' 12'],
  testSuite: ['1', ' 2'],
};
