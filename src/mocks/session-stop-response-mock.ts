import { SessionStopResponse } from '../app/models/interfaces/slabs-custom-components/session-stop-response.interface';

export const sessionStopResponseMock: SessionStopResponse = {
  executionProgressive: '369',
};
