import { TestCaseStatus } from '../app/models/enums/test-case-status.enum';
import { TestCaseModel } from '../app/models/interfaces/slabs-custom-components/test-case.interface';

export const testCasesSuite1Mock: TestCaseModel[] = [
  {
    id: '1',
    name: 'test case 1.1 name',
    description: 'test case 1.1 description',
    status: TestCaseStatus.UNDEFINED,
    startDate: '2024-07-16T18:23:11.135',
    endDate: null!,
    testSuiteId: '1',
    testSuiteDescription: 'This is the Consent v.2 Test Suite description',
    testSuiteLastUpdate: '2024-03-23T18:23:11.135',
    stepsList: [
      {
        id: '1',
        description: 'Step 1: UBL invoice upload',
        date: null!,
        progressiveId: '1',
        result: TestCaseStatus.FAILURE,
      },
      {
        id: '2',
        description: 'Step 2: Validate invoice against UBL 2.1 Invoice Schema',
        date: null!,
        progressiveId: '2',
        result: TestCaseStatus.FAILURE,
      },
    ],
    session: 'session 1',
    version: '1.0',
  },
  {
    id: '2',
    name: 'test case 2.1 name',
    description: 'test case 2.1 description',
    status: TestCaseStatus.UNDEFINED,
    startDate: '2024-07-17T13:01:05.135',
    endDate: null!,
    testSuiteId: '1',
    testSuiteDescription: 'This is the Consent v.2 Test Suite description',
    testSuiteLastUpdate: '2024-03-23T18:23:11.135',
    stepsList: [
      {
        id: 'sendData',
        description: 'Step 1: UBL invoice upload',
        date: null!,
        progressiveId: '1',
        result: TestCaseStatus.SUCCESS,
      },
      {
        id: 'checkStatus',
        description: 'Step 2: Validate invoice against UBL 2.1 Invoice Schema',
        date: null!,
        progressiveId: '1',
        result: TestCaseStatus.FAILURE,
      },
    ],
    session: 'session 2',
    version: '1.0',
  },
];

export const testCasesSuite2Mock: TestCaseModel[] = [
  {
    id: '3',
    testSuiteId: '2',
    name: 'Test Case n.3',
    description: 'Description for the Test Case 3',
    status: TestCaseStatus.UNDEFINED,
    startDate: '2024-07-17T13:01:05.135',
    endDate: null!,
    testSuiteDescription: 'This is the Test Suite n.2',
    testSuiteLastUpdate: '2024-03-23T18:23:11.135',
    stepsList: [
      {
        id: 'checkBody',
        description: 'This is the Step 3.1',
        date: null!,
        progressiveId: '1',
        result: TestCaseStatus.FAILURE,
      },
      {
        id: 'sendStatus',
        description: 'This is the Step 3.2',
        date: null!,
        progressiveId: '1',
        result: TestCaseStatus.SUCCESS,
      },
    ],
    session: 'session 3',
    version: '1.0',
  },
  {
    id: '4',
    testSuiteId: '2',
    name: 'Test Case n.4',
    description: 'Description for the Test Case 4',
    status: TestCaseStatus.UNDEFINED,
    startDate: '2024-07-17T13:01:05.135',
    endDate: null!,
    testSuiteDescription: 'This is the Test Suite n.2',
    testSuiteLastUpdate: '2024-03-23T18:23:11.135',
    stepsList: [
      {
        id: '4.1',
        description: 'This is the Step 4.1',
        date: null!,
        progressiveId: '1',
        result: TestCaseStatus.FAILURE,
      },
      {
        id: '4.2',
        description: 'This is the Step 4.2',
        date: null!,
        progressiveId: '1',
        result: TestCaseStatus.FAILURE,
      },
    ],
    session: 'session 4',
    version: '1.0',
  },
  {
    id: '5',
    testSuiteId: '2',
    name: 'Test Case n.5',
    description: 'Description for the Test Case 5',
    status: TestCaseStatus.UNDEFINED,
    startDate: '2024-07-17T13:01:05.135',
    endDate: null!,
    testSuiteDescription: 'This is the Test Suite n.2',
    testSuiteLastUpdate: '2024-03-23T18:23:11.135',
    stepsList: [
      {
        id: '5.1',
        description: 'This is the Step 5.1',
        date: null!,
        progressiveId: '1',
        result: TestCaseStatus.SUCCESS,
      },
      {
        id: '5.2',
        description: 'This is the Step 5.2',
        date: null!,
        progressiveId: '1',
        result: TestCaseStatus.SUCCESS,
      },
    ],
    session: 'session 5',
    version: '1.0',
  },
];

export const testCasesSuite3Mock: TestCaseModel[] = [
  {
    id: '6',
    name: 'Test Case n.6',
    description: 'test case 3.1 description',
    status: TestCaseStatus.UNDEFINED,
    startDate: '2024-07-16T18:23:11.135',
    endDate: null!,
    testSuiteId: '3',
    testSuiteDescription: 'This is the Consent v.2 Test Suite description',
    testSuiteLastUpdate: '2024-03-23T18:23:11.135',
    stepsList: [
      {
        id: '1',
        description: 'Step 1: UBL invoice upload',
        date: null!,
        progressiveId: '1',
        result: TestCaseStatus.FAILURE,
      },
      {
        id: '2',
        description: 'Step 2: Validate invoice against UBL 2.1 Invoice Schema',
        date: null!,
        progressiveId: '1',
        result: TestCaseStatus.SUCCESS,
      },
    ],
    session: 'session 6',
    version: '1.0',
  },
  {
    id: '7',
    name: 'Test Case n.7',
    description: 'test case 3.2 description',
    status: TestCaseStatus.UNDEFINED,
    startDate: '2024-07-17T13:01:05.135',
    endDate: null!,
    testSuiteId: '3',
    testSuiteDescription: 'This is the Consent v.2 Test Suite description',
    testSuiteLastUpdate: '2024-03-23T18:23:11.135',
    stepsList: [
      {
        id: '1',
        description: 'Step 1: UBL invoice upload',
        date: null!,
        progressiveId: '1',
        result: TestCaseStatus.FAILURE,
      },
      {
        id: '2',
        description: 'Step 2: Validate invoice against UBL 2.1 Invoice Schema',
        date: null!,
        progressiveId: '1',
        result: TestCaseStatus.FAILURE,
      },
    ],
    session: 'session 7',
    version: '1.0',
  },
];

export const testCasesSuite4Mock: TestCaseModel[] = [
  {
    id: '8',
    name: 'test case 1.8 name',
    description: 'test case 1.8 description',
    status: TestCaseStatus.UNDEFINED,
    startDate: '2024-07-16T18:23:11.135',
    endDate: null!,
    testSuiteId: '4',
    testSuiteDescription: 'This is the Consent v.2 Test Suite description',
    testSuiteLastUpdate: '2024-03-23T18:23:11.135',
    stepsList: [
      {
        id: '1',
        description: 'Step 1: UBL invoice upload',
        date: null!,
        progressiveId: '1',
        result: TestCaseStatus.FAILURE,
      },
      {
        id: '2',
        description: 'Step 2: Validate invoice against UBL 2.1 Invoice Schema',
        date: null!,
        progressiveId: '1',
        result: TestCaseStatus.FAILURE,
      },
    ],
    session: 'session 8',
    version: '1.0',
  },
];

export const testCasesSuite5Mock: TestCaseModel[] = [
  {
    id: '9',
    testSuiteId: '5',
    name: 'Test Case n.9',
    description: 'Description for the Test Case 9',
    status: TestCaseStatus.UNDEFINED,
    startDate: '2024-07-17T13:01:05.135',
    endDate: null!,
    testSuiteDescription: 'This is the Test Suite n.5',
    testSuiteLastUpdate: '2024-03-23T18:23:11.135',
    stepsList: [
      {
        id: '3.1',
        description: 'This is the Step 3.1',
        date: null!,
        progressiveId: '1',
        result: TestCaseStatus.FAILURE,
      },
      {
        id: '3.2',
        description: 'This is the Step 3.2',
        date: null!,
        progressiveId: '1',
        result: TestCaseStatus.SUCCESS,
      },
    ],
    session: 'session 9',
    version: '1.0',
  },
  {
    id: '10',
    testSuiteId: '5',
    name: 'Test Case n.10',
    description: 'Description for the Test Case 10',
    status: TestCaseStatus.UNDEFINED,
    startDate: '2024-07-17T13:01:05.135',
    endDate: null!,
    testSuiteDescription: 'This is the Test Suite n.5',
    testSuiteLastUpdate: '2024-03-23T18:23:11.135',
    stepsList: [
      {
        id: '4.1',
        description: 'This is the Step 4.1',
        date: null!,
        progressiveId: '1',
        result: TestCaseStatus.SUCCESS,
      },
      {
        id: '4.2',
        description: 'This is the Step 4.2',
        date: null!,
        progressiveId: '1',
        result: TestCaseStatus.SUCCESS,
      },
    ],
    session: 'session 10',
    version: '1.0',
  },
];

// 2024-07-17 12:01:05.135
// 2024-07-18 12:11:05.135
// 2024-07-16 02:05:11.135
// 2024-07-16 18:23:51.135
// 2024-07-17 13:11:15.135
// 2024-07-18 05:00:05.135
// 2024-07-17 11:45:12.135
// 2024-07-16 12:59:05.135
