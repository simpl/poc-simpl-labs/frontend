// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

module.exports = function (config) {
    config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
        require('karma-jasmine'),
        require('karma-chrome-launcher'),
        require('karma-jasmine-html-reporter'),
        require('karma-coverage'),
        require('karma-junit-reporter'),
        require('@angular-devkit/build-angular/plugins/karma')
    ],
    client: {
        jasmine: {
        },
        clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    jasmineHtmlReporter: {
        suppressAll: true // removes the duplicated traces
    },
    junitReporter: {
        outputDir: '.', 
        outputFile: 'junit.xml', 
        suite: '', 
        useBrowserName: false,
        nameFormatter: undefined, 
        classNameFormatter: undefined, 
        properties: {}, 
        xmlVersion: null
    },
    coverageReporter: {
        dir: require('path').join(__dirname, './coverage'),
        subdir: '.',
        reporters: [
        { type: 'html', subdir: 'html-report' },
        { type: 'lcov', subdir: 'lcov-report' }
        ]
    },
    reporters: ['progress', 'kjhtml'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['ChromeHeadlessNoSandbox'],
        customLaunchers: {
            ChromeHeadlessNoSandbox: {
            base: 'ChromeHeadless',
            flags: ['--no-sandbox']
        }
    },
    singleRun: false,
    restartOnFileChange: true
    });
};


